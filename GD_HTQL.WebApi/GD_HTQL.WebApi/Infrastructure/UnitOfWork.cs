﻿using AutoMapper;
using GD_HTQL.WebApi.Data.Models;
using GD_HTQL.WebApi.Repository;
using GD_HTQL.WebApi.Services.IRepository;
using GD_HTQL.WebApi.Services.Repository;

namespace GD_HTQL.WebApi.Infrastructure
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public ITinhRepository DMTinhRepos { get; private set; }
        public IHuyenRepository DMHuyenRepos { get; private set; }
        public IXaRepository DMXaRepos { get; private set; }
        public ITacGiaRepository TacGiaRepos { get; private set; }
        public INSCoQuanRepository NSCoQuanRepos { get; private set; }
        public INSCanBoRepository NSCanBoRepos { get; private set; }
        public INhaCungCapRepository NhaCungCapRepos { get; private set; }
        public INhaThauRepository NhaThauRepos { get; private set; }
        public IQuanLyTuongTacRepository QuanLyTuongTacRepos { get; private set; }

        public ICongTyRepository CongTyRepos { get; private set; }
        public IChucVuRepository ChucVuRepos { get; private set; }
        public IPhongBanRepository PhongBanRepos { get; private set; }
        public INhanVienRepository NhanVienRepos { get; private set; }

        public ILoaiSanPhamRepository LoaiSanPhamRepos { get; private set; }
        public IKhoiLopRepository KhoiLopRepos { get; private set; }
        public IMonHocRepository MonHocRepos { get; private set; }
        public IFileRepository FileRepos { get; private set; }

        public ILoaiVanBanRepository LoaiVanBanRepos { get; private set; }
        public IVanBanRepository VanBanRepos { get; private set; }
        public INhomCongViecRepository NhomCongViecRepos { get; private set; }
        public ICongViecRepository CongViecRepos { get; private set; }
        public ITrangThaiCongViecRepository TrangThaiCongViecRepos { get; private set; }
        public ISanPhamRepository SanPhamRepos { get; private set; }
        public IDuToanRepository DuToanRepos { get; private set; }
        public IKhuVucRepository KhuVucRepos { get; private set; }
        public ILoaiNhaCungCapRepository LoaiNhaCungCapRepos { get; private set; }
        public ILoaiNhaThauRepository LoaiNhaThauRepos { get; private set; }
        public IViTriRepository ViTriRepos { get; private set; }
        public ILoaiDaiLyRepository LoaiDaiLyRepos { get; private set; }
        public IDaiLyRepository DaiLyRepos { get; private set; }   
        
        public IQuanLyNhanVienRepository QuanLyNhanVienRepos { get; private set; }
        public IThongTuRepository ThongTuRepos { get; private set; }
        public ITinhNhanVienRepository TinhNhanVienRepos { get; private set; }
        public IDonViTinhRepository DonViTinhRepos { get; private set; }
        public INT_GiaTriCoHoiRepository NT_GiaTriCoHoiRepos { get; private set; }
        public INguonVonRepository NguonVonRepos { get; private set; }

        public IKHCT_NgayRepository KHCT_NgayRepos { get; private set; }
        public IKHCT_TuanRepository KHCT_TuanRepos { get; private set; }
        public IKHCT_ThangRepository KHCT_ThangRepos { get; private set; }

        public IKHCT_NoiDungRepository KHCT_NoiDungRepos { get; private set; }
        public IKHCT_NguoiDiCungRepository KHCT_NguoiDiCungRepos { get; private set; }
        public IKHCT_ChiPhiRepository KHCT_ChiPhiRepos { get; private set; }
        public IKHCT_XeRepository KHCT_XeRepos { get; private set; }
        public IKHCT_Ngay_LichSuRepository KHCT_Ngay_LichSuRepos { get; private set; }
        public ICtyDuToanRepository CtyDuToanRepos { get; private set; }

        public ILichSuDuToanRepository LichSuDuToanRepos { get; private set; }
        public IKHCT_ChiPhiKhacRepository KHCT_ChiPhiKhacRepos { get; private set; }

        public INT_DuToanRepository NT_DuToanRepos { get; private set; }

        public INghiPhepRepository NghiPhepRepos { get; private set; }
        public INghiPhep_LichSuRepository NghiPhep_LichSuRepos { get; private set; }
        public INSDuToanRepository NSDuToanRepos { get; private set; }
        public ILichSuCongViecRepository LichSuCongViecRepos { get; private set; }

        public IThongBaoCauHinhRepository ThongBaoCauHinhRepos { get; private set; }
        public IThongBaoNhanVienRepository ThongBaoNhanVienRepos { get; private set; }
        public UnitOfWork(ApplicationDbContext context , IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
            DMTinhRepos = new TinhRepository(_context,_mapper);
            DMHuyenRepos = new HuyenRepository(_context, _mapper);
            DMXaRepos = new XaRepository(_context, _mapper);

            TacGiaRepos = new TacGiaRepository(_context, _mapper);
            NSCoQuanRepos = new NSCoQuanRepository(_context, _mapper);
            NSCanBoRepos = new NSCanBoRepository(_context, _mapper);
            NhaCungCapRepos = new NhaCungCapRepository(_context, _mapper);
            NhaThauRepos = new NhaThauRepository(_context, _mapper);
            QuanLyTuongTacRepos = new QuanLyTuongTacRepository(_context, _mapper);

            CongTyRepos = new CongTyRepository(_context, _mapper);
            ChucVuRepos = new ChucVuRepository(_context, _mapper);
            PhongBanRepos = new PhongBanRepository(_context, _mapper);
            NhanVienRepos = new NhanVienRepository(_context, _mapper);

            MonHocRepos = new MonHocRepository(_context, _mapper);
            LoaiSanPhamRepos = new LoaiSanPhamRepository(_context, _mapper);
            KhoiLopRepos = new KhoiLopRepository(_context, _mapper);
            FileRepos = new FileRepository(_context, _mapper);
            LoaiVanBanRepos = new LoaiVanBanRepository(_context, _mapper);
            VanBanRepos = new VanBanRepository(_context, _mapper);

            NhomCongViecRepos = new NhomCongViecRepository(_context, _mapper);
            CongViecRepos = new CongViecRepository(_context, _mapper);
            TrangThaiCongViecRepos = new TrangThaiCongViecRepository (_context, _mapper);
            SanPhamRepos = new SanPhamRepository (_context,_mapper);
            DuToanRepos = new  DuToanRepository(_context, _mapper);
            KhuVucRepos = new KhuVucRepository(_context, _mapper);
            LoaiNhaCungCapRepos = new LoaiNhaCungCapRepository(_context, _mapper);
            LoaiNhaThauRepos = new LoaiNhaThauRepository(_context, _mapper);
            ViTriRepos = new ViTriRepository(_context, _mapper);
            LoaiDaiLyRepos = new LoaiDaiLyRepository(_context, _mapper);
            DaiLyRepos= new DaiLyRepository(_context, _mapper);  
            QuanLyNhanVienRepos = new QuanLyNhanVienRepository(_context, _mapper);
            ThongTuRepos = new ThongTuRepository(_context, _mapper);
            TinhNhanVienRepos = new TinhNhanVienRepository(_context, _mapper);
            DonViTinhRepos = new DonViTinhRepository(_context, _mapper);
            NT_GiaTriCoHoiRepos = new NT_GiaTriCoHoiRepository(_context, _mapper);
            NguonVonRepos = new NguonVonRepository(_context, _mapper);  
                   
            KHCT_ThangRepos = new KHCT_ThangRepository(_context, _mapper);
            KHCT_TuanRepos = new KHCT_TuanRepository(_context, _mapper);
            KHCT_NgayRepos = new KHCT_NgayRepository(_context, _mapper);

            KHCT_NoiDungRepos = new KHCT_NoiDungRepository(_context, _mapper);
            KHCT_NguoiDiCungRepos = new KHCT_NguoiDiCungRepository(_context, _mapper);
            KHCT_ChiPhiRepos = new KHCT_ChiPhiRepository(_context, _mapper);
            KHCT_XeRepos = new KHCT_XeRepository(_context, _mapper);
            KHCT_Ngay_LichSuRepos = new KHCT_Ngay_LichSuRepository(_context, _mapper);

            CtyDuToanRepos = new CtyDuToanRepository(_context, _mapper);    

            LichSuDuToanRepos = new LichSuDuToanRepository(_context, _mapper);
            KHCT_ChiPhiKhacRepos = new KHCT_ChiPhiKhacRepository(_context, _mapper);

            NT_DuToanRepos = new NT_DuToanRepository(_context, _mapper);

            NghiPhepRepos = new NghiPhepRepository(_context, _mapper);
            NghiPhep_LichSuRepos = new NghiPhep_LichSuRepository(_context, _mapper);

            NSDuToanRepos = new NSDuToanRepository(_context, _mapper);
            LichSuCongViecRepos = new LichSuCongViecRepository(_context, _mapper);

            ThongBaoCauHinhRepos = new ThongBaoCauHinhRepository(_context, _mapper);    
            ThongBaoNhanVienRepos = new ThongBaoNhanVienRepository(_context, _mapper);  
        }
        public int Save()
        {
            return _context.SaveChanges();
        }     
        public void Dispose()
        {
            _context.Dispose();
        }
    }

}

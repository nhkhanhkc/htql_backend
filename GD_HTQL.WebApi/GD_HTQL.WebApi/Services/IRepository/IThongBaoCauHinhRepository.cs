﻿
using GD_HTQL.WebApi.Data.Models.EF.ThongBaoModels;
using GD_HTQL.WebApi.Infrastructure;


namespace GD_HTQL.WebApi.Services.IRepository
{
    public interface IThongBaoCauHinhRepository :IGenericRepository<ThongBaoCauHinh>
    {
       
    }
}

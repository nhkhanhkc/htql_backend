﻿using GD_HTQL.WebApi.Data.Models.EF.DuAnModels;
using GD_HTQL.WebApi.Data.Models.ViewModels;
using GD_HTQL.WebApi.Infrastructure;

namespace GD_HTQL.WebApi.Services.IRepository
{
    public interface INSCoQuanRepository : IGenericRepository<NSCoQuan>
    {    
        // Quy ước TinhID = 0 lấy tất cả các cơ quan
        Task<IEnumerable<NSCoQuan>> GetNSCoQuanByTuKhoa(string? Key, int _TinhID);
        Task<IEnumerable<NSCoQuan>> GetByNhanVien(int nhanVienID);
        bool CheckMaSoThue(string? _maSoThue, int _coQuanID);
        Task<IEnumerable<NSCoQuanViewModel>> GetDanhSach(int nhaVienID);
        Task<IEnumerable<NSCoQuanBaoCao>> GetBaoCao(int nhaVienID);
        Task<IEnumerable<NSCoQuan>> GetALl();
    }
}

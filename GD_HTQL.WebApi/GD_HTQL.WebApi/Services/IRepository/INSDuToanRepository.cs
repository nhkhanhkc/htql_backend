﻿using GD_HTQL.WebApi.Data.Models.EF.DuAnModels;
using GD_HTQL.WebApi.Data.Models.ViewModels;
using GD_HTQL.WebApi.Infrastructure;

namespace GD_HTQL.WebApi.Services.IRepository
{
    public interface INSDuToanRepository : IGenericRepository<NSDuToan>
    {
        Task<List<NSDuToanView>> GetDanhSach(int coQuanID);
        Task<string> SaveFile(string fileBase64, string fileName);
        Task<List<BuocThiTruong>> GetBuocThiTruong();
        void DeleteFile(string path);
    }
}

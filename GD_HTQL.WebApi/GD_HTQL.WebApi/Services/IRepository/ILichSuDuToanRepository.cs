﻿using GD_HTQL.WebApi.Data.Models.EF.SanPhamModels;
using GD_HTQL.WebApi.Infrastructure;

namespace GD_HTQL.WebApi.Services.IRepository
{
    public interface ILichSuDuToanRepository : IGenericRepository<LichSuDuToan>
    {
    }
}

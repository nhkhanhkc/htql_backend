﻿using GD_HTQL.WebApi.Data.Models.EF.CongViecModels;
using GD_HTQL.WebApi.Infrastructure;

namespace GD_HTQL.WebApi.Services.IRepository
{
    public interface ITrangThaiCongViecRepository : IGenericRepository<TrangThaiCongViec>
    {
    }
}

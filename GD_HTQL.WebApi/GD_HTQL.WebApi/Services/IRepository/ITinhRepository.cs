﻿using GD_HTQL.WebApi.Data.Models.Dtos;
using GD_HTQL.WebApi.Data.Models.EF.DanhMucModels;
using GD_HTQL.WebApi.Infrastructure;
using System;


namespace GD_HTQL.WebApi.Services.IRepository
{
    public interface ITinhRepository : IGenericRepository<DMTinh> 
    {
    }
}

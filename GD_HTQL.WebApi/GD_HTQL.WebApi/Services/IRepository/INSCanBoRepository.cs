﻿using GD_HTQL.WebApi.Data.Models.EF.DuAnModels;
using GD_HTQL.WebApi.Infrastructure;


namespace GD_HTQL.WebApi.Services.IRepository
{
    public interface INSCanBoRepository : IGenericRepository<NSCanBo>
    {     
        Task<IEnumerable<NSCanBo>> GetNSCanBoByTuKhoa(string? Key, int CoQuanID);      
        Task<IEnumerable<NSCanBo>> GetNSCanBoByCoQuan(int CoQuanID);
    }
}

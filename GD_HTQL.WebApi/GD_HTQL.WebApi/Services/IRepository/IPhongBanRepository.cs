﻿using GD_HTQL.WebApi.Data.Models.EF.NhanVienModels;
using GD_HTQL.WebApi.Infrastructure;
namespace GD_HTQL.WebApi.Services.IRepository
{
    public interface IPhongBanRepository :IGenericRepository<PhongBan>
    {       
        Task<IEnumerable<PhongBan>> GetPhongBanByTuKhoa(string Key, int CongtyID);
        bool DoiTrangThai(int ID);
    }
}

﻿using GD_HTQL.WebApi.Data.Models.EF.NhanVienModels;
using GD_HTQL.WebApi.Data.Models.ViewModels;
using GD_HTQL.WebApi.Infrastructure;

namespace GD_HTQL.WebApi.Services.IRepository
{
    public interface ITinhNhanVienRepository : IGenericRepository<TinhNhanVien>
    {
        Task<IEnumerable<TinhNhanVienViewModel>> GetByNhanVien(int nhanVienID);
    }
}

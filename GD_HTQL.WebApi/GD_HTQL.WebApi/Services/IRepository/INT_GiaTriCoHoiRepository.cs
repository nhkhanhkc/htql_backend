﻿using GD_HTQL.WebApi.Data.Models.EF.NhaThauModels;
using GD_HTQL.WebApi.Data.Models.ViewModels;
using GD_HTQL.WebApi.Infrastructure;

namespace GD_HTQL.WebApi.Services.IRepository
{
    public interface INT_GiaTriCoHoiRepository : IGenericRepository<NT_GiaTriCoHoi>
    {
        Task<IEnumerable<GiaTriCoHoiViewModel>> GetDetails(int nhaThauID, int gtch_Id);
        Task ThemLoaiSanPham(List<int> loaiSanPham, int gtchID);
        public void XoaLoaiSanPham(int gtchID);
        Task<List<NhanVienShortView>> GetNhanVien();
    }
}

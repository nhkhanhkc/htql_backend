﻿using GD_HTQL.WebApi.Data.Models.EF.NghiPhepModels;
using GD_HTQL.WebApi.Data.Models.ViewModels;
using GD_HTQL.WebApi.Infrastructure;

namespace GD_HTQL.WebApi.Services.IRepository
{
    public interface INghiPhepRepository : IGenericRepository<NghiPhep>
    {
        Task<List<NghiPhep_TrangThai>> GetTrangThai();
        Task<ICollection<NghiPhepViewModel>> GetDanhSach(int nhanVienID);
        Task<NghiPhepViewModel> GetDetailsByID(int nghiPhepID);
        Task<List<NghiPhepLoai>> GetLoaiNghiPhep();
        Task<NghiPhepNhanVienView> GetByNhanVien(int nhanvienID);
    }
}

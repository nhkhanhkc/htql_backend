﻿using GD_HTQL.WebApi.Data.Models.EF.CongViecModels;
using GD_HTQL.WebApi.Data.Models.ViewModels;
using GD_HTQL.WebApi.Infrastructure;

namespace GD_HTQL.WebApi.Services.IRepository
{
    public interface ICongViecRepository : IGenericRepository<CongViec>
    {
        Task<List<TrangThaiCongViec>> GetTrangThai();
        Task<IEnumerable<CongViecViewModel>> GetDanhSach(int nhanviecID, int giaoViecID, bool week, bool month, int nhomID);
        Task<CongViecViewModel> GetDetail(int congViecID);
        Task<List<DoUuTien>> GetDoUuTien();
    }
}

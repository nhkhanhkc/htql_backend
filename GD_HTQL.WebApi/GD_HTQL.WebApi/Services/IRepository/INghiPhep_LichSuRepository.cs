﻿using GD_HTQL.WebApi.Data.Models.EF.NghiPhepModels;
using GD_HTQL.WebApi.Infrastructure;

namespace GD_HTQL.WebApi.Services.IRepository
{
    public interface INghiPhep_LichSuRepository : IGenericRepository<NghiPhep_LichSu>
    {
    }
}

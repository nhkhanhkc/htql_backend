﻿namespace GD_HTQL.WebApi.Services.IRepository
{
    public interface IDbInitializer
    {
        public void Initialize();
    }
}

﻿using GD_HTQL.WebApi.Data.Models.EF.SanPhamModels;
using GD_HTQL.WebApi.Data.Models.ViewModels;
using GD_HTQL.WebApi.Infrastructure;

namespace GD_HTQL.WebApi.Services.IRepository
{
    public interface ISanPhamRepository : IGenericRepository<SanPham>
    {       
        Task<IEnumerable<SanPhamViewModel>> GetByTuKhoa(string? Key);
        bool CheckMaSanPham(string _MaSanPham, int sanphamID);
        Task<IEnumerable<SanPhamViewModel>> GetDanhSach(string tukhoa, int loaiID, int monhocID, int khoilopID, int pageCurrent, int pageSize);
        Task ThemThongTu(List<int> sanPhamThongTu, int sanphamID,string Loai);
        Task ThemMonHoc(List<int> sanPhamMonHoc, int sanphamID, string Loai);
        Task ThemKhoiLop(List<int> sanPhamKhoiLop, int sanphamID, string Loai);
        void XoaSanPhamThongTu( int sanphamID);
        void XoaSanPhamMonHoc(int sanphamID);
        void XoaSanPhamKhoiLop(int sanphamID);

        IEnumerable<SanPhamViewModel> GetInclude();
        IEnumerable<SanPhamViewModel> GetByThongTu(int thongTuID);
        IEnumerable<SanPham> GetLikeTenSanPham(string tenSanPham);
    }
}

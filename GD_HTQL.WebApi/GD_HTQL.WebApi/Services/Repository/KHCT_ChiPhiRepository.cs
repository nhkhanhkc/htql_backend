﻿using AutoMapper;
using GD_HTQL.WebApi.Data.Models;
using GD_HTQL.WebApi.Data.Models.EF.CongTacModels;
using GD_HTQL.WebApi.Infrastructure;
using GD_HTQL.WebApi.Services.IRepository;
using Microsoft.EntityFrameworkCore;

namespace GD_HTQL.WebApi.Services.Repository
{
    public class KHCT_ChiPhiRepository : GenericRepository<KHCT_ChiPhi>, IKHCT_ChiPhiRepository
    {
        public KHCT_ChiPhiRepository(ApplicationDbContext context, IMapper mapper) : base(context, mapper)
        {
        }
        public async Task<IEnumerable<KHCT_HangMucChiPhi>> GetDsHangMuc()
        {
            var objHangMuc = await _context.KHCT_HangMucChiPhi.ToListAsync();

            return objHangMuc;
        }
        public async Task<IEnumerable<KHCT_LoaiChiPhi>> GetDsLoai()
        {
            var objLoai = await _context.KHCT_LoaiChiPhi.ToListAsync();

            return objLoai;
        }
        public async Task<IEnumerable<KHCT_ChiPhi>> GetDsChiPhi(int KHCT_Id)
        {
            var objChiPhi = await _context.KHCT_ChiPhi.Where(x=>x.NgayID == KHCT_Id).Include(x=>x.KHCT_LoaiChiPhi).Include(x=>x.KHCT_HangMucChiPhi).ToListAsync();

            return objChiPhi;
        }
    }
}

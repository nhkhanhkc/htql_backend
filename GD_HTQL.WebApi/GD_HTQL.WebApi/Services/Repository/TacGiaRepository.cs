﻿using AutoMapper;
using GD_HTQL.WebApi.Common;
using GD_HTQL.WebApi.Data.Models;
using GD_HTQL.WebApi.Data.Models.EF.TacGiaModels;
using GD_HTQL.WebApi.Infrastructure;
using GD_HTQL.WebApi.Services.IRepository;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace GD_HTQL.WebApi.Services.Repository
{
    public class TacGiaRepository : GenericRepository<TacGia>, ITacGiaRepository 
    {
     
        public TacGiaRepository(ApplicationDbContext context, IMapper mapper) : base(context, mapper)
        {          
        }
        public async Task<IEnumerable<TacGia>> GetTacGiaByTuKhoa(string? Key)
        {       
            IEnumerable<TacGia> objTacGia = await _context.TacGia.OrderByDescending(x => x.TacGiaID).ToListAsync();
            if (!string.IsNullOrEmpty(Key))
            {
                objTacGia = objTacGia.AsQueryable().Where(ExpressionTacGiaTheoTuKhoa(Key)).OrderByDescending(x => x.TacGiaID).ToList();
            }
            return objTacGia;
        }           
        public Expression<Func<TacGia, bool>> ExpressionTacGiaTheoTuKhoa(string _key)
        {
            _key = ClassCommon.RemoveUnicode(_key).ToLower();
            return x => ClassCommon.RemoveUnicode(x.TenTacGia).ToLower().Contains(_key);
        }
    }
}

﻿using AutoMapper;
using GD_HTQL.WebApi.Data.Models;
using GD_HTQL.WebApi.Data.Models.Dtos;
using GD_HTQL.WebApi.Data.Models.EF.NhanVienModels;
using GD_HTQL.WebApi.Data.Models.ViewModels;
using GD_HTQL.WebApi.Infrastructure;
using GD_HTQL.WebApi.Services.IRepository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;


namespace GD_HTQL.WebApi.Services.Repository
{
    public class CongTyRepository : GenericRepository<CongTy>,ICongTyRepository
    {     
        public CongTyRepository(ApplicationDbContext context, IMapper mapper) :base(context,mapper) 
        {         
        }      
        // Quy ước TinhID = 0 lấy tất cả các cơ quan
        public async Task<IEnumerable<CongTy>> GetCongTyByTuKhoa(string Key, int _TinhID)
        {
            var objCongTy = await _context.CongTy.Where(x => x.TenCongTy.Contains(Key) && (_TinhID == 0 || x.TinhID == _TinhID)).ToListAsync();
            return objCongTy;
        }

        public async Task<IEnumerable<NhanVienShortView>> GetNhanVien(int ctyID)
        {
            List<NhanVienShortView> nhanVienShortViews = new List<NhanVienShortView>();
            var objCongTy = await _context.CongTy.Where(x=>x.CongTyID ==ctyID).Include(x=>x.PhongBan).Include(x=>x.PhongBan).FirstOrDefaultAsync();
            if (objCongTy != null)
            {
                var objPhongBan = objCongTy.PhongBan ==null ? null : objCongTy.PhongBan.Select(x=>x.PhongBanID).ToList();
                if (objPhongBan != null && objPhongBan.Count > 0)
                {
                    var objChucVu = await _context.ChucVu.Where(x=> objPhongBan.Contains(x.PhongBanID)).Select(x=>x.ChucVuID).ToListAsync();
                    if(objChucVu !=null && objChucVu.Count > 0)
                    {
                        var objNhanVien = _context.ChucVuNhanVien.Where(x => objChucVu.Contains(x.ChucVuID)).Select(x => x.NhanVienID).ToList();
                        if (objNhanVien != null && objNhanVien.Count > 0)
                        {
                            var lstNhanVien = await _context.NhanVien.Where(x => objNhanVien.Contains(x.NhanVienID)).ToListAsync();
                            nhanVienShortViews = _mapper.Map<List<NhanVienShortView>>(lstNhanVien);
                        }
                    }
                }
            }
            return nhanVienShortViews;
        }

        public bool DoiTrangThai(int ID)
        {
            bool result = false;
            var objCongTy= _context.CongTy.Find(ID);
            if (objCongTy != null)
            {
                if (objCongTy.Active == true)
                {
                    objCongTy.Active = false;
                }
                else
                {
                    objCongTy.Active = true;
                }
                _context.Entry(objCongTy).State = EntityState.Modified;
                _context.SaveChanges();
                result = true;
            }
            else
            {
                result = false;
            }
            return result;
        }      
    }
}

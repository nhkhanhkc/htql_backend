﻿using AutoMapper;
using GD_HTQL.WebApi.Common;
using GD_HTQL.WebApi.Data.Models;
using GD_HTQL.WebApi.Data.Models.CustomModels;
using GD_HTQL.WebApi.Data.Models.Dtos.SanPham;
using GD_HTQL.WebApi.Data.Models.EF.SanPhamModels;
using GD_HTQL.WebApi.Data.Models.ViewModels;
using GD_HTQL.WebApi.Infrastructure;
using GD_HTQL.WebApi.Services.IRepository;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;


namespace GD_HTQL.WebApi.Services.Repository
{
    public class ThongTuRepository : GenericRepository<ThongTu> , IThongTuRepository
    {
        public ThongTuRepository(ApplicationDbContext context, IMapper mapper) : base(context, mapper)
        {          
        }
        public void AddListSanPhamThongTu(AddSanPhamThongTu addSanPham)
        {
            List<SanPhamThongTu> lstSanPhamThongTu = new List<SanPhamThongTu>();

            var objSPTT = _context.SanPhamThongTu.Where(x => x.ThongTuID == addSanPham.ThongTuID ).ToList();
            foreach (var it in addSanPham.lstSanPham)
            {
                SanPhamThongTu sanPhamThongTu = new SanPhamThongTu();
                sanPhamThongTu.SanPhamID = it;
                sanPhamThongTu.ThongTuID = addSanPham.ThongTuID;
                lstSanPhamThongTu.Add(sanPhamThongTu);
            }
            //var objSanPhamThongTuOld = _context.SanPhamThongTu.ToList();
            //var objNew = lstSanPhamThongTu.Except(objSanPhamThongTuOld);

            // xóa Sản phẩm thông tư củ          
            _context.SanPhamThongTu.RemoveRange(objSPTT);
             _context.SaveChanges();
            _context.SanPhamThongTu.AddRange(lstSanPhamThongTu);
             _context.SaveChanges();

        }
        public async Task<List<MonHocDto>> GetMonHoc(int thongTuID)
        {      
         
            var objMonHoc =  await (from tt in _context.MonHocThongTu.Where(x => x.ThongTuID == thongTuID)
                                    join mh in _context.MonHoc on tt.MonHocID equals mh.MonHocID
                                    select new MonHocDto
                                    {
                                        MonHocID = mh.MonHocID,
                                        TenMonHoc = mh.TenMonHoc,
                                    }).ToListAsync();
            return objMonHoc;
        }
        public void AddListMonHocThongTu(AddMonHocThongTu addMonHoc)
        {
            var objMHTT = _context.MonHocThongTu.Where(x => x.ThongTuID == addMonHoc.ThongTuID).ToList();
            List<MonHocThongTu> lstMonHocThongTu = new List<MonHocThongTu>();
            foreach (var it in addMonHoc.lstMonHoc)
            {
                MonHocThongTu monHocThongTu = new MonHocThongTu();
                monHocThongTu.MonHocID = it;
                monHocThongTu.ThongTuID = addMonHoc.ThongTuID;
                lstMonHocThongTu.Add(monHocThongTu);
            }

            // xóa Sản phẩm thông tư củ 

            _context.MonHocThongTu.RemoveRange(objMHTT);
             _context.SaveChanges();

            _context.MonHocThongTu.AddRange(lstMonHocThongTu);
             _context.SaveChanges();

        }
        public async Task<List<ThongTuViewModel>> GetListDetails()
        {       
            var lstThongTuView = await _context.ThongTu.Include(x => x.SanPham).Select(x => new ThongTuViewModel
            {
                ThongTuID = x.ThongTuID,
                TenThongTu = x.TenThongTu,
                MoTa = x.MoTa,
                lstSanPham = x.SanPham == null ? null : _mapper.Map<List<SanPhamDto>>(x.SanPham)
            }).ToListAsync();
            if (lstThongTuView.Count() > 0)
            {
                foreach (var item in lstThongTuView)
                {
                    var objMonHoc = await (from tt in _context.MonHocThongTu.Where(x => x.ThongTuID == item.ThongTuID)
                                           join mh in _context.MonHoc on tt.MonHocID equals mh.MonHocID
                                           select new MonHocDto
                                           {
                                               MonHocID = mh.MonHocID,
                                               TenMonHoc = mh.TenMonHoc,
                                           }).ToListAsync();
                    item.lstMonHoc = objMonHoc;
                }
            }           
            return lstThongTuView;
        }

        public async Task<List<IndexSanPhamView>> FindSanPham(List<IndexSanPham> lstSanPham)
        {
            List<IndexSanPhamView> lstView = new List<IndexSanPhamView>();
            var objSanPham = await _context.SanPham.Include(x => x.ThongTu).Include(x => x.MonHoc).Include(x => x.KhoiLop).Include(x => x.LoaiSanPham).Include(x => x.DonViTinh).ToListAsync();
           foreach (var item in lstSanPham)
            {
                IndexSanPhamView sanPhamView = new IndexSanPhamView();
                sanPhamView.index = item.index;
                sanPhamView.productName = item.productName;
                var obj = objSanPham.AsQueryable().Where(ExpressionSanPham(item.productName)).Include(x => x.ThongTu).Include(x => x.MonHoc).Include(x => x.KhoiLop).Include(x => x.LoaiSanPham).Include(x => x.DonViTinh).ToList();
                if (obj.Count() > 0)
                {                 
                    sanPhamView.SanPhamView = _mapper.Map<List<SanPhamViewDto>>(obj);                  
                }
                lstView.Add(sanPhamView);
            }                                                                                                                           
            return lstView;
        }
        public Expression<Func<SanPham, bool>> ExpressionSanPham(string _key)
        {
            _key = ClassCommon.RemoveUnicode(_key).ToLower();
            return x => ClassCommon.RemoveUnicode(x.TenSanPham).ToLower().Contains(_key);
        }

    }
}

﻿using AutoMapper;
using GD_HTQL.WebApi.Data.Models;
using GD_HTQL.WebApi.Data.Models.EF.DanhMucModels;
using GD_HTQL.WebApi.Infrastructure;
using GD_HTQL.WebApi.Services.IRepository;

namespace GD_HTQL.WebApi.Services.Repository
{
    public class XaRepository :GenericRepository<DMXa> , IXaRepository
    {
        public XaRepository(ApplicationDbContext context, IMapper mapper) :base(context, mapper) 
        {
        }      
    }
}

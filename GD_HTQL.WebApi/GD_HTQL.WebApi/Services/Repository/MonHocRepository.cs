﻿using AutoMapper;
using GD_HTQL.WebApi.Data.Models;
using GD_HTQL.WebApi.Data.Models.Dtos;
using GD_HTQL.WebApi.Data.Models.EF.SanPhamModels;
using GD_HTQL.WebApi.Infrastructure;
using GD_HTQL.WebApi.Services.IRepository;
using Microsoft.EntityFrameworkCore;


namespace GD_HTQL.WebApi.Services.Repository
{
    public class MonHocRepository : GenericRepository<MonHoc>,IMonHocRepository
    {     
        public MonHocRepository(ApplicationDbContext context, IMapper mapper) :base(context, mapper) 
        {          
        }    
    }
}

﻿using AutoMapper;
using GD_HTQL.WebApi.Data.Models;
using GD_HTQL.WebApi.Data.Models.EF.NhaThauModels;
using GD_HTQL.WebApi.Infrastructure;
using GD_HTQL.WebApi.Services.IRepository;

namespace GD_HTQL.WebApi.Services.Repository
{
    public class LoaiNhaThauRepository : GenericRepository<LoaiNhaThau>, ILoaiNhaThauRepository
    {
        public LoaiNhaThauRepository(ApplicationDbContext context, IMapper mapper) : base(context, mapper)
        {
        }
    }
}

﻿using AutoMapper;
using GD_HTQL.WebApi.Data.Models;
using GD_HTQL.WebApi.Data.Models.EF.NhanVienModels;
using GD_HTQL.WebApi.Data.Models.ViewModels;
using GD_HTQL.WebApi.Infrastructure;
using GD_HTQL.WebApi.Services.IRepository;
using Microsoft.EntityFrameworkCore;

namespace GD_HTQL.WebApi.Services.Repository
{
    public class TinhNhanVienRepository : GenericRepository<TinhNhanVien>, ITinhNhanVienRepository
    {
        public TinhNhanVienRepository(ApplicationDbContext context, IMapper mapper) : base(context, mapper)
        {

        }
        public async Task<IEnumerable<TinhNhanVienViewModel>> GetByNhanVien(int nhanVienID)
        {
            var objTinhNhanVien = await (from tinhnv in _context.TinhNhanVien
                                   where tinhnv.NhanVienID == nhanVienID
                                   join tinh in _context.DMTinh
                                   on tinhnv.TinhID equals tinh.TinhID
                                   select new TinhNhanVienViewModel
                                   {
                                       ID =tinhnv.ID,
                                       TinhID = tinhnv.TinhID,
                                       TenTinh = tinh.TenTinh,
                                       NhanVienID = tinhnv.NhanVienID
                                   }).ToListAsync();
            return objTinhNhanVien;
        }
    }
}

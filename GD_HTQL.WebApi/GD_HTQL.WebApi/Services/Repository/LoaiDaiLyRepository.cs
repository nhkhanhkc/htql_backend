﻿using AutoMapper;
using GD_HTQL.WebApi.Data.Models;
using GD_HTQL.WebApi.Data.Models.EF.DaiLyModels;
using GD_HTQL.WebApi.Infrastructure;
using GD_HTQL.WebApi.Services.IRepository;

namespace GD_HTQL.WebApi.Services.Repository
{
    public class LoaiDaiLyRepository : GenericRepository<LoaiDaiLy>, ILoaiDaiLyRepository 
    { 
        public LoaiDaiLyRepository(ApplicationDbContext context, IMapper mapper) : base(context, mapper)
        {
        }
    }
}

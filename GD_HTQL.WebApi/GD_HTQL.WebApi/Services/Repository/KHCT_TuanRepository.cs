﻿using AutoMapper;
using GD_HTQL.WebApi.Common;
using GD_HTQL.WebApi.Data.Models;
using GD_HTQL.WebApi.Data.Models.CustomModels;
using GD_HTQL.WebApi.Data.Models.EF.CongTacModels;
using GD_HTQL.WebApi.Infrastructure;
using GD_HTQL.WebApi.Services.IRepository;

namespace GD_HTQL.WebApi.Services.Repository
{
    public class KHCT_TuanRepository : GenericRepository<KHCT_Tuan>, IKHCT_TuanRepository
    {
        public KHCT_TuanRepository(ApplicationDbContext context, IMapper mapper) : base(context, mapper)
        {
        }
        public DateOfWeekModel GetDateOfWeekModel(int thangID,int tuan)
        {
            //ngày bắt đầu ngày kết thúc tuần
            DateOfWeekModel model = new DateOfWeekModel();
            var objThang = _context.KHCT_Thang.Where(x => x.ThangID == thangID).FirstOrDefault();
            if (objThang != null)
            {
                int _week = 0;
                DateTime _dateStartmonth = ClassCommon.GetDateStartMonth(objThang.Thang,objThang.Nam);
                if (_dateStartmonth.DayOfWeek == DayOfWeek.Sunday)
                {
                    _week = ClassCommon.GetNumberWeek(_dateStartmonth.AddDays(1));
                }
                else
                {
                    _week = ClassCommon.GetNumberWeek(_dateStartmonth);
                }
                _week = _week + (tuan - 1);               
                DateTime _dateStartWeek = ClassCommon.GetDateStartWeek(_week, objThang.Nam);
                DateTime _dateEndWeek = ClassCommon.GetDateEndWeek(_week, objThang.Nam);
                model.Tuan = _week;
                model.TuNgay = _dateStartWeek;
                model.DenNgay = _dateEndWeek;
            }   
            return model;
        }
    }
}

﻿using AutoMapper;
using GD_HTQL.WebApi.Common;
using GD_HTQL.WebApi.Data.Models;
using GD_HTQL.WebApi.Data.Models.Dtos.CongViec;
using GD_HTQL.WebApi.Data.Models.Dtos.NhanVien;
using GD_HTQL.WebApi.Data.Models.EF.NhanVienModels;
using GD_HTQL.WebApi.Data.Models.EF.VanBanModels;
using GD_HTQL.WebApi.Infrastructure;
using GD_HTQL.WebApi.Services.IRepository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace GD_HTQL.WebApi.Services.Repository
{
    public class FileRepository : GenericRepository<FileDinhKem>,IFileRepository
    {        
        public FileRepository(ApplicationDbContext context, IMapper mapper) :base(context, mapper) 
        {       
        }          
        public async Task AddFileDinhKem(List<FileDinhKemDto> fileData, int NhanVienID)
        {
            try
            {
                List<FileDinhKem> lstFile = _context.FileDinhKem.Where(x => x.NhanVienID == NhanVienID).ToList();

                foreach (var file in fileData)
                {
                    var objFile = lstFile.Where(x => x.FileUrl == file.FileUrl && x.NhanVienID == NhanVienID).FirstOrDefault();
                   
                    if (objFile  == null)
                    {
                        if (!string.IsNullOrEmpty(file.FileUrl))
                        {
                            string fileBase64 = file.FileUrl;
                            int index = fileBase64.IndexOf("base64,");
                            if (index != -1)
                            {
                                fileBase64 = fileBase64.Remove(0, index);
                            }
                            byte[] imageBytes = Convert.FromBase64String(fileBase64.Replace("base64,", string.Empty));
                            FileDinhKem fileDinhKem = new FileDinhKem()
                            {
                                FileName = file.FileName,
                                FileType = file.FileType,
                                NhanVienID = NhanVienID,
                                LoaiID = file.LoaiID
                            };
                            using (MemoryStream stream = new MemoryStream(imageBytes))
                            {
                                var _urlSaveFile = new DirectoryInfo(Path.Combine(Directory.GetCurrentDirectory() + "\\Resources\\Uploads\\DinhKem\\"));
                                if (!_urlSaveFile.Exists)
                                {
                                    _urlSaveFile.Create();
                                }
                                var content = new System.IO.MemoryStream(stream.ToArray());
                                string CustomName = ClassCommon.RandomString(19) + "_" + file.FileName;

                                fileDinhKem.FileUrl = "/Resources/Uploads/DinhKem/" + CustomName;
                                await CopyStream(content, _urlSaveFile + CustomName);
                            }

                            _context.FileDinhKem.Add(fileDinhKem);
                            await _context.SaveChangesAsync();
                        }                      
                    }
                    else
                    {
                        lstFile.Remove(objFile);
                    }                   
                }
                if(lstFile.Count > 0)
                {
                    foreach (var f in lstFile)
                    {
                        var filePath = Path.Combine(Directory.GetCurrentDirectory() + "\\Resources\\Uploads\\DinhKem\\", f.FileName);
                        if (File.Exists(filePath))
                        {
                            File.Delete(filePath);
                        }
                    }
                    _context.FileDinhKem.RemoveRange(lstFile);
                    _context.SaveChanges();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task AddFileCongViec(List<FileCongViecDto> fileData, int congViecID)
        {
            try
            {
                List<FileCongViec> lstFile = _context.FileCongViec.Where(x => x.CongViecID == congViecID).ToList();

               
                foreach (var file in fileData)
                {
                    int _loaifile = 2;
                    if (file.FileType == "image/png" || file.FileType == "image/jpeg" || file.FileType == "image/jpg" || file.FileType == "image/gif" || file.FileType == "image/tiff")
                    {
                        _loaifile = 1;
                    }

                    var objFile = lstFile.Where(x => x.FileUrl == file.FileUrl && x.CongViecID == congViecID).FirstOrDefault();

                    if (objFile == null)
                    {
                        if (!string.IsNullOrEmpty(file.FileUrl))
                        {
                            string fileBase64 = file.FileUrl;
                            int index = fileBase64.IndexOf("base64,");
                            if (index != -1)
                            {
                                fileBase64 = fileBase64.Remove(0, index);
                                byte[] imageBytes = Convert.FromBase64String(fileBase64.Replace("base64,", string.Empty));
                                FileCongViec filecongViec = new FileCongViec()
                                {
                                    FileName = file.FileName,
                                    FileType = file.FileType,
                                    CongViecID = congViecID,
                                    Loai = _loaifile
                                };
                                using (MemoryStream stream = new MemoryStream(imageBytes))
                                {
                                    var _urlSaveFile = new DirectoryInfo(Path.Combine(Directory.GetCurrentDirectory() + "\\Resources\\Uploads\\CongViec\\"));
                                    if (!_urlSaveFile.Exists)
                                    {
                                        _urlSaveFile.Create();
                                    }
                                    var content = new System.IO.MemoryStream(stream.ToArray());
                                    string CustomName = ClassCommon.RandomString(19) + "_" + file.FileName;

                                    filecongViec.FileUrl = "/Resources/Uploads/CongViec/" + CustomName;
                                    await CopyStream(content, _urlSaveFile + CustomName);
                                }
                                _context.FileCongViec.Add(filecongViec);
                                await _context.SaveChangesAsync();
                            }                          
                        }
                    }
                    else
                    {
                        lstFile.Remove(objFile);
                    }
                }
                if (lstFile.Count > 0)
                {
                    // Xóa file

                    foreach (var f in lstFile)
                    {
                        var filePath = Path.Combine(Directory.GetCurrentDirectory() + "\\Resources\\Uploads\\CongViec\\", f.FileName);
                        if (File.Exists(filePath))
                        {
                            File.Delete(filePath);
                        }
                    }                  
                    _context.FileCongViec.RemoveRange(lstFile);                  
                    _context.SaveChanges();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public async Task PostFileVanBan(List<IFormFile> fileData, int VanBanID)
        {
            try
            {
                foreach (var file in fileData)
                {
                    FileVanBan fileVanBan = new FileVanBan()
                    {
                        FileName = file.FileName,
                        FileType = file.ContentType,
                        VanBanID = VanBanID
                    };
                    using (var stream = new MemoryStream())
                    {
                        file.CopyTo(stream);
                        fileVanBan.FileBase64 = Convert.ToBase64String(stream.ToArray());
                        var content = new System.IO.MemoryStream(stream.ToArray());
                        var FoderFileVanBan = new DirectoryInfo(Path.Combine(Directory.GetCurrentDirectory(), "Uploads\\FileVanBan"));
                        if (!FoderFileVanBan.Exists)
                        {
                            FoderFileVanBan.Create();
                        }

                        var path = Path.Combine(Directory.GetCurrentDirectory(), "Uploads\\FileVanBan", file.FileName);
                        await CopyStream(content, path);
                    }
                                    
                    _context.FileVanBan.Add(fileVanBan);
                    await _context.SaveChangesAsync();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    
        public bool DeleteFileVanBan(int ID)
        {
            bool result = false;
            var objFileVanBan = _context.FileVanBan.Find(ID);
            if (objFileVanBan != null)
            {
                var filePath = Path.Combine(Directory.GetCurrentDirectory(), "Uploads\\FileVanBan", objFileVanBan.FileName);
                if (File.Exists(filePath))
                {
                    File.Delete(filePath);
                }
                _context.Entry(objFileVanBan).State = EntityState.Deleted;
                _context.SaveChanges();
                result = true;
            }
            else
            {
                result = false;
            }
            return result;
        }
        public async Task<IEnumerable<FileVanBan>> GetFileVanBanByVanBanID(int _VanBanID)
        {
            return await _context.FileVanBan.Where(x => x.VanBanID == _VanBanID).ToListAsync();
        }

        public async Task<IEnumerable<FileDinhKem>> GetFileDinhKemByNhanVienID(int _NhanVienID)
        {
            return await _context.FileDinhKem.Where(x => x.NhanVienID == _NhanVienID).ToListAsync();
        }
           
        public async Task<FileContentResult> DowLoadFileVanBanByID(int ID)
        {
            var FileEntity = await _context.FileVanBan.FirstOrDefaultAsync(x => x.FileID == ID);

            if (FileEntity == null)
            {
                return null;
            }
            else
            {
                var fileContentResult = new FileContentResult(Convert.FromBase64String(FileEntity.FileBase64),FileEntity.FileType)
                {
                    FileDownloadName = FileEntity.FileName
                };
                return fileContentResult;
            }
        }
        public async Task CopyStream(Stream stream, string downloadPath)
        {
            using (var fileStream = new FileStream(downloadPath, FileMode.Create, FileAccess.Write))
            {
                await stream.CopyToAsync(fileStream);
            }
        }       
    }
}

﻿using AutoMapper;
using GD_HTQL.WebApi.Data.Models;
using GD_HTQL.WebApi.Data.Models.EF.CongTacModels;
using GD_HTQL.WebApi.Infrastructure;
using GD_HTQL.WebApi.Services.IRepository;

namespace GD_HTQL.WebApi.Services.Repository
{
    public class KHCT_Ngay_LichSuRepository : GenericRepository<KHCT_Ngay_LichSu>,IKHCT_Ngay_LichSuRepository
    {
        public KHCT_Ngay_LichSuRepository(ApplicationDbContext context, IMapper mapper) : base(context, mapper)
        {
        }
    }
}

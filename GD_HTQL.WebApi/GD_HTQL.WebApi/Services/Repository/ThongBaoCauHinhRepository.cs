﻿using AutoMapper;
using GD_HTQL.WebApi.Data.Models;
using GD_HTQL.WebApi.Data.Models.EF.ThongBaoModels;
using GD_HTQL.WebApi.Infrastructure;
using GD_HTQL.WebApi.Services.IRepository;


namespace GD_HTQL.WebApi.Services.Repository
{
    public class ThongBaoCauHinhRepository : GenericRepository<ThongBaoCauHinh>, IThongBaoCauHinhRepository
    {
        public ThongBaoCauHinhRepository(ApplicationDbContext context, IMapper mapper) : base(context, mapper)
        {
        }
    }
}

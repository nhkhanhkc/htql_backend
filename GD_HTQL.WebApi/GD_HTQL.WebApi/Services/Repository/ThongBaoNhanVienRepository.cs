﻿using AutoMapper;
using GD_HTQL.WebApi.Data.Models;
using GD_HTQL.WebApi.Data.Models.CustomModels;
using GD_HTQL.WebApi.Data.Models.EF.ThongBaoModels;
using GD_HTQL.WebApi.Data.Models.ViewModels;
using GD_HTQL.WebApi.Infrastructure;
using GD_HTQL.WebApi.Services.IRepository;
using Microsoft.EntityFrameworkCore;

namespace GD_HTQL.WebApi.Services.Repository
{
    public class ThongBaoNhanVienRepository : GenericRepository<ThongBaoNhanVien> , IThongBaoNhanVienRepository
    {
        public ThongBaoNhanVienRepository(ApplicationDbContext context, IMapper mapper) : base(context, mapper)
        {
        }
        public async Task<IEnumerable<NhanVienShortView>> GetNhanVien(List<int> nhanVienID)
        {
            List<NhanVienShortView> nhanVienShorts = new List<NhanVienShortView>();
            var obj = await _context.NhanVien.Where(x => nhanVienID.Contains(x.NhanVienID)).ToListAsync();
            nhanVienShorts = _mapper.Map<List<NhanVienShortView>>(obj);
            return nhanVienShorts;
        }
        public void AddMulti(ThongBaoNhanVienCustom thongBaoNhanVien)
        {
            // Xóa Quản lý nhân viên Củ 
            var objThongBao = _context.ThongBaoNhanVien.Where(x => x.ThongBaoID == thongBaoNhanVien.ThongBaoID).ToList();
            _context.ThongBaoNhanVien.RemoveRange(objThongBao);
            _context.SaveChanges();

            List<ThongBaoNhanVien> lstQLNV = new List<ThongBaoNhanVien>();
            if (thongBaoNhanVien.lstNhanVienID.Count() > 0)
            {
                foreach (var it in thongBaoNhanVien.lstNhanVienID)
                {
                    ThongBaoNhanVien obj = new ThongBaoNhanVien()
                    {
                        Id = 0,
                        NhanVienID = it,
                        ThongBaoID = thongBaoNhanVien.ThongBaoID
                    };
                    lstQLNV.Add(obj);
                }
                _context.ThongBaoNhanVien.AddRange(lstQLNV);
                _context.SaveChanges();
            }
        }
    }
}

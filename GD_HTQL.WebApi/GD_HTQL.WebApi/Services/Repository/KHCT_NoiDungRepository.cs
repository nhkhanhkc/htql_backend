﻿using AutoMapper;
using GD_HTQL.WebApi.Data.Models;
using GD_HTQL.WebApi.Data.Models.EF.CongTacModels;
using GD_HTQL.WebApi.Infrastructure;
using GD_HTQL.WebApi.Services.IRepository;

namespace GD_HTQL.WebApi.Services.Repository
{
    public class KHCT_NoiDungRepository : GenericRepository<KHCT_NoiDung>, IKHCT_NoiDungRepository
    {
        public KHCT_NoiDungRepository(ApplicationDbContext context, IMapper mapper) : base(context, mapper)
        {
        }
    }
}

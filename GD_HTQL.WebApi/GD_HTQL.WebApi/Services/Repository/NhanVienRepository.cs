﻿using AutoMapper;
using GD_HTQL.WebApi.Common;
using GD_HTQL.WebApi.Data.Models;
using GD_HTQL.WebApi.Data.Models.Dtos.NhanVien;
using GD_HTQL.WebApi.Data.Models.EF.NhanVienModels;
using GD_HTQL.WebApi.Data.Models.ViewModels;
using GD_HTQL.WebApi.Infrastructure;
using GD_HTQL.WebApi.Services.IRepository;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;

namespace GD_HTQL.WebApi.Services.Repository
{
    public class NhanVienRepository : GenericRepository<NhanVien>,INhanVienRepository
    {   
        public NhanVienRepository(ApplicationDbContext context, IMapper mapper) :base(context, mapper) 
        {                  
        }
        public async Task<IEnumerable<NhanVienViewModel>> GetNhanVien(bool isActive)
        {
            var objChuVuInclude =  _context.ChucVu.Include(x => x.PhongBan).Include(x => x.PhongBan.CongTy).ToList();
            var obj  = await _context.NhanVien.Where(x=>x.Active ==isActive).Include(x=>x.FileDinhKem).Include(x=>x.ChucVu).Select(x=> new NhanVienViewModel
            {
               NhanVienID = x.NhanVienID,
               TenNhanVien = x.TenNhanVien,
               GioiTinh = x.GioiTinh == 1 ? "Nam": x.GioiTinh == 0 ? "Nữ" :"",
               SoDienThoai = x.SoDienThoai,
               Email = x.Email,
               NgaySinh = x.NgaySinh ==null ?"":Convert.ToDateTime(x.NgaySinh).ToString("dd/MM/yyyy"),
               NgayKyHopDong = x.NgayKyHopDong == null ? "" : Convert.ToDateTime(x.NgayKyHopDong).ToString("dd/MM/yyyy"),
               DiaChi = x.DiaChi,
               Avartar = x.FileDinhKem == null? "": x.FileDinhKem.Where(x=>x.LoaiID == 1).FirstOrDefault() == null ? "" : x.FileDinhKem.Where(x => x.LoaiID == 1).FirstOrDefault().FileUrl  ,
               Active = x.Active,
               lstChucVuView =x.ChucVu ==null ? null : getChiTiet(x.ChucVu.ToList(),_mapper, objChuVuInclude),

            }).ToListAsync();
            return obj;
        }   
        public async Task<NhanVien> GetNhanVienByUserID(string ID)
        {
            return await _context.NhanVien.Where(x=>x.UserID == ID).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<NhanVien>> GetNhanVienByTuKhoa(string Key)
        {
            var objNhanVien = await _context.NhanVien.Where(ten => ten.TenNhanVien.Contains(Key)).ToListAsync();
            return objNhanVien;
        }        
        public async Task<ChucVuNhanVien> ThemChucVu(ChucVuNhanVien chucVuNhanVien)
        {
            _context.ChucVuNhanVien.Add(chucVuNhanVien);
            await _context.SaveChangesAsync();
            return chucVuNhanVien;
        }
        public bool CheckChucVuExits(ChucVuNhanVien chucVuNhanVien)
        {
            var objChucVuNhanVien = _context.ChucVuNhanVien.Where(x => x.ChucVuID == chucVuNhanVien.ChucVuID && x.NhanVienID == chucVuNhanVien.NhanVienID).FirstOrDefault();
            if (objChucVuNhanVien == null)
            {
                return true;
            }
            else
            {
                return false;
            }          
        }
        public bool XoaChucVu(ChucVuNhanVien objViTriNhanVien)
        {
            bool result = false;
            var ViTriXoa = _context.ChucVuNhanVien.First(row => row.ChucVuID == objViTriNhanVien.ChucVuID && row.NhanVienID == objViTriNhanVien.NhanVienID);
            if (ViTriXoa != null)
            {
                _context.Entry(ViTriXoa).State = EntityState.Deleted;
                _context.SaveChanges();
                result = true;
            }
            else
            {
                result = false;
            }
            return result;
        }
        public async Task<NhanVien> DoiTrangThai(int ID , ClaimsPrincipal User)
        {      
            var  objNhanVien = await _context.NhanVien.FindAsync(ID);
            if (objNhanVien != null)
            {
                if (objNhanVien.Active == true)
                {
                    objNhanVien.Active = false;
                }
                else
                {
                    objNhanVien.Active = true;
                }
                objNhanVien.ActiveDate = DateTime.Now;            
                objNhanVien.UpdateBy = ClassCommon.GetTenNhanVien(User);
                _context.Entry(objNhanVien).State = EntityState.Modified;
                 _context.SaveChanges();
                return objNhanVien;
            }
            else
            {
                return null;
            }           
        }
        public async Task<NhanVienDetails> GetDetails(int ID)
        {       
            var objNhaVien = await _context.NhanVien.Where(x => x.NhanVienID == ID).Include(x=>x.FileDinhKem).Select(x => new NhanVienDetails
            {
                NhanVienID = x.NhanVienID,
                TenNhanVien = x.TenNhanVien,
                NgaySinh= x.NgaySinh,
                NgayKyHopDong= x.NgayKyHopDong,
                SoDienThoai= x.SoDienThoai,
                GioiTinh = x.GioiTinh == 1 ? "Nam" : x.GioiTinh == 0 ? "Nữ" : "",
                Email = x.Email,
                DiaChi= x.DiaChi,
                lstFile = x.FileDinhKem == null ? null : _mapper.Map<List<FileDinhKemView>>(x.FileDinhKem),
                Active = x.Active
               
            }).FirstOrDefaultAsync();
            if (objNhaVien != null)
            {
                var objChuVuNhanVien = _context.ChucVuNhanVien.Where(x => x.NhanVienID == ID).ToList();
                if (objChuVuNhanVien.Count > 0)
                {
                   foreach (var it in objChuVuNhanVien)
                    {                     
                        var objChuVu = _context.ChucVu.Where(x => x.ChucVuID == it.ChucVuID).Include(x=>x.PhongBan).Include(x=>x.PhongBan.CongTy).FirstOrDefault();
                        if (objChuVu !=null)
                        {
                            ChucVuView objchucVuView = new ChucVuView();
                            objchucVuView.lstChucVu = _mapper.Map<ChucVuDto>(objChuVu);
                            if (objChuVu.PhongBan != null)
                            {
                                objchucVuView.lstPhongBan = _mapper.Map<PhongBanDto>(objChuVu.PhongBan);
                                if (objChuVu.PhongBan.CongTy != null)
                                {
                                    objchucVuView.lstCongTy = _mapper.Map<CongTyDto>(objChuVu.PhongBan.CongTy);
                                }
                            }
                            objNhaVien.lstChucVuView.Add(objchucVuView);
                        }                      
                    }
               }
                return objNhaVien;
            }
            else
            {
                return null;
            }          
        }

        public async Task<List<NhanVienDto>> GetByChucVu(int chucvuID)
        {
            List<NhanVienDto> nhanVienDtos = new List<NhanVienDto>();
            var lstNhanVienId =  _context.ChucVuNhanVien.Where(x => x.ChucVuID == chucvuID).Select(x => x.NhanVienID).Distinct().ToList();
            if(lstNhanVienId.Count > 0)
            {
                var objNhanVien = await _context.NhanVien.Where(x => lstNhanVienId.Contains(x.NhanVienID)).ToListAsync();
                 nhanVienDtos = _mapper.Map<List<NhanVienDto>>(objNhanVien);
            }
            return nhanVienDtos;
        }
        public async Task<List<FileLoai>> GetLoaiFile()
        {       
            var lstLoai =await _context.FileLoai.ToListAsync();              
            return lstLoai;
        }    
        public static List<ChucVuView> getChiTiet(List<ChucVu> chucVus, IMapper _mapper, List<ChucVu> chucVuInclude)
        {
           List<ChucVuView> lstChucVuView = new List<ChucVuView>();
            foreach (var it in chucVus)
            {
                ChucVuView objchucVuView = new ChucVuView();
                var objChuVu = chucVuInclude.Where(x => x.ChucVuID == it.ChucVuID).FirstOrDefault();
                if (objChuVu != null)
                {
                    objchucVuView.lstChucVu = _mapper.Map<ChucVuDto>(objChuVu);
                    if (objChuVu.PhongBan != null)
                    {
                        objchucVuView.lstPhongBan = _mapper.Map<PhongBanDto>(objChuVu.PhongBan);
                        if (objChuVu.PhongBan.CongTy != null)
                        {
                            objchucVuView.lstCongTy = _mapper.Map<CongTyDto>(objChuVu.PhongBan.CongTy);
                        }
                    }
                    lstChucVuView.Add(objchucVuView);
                }
            }
            return lstChucVuView;
        }
        public async Task<List<NhanVienShortView>> GetShort()
        {
            var objNhanVien = await _context.NhanVien.Where(x => x.Active == true).ToListAsync();
            var objShort = _mapper.Map<List<NhanVienShortView>>(objNhanVien);
            return objShort;
        }
    }
}

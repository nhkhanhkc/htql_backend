﻿using GD_HTQL.WebApi.Data.Models;
using GD_HTQL.WebApi.Data.Models.EF.NghiPhepModels;
using GD_HTQL.WebApi.Services.IRepository;
using GD_HTQL.WebApi.Telegram;
using Microsoft.EntityFrameworkCore;
using System.Globalization;

namespace GD_HTQL.WebApi.Services.Repository
{
    public class SendTelegramRepository : ISendTelegramRepository
    {
        private readonly ApplicationDbContext _context;
        private readonly WTelegramService _WT;
        public SendTelegramRepository(ApplicationDbContext context, WTelegramService WT)
        {
            _context = context;
            _WT = WT;
        }
        public async Task SendTeleNhaThau_DuToan(int ID, string tieude)
        {

            var objThongBao = _context.ThongBaoCauHinh.Where(x => x.MaThongBao == "nt_dutoan" && x.Active == true).FirstOrDefault();
            if (objThongBao != null)
            {                        
                var objDuToan = await _context.NT_DuToan.Where(x => x.DuToanID == ID).Include(x => x.NhaThau).Include(x => x.Tinh).Include(x => x.NT_TrangThaiDuToan).Include(x => x.NhanVien).FirstOrDefaultAsync();
                if (objDuToan != null)
                {
                    CultureInfo cul = CultureInfo.GetCultureInfo("vi-VN");
                    string _doanhthu = objDuToan.DoanhThuDuKien == null ? "" :Convert.ToDecimal(objDuToan.DoanhThuDuKien).ToString("#,###", cul.NumberFormat) +"đ";
                    string _noiDung = objThongBao.NoiDung.Replace("<TIEUDE>", tieude).Replace("<NHATHAU>", objDuToan.NhaThau.TenCongTy)
                                     .Replace("<NHANVIENNHAP>", objDuToan.CreateBy).Replace("<TENDUTOAN>", objDuToan.TenDuToan)
                                     .Replace("<TINH>", objDuToan.Tinh.TenTinh).Replace("<LOAIBAOGIA>", objDuToan.NT_TrangThaiDuToan.TenTrangThai).Replace("<DOANHTHU>", _doanhthu);
                    await sendMessage(objThongBao.TenNhom, _noiDung);
                }
            }
        }
        public async Task SendTeleNhaThau_CoHoi(int ID, string tieude)
        {
            var objThongBao = _context.ThongBaoCauHinh.Where(x => x.MaThongBao == "nt_baocaotiepxuc" && x.Active == true).FirstOrDefault();
            if (objThongBao != null)
            {                          
                var objCoHoi = await _context.NT_GiaTriCoHoi.Where(x => x.GTCHID == ID).Include(x => x.NhaThau).Include(x => x.NT_The).Include(x => x.NguonVon).FirstOrDefaultAsync();
                if (objCoHoi != null)
                {
                    string nhomhang = "";
                    var objLoaiSanPham = _context.NT_LoaiSanPham.Where(x => x.GTCHID == ID).ToList();
                    if (objLoaiSanPham.Count > 0)
                    {
                        foreach (var it in objLoaiSanPham)
                        {
                            var obj = _context.LoaiSanPham.Where(x => x.LoaiSanPhamID == it.LoaiSanPhamID).FirstOrDefault();
                            if (obj != null)
                            {
                                nhomhang += obj.TenLoaiSanPham + "   ";
                            }
                        }
                    }

                    string quantam = objCoHoi.IsQuanTamHopTac == true ? "Có" : "Không";
                    string hopdong = objCoHoi.IsKyHopDongNguyenTac == true ? "Có" : "Không";
                    string _tinh = "";
                    var objTinh = _context.DMTinh.Where(x => x.TinhID == objCoHoi.NhaThau.TinhID).FirstOrDefault();
                    if (objTinh != null)
                    {
                        _tinh = objTinh.TenTinh;
                    }

                    string _noiDung = objThongBao.NoiDung.Replace("<TIEUDE>", tieude).Replace("<NHATHAU>", objCoHoi.NhaThau.TenCongTy).Replace("<TINH>", _tinh)
                                                         .Replace("<NHOMHANG>", nhomhang).Replace("<QUANTAM>", quantam)
                                                         .Replace("<HOPDONG>", hopdong).Replace("<NGUONVON>", objCoHoi.NguonVon.TenNguonVon)
                                                         .Replace("<THE>", objCoHoi.NT_The.TenThe).Replace("<GHICHU>", objCoHoi.GhiChu);
                    await sendMessage(objThongBao.TenNhom, _noiDung);
                }              
            }
        }
        public async Task SendTeleDuAn_TiepXuc(int tuongTacID, string tieude)
        {
            var objThongBao = _context.ThongBaoCauHinh.Where(x => x.MaThongBao == "duan_baocaotiepxuc" && x.Active == true).FirstOrDefault();
            if (objThongBao != null)
            {
                var objTiepXuc = await _context.QuanLyTuongTac.Where(x => x.TuongTacID == tuongTacID).Include(x => x.NSCoQuan).Include(x => x.NhanVien).Include(x => x.BuocThiTruong).Include(x=>x.NSNguonVon).FirstOrDefaultAsync();
                string _tinh = "";
                var objTinh = _context.DMTinh.Where(x => x.TinhID == objTiepXuc.NSCoQuan.TinhID).FirstOrDefault();
                if (objTinh != null)
                {
                    _tinh = objTinh.TenTinh;
                }
                string _noiDung = objThongBao.NoiDung.Replace("<TIEUDE>", tieude).Replace("<COQUAN>", objTiepXuc.NSCoQuan.TenCoQuan).Replace("<TINH>",_tinh).Replace("<NHOMHANGQUANTAM>", objTiepXuc.NhomHangQuanTam)
                                                     .Replace("<NGUONVON>", objTiepXuc.NSNguonVon.TenNguonVon).Replace("<TENBUOCTHITRUONG>", objTiepXuc.BuocThiTruong.BuocThiTruongTen).Replace("<GHICHU>", objTiepXuc.GhiChu);
                await sendMessage(objThongBao.TenNhom, _noiDung);
            }
        }
        public async Task SendTeleDuAn_CoHoi(int duToanID, string tieude)
        {
            var objThongBao = _context.ThongBaoCauHinh.Where(x => x.MaThongBao == "da_dutoan" && x.Active == true).FirstOrDefault();
            if (objThongBao != null)
            {
                var objDuToan = await _context.NSDuToan.Where(x => x.DuToanID == duToanID).Include(x => x.NSCoQuan).Include(x => x.NhanVien).Include(x => x.BuocThiTruong).FirstOrDefaultAsync();
                if (objDuToan != null)
                {
                    string _tinh = "";
                    var objTinh = _context.DMTinh.Where(x => x.TinhID == objDuToan.NSCoQuan.TinhID).FirstOrDefault();
                    if (objTinh != null)
                    {
                        _tinh = objTinh.TenTinh;
                    }
                    CultureInfo cul = CultureInfo.GetCultureInfo("vi-VN");
                    string _doanhthu = objDuToan.DoanhThuDuKien == null ? "" : Convert.ToDecimal(objDuToan.DoanhThuDuKien).ToString("#,###", cul.NumberFormat) + "đ";
                    string _noiDung = objThongBao.NoiDung.Replace("<TIEUDE>", tieude).Replace("<TENDUTOAN>", objDuToan.TenDuToan).Replace("<TENCOQUAN>", objDuToan.NSCoQuan.TenCoQuan).Replace("<TINH>",_tinh)
                                     .Replace("<TENBUOCTHITRUONG>", objDuToan.BuocThiTruong.BuocThiTruongTen).Replace("<DOANHTHU>", _doanhthu).Replace("<GHICHU>", objDuToan.GhiChu);
                    await sendMessage(objThongBao.TenNhom, _noiDung);
                }
            }
        }
        public async Task SendTelegramNghiPhep(NghiPhep mess,string tieude)
        {
            var objThongBao = _context.ThongBaoCauHinh.Where(x => x.MaThongBao == "nghiphep" && x.Active == true).FirstOrDefault();
            if (objThongBao != null)
            {
                string _noiDung = objThongBao.NoiDung.Replace("<NHANVIEN>", mess.HoTen).Replace("<CHUCVU>", mess.TenChucVu)
                                                     .Replace("<PHONGBAN>", mess.TenPhongBan).Replace("<CONGTY>", mess.TenCongTy)
                                                     .Replace("<NGAYNGHI>", mess.NgayNghi.ToString()).Replace("<THU>", mess.Thu)
                                                     .Replace("<TUNGAY>", mess.TuNgay.ToString("dd/MM/yyyy")).Replace("<DENNGAY>", mess.DenNgay.ToString("dd/MM/yyyy"))
                                                     .Replace("<LYDO>",mess.LyDo).Replace("<TIEUDE>",tieude);
                await sendMessage(objThongBao.TenNhom, _noiDung);
            }
        }

        public async Task SendTele_GiaoViec(int congViecID, string tieude, int lsID)
        {
            var objThongBao = await _context.ThongBaoCauHinh.Where(x => x.MaThongBao == "giaoviec" && x.Active == true).ToListAsync();
            var objCongViec = await _context.CongViec.Where(x => x.CongViecID == congViecID).Include(x=>x.DoUuTien).Include(x=>x.NhomCongViec).FirstOrDefaultAsync();
            if (objCongViec != null)
            {
                if (objThongBao.Count > 0)
                {
                    foreach (var it in objThongBao)
                    {
                        var objGroupCongViec = _context.ThongBaoNhanVien.Where(x => x.NhanVienID == objCongViec.NguoiThucHienID && x.ThongBaoID == it.ThongBaoID).ToList();
                        // Kiểm tra người thực hiện ở trong group nào
                        if (objGroupCongViec.Count() > 0 )
                        {
                            string _BaoCaoCongViec = string.Empty;
                            if (lsID > 0)
                            {
                                var objLS = _context.LichSuCongViec.Where(x => x.LSID == lsID).FirstOrDefault();
                                if (objLS != null)
                                {
                                    _BaoCaoCongViec = "BÁO CÁO CÔNG VIỆC \n ";
                                    _BaoCaoCongViec += "Nội dung thực hiện: " + objLS.MoTa + " \n";
                                    _BaoCaoCongViec += "Tiến độ thực hiện: " + objLS.TienDo + "% \n";
                                    _BaoCaoCongViec += "CHI TIẾT CÔNG VIỆC \n";
                                }                              
                            }

                            string _noiDung = it.NoiDung.Replace("<TIEUDE>", tieude).Replace("<BAOCAOCONGVIEC>", _BaoCaoCongViec).Replace("<TENCONGVIEC>", objCongViec.TenCongViec)
                                                  .Replace("<MOTA>", objCongViec.MoTa).Replace("<NGUOITAO>", objCongViec.CreateBy)
                                                  .Replace("<NGUOITHUCHIEN>", objCongViec.NguoiThucHienTen).Replace("<DOUUTIEN>", objCongViec.DoUuTien.TenUuTien)
                                                  .Replace("<TUNGAY>", objCongViec.NgayBatDau.ToString("dd/MM/yyyy")).Replace("<DENNGAY>", objCongViec.NgayKetThuc.ToString("dd/MM/yyyy"));
                            await sendMessage(it.TenNhom, _noiDung);
                        }                    
                    }
                }
            }          
        }

        // Kế hoạch công tác
        public async Task SendTeleKHCT_Thang(int ID, string tieude)
        {
            var objThongBao = _context.ThongBaoCauHinh.Where(x => x.MaThongBao == "khct_thang" && x.Active == true).FirstOrDefault();
            if (objThongBao != null)
            {
                var obj = _context.KHCT_Thang.Where(x => x.ThangID == ID).FirstOrDefault();
                if (obj != null)
                {
                    string _noiDung = objThongBao.NoiDung.Replace("<TIEUDE>", tieude).Replace("<KEHOACH>", obj.TieuDe)
                                                         .Replace("<NOIDUNG>", obj.NoiDung).Replace("<THANG>", obj.Thang.ToString())
                                                         .Replace("<TUNGAY>", obj.TuNgay.ToString("dd/MM/yyyy")).Replace("<DENNGAY>", obj.DenNgay.ToString("dd/MM/yyyy"));
                    await sendMessage(objThongBao.TenNhom, _noiDung);
                }                          
            }
        }
        public async Task SendTeleKHCT_Tuan(int ID, string tieude)
        {
            var objThongBao = _context.ThongBaoCauHinh.Where(x => x.MaThongBao == "khct_tuan" && x.Active == true).FirstOrDefault();
            if (objThongBao != null)
            {
                var obj = _context.KHCT_Tuan.Where(x => x.TuanID == ID).Include(x=>x.KHCT_Thang).FirstOrDefault();
                if (obj != null)
                {
                    string _noiDung = objThongBao.NoiDung.Replace("<TIEUDE>", tieude).Replace("<KEHOACH>", obj.TieuDe)
                                                         .Replace("<NOIDUNG>", obj.NoiDung).Replace("<THANG>", obj.KHCT_Thang.Thang.ToString()).Replace("TUAN",obj.Tuan_Thang.ToString())
                                                         .Replace("<TUNGAY>", obj.TuNgay.ToString("dd/MM/yyyy")).Replace("<DENNGAY>", obj.DenNgay.ToString("dd/MM/yyyy"));
                    await sendMessage(objThongBao.TenNhom, _noiDung);
                }
            }
        }
        public async Task SendTeleKHCT_Ngay(int ID, string tieude)
        {
            var objThongBao = _context.ThongBaoCauHinh.Where(x => x.MaThongBao == "khct_ngay" && x.Active == true).FirstOrDefault();
            if (objThongBao != null)
            {
                var obj = _context.KHCT_Ngay.Where(x => x.NgayID == ID).FirstOrDefault();
                if (obj != null)
                {
                    string _noiDung = objThongBao.NoiDung.Replace("<TIEUDE>", tieude).Replace("<MUCDICH>", obj.MucDich)
                                                         .Replace("<TUNGAY>", obj.TuNgay.ToString("dd/MM/yyyy")).Replace("<DENNGAY>", obj.DenNgay.ToString("dd/MM/yyyy"));
                    await sendMessage(objThongBao.TenNhom, _noiDung);
                }
            }
        }


        public async Task sendMessage(string tenNhom, string noiDung)
        {
            //var chats = await WT.Client.Contacts_GetContacts();
            //var contacts = chats.contacts.ToList();
            //var User = chats.users.ToList();
            // Send Channel
            var Channels = await _WT.Client.Messages_GetAllChats();
            var chat = Channels.chats.ToList();
            var chat1 = chat.Where(x => x.Value.Title == tenNhom).FirstOrDefault();
            if (chat1.Value != null)
            {
                await _WT.Client.SendMessageAsync(chat1.Value, noiDung);
            }
        }
    }
}

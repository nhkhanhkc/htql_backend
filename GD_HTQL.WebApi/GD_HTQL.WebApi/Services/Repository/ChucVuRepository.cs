﻿using AutoMapper;
using GD_HTQL.WebApi.Common;
using GD_HTQL.WebApi.Data.Models;
using GD_HTQL.WebApi.Data.Models.EF.NhanVienModels;
using GD_HTQL.WebApi.Infrastructure;
using GD_HTQL.WebApi.Services.IRepository;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;


namespace GD_HTQL.WebApi.Services.Repository
{
    public class ChucVuRepository : GenericRepository<ChucVu>,IChucVuRepository
    {    
        public ChucVuRepository(ApplicationDbContext context , IMapper mapper) :base(context,mapper) 
        {         
        }    
        public async Task<IEnumerable<ChucVu>> GetChucVuByTuKhoa(string Key, int _PhongBanID)
        {
            var objChucVu = await _context.ChucVu.Where(x => (_PhongBanID == 0 || x.PhongBanID == _PhongBanID)).ToListAsync();
            if (!string.IsNullOrEmpty(Key))
            {
                objChucVu = objChucVu.AsQueryable().Where(ExpressionDuToanTheoChucVu(Key)).OrderByDescending(x => x.ChucVuID).ToList();
            }
            return objChucVu;
        }

        public async Task<IEnumerable<ChucVuNhanVien>> GetChucVuNhanVien( int chucVuID)
        {
            var objChucVu = await _context.ChucVuNhanVien.Where(x => x.ChucVuID == chucVuID).ToListAsync();
            
            return objChucVu;
        }
        public async Task<IEnumerable<ChucVuNhanVien>> GetByNhanVien(int nhanVienID)
        {
            var objChucVu = await _context.ChucVuNhanVien.Where(x => x.NhanVienID == nhanVienID).ToListAsync();

            return objChucVu;
        }

        public bool DoiTrangThai(int ID)
        {
            bool result = false;
            var objChucVu = _context.ChucVu.Find(ID);
            if (objChucVu != null)
            {
                if (objChucVu.Active == true)
                {
                    objChucVu.Active = false;
                }
                else
                {
                    objChucVu.Active = true;
                }
                _context.Entry(objChucVu).State = EntityState.Modified;
                _context.SaveChanges();
                result = true;
            }
            else
            {
                result = false;
            }
            return result;
        }
        public Expression<Func<ChucVu, bool>> ExpressionDuToanTheoChucVu(string _key)
        {
            _key = ClassCommon.RemoveUnicode(_key).ToLower();
            return x => ClassCommon.RemoveUnicode(x.TenChucVu).ToLower().Contains(_key);
        }
    }
}

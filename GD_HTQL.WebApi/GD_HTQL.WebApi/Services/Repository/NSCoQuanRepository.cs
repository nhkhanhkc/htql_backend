﻿using AutoMapper;
using GD_HTQL.WebApi.Common;
using GD_HTQL.WebApi.Data.Models;
using GD_HTQL.WebApi.Data.Models.Dtos.DanhMuc;
using GD_HTQL.WebApi.Data.Models.Dtos.DuAn;
using GD_HTQL.WebApi.Data.Models.Dtos.NhanVien;
using GD_HTQL.WebApi.Data.Models.EF.DuAnModels;
using GD_HTQL.WebApi.Data.Models.EF.NhanVienModels;
using GD_HTQL.WebApi.Data.Models.ViewModels;
using GD_HTQL.WebApi.Infrastructure;
using GD_HTQL.WebApi.Services.IRepository;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace GD_HTQL.WebApi.Services.Repository
{
    public class NSCoQuanRepository : GenericRepository<NSCoQuan>, INSCoQuanRepository
    {      
        public NSCoQuanRepository(ApplicationDbContext context, IMapper mapper) : base(context, mapper) 
        {              
        }
      
        // Quy ước TinhID = 0 lấy tất cả các cơ quan
        public async Task<IEnumerable<NSCoQuan>> GetNSCoQuanByTuKhoa(string? _key, int _TinhID)
        {
            IEnumerable<NSCoQuan> objNSCoQuan = await _context.NSCoQuan.Where(x=>(_TinhID ==0 || x.TinhID == _TinhID) && x.Active ==true).OrderByDescending(x => x.CoQuanID).ToListAsync();
            if (!string.IsNullOrEmpty(_key))
            {
                objNSCoQuan = objNSCoQuan.AsQueryable().Where(ExpressionCoQuanTheoTuKhoa(_key)).OrderByDescending(x => x.CoQuanID).ToList();
            }          
            return objNSCoQuan;
        }          
        public Expression<Func<NSCoQuan, bool>> ExpressionCoQuanTheoTuKhoa(string _key)
        {
            _key = ClassCommon.RemoveUnicode(_key).ToLower();
            return x =>ClassCommon.RemoveUnicode(x.TenCoQuan).ToLower().Contains(_key);
        }

        public async Task<IEnumerable<NSCoQuan>> GetByNhanVien(int nhaVienID)
        {
            List<NSCoQuan> objCoQuan = new List<NSCoQuan>();
            var objTinh_NhanVien = _context.TinhNhanVien.Where(x => x.NhanVienID == nhaVienID).Select(x => x.TinhID).ToList();
            if (objTinh_NhanVien.Count > 0)
            {
                objCoQuan = await _context.NSCoQuan.Where(x =>x.TinhID !=null && objTinh_NhanVien.Contains((int)x.TinhID)).Include(x=>x.NSCanBo).ToListAsync();
            }
            return objCoQuan;
        }

        public async Task<IEnumerable<NSCoQuan>> GetALl()
        {                    
             var  objCoQuan = await _context.NSCoQuan.Include(x => x.NSCanBo).ToListAsync();          
            return objCoQuan;
        }
        public bool CheckMaSoThue(string? _maSoThue, int _coQuanID)
        {
            bool result = false;
            var objNhaThau = _context.NSCoQuan.Where(x => x.MaSoThue == _maSoThue && (_coQuanID == 0 || x.CoQuanID != _coQuanID)).ToList();
            if (objNhaThau.Count() == 0)
            {
                result = true;
            }
            else
            {
                result = false;
            }
            return result;
        }
        public async Task<IEnumerable<NSCoQuanViewModel>> GetDanhSach(int nhaVienID)
        {
            List<NSCoQuanViewModel> lstCoQuanView = new List<NSCoQuanViewModel>();
            List<NSCoQuan> lstCoQuan = new List<NSCoQuan>();
            if (nhaVienID == 0)
            {
                lstCoQuan = await _context.NSCoQuan.Include(x => x.NhanVien).Include(x => x.Huyen).Include(x => x.NSCanBo).Include(x => x.TuongTac).Include(x => x.NSDuToan).ToListAsync();
            }
            else
            {
                var objTinh_NhanVien = _context.TinhNhanVien.Where(x => x.NhanVienID == nhaVienID).Select(x => x.TinhID).ToList();
                if (objTinh_NhanVien.Count > 0)
                {
                    lstCoQuan = await _context.NSCoQuan.Where(x => objTinh_NhanVien.Contains((int)x.TinhID)).Include(x => x.NhanVien).Include(x => x.Huyen).Include(x => x.NSCanBo).Include(x => x.TuongTac).Include(x => x.NSDuToan).ToListAsync();
                }
            }
            if (lstCoQuan.Count() > 0)
            {
                var lstTinh = await _context.DMTinh.ToListAsync();
                var lstDuToan = await _context.NSDuToan.Include(x => x.NSCoQuan).Include(x => x.BuocThiTruong).Include(x => x.NhanVien).ToListAsync();
                var lstTuongTac = await _context.QuanLyTuongTac.Include(x => x.NSCoQuan).Include(x => x.BuocThiTruong).Include(x => x.NhanVien).ToListAsync();
                foreach (var item in lstCoQuan)
                {                   
                    if (item.NSDuToan != null && item.NSDuToan.Count() > 0)
                    {
                        var objDT = item.NSDuToan.OrderByDescending(x => x.DuToanID).Select(x => x.DuToanID).Take(1).FirstOrDefault();
                        //var obj =  _context.NSDuToan.Where(x => x.DuToanID == objCH).Include(x => x.NSCoQuan).Include(x => x.BuocThiTruong).Include(x => x.NhanVien).ToList();
                        var objDuToan = lstDuToan.Where(x => x.DuToanID == objDT).ToList();
                        item.NSDuToan = objDuToan;
                    }
                    if (item.TuongTac != null && item.TuongTac.Count() > 0)
                    {
                        var objTT = item.TuongTac.OrderByDescending(x => x.TuongTacID).Select(x => x.TuongTacID).Take(1).FirstOrDefault();
                        //var obj = await _context.QuanLyTuongTac.Where(x => x.TuongTacID == objCH).Include(x => x.NSCoQuan).Include(x => x.BuocThiTruong).Include(x => x.NhanVien).ToListAsync();
                        var objTuongTac = lstTuongTac.Where(x => x.TuongTacID == objTT).ToList();
                        item.TuongTac = objTuongTac;
                    }

                    // Add Model tỉnh 
                   var objCoQuanView = _mapper.Map<NSCoQuanViewModel>(item);
                    if(objCoQuanView.TinhID != null)
                    {
                        var objTinh = lstTinh.Where(x=>x.TinhID == objCoQuanView.TinhID).FirstOrDefault();
                        objCoQuanView.Tinh = _mapper.Map<DMTinhDto>(objTinh);
                    }
                    lstCoQuanView.Add(objCoQuanView);
                }
                //lstCoQuanView = _mapper.Map<List<NSCoQuanViewModel>>(lstCoQuan);

            }
            return lstCoQuanView;
        }

        public async Task<IEnumerable<NSCoQuanBaoCao>> GetBaoCao(int nhaVienID)
        {
         
            List<NSCoQuanBaoCao> lstCoQuanView = new List<NSCoQuanBaoCao>();
            List<NSCoQuan> lstCoQuan = new List<NSCoQuan>();
            if (nhaVienID == 0)
            {
                lstCoQuan = await _context.NSCoQuan.Include(x => x.NhanVien).Include(x => x.Huyen).Include(x => x.NSCanBo).Include(x => x.TuongTac).Include(x => x.NSDuToan).Where(x=> (x.TuongTac !=null && x.TuongTac.Count() > 0) ||( x.NSDuToan !=null && x.NSDuToan.Count() > 0)).ToListAsync();
            }
            else
            {
                var objTinh_NhanVien = _context.TinhNhanVien.Where(x => x.NhanVienID == nhaVienID).Select(x => x.TinhID).ToList();
                if (objTinh_NhanVien.Count > 0)
                {
                    lstCoQuan = await _context.NSCoQuan.Where(x => objTinh_NhanVien.Contains((int)x.TinhID)).Include(x => x.NhanVien).Include(x => x.Huyen).Include(x => x.NSCanBo).Include(x => x.TuongTac).Include(x => x.NSDuToan).Where(x => (x.TuongTac != null && x.TuongTac.Count() > 0) || (x.NSDuToan != null && x.NSDuToan.Count() > 0)).ToListAsync();
                }
            }
            if (lstCoQuan.Count() > 0)
            {             
                var lstDuToan = await _context.NSDuToan.Include(x => x.NSCoQuan).Include(x => x.BuocThiTruong).Include(x => x.NhanVien).ToListAsync();
                var lstTuongTac = await _context.QuanLyTuongTac.Include(x => x.NSCoQuan).Include(x => x.BuocThiTruong).Include(x => x.NhanVien).ToListAsync();
                var objChuVu = _context.ChucVu.Include(x => x.PhongBan).Include(x => x.PhongBan.CongTy).ToList();
                var objNhanVien = _context.NhanVien.ToList();
                var objChucVuNhanVien = _context.ChucVuNhanVien.ToList();
                foreach (var item in lstCoQuan)
                {
                    NSCoQuanBaoCao baocao = new NSCoQuanBaoCao();
                    baocao.TenCoQuan = item.TenCoQuan;
                    baocao.CoQuanID = item.CoQuanID;

                    if (item.NSDuToan != null && item.NSDuToan.Count() > 0)
                    {
                        var objDT = item.NSDuToan.OrderByDescending(x => x.DuToanID).Select(x => x.DuToanID).Take(1).FirstOrDefault();                     
                        var objDuToan = lstDuToan.Where(x => x.DuToanID == objDT).FirstOrDefault();
                        if (objDuToan != null && objDuToan.NhanVien !=null && objDuToan.NhanVien.Active ==true)
                        {
                            baocao.ThoiGian = objDuToan.ThoiGian;
                            baocao.DoanhThuDuKien = objDuToan.DoanhThuDuKien;
                            baocao.DoanhThuThucTe = objDuToan.DoanhThuThucTe;
                            baocao.BuocThiTruong = _mapper.Map<BuocThiTruongDto>(objDuToan.BuocThiTruong);
                            baocao.NhanVien = GetInfo(objDuToan.NhanVienID, objNhanVien, objChuVu, objChucVuNhanVien);
                            baocao.ThoiGianKetThucDuKien = objDuToan.ThoiGianKetThucDuKien;
                            lstCoQuanView.Add(baocao);
                        }
                    }
                    else
                    {
                        if (item.TuongTac != null && item.TuongTac.Count() > 0)
                        {
                            var objTT = item.TuongTac.OrderByDescending(x => x.TuongTacID).Select(x => x.TuongTacID).Take(1).FirstOrDefault();

                            var objTuongTac = lstTuongTac.Where(x => x.TuongTacID == objTT).FirstOrDefault();
                            if (objTuongTac != null && objTuongTac.NhanVien != null && objTuongTac.NhanVien.Active == true)
                            {
                                baocao.ThoiGian = objTuongTac.ThoiGian;
                                baocao.DoanhThuDuKien = objTuongTac.DoanhThuDuKien;
                                baocao.DoanhThuThucTe = null;
                                baocao.BuocThiTruong = _mapper.Map<BuocThiTruongDto>(objTuongTac.BuocThiTruong);
                                baocao.NhanVien = GetInfo(objTuongTac.NhanVienID, objNhanVien, objChuVu, objChucVuNhanVien);
                                baocao.ThoiGianKetThucDuKien = objTuongTac.ThoiGianKetThucDuKien;
                                lstCoQuanView.Add(baocao);
                            }
                        }
                    }                                                
                }             
            }
            return lstCoQuanView;
        }
        public NhanVienInfo GetInfo(int ID,List<NhanVien> lstNhanVien,List<ChucVu> lstChucVu , List<ChucVuNhanVien> lstCVNV)
        {
            var objNhaVien =  lstNhanVien.Where(x => x.NhanVienID == ID).Select(x => new NhanVienInfo
            {
                NhanVienID = x.NhanVienID,
                TenNhanVien = x.TenNhanVien,               
            }).FirstOrDefault();
            if (objNhaVien != null)
            {
                var objChuVuNhanVien = lstCVNV.Where(x => x.NhanVienID == ID).ToList();
                if (objChuVuNhanVien.Count > 0)
                {
                    foreach (var it in objChuVuNhanVien)
                    {

                        var objChuVu = lstChucVu.Where(x => x.ChucVuID == it.ChucVuID).FirstOrDefault();
                        if (objChuVu != null)
                        {
                            ChucVuView objchucVuView = new ChucVuView();
                            objchucVuView.lstChucVu = _mapper.Map<ChucVuDto>(objChuVu);
                            if (objChuVu.PhongBan != null)
                            {
                                objchucVuView.lstPhongBan = _mapper.Map<PhongBanDto>(objChuVu.PhongBan);
                                if (objChuVu.PhongBan.CongTy != null)
                                {
                                    objchucVuView.lstCongTy = _mapper.Map<CongTyDto>(objChuVu.PhongBan.CongTy);
                                }
                            }
                            objNhaVien.lstChucVuView.Add(objchucVuView);
                        }
                    }
                }
                return objNhaVien;
            }
            else
            {
                return null;
            }
        }
    }
}

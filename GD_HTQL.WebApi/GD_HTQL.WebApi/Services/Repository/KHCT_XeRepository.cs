﻿using AutoMapper;
using GD_HTQL.WebApi.Data.Models;
using GD_HTQL.WebApi.Data.Models.EF.CongTacModels;
using GD_HTQL.WebApi.Infrastructure;
using GD_HTQL.WebApi.Services.IRepository;

namespace GD_HTQL.WebApi.Services.Repository
{
    public class KHCT_XeRepository : GenericRepository<KHCT_Xe>, IKHCT_XeRepository
    {
        public KHCT_XeRepository(ApplicationDbContext context, IMapper mapper) : base(context, mapper)
        {
        }
    }
}

﻿using AutoMapper;
using GD_HTQL.WebApi.Common;
using GD_HTQL.WebApi.Data.Models;
using GD_HTQL.WebApi.Data.Models.Dtos.NhanVien;
using GD_HTQL.WebApi.Data.Models.EF.DuAnModels;
using GD_HTQL.WebApi.Data.Models.ViewModels;
using GD_HTQL.WebApi.Infrastructure;
using GD_HTQL.WebApi.Services.IRepository;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace GD_HTQL.WebApi.Services.Repository
{
    public class QuanLyTuongTacRepository : GenericRepository<QuanLyTuongTac>, IQuanLyTuongTacRepository
    {  
        public QuanLyTuongTacRepository(ApplicationDbContext context, IMapper mapper) :base(context, mapper) 
        {          
        }
        public async Task<IEnumerable<TuongTacViewModel>> GetQuanLyTuongTac()
        {
             var obj = await _context.QuanLyTuongTac.Where(x => x.Active == true).Include(x=>x.NSCoQuan).Include(x=>x.NSCoQuan).Include(x=>x.NSNguonVon).Select(x => new TuongTacViewModel
             {
                 TuongTacID = x.TuongTacID,
                 TenCoQuan = x.NSCoQuan == null ? "": x.NSCoQuan.TenCoQuan,
                 TenTinh = x.NSCoQuan == null ? "" : x.NSCoQuan.Huyen == null ?"" : x.NSCoQuan.Huyen.DMTinh.TenTinh,
                 BuocThiTruong = x.BuocThiTruong ==null ?"": x.BuocThiTruong.BuocThiTruongTen,
                 NhomHangQuanTam = x.NhomHangQuanTam,
                 ThongTinTiepXuc = x.ThongTinTiepXuc,
                 ThongTinLienHe = x.ThongTinLienHe,
                 ThoiGian = x.ThoiGian,
                 CanBoTiepXuc = x.CanBoTiepXuc,
                 NhanVienID = x.NhanVienID,
                 TenNhanVien = x.NhanVien == null ? "" : x.NhanVien.TenNhanVien,
                 DoanhThuDuKien = x.DoanhThuDuKien == null ? 0 : Convert.ToDecimal(x.DoanhThuDuKien),
                 GhiChu = x.GhiChu,
                 NguonVonID = x.NguonVonID,
                 TenNguonVon = x.NSNguonVon == null ?"": x.NSNguonVon.TenNguonVon,
                 ThoiGianKetThucDuKien = x.ThoiGianKetThucDuKien
             }).ToListAsync();
            return obj;
        }

        public async Task<IEnumerable<TuongTacDetails>> GetDetails()
        {
            List<TuongTacDetails> tuongTacDetails = new List<TuongTacDetails>();

            var obj = await _context.QuanLyTuongTac.Include(x => x.BuocThiTruong).Include(x => x.NhanVien).Include(x => x.NSCoQuan).Include(x => x.NSNguonVon).ToListAsync();
            tuongTacDetails = _mapper.Map<List<TuongTacDetails>>(obj);
            foreach ( var item in tuongTacDetails)
            {
                var objChuVuNhanVien = _context.ChucVuNhanVien.Where(x => x.NhanVienID == item.NhanVienID).ToList();
                if (objChuVuNhanVien.Count > 0)
                {
                    foreach (var it in objChuVuNhanVien)
                    {

                        var objChuVu = _context.ChucVu.Where(x => x.ChucVuID == it.ChucVuID).Include(x => x.PhongBan).Include(x => x.PhongBan.CongTy).FirstOrDefault();
                        if (objChuVu != null)
                        {
                            ChucVuView objchucVuView = new ChucVuView();
                            objchucVuView.lstChucVu = _mapper.Map<ChucVuDto>(objChuVu);
                            if (objChuVu.PhongBan != null)
                            {
                                objchucVuView.lstPhongBan = _mapper.Map<PhongBanDto>(objChuVu.PhongBan);
                                if (objChuVu.PhongBan.CongTy != null)
                                {
                                    objchucVuView.lstCongTy = _mapper.Map<CongTyDto>(objChuVu.PhongBan.CongTy);
                                }
                            }
                            item.lstChucVuView.Add(objchucVuView);
                        }
                    }
                }
            }
           
            return tuongTacDetails;
        }

        public async Task<QuanLyTuongTac> GetQuanLyTuongTacByID(int ID)
        {
            return await _context.QuanLyTuongTac.FindAsync(ID);
        }
        public async Task<IEnumerable<TuongTacViewModel>> GetQuanLyTuongTacByTuKhoa(string? Key , int tinhID)
        {
             IEnumerable<TuongTacViewModel> objQuanLyTuongTac = await _context.QuanLyTuongTac.Include(x=>x.NSCoQuan).Where(x => (tinhID == 0 || x.NSCoQuan.TinhID == tinhID) && x.Active == true).Select(x => new TuongTacViewModel
             {
                 TuongTacID = x.TuongTacID,
                 TenCoQuan = x.NSCoQuan == null ? "" : x.NSCoQuan.TenCoQuan,
                 TenTinh = x.NSCoQuan == null ? "" : x.NSCoQuan.Huyen == null ? "" : x.NSCoQuan.Huyen.DMTinh.TenTinh,
                 BuocThiTruong = x.BuocThiTruong == null ? "" : x.BuocThiTruong.BuocThiTruongTen,
                 NhomHangQuanTam = x.NhomHangQuanTam,
                 ThongTinTiepXuc = x.ThongTinTiepXuc,
                 ThongTinLienHe = x.ThongTinLienHe,
                 ThoiGian = x.ThoiGian,
                 CanBoTiepXuc = x.CanBoTiepXuc,
                 NhanVienID = x.NhanVienID,
                 TenNhanVien = x.NhanVien == null ? "" : x.NhanVien.TenNhanVien,
                 DoanhThuDuKien = x.DoanhThuDuKien == null ? 0 : Convert.ToDecimal(x.DoanhThuDuKien),
                 GhiChu = x.GhiChu,
                 NguonVonID = x.NguonVonID,
                 TenNguonVon = x.NSNguonVon == null ? "" : x.NSNguonVon.TenNguonVon,
                 ThoiGianKetThucDuKien = x.ThoiGianKetThucDuKien
             })
                .OrderByDescending(x => x.TuongTacID).ToListAsync();
            if (!string.IsNullOrEmpty(Key))
            {
                objQuanLyTuongTac = objQuanLyTuongTac.AsQueryable().Where(ExpressionTuongTacTheoTuKhoa(Key)).OrderByDescending(x => x.TuongTacID).ToList();
            }
            return objQuanLyTuongTac;
        }

        public async Task<IEnumerable<BuocThiTruong>> GetBuocThiTruong()
        {
            return await _context.BuocThiTruong.Take(4).ToListAsync();
        }
        public async Task<IEnumerable<NSNguonVon>> GetNguonVon()
        {
            return await _context.NSNguonVon.ToListAsync();
        }

        public Expression<Func<TuongTacViewModel, bool>> ExpressionTuongTacTheoTuKhoa(string _key)
        {
            _key = ClassCommon.RemoveUnicode(_key).ToLower();
            return x => ClassCommon.RemoveUnicode(x.TenCoQuan).ToLower().Contains(_key) || ClassCommon.RemoveUnicode(x.TenTinh).ToLower().Contains(_key) || ClassCommon.RemoveUnicode(x.TenNhanVien).ToLower().Contains(_key);
        }
    }
}

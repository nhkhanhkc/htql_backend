﻿using AutoMapper;
using GD_HTQL.WebApi.Data.Models;
using GD_HTQL.WebApi.Data.Models.Dtos;
using GD_HTQL.WebApi.Data.Models.EF.VanBanModels;
using GD_HTQL.WebApi.Infrastructure;
using GD_HTQL.WebApi.Services.IRepository;


namespace GD_HTQL.WebApi.Repository
{
    public class LoaiVanBanRepository : GenericRepository<LoaiVanBan>,ILoaiVanBanRepository
    {      
        public LoaiVanBanRepository(ApplicationDbContext context, IMapper mapper) :base(context, mapper) 
        {           
        }       
    }
}

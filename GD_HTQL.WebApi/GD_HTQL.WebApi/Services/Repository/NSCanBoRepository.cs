﻿using AutoMapper;
using GD_HTQL.WebApi.Common;
using GD_HTQL.WebApi.Data.Models;
using GD_HTQL.WebApi.Data.Models.EF.DuAnModels;
using GD_HTQL.WebApi.Infrastructure;
using GD_HTQL.WebApi.Services.IRepository;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace GD_HTQL.WebApi.Services.Repository
{
    public class NSCanBoRepository : GenericRepository<NSCanBo> , INSCanBoRepository
    {       
        public NSCanBoRepository(ApplicationDbContext context, IMapper mapper) : base(context, mapper) 
        {                 
        }
       
        public async Task<NSCanBo> GetNSCanBoByID(int ID)
        {
            return await _context.NSCanBo.FindAsync(ID);
        }
        public async Task<IEnumerable<NSCanBo>> GetNSCanBoByTuKhoa(string? _key ,int _coQuanID)
        {
            IEnumerable<NSCanBo> objNSCanBo = await _context.NSCanBo.Where(x=> (_coQuanID ==0 || _coQuanID == x.CoQuanID) && x.Active ==true).OrderByDescending(x => x.CanBoID).ToListAsync();
            if (!string.IsNullOrEmpty(_key))
            {
                objNSCanBo = objNSCanBo.AsQueryable().Where(ExpressionCanBoTheoTuKhoa(_key)).OrderByDescending(x => x.CanBoID).ToList();
            }
              
            return objNSCanBo;
        }

        public async Task<IEnumerable<NSCanBo>> GetNSCanBoByCoQuan(int _coQuanID)
        {
            IEnumerable<NSCanBo> objNSCanBo = await _context.NSCanBo.Where(x=>x.CoQuanID ==_coQuanID && x.Active ==true).ToListAsync();           
            return objNSCanBo;
        }     
      
        public Expression<Func<NSCanBo, bool>> ExpressionCanBoTheoTuKhoa(string _key)
        {
            _key = ClassCommon.RemoveUnicode(_key).ToLower();
            return x => ClassCommon.RemoveUnicode(x.HoVaTen).ToLower().Contains(_key);
        }
    }
}

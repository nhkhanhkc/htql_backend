﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using GD_HTQL.WebApi.Services.IRepository;
using GD_HTQL.WebApi.Data.Models;
using GD_HTQL.WebApi.Data.Models.Dtos;
using GD_HTQL.WebApi.Infrastructure;
using GD_HTQL.WebApi.Data.Models.EF.SanPhamModels;

namespace GD_HTQL.WebApi.Services.Repository
{
    public class KhoiLopRepository : GenericRepository<KhoiLop>,IKhoiLopRepository
    {     
        public KhoiLopRepository(ApplicationDbContext context, IMapper mapper) :base(context, mapper) 
        {       
        }     
    }
}

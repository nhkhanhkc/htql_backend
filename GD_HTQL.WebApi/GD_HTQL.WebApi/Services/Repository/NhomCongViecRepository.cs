﻿using AutoMapper;
using GD_HTQL.WebApi.Data.Models;
using GD_HTQL.WebApi.Data.Models.EF.CongViecModels;
using GD_HTQL.WebApi.Infrastructure;
using GD_HTQL.WebApi.Services.IRepository;

namespace GD_HTQL.WebApi.Services.Repository
{
    public class NhomCongViecRepository : GenericRepository<NhomCongViec>,INhomCongViecRepository
    {
        public NhomCongViecRepository(ApplicationDbContext context, IMapper mapper) :base(context, mapper) 
        {
        }       
    }
}

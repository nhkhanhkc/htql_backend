﻿using AutoMapper;
using GD_HTQL.WebApi.Common;
using GD_HTQL.WebApi.Data.Models;
using GD_HTQL.WebApi.Data.Models.Dtos.CongTac;
using GD_HTQL.WebApi.Data.Models.EF.CongTacModels;
using GD_HTQL.WebApi.Data.Models.ViewModels;
using GD_HTQL.WebApi.Infrastructure;
using GD_HTQL.WebApi.Services.IRepository;
using Microsoft.EntityFrameworkCore;

namespace GD_HTQL.WebApi.Services.Repository
{
    public class KHCT_NgayRepository : GenericRepository<KHCT_Ngay>, IKHCT_NgayRepository
    {
        public KHCT_NgayRepository(ApplicationDbContext context, IMapper mapper) : base(context, mapper)
        {
        }
        public async Task AddLichSu(KHCT_Ngay_LichSu objLichSu)
        {
            _context.KHCT_Ngay_LichSu.Add(objLichSu);
            await _context.SaveChangesAsync();
        }
        public async Task<ICollection<KHCT_Ngay_ShortView>> LstKHCT_NgayView(int nhanVienID, bool week, bool month)
        {
            int _currentWeek = ClassCommon.GetNumberWeek(DateTime.Now);
            DateTime _dateStartWeek = ClassCommon.GetDateStartWeek(_currentWeek, DateTime.Now.Year);
            DateTime _dateEndWeek = ClassCommon.GetDateEndWeek(_currentWeek, DateTime.Now.Year);
            DateTime _dateStartmonth = ClassCommon.GetDateStartMonth(DateTime.Now.Month, DateTime.Now.Year);
            DateTime _dateEndMonth = ClassCommon.GetDateEndMonth(DateTime.Now.Month, DateTime.Now.Year);

            var lstNgay = await _context.KHCT_Ngay.Where(x => (nhanVienID == 0 || nhanVienID == x.NguoiTaoID)                                                          
                                                            && (week == false || (x.TuNgay.Date >= _dateStartWeek.Date && x.TuNgay.Date <= _dateEndWeek.Date))
                                                            && (month == false || (x.TuNgay.Date >= _dateStartmonth.Date && x.TuNgay.Date <= _dateEndMonth.Date))
                                                            ).Include(x=>x.KHCT_Tuan).OrderByDescending(x => x.NgayID).ToListAsync();
            List<KHCT_Ngay_ShortView> lstView = new List<KHCT_Ngay_ShortView>();
            foreach (var item in lstNgay)
            {
                KHCT_Ngay_ShortView objView = new KHCT_Ngay_ShortView();
                objView = _mapper.Map<KHCT_Ngay_ShortView>(item);
                //var obj = await _context.KHCT_Ngay_LichSu.Where(x => x.NgayID == item.NgayID).Include(x => x.KHCT_Ngay_TrangThai).Include(x => x.NhanVien).OrderByDescending(x => x.ThoiGan).FirstOrDefaultAsync();
                var obj = await _context.KHCT_Ngay_LichSu.Where(x => x.NgayID == item.NgayID).Include(x => x.KHCT_Ngay_TrangThai).Include(x => x.NhanVien).OrderByDescending(x => x.ThoiGan).ToListAsync();
                if (obj.Count() > 0)
                {
                    objView.KHCT_Ngay_LichSu = _mapper.Map<List<KHCT_Ngay_LichSuDto>>(obj);
                }
                lstView.Add(objView);              
            }
            return lstView;          
        }
        public async Task<KHCT_Ngay_ViewModel> GetDetailsByID(int ngayID)
        {
            var lstNgay = await _context.KHCT_Ngay.Where(x => x.NgayID ==ngayID ).Include(x => x.KHCT_Tuan).Include(x=>x.KHCT_Xe).Include(x=>x.KHCT_ChiPhi).Include(x => x.KHCT_NoiDung).Include(x => x.KHCT_NguoiDiCung).Include(x=>x.KHCT_ChiPhiKhac).FirstOrDefaultAsync();
            //if (lstNgay != null && lstNgay.KHCT_ChiPhi !=null && lstNgay.KHCT_ChiPhi.Count >0)
            //{
            //    var objChiPhi = _context.KHCT_ChiPhi.Where(x => x.NgayID == lstNgay.NgayID).Include(x => x.KHCT_LoaiChiPhi).Include(x => x.KHCT_LoaiChiPhi).ToList();
            //    lstNgay.KHCT_ChiPhi = objChiPhi;
            //}
         
            KHCT_Ngay_ViewModel objView = new KHCT_Ngay_ViewModel();
            if (lstNgay !=null)
            {
                var objChiPhi = _context.KHCT_ChiPhi.Where(x => x.NgayID == lstNgay.NgayID).Include(x => x.KHCT_LoaiChiPhi).Include(x => x.KHCT_HangMucChiPhi).ToList();
                lstNgay.KHCT_ChiPhi = objChiPhi;

                //var obj = _context.KHCT_Ngay_LichSu.Where(x => x.NgayID == lstNgay.NgayID).Include(x => x.KHCT_Ngay_TrangThai).Include(x => x.NhanVien).OrderByDescending(x => x.ThoiGan).FirstOrDefault();
                var obj = _context.KHCT_Ngay_LichSu.Where(x => x.NgayID == lstNgay.NgayID).Include(x => x.KHCT_Ngay_TrangThai).Include(x => x.NhanVien).OrderByDescending(x => x.ThoiGan).ToList();
                objView = _mapper.Map<KHCT_Ngay_ViewModel>(lstNgay);
                if (obj.Count() > 0 )
                {
                    objView.KHCT_Ngay_LichSu = _mapper.Map<List<KHCT_Ngay_LichSuDto>>(obj);
                }           
            }                             
            return objView;
        }
        public async Task<List<KHCT_Ngay_TrangThai>> GetTrangThai()
        {
            var obj = await _context.KHCT_Ngay_TrangThai.ToListAsync();
            return obj;
        }    
    }
}

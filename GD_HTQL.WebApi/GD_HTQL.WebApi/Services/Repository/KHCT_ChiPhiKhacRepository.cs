﻿using AutoMapper;
using GD_HTQL.WebApi.Data.Models;
using GD_HTQL.WebApi.Data.Models.EF.CongTacModels;
using GD_HTQL.WebApi.Infrastructure;
using GD_HTQL.WebApi.Services.IRepository;

namespace GD_HTQL.WebApi.Services.Repository
{
    public class KHCT_ChiPhiKhacRepository : GenericRepository<KHCT_ChiPhiKhac> , IKHCT_ChiPhiKhacRepository
    {
        public KHCT_ChiPhiKhacRepository(ApplicationDbContext context, IMapper mapper) : base(context, mapper)
        {
        }
    }
}

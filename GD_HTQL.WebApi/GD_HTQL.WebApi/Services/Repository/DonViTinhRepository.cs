﻿using AutoMapper;
using GD_HTQL.WebApi.Data.Models;
using GD_HTQL.WebApi.Data.Models.EF.SanPhamModels;
using GD_HTQL.WebApi.Infrastructure;
using GD_HTQL.WebApi.Services.IRepository;

namespace GD_HTQL.WebApi.Services.Repository
{
    public class DonViTinhRepository : GenericRepository<DonViTinh>, IDonViTinhRepository
    {
        public DonViTinhRepository(ApplicationDbContext context, IMapper mapper) : base(context, mapper)
        {
        }
    }
}

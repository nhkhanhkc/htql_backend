﻿using AutoMapper;
using GD_HTQL.WebApi.Data.Models;
using GD_HTQL.WebApi.Data.Models.Dtos.NhanVien;
using GD_HTQL.WebApi.Data.Models.EF.NhaThauModels;
using GD_HTQL.WebApi.Data.Models.ViewModels;
using GD_HTQL.WebApi.Infrastructure;
using GD_HTQL.WebApi.Services.IRepository;
using Microsoft.EntityFrameworkCore;

namespace GD_HTQL.WebApi.Services.Repository
{
    public class NT_GiaTriCoHoiRepository : GenericRepository<NT_GiaTriCoHoi>, INT_GiaTriCoHoiRepository
    {
        public NT_GiaTriCoHoiRepository(ApplicationDbContext context, IMapper mapper) : base(context, mapper)
        {
        }
        public async Task<IEnumerable<GiaTriCoHoiViewModel>> GetDetails(int nhaThauID, int gtch_ID)
        {
            List<GiaTriCoHoiViewModel> giaTriCoHoiViews = new List<GiaTriCoHoiViewModel>();

            var obj = await _context.NT_GiaTriCoHoi.Where(x=> (nhaThauID ==0 || x.NhaThauID == nhaThauID) && (gtch_ID == 0 || x.GTCHID == gtch_ID)
                                                         ).Include(x => x.LoaiSanPham).Include(x => x.NhaThau).Include(x => x.NhanVien).Include(x => x.NguonVon).Include(x => x.NT_The).ToListAsync();



            giaTriCoHoiViews = _mapper.Map<List<GiaTriCoHoiViewModel>>(obj);
            foreach (var item in giaTriCoHoiViews)
            {
                var objChuVuNhanVien = _context.ChucVuNhanVien.Where(x => x.NhanVienID == item.NhanVienID).ToList();
                if (objChuVuNhanVien.Count > 0)
                {
                    foreach (var it in objChuVuNhanVien)
                    {

                        var objChuVu = _context.ChucVu.Where(x => x.ChucVuID == it.ChucVuID).Include(x => x.PhongBan).Include(x => x.PhongBan.CongTy).FirstOrDefault();
                        if (objChuVu != null)
                        {
                            ChucVuView objchucVuView = new ChucVuView();
                            objchucVuView.lstChucVu = _mapper.Map<ChucVuDto>(objChuVu);
                            if (objChuVu.PhongBan != null)
                            {
                                objchucVuView.lstPhongBan = _mapper.Map<PhongBanDto>(objChuVu.PhongBan);
                                if (objChuVu.PhongBan.CongTy != null)
                                {
                                    objchucVuView.lstCongTy = _mapper.Map<CongTyDto>(objChuVu.PhongBan.CongTy);
                                }
                            }
                            item.lstChucVuView.Add(objchucVuView);

                        }
                    }
                }
            }
            return giaTriCoHoiViews;
        }

        public async Task ThemLoaiSanPham(List<int> loaiSanPham, int gtchID)
        {
            List<NT_LoaiSanPham> lstLoaiSanPham = new List<NT_LoaiSanPham>();
            foreach (var it in loaiSanPham)
            {
                NT_LoaiSanPham loaisp = new NT_LoaiSanPham();
                loaisp.LoaiSanPhamID = it;
                loaisp.GTCHID = gtchID;
                lstLoaiSanPham.Add(loaisp);
            }
            _context.NT_LoaiSanPham.AddRange(lstLoaiSanPham);
            await _context.SaveChangesAsync();
        }
        public void XoaLoaiSanPham(int gtchID)
        {
            var objLoaiSanPham = _context.NT_LoaiSanPham.Where(x => x.GTCHID == gtchID).ToList();
            if (objLoaiSanPham != null)
            {
                _context.NT_LoaiSanPham.RemoveRange(objLoaiSanPham);
                _context.SaveChanges();
            }
        }
        public async Task<List<NhanVienShortView>> GetNhanVien()
        {
            List<NhanVienShortView> lstNhanVien = new List<NhanVienShortView>();
            var objNhanVien = await _context.NT_GiaTriCoHoi.Include(x=> x.NhanVien).Select(x=>x.NhanVien).Distinct().ToListAsync();
            if (objNhanVien.Count > 0)
            {
                lstNhanVien = _mapper.Map<List<NhanVienShortView>>(objNhanVien);
            }
            return lstNhanVien;
        }
    }
}

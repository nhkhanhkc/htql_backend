﻿using AutoMapper;
using GD_HTQL.WebApi.Data.Models;
using GD_HTQL.WebApi.Data.Models.EF.VanBanModels;
using GD_HTQL.WebApi.Infrastructure;
using GD_HTQL.WebApi.Services.IRepository;


namespace GD_HTQL.WebApi.Services.Repository
{
    public class VanBanRepository : GenericRepository<VanBan>,IVanBanRepository
    {      
        public VanBanRepository(ApplicationDbContext context, IMapper mapper) :base(context, mapper) 
        {
        }        
    }
}

﻿using AutoMapper;
using GD_HTQL.WebApi.Common;
using GD_HTQL.WebApi.Data.Models;
using GD_HTQL.WebApi.Data.Models.EF.NhaThauModels;
using GD_HTQL.WebApi.Data.Models.ViewModels;
using GD_HTQL.WebApi.Infrastructure;
using GD_HTQL.WebApi.Services.IRepository;
using Microsoft.EntityFrameworkCore;

namespace GD_HTQL.WebApi.Services.Repository
{
    public class NT_DuToanRepository : GenericRepository<NT_DuToan>, INT_DuToanRepository
    {
        public NT_DuToanRepository(ApplicationDbContext context, IMapper mapper) : base(context, mapper)
        {
        }
        public async Task<string> SaveFile(string fileBase64,string fileName)
        {
            string _Url = "";
            try
            {
                int index = fileBase64.IndexOf("base64,");
                if (index != -1)
                {
                    fileBase64 = fileBase64.Remove(0, index);
                }
                byte[] imageBytes = Convert.FromBase64String(fileBase64.Replace("base64,",string.Empty));

                using (MemoryStream stream = new MemoryStream(imageBytes))
                {
                    var _urlSaveFile = new DirectoryInfo(Path.Combine(Directory.GetCurrentDirectory() + "\\Resources\\Uploads\\NTDuToan\\"));
                    if (!_urlSaveFile.Exists)
                    {
                        _urlSaveFile.Create();
                    }
                    var content = new System.IO.MemoryStream(stream.ToArray());
                    string CustomName = ClassCommon.RandomString(19) + "_" + fileName;
                   
                    _Url = "/Resources/Uploads/NTDuToan/" + CustomName;
                    await CopyStream(content, _urlSaveFile + CustomName);
                }
                return _Url;
            }
            catch (Exception)
            {             
                return _Url;
            }
        }
        public async Task<List<NT_TrangThaiDuToan>> GetTrangThai()
        {
            var obj = await _context.NT_TrangThaiDuToan.ToListAsync();
            return obj;
        }

        public async Task<List<NT_DuToanView>> GetDanhSach(int nhaThauID)
        {
            List<NT_DuToanView> lstDuToanViews = new List<NT_DuToanView>();
            var objDuToan = await _context.NT_DuToan.Where(x=> nhaThauID ==0 || x.NhaThauID ==nhaThauID).Include(x=>x.NhaThau).Include(x=>x.Tinh).Include(x=>x.NT_TrangThaiDuToan).Include(x=>x.NhanVien).ToListAsync();
            if (objDuToan.Count() > 0)
            {
                lstDuToanViews = _mapper.Map<List<NT_DuToanView>>(objDuToan);
            }
           
            return lstDuToanViews;
        }
        public async Task CopyStream(Stream stream, string downloadPath)
        {
            using (var fileStream = new FileStream(downloadPath, FileMode.Create, FileAccess.Write))
            {
                await stream.CopyToAsync(fileStream);
            }
        }
        public void DeleteFile(string path)
        {
            string _path = Path.Combine(Directory.GetCurrentDirectory(), path);
            if (File.Exists(_path))
            {
                File.Delete(_path);
            }
        }
    }
}

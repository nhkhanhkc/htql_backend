﻿using AutoMapper;
using GD_HTQL.WebApi.Common;
using GD_HTQL.WebApi.Data.Models;
using GD_HTQL.WebApi.Data.Models.EF.SanPhamModels;
using GD_HTQL.WebApi.Data.Models.ViewModels;
using GD_HTQL.WebApi.Infrastructure;
using GD_HTQL.WebApi.Services.IRepository;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace GD_HTQL.WebApi.Services.Repository
{
    public class DuToanRepository : GenericRepository<DuToan>, IDuToanRepository
    {
        public DuToanRepository(ApplicationDbContext context, IMapper mapper) : base(context, mapper)
        {
        }
        public async Task<IEnumerable<DuToan>> GetDuToanByTuKhoa(string? Key)
        {
            var objDuToan = await _context.DuToan.OrderByDescending(x => x.DuToanID).ToListAsync();
            if (!string.IsNullOrEmpty(Key))
            {
                objDuToan = objDuToan.AsQueryable().Where(ExpressionDuToanTheoTuKhoa(Key)).OrderByDescending(x => x.DuToanID).ToList();
            }
            return objDuToan;
        }
        public async Task<IEnumerable<DuToanViewModel>> GetDanhSach()
        {
            var objDuToan = await _context.DuToan.OrderByDescending(x => x.DuToanID).ToListAsync();
            List<DuToanViewModel> lstDuToanView = new List<DuToanViewModel>();
            foreach (var item in objDuToan)
            {
                var obj = _context.LichSuDuToan.Where(x => x.DuToanID == item.DuToanID).Include(x=>x.TrangThaiDuToan).Include(x=>x.NhanVien).OrderByDescending(x=> x.ThoiGan).FirstOrDefault();
                if (obj != null)
                {
                    var duToanView = new DuToanViewModel()
                    {
                        DuToanID = item.DuToanID,
                        TenDuToan = item.TenDuToan,
                        GhiChu = item.GhiChu,
                        SanPhamDuToan = item.SanPhamDuToan,
                        LoaiGia = item.LoaiGia,
                        TongGiaBan = item.TongGiaBan,
                        TongGiaVon = item.TongGiaVon,
                        NguoiTaoID = item.NhanVienID,
                        TenNguoiTao = item.CreateBy,
                        TrangThaiID = obj.TrangThaiDuToanID,
                        TenTrangThai = obj.TrangThaiDuToan == null ? "" : obj.TrangThaiDuToan.TenTrangThai,
                        NguoiDuyetID = obj.NhanVienID,
                        TenNguoiDuyet = obj.NhanVien == null ? "" : obj.NhanVien.TenNhanVien
                    };
                    lstDuToanView.Add(duToanView);
                }
            }                      
            return lstDuToanView;
        }
        public async Task CapNhatTrangThai(int _NhanVienID, int _TrangThaiID, int _DuToanID)
        {
            LichSuDuToan LichSu = new LichSuDuToan();
            LichSu.DuToanID = _DuToanID;
            LichSu.NhanVienID = _NhanVienID;
            LichSu.ThoiGan = DateTime.Now;
            LichSu.TrangThaiDuToanID = _TrangThaiID;
            await AddLichSuDuToan(LichSu);
        }
        public async Task AddLichSuDuToan(LichSuDuToan objNhanVienDuToan)
        {
            _context.LichSuDuToan.Add(objNhanVienDuToan);
            await _context.SaveChangesAsync();
        }
        public Expression<Func<DuToan, bool>> ExpressionDuToanTheoTuKhoa(string _key)
        {
            _key = ClassCommon.RemoveUnicode(_key).ToLower();
            return x => ClassCommon.RemoveUnicode(x.TenDuToan).ToLower().Contains(_key);
        }
        public async Task DeleteLichSuDuToan(int duToanID)
        {
            var objLichSu = _context.LichSuDuToan.Where(x => x.DuToanID == duToanID).ToList();
            if(objLichSu.Count > 0)
            {
                _context.LichSuDuToan.RemoveRange(objLichSu);
                await _context.SaveChangesAsync();
            }        
        }
        public async Task<IEnumerable<TrangThaiDuToan>> GetDsTrangThai()
        {
            var objTrangThai = await _context.TrangThaiDuToan.ToListAsync();

            return objTrangThai;
        }
    }
 }



﻿using AutoMapper;
using GD_HTQL.WebApi.Common;
using GD_HTQL.WebApi.Data.Models;
using GD_HTQL.WebApi.Data.Models.Dtos.CongViec;
using GD_HTQL.WebApi.Data.Models.EF.CongViecModels;
using GD_HTQL.WebApi.Data.Models.ViewModels;
using GD_HTQL.WebApi.Infrastructure;
using GD_HTQL.WebApi.Services.IRepository;
using Microsoft.EntityFrameworkCore;

namespace GD_HTQL.WebApi.Services.Repository
{
    public class CongViecRepository :GenericRepository<CongViec> ,ICongViecRepository
    {      
        public CongViecRepository(ApplicationDbContext context, IMapper mapper) :base(context,mapper) 
        {         
        }                                   
        public async Task<List<TrangThaiCongViec>> GetTrangThai()
        {
            return await _context.TrangThaiCongViec.ToListAsync();
        }
        public async Task<List<DoUuTien>> GetDoUuTien()
        {
            return await _context.DoUuTien.ToListAsync();
        }
        public async Task<IEnumerable<CongViecViewModel>> GetDanhSach(int nhanviecID, int giaoViecID, bool week , bool month , int nhomID)
        { 
            int _currentWeek = ClassCommon.GetNumberWeek(DateTime.Now);
            DateTime _dateStartWeek = ClassCommon.GetDateStartWeek(_currentWeek, DateTime.Now.Year);
            DateTime _dateEndWeek = ClassCommon.GetDateEndWeek(_currentWeek, DateTime.Now.Year);
            DateTime _dateStartmonth = ClassCommon.GetDateStartMonth(DateTime.Now.Month, DateTime.Now.Year);
            DateTime _dateEndMonth = ClassCommon.GetDateEndMonth(DateTime.Now.Month, DateTime.Now.Year);
            List<CongViecViewModel> lstCongViecView = new List<CongViecViewModel>();
            // Quản lý 
            List<int> lstnhanVienID = new List<int>();
            if (nhanviecID != 0)
            {
                 lstnhanVienID = _context.QuanLyNhanVien.Where(x => x.QuanLyID == nhanviecID).Select(x => x.NhanVienID).ToList();             
            }
            lstnhanVienID.Add(nhanviecID);

            foreach (var it in lstnhanVienID)
            {
                var objCongViec = await _context.CongViec.Where(x => (it == 0 || it == x.NguoiThucHienID)
                                                          && (giaoViecID == 0 || giaoViecID == x.NguoiTaoID)
                                                          && (week == false || (x.NgayBatDau.Date >= _dateStartWeek.Date && x.NgayBatDau.Date <= _dateEndWeek.Date))
                                                          && (month == false || (x.NgayBatDau.Date >= _dateStartmonth.Date && x.NgayBatDau.Date <= _dateEndMonth.Date))
                                                          && (nhomID == 0 || nhomID == x.NhomCongViecID)
                                                         ).Include(x => x.DoUuTien).Include(x => x.NhomCongViec).OrderByDescending(x => x.CongViecID).ToListAsync();
             
                foreach (var item in objCongViec)
                {

                    var objChuVu = _context.ChucVu.Where(x => x.ChucVuID == item.ChuVuID).Include(x => x.PhongBan).Include(x => x.PhongBan.CongTy).FirstOrDefault();
                    var obj = await _context.LichSuCongViec.Where(x => x.CongViecID == item.CongViecID).Include(x => x.TrangThaiCongViec).Include(x => x.NhanVien).OrderBy(x => x.ThoiGian).ToListAsync();
                    var objFile = await _context.FileCongViec.Where(x => x.CongViecID == item.CongViecID).ToListAsync();
                    var congViecView = new CongViecViewModel()
                    {
                        CongViecID = item.CongViecID,
                        TenCongViec = item.TenCongViec,
                        MoTa = item.MoTa,
                        NgayBatDau = item.NgayBatDau,
                        NgayKetThuc = item.NgayKetThuc,
                        UuTienID = item.UuTienID,
                        TenDoUuTien = item.DoUuTien == null ? "" : item.DoUuTien.TenUuTien,
                        NguoiTaoID = item.NguoiTaoID,
                        TenNguoiTao = item.CreateBy,
                        NhomCongViecID = item.NhomCongViecID,
                        TenNhomCongViec = item.NhomCongViec == null ? "" : item.NhomCongViec.TenNhomCongViec,
                        CongTyID = item.CongTyID,
                        TenCongTy = objChuVu == null ? "" : objChuVu.PhongBan.CongTy.TenCongTy,
                        PhongBanID = item.PhongBanID,
                        TenPhongBan = objChuVu == null ? "" : objChuVu.PhongBan.TenPhongBan,
                        ChuVuID = item.ChuVuID,
                        TenChucVu = objChuVu == null ? "" : objChuVu.TenChucVu,
                        NguoiThucHienID = item.NguoiThucHienID,
                        NguoiThucHienTen = item.NguoiThucHienTen,
                        FileCongViec  = _mapper.Map<List<FileCongViecDto>>(objFile),
                        LichSuCongViec = _mapper.Map<List<LichSuCongViecView>>(obj)
                    };
                    lstCongViecView.Add(congViecView);
                }
            }              
            return lstCongViecView;
        }
        public async Task<CongViecViewModel> GetDetail(int congViecID)
        {
            CongViecViewModel congViecView = new CongViecViewModel();
            var item = await _context.CongViec.Where(x => x.CongViecID ==congViecID ).Include(x => x.DoUuTien).Include(x => x.NhomCongViec).OrderByDescending(x => x.CongViecID).FirstOrDefaultAsync();
            if(item != null)
            {
                var objChuVu = _context.ChucVu.Where(x => x.ChucVuID == item.ChuVuID).Include(x => x.PhongBan).Include(x => x.PhongBan.CongTy).FirstOrDefault();
                var obj = await _context.LichSuCongViec.Where(x => x.CongViecID == item.CongViecID).Include(x => x.TrangThaiCongViec).Include(x => x.NhanVien).OrderBy(x => x.ThoiGian).ToListAsync();
                var objFile = await _context.FileCongViec.Where(x => x.CongViecID == item.CongViecID).ToListAsync();
                congViecView = new CongViecViewModel()
                {
                    CongViecID = item.CongViecID,
                    TenCongViec = item.TenCongViec,
                    MoTa = item.MoTa,
                    NgayBatDau = item.NgayBatDau,
                    NgayKetThuc = item.NgayKetThuc,
                    UuTienID = item.UuTienID,
                    TenDoUuTien = item.DoUuTien == null ? "" : item.DoUuTien.TenUuTien,
                    NguoiTaoID = item.NguoiTaoID,
                    TenNguoiTao = item.CreateBy,
                    NhomCongViecID = item.NhomCongViecID,
                    TenNhomCongViec = item.NhomCongViec == null ? "" : item.NhomCongViec.TenNhomCongViec,
                    CongTyID = item.CongTyID,
                    TenCongTy = objChuVu == null ? "" : objChuVu.PhongBan.CongTy.TenCongTy,
                    PhongBanID = item.PhongBanID,
                    TenPhongBan = objChuVu == null ? "" : objChuVu.PhongBan.TenPhongBan,
                    ChuVuID = item.ChuVuID,
                    TenChucVu = objChuVu == null ? "" : objChuVu.TenChucVu,
                    NguoiThucHienID = item.NguoiThucHienID,
                    NguoiThucHienTen = item.NguoiThucHienTen,
                    FileCongViec = _mapper.Map<List<FileCongViecDto>>(objFile),
                    LichSuCongViec = _mapper.Map<List<LichSuCongViecView>>(obj)
                };            
            }
            return congViecView;
        }
    }
}

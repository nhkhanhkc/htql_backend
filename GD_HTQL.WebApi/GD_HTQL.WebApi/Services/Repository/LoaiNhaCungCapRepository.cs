﻿using AutoMapper;
using GD_HTQL.WebApi.Data.Models;
using GD_HTQL.WebApi.Data.Models.EF.NhaCungCapModels;
using GD_HTQL.WebApi.Infrastructure;
using GD_HTQL.WebApi.Services.IRepository;

namespace GD_HTQL.WebApi.Services.Repository
{
    public class LoaiNhaCungCapRepository : GenericRepository<LoaiNhaCungCap>,ILoaiNhaCungCapRepository
    {
        public LoaiNhaCungCapRepository(ApplicationDbContext context, IMapper mapper) : base(context, mapper)
        {
        }
    }
}

﻿using AutoMapper;
using GD_HTQL.WebApi.Data.Models;
using GD_HTQL.WebApi.Data.Models.EF.NhanVienModels;
using GD_HTQL.WebApi.Infrastructure;
using GD_HTQL.WebApi.Services.IRepository;

namespace GD_HTQL.WebApi.Services.Repository
{
    public class ViTriRepository : GenericRepository<ViTri> , IViTriRepository
    {
        public ViTriRepository(ApplicationDbContext context, IMapper mapper) : base(context, mapper)
        {
        }
    }
}

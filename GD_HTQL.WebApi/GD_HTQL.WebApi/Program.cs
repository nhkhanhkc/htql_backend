using GD_HTQL.WebApi.Common;
using GD_HTQL.WebApi.Data.Models;
using GD_HTQL.WebApi.Infrastructure;
using GD_HTQL.WebApi.Repository;
using GD_HTQL.WebApi.Services.IRepository;
using GD_HTQL.WebApi.Services.Repository;
using GD_HTQL.WebApi.Telegram;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.FileProviders;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.Text.Json.Serialization;

var builder = WebApplication.CreateBuilder(args);
ConfigurationManager configuration = builder.Configuration;
// Add services to the container.
builder.Services.AddDbContext<ApplicationDbContext>
    (options =>
    {
        options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection"));
        options.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
    });

builder.Services.AddIdentity<IdentityUser, IdentityRole>(options =>
{
    options.SignIn.RequireConfirmedAccount = false;
    options.SignIn.RequireConfirmedEmail = false;
    options.Password.RequiredLength = 1;
    options.Password.RequireLowercase = false;
    options.Password.RequireUppercase = false;
    options.Password.RequireNonAlphanumeric = false;
    options.Password.RequireDigit = false;

})
    .AddEntityFrameworkStores<ApplicationDbContext>()
    .AddDefaultTokenProviders();

builder.Services.AddAuthentication(options =>
{
    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
})
// Adding Jwt Bearer
.AddJwtBearer(options =>
{
    options.SaveToken = true;
    options.RequireHttpsMetadata = false;
    options.TokenValidationParameters = new TokenValidationParameters()
    {
        ValidateIssuer = true,
        ValidateAudience = true,
        ValidAudience = configuration["JWT:ValidAudience"],
        ValidIssuer = configuration["JWT:ValidIssuer"],
        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["JWT:Secret"]))
    };
});

//builder.Services.AddAuthorization(options =>
//{
//    options.AddPolicy("quantriKhachhang", policy => policy.RequireClaim("khachhangManager","true"));
//});

// dependency injection
builder.Services.AddTransient(typeof(IGenericRepository<>), typeof(GenericRepository<>));
builder.Services.AddTransient<IUnitOfWork, UnitOfWork>();

builder.Services.AddScoped<IDbInitializer, DbInitializer>();
builder.Services.AddTransient<ITinhRepository, TinhRepository>();
builder.Services.AddTransient<IHuyenRepository, HuyenRepository>();
builder.Services.AddTransient<IXaRepository, XaRepository>();

// Cong viec
builder.Services.AddTransient<INhomCongViecRepository, NhomCongViecRepository>();
builder.Services.AddTransient<ICongViecRepository, CongViecRepository>();
builder.Services.AddTransient<ITrangThaiCongViecRepository, TrangThaiCongViecRepository>();

// Khac hang
builder.Services.AddTransient<ITacGiaRepository, TacGiaRepository>();
builder.Services.AddTransient<INhaCungCapRepository, NhaCungCapRepository>();
builder.Services.AddTransient<INhaThauRepository, NhaThauRepository>();
builder.Services.AddTransient<IQuanLyTuongTacRepository, QuanLyTuongTacRepository>();
builder.Services.AddTransient<INSCoQuanRepository, NSCoQuanRepository>();
builder.Services.AddTransient<INSCanBoRepository, NSCanBoRepository>();

builder.Services.AddTransient<ISanPhamRepository, SanPhamRepository>();
builder.Services.AddTransient<IDuToanRepository, DuToanRepository>();

// Nhan vien
builder.Services.AddTransient<INhanVienRepository, NhanVienRepository>();
builder.Services.AddTransient<ICongTyRepository, CongTyRepository>();
builder.Services.AddTransient<IPhongBanRepository, PhongBanRepository>();
builder.Services.AddTransient<IChucVuRepository, ChucVuRepository>();

// Van ban
builder.Services.AddTransient<ILoaiVanBanRepository, LoaiVanBanRepository>();
builder.Services.AddTransient<IVanBanRepository, VanBanRepository>();

builder.Services.AddTransient<IFileRepository, FileRepository>();

// San pham
builder.Services.AddTransient<ILoaiSanPhamRepository, LoaiSanPhamRepository>();
builder.Services.AddTransient<IMonHocRepository, MonHocRepository>();
builder.Services.AddTransient<IKhoiLopRepository, KhoiLopRepository>();

builder.Services.AddTransient<ILoaiNhaCungCapRepository, LoaiNhaCungCapRepository>();
builder.Services.AddTransient<ILoaiNhaThauRepository, LoaiNhaThauRepository>();

builder.Services.AddScoped<IImportRepository, ImportRepository>();
builder.Services.AddScoped<IViTriRepository, ViTriRepository>();

builder.Services.AddScoped<IDaiLyRepository, DaiLyRepository>();
builder.Services.AddScoped<ILoaiDaiLyRepository, LoaiDaiLyRepository>();
builder.Services.AddScoped<IQuanLyNhanVienRepository, QuanLyNhanVienRepository>();
builder.Services.AddScoped<IThongTuRepository, ThongTuRepository>();
builder.Services.AddScoped<ITinhNhanVienRepository, TinhNhanVienRepository>();
builder.Services.AddScoped<INT_GiaTriCoHoiRepository, NT_GiaTriCoHoiRepository>();
builder.Services.AddScoped<INguonVonRepository, NguonVonRepository>();

builder.Services.AddScoped<IKHCT_ThangRepository, KHCT_ThangRepository>();
builder.Services.AddScoped<IKHCT_TuanRepository, KHCT_TuanRepository>();
builder.Services.AddScoped<IKHCT_NgayRepository, KHCT_NgayRepository>();
builder.Services.AddScoped<IKHCT_ChiPhiRepository, KHCT_ChiPhiRepository>();
builder.Services.AddScoped<IKHCT_NguoiDiCungRepository, KHCT_NguoiDiCungRepository>();
builder.Services.AddScoped<IKHCT_NoiDungRepository, KHCT_NoiDungRepository>();
builder.Services.AddScoped<IKHCT_XeRepository, KHCT_XeRepository>();
builder.Services.AddScoped<IKHCT_Ngay_LichSuRepository,KHCT_Ngay_LichSuRepository>();

builder.Services.AddScoped<ICtyDuToanRepository, CtyDuToanRepository>();
builder.Services.AddScoped<IKHCT_ChiPhiKhacRepository, KHCT_ChiPhiKhacRepository>();

builder.Services.AddScoped<INT_DuToanRepository, NT_DuToanRepository>();
builder.Services.AddScoped<INghiPhepRepository, NghiPhepRepository>();
builder.Services.AddScoped<INghiPhep_LichSuRepository, NghiPhep_LichSuRepository>();

builder.Services.AddScoped<INSDuToanRepository, NSDuToanRepository>();

builder.Services.AddScoped<ILichSuCongViecRepository, LichSuCongViecRepository>();

builder.Services.AddScoped<IThongBaoCauHinhRepository, ThongBaoCauHinhRepository>();
builder.Services.AddScoped<IThongBaoNhanVienRepository, ThongBaoNhanVienRepository>();
builder.Services.AddScoped<ISendTelegramRepository, SendTelegramRepository>();

builder.Services.AddControllers().AddJsonOptions(x => x.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles);

builder.Services.AddCors(c => {
    c.AddPolicy("AllowOrigin", options => options.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
});

builder.Services.AddAutoMapper(typeof(Program));
//builder.Services.AddControllers();
builder.Services.AddControllers().AddJsonOptions(options =>
{
    options.JsonSerializerOptions.Converters.Add(new DateConverter());
});

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddSingleton<WTelegramService>();
builder.Services.AddHostedService(x => x.GetService<WTelegramService>());


var app = builder.Build();

// Configure the HTTP request pipeline.
//if (app.Environment.IsDevelopment())
//{
//    app.UseSwagger();
//    app.UseSwaggerUI();
//}
app.UseCors("AllowOrigin");

app.UseSwagger();
app.UseSwaggerUI();

app.UseHttpsRedirection();
app.UseAuthentication();
app.UseAuthorization();
app.MapControllers();
app.UseStaticFiles();
app.UseStaticFiles(new StaticFileOptions()
{
    FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), @"Resources")),
    RequestPath = new PathString("/Resources")

});
app.Run();

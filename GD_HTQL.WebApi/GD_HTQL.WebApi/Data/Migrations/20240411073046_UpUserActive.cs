﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GD_HTQL.WebApi.Data.Migrations
{
    /// <inheritdoc />
    public partial class UpUserActive : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TrangThai",
                table: "NhanVien");

            migrationBuilder.AddColumn<bool>(
                name: "Active",
                table: "PhongBan",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Active",
                table: "NhanVien",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Active",
                table: "CongTy",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Active",
                table: "ChucVu",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Active",
                table: "PhongBan");

            migrationBuilder.DropColumn(
                name: "Active",
                table: "NhanVien");

            migrationBuilder.DropColumn(
                name: "Active",
                table: "CongTy");

            migrationBuilder.DropColumn(
                name: "Active",
                table: "ChucVu");

            migrationBuilder.AddColumn<int>(
                name: "TrangThai",
                table: "NhanVien",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GD_HTQL.WebApi.Data.Migrations
{
    /// <inheritdoc />
    public partial class AddNT_giatricohoi : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "NT_GiaTriCoHoi",
                columns: table => new
                {
                    GTCHID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NhaThauID = table.Column<int>(type: "int", nullable: false),
                    ThoiGianTiepXuc = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CanBoTiepXuc = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SoDienThoai = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NhomHangTuVan = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NhomHangQuanTam = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NguonVon = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ThoiGianTrienKhai = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DoanhThuDuKien = table.Column<decimal>(type: "decimal(18,0)", nullable: true),
                    ThongTinTiepXuc = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    GhiChu = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NhanVienID = table.Column<int>(type: "int", nullable: false),
                    Active = table.Column<bool>(type: "bit", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NT_GiaTriCoHoi", x => x.GTCHID);
                    table.ForeignKey(
                        name: "FK_NT_GiaTriCoHoi_NhaThau_NhaThauID",
                        column: x => x.NhaThauID,
                        principalTable: "NhaThau",
                        principalColumn: "NhaThauID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_NT_GiaTriCoHoi_NhanVien_NhanVienID",
                        column: x => x.NhanVienID,
                        principalTable: "NhanVien",
                        principalColumn: "NhanVienID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_NT_GiaTriCoHoi_NhanVienID",
                table: "NT_GiaTriCoHoi",
                column: "NhanVienID");

            migrationBuilder.CreateIndex(
                name: "IX_NT_GiaTriCoHoi_NhaThauID",
                table: "NT_GiaTriCoHoi",
                column: "NhaThauID");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "NT_GiaTriCoHoi");
        }
    }
}

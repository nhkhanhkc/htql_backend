﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GD_HTQL.WebApi.Data.Migrations
{
    /// <inheritdoc />
    public partial class UpdaSp : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "DonGia",
                table: "SanPham",
                newName: "GiaKhachHang");

            migrationBuilder.AddColumn<decimal>(
                name: "GiaDaiLy",
                table: "SanPham",
                type: "decimal(18,4)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "KhoiLopID",
                table: "SanPham",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "LoaiSanPhamID",
                table: "SanPham",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MonHocID",
                table: "SanPham",
                type: "int",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "FileType",
                table: "FileVanBan",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(50)",
                oldMaxLength: 50);

            migrationBuilder.AlterColumn<byte[]>(
                name: "FileData",
                table: "FileVanBan",
                type: "varbinary(max)",
                nullable: true,
                oldClrType: typeof(byte[]),
                oldType: "varbinary(max)");

            migrationBuilder.AlterColumn<string>(
                name: "FileType",
                table: "FileDinhKem",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(50)",
                oldMaxLength: 50);

            migrationBuilder.AlterColumn<byte[]>(
                name: "FileData",
                table: "FileDinhKem",
                type: "varbinary(max)",
                nullable: true,
                oldClrType: typeof(byte[]),
                oldType: "varbinary(max)");

            migrationBuilder.CreateTable(
                name: "KhoiLop",
                columns: table => new
                {
                    KhoiLopID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TenKhoiLop = table.Column<string>(type: "nvarchar(225)", maxLength: 225, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_KhoiLop", x => x.KhoiLopID);
                });

            migrationBuilder.CreateTable(
                name: "LoaiSanPham",
                columns: table => new
                {
                    LoaiSanPhamID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TenLoaiSanPham = table.Column<string>(type: "nvarchar(225)", maxLength: 225, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LoaiSanPham", x => x.LoaiSanPhamID);
                });

            migrationBuilder.CreateTable(
                name: "MonHoc",
                columns: table => new
                {
                    MonHocID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TenMonHoc = table.Column<string>(type: "nvarchar(225)", maxLength: 225, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MonHoc", x => x.MonHocID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SanPham_KhoiLopID",
                table: "SanPham",
                column: "KhoiLopID");

            migrationBuilder.CreateIndex(
                name: "IX_SanPham_LoaiSanPhamID",
                table: "SanPham",
                column: "LoaiSanPhamID");

            migrationBuilder.CreateIndex(
                name: "IX_SanPham_MonHocID",
                table: "SanPham",
                column: "MonHocID");

            migrationBuilder.AddForeignKey(
                name: "FK_SanPham_KhoiLop_KhoiLopID",
                table: "SanPham",
                column: "KhoiLopID",
                principalTable: "KhoiLop",
                principalColumn: "KhoiLopID");

            migrationBuilder.AddForeignKey(
                name: "FK_SanPham_LoaiSanPham_LoaiSanPhamID",
                table: "SanPham",
                column: "LoaiSanPhamID",
                principalTable: "LoaiSanPham",
                principalColumn: "LoaiSanPhamID");

            migrationBuilder.AddForeignKey(
                name: "FK_SanPham_MonHoc_MonHocID",
                table: "SanPham",
                column: "MonHocID",
                principalTable: "MonHoc",
                principalColumn: "MonHocID");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SanPham_KhoiLop_KhoiLopID",
                table: "SanPham");

            migrationBuilder.DropForeignKey(
                name: "FK_SanPham_LoaiSanPham_LoaiSanPhamID",
                table: "SanPham");

            migrationBuilder.DropForeignKey(
                name: "FK_SanPham_MonHoc_MonHocID",
                table: "SanPham");

            migrationBuilder.DropTable(
                name: "KhoiLop");

            migrationBuilder.DropTable(
                name: "LoaiSanPham");

            migrationBuilder.DropTable(
                name: "MonHoc");

            migrationBuilder.DropIndex(
                name: "IX_SanPham_KhoiLopID",
                table: "SanPham");

            migrationBuilder.DropIndex(
                name: "IX_SanPham_LoaiSanPhamID",
                table: "SanPham");

            migrationBuilder.DropIndex(
                name: "IX_SanPham_MonHocID",
                table: "SanPham");

            migrationBuilder.DropColumn(
                name: "GiaDaiLy",
                table: "SanPham");

            migrationBuilder.DropColumn(
                name: "KhoiLopID",
                table: "SanPham");

            migrationBuilder.DropColumn(
                name: "LoaiSanPhamID",
                table: "SanPham");

            migrationBuilder.DropColumn(
                name: "MonHocID",
                table: "SanPham");

            migrationBuilder.RenameColumn(
                name: "GiaKhachHang",
                table: "SanPham",
                newName: "DonGia");

            migrationBuilder.AlterColumn<string>(
                name: "FileType",
                table: "FileVanBan",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(50)",
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<byte[]>(
                name: "FileData",
                table: "FileVanBan",
                type: "varbinary(max)",
                nullable: false,
                defaultValue: new byte[0],
                oldClrType: typeof(byte[]),
                oldType: "varbinary(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "FileType",
                table: "FileDinhKem",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(50)",
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<byte[]>(
                name: "FileData",
                table: "FileDinhKem",
                type: "varbinary(max)",
                nullable: false,
                defaultValue: new byte[0],
                oldClrType: typeof(byte[]),
                oldType: "varbinary(max)",
                oldNullable: true);
        }
    }
}

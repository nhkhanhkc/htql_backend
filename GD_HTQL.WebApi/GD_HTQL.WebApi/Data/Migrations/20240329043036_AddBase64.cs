﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GD_HTQL.WebApi.Data.Migrations
{
    /// <inheritdoc />
    public partial class AddBase64 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "avartar",
                table: "NhanVien");

            migrationBuilder.AddColumn<string>(
                name: "FileBase64",
                table: "FileVanBan",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "FileBase64",
                table: "FileDinhKem",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FileBase64",
                table: "FileVanBan");

            migrationBuilder.DropColumn(
                name: "FileBase64",
                table: "FileDinhKem");

            migrationBuilder.AddColumn<string>(
                name: "avartar",
                table: "NhanVien",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}

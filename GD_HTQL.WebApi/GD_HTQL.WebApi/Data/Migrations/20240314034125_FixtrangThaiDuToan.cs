﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GD_HTQL.WebApi.Data.Migrations
{
    /// <inheritdoc />
    public partial class FixtrangThaiDuToan : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DuToan_TrangThaiDuToan_TrangThaiDuToanTrangThaiID",
                table: "DuToan");

            migrationBuilder.DropIndex(
                name: "IX_DuToan_TrangThaiDuToanTrangThaiID",
                table: "DuToan");

            migrationBuilder.DropColumn(
                name: "NgayTao",
                table: "DuToan");

            migrationBuilder.DropColumn(
                name: "TrangThaiDuToanTrangThaiID",
                table: "DuToan");

            migrationBuilder.AddColumn<int>(
                name: "SoLuong",
                table: "SanPhamDuToan",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_DuToan_TrangThaiID",
                table: "DuToan",
                column: "TrangThaiID");

            migrationBuilder.AddForeignKey(
                name: "FK_DuToan_TrangThaiDuToan_TrangThaiID",
                table: "DuToan",
                column: "TrangThaiID",
                principalTable: "TrangThaiDuToan",
                principalColumn: "TrangThaiID",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DuToan_TrangThaiDuToan_TrangThaiID",
                table: "DuToan");

            migrationBuilder.DropIndex(
                name: "IX_DuToan_TrangThaiID",
                table: "DuToan");

            migrationBuilder.DropColumn(
                name: "SoLuong",
                table: "SanPhamDuToan");

            migrationBuilder.AddColumn<DateTime>(
                name: "NgayTao",
                table: "DuToan",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TrangThaiDuToanTrangThaiID",
                table: "DuToan",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_DuToan_TrangThaiDuToanTrangThaiID",
                table: "DuToan",
                column: "TrangThaiDuToanTrangThaiID");

            migrationBuilder.AddForeignKey(
                name: "FK_DuToan_TrangThaiDuToan_TrangThaiDuToanTrangThaiID",
                table: "DuToan",
                column: "TrangThaiDuToanTrangThaiID",
                principalTable: "TrangThaiDuToan",
                principalColumn: "TrangThaiID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

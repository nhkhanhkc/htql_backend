﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GD_HTQL.WebApi.Data.Migrations
{
    /// <inheritdoc />
    public partial class Uppppppppp : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TrangThaiID",
                table: "DuToan");

            migrationBuilder.RenameColumn(
                name: "ChiTiet",
                table: "DuToan",
                newName: "GhiChu");

            migrationBuilder.AddColumn<int>(
                name: "TrangThaiDuToanID",
                table: "DuToan",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_DuToan_TrangThaiDuToanID",
                table: "DuToan",
                column: "TrangThaiDuToanID");

            migrationBuilder.AddForeignKey(
                name: "FK_DuToan_TrangThaiDuToan_TrangThaiDuToanID",
                table: "DuToan",
                column: "TrangThaiDuToanID",
                principalTable: "TrangThaiDuToan",
                principalColumn: "TrangThaiID");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DuToan_TrangThaiDuToan_TrangThaiDuToanID",
                table: "DuToan");

            migrationBuilder.DropIndex(
                name: "IX_DuToan_TrangThaiDuToanID",
                table: "DuToan");

            migrationBuilder.DropColumn(
                name: "TrangThaiDuToanID",
                table: "DuToan");

            migrationBuilder.RenameColumn(
                name: "GhiChu",
                table: "DuToan",
                newName: "ChiTiet");

            migrationBuilder.AddColumn<int>(
                name: "TrangThaiID",
                table: "DuToan",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GD_HTQL.WebApi.Data.Migrations
{
    /// <inheritdoc />
    public partial class EditDutoan : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_NhanVienDuToan",
                table: "NhanVienDuToan");

            migrationBuilder.DropIndex(
                name: "IX_NhanVienDuToan_NhanVienID",
                table: "NhanVienDuToan");

            migrationBuilder.AddPrimaryKey(
                name: "PK_NhanVienDuToan",
                table: "NhanVienDuToan",
                columns: new[] { "NhanVienID", "DuToanID" });

            migrationBuilder.CreateIndex(
                name: "IX_NhanVienDuToan_DuToanID",
                table: "NhanVienDuToan",
                column: "DuToanID");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_NhanVienDuToan",
                table: "NhanVienDuToan");

            migrationBuilder.DropIndex(
                name: "IX_NhanVienDuToan_DuToanID",
                table: "NhanVienDuToan");

            migrationBuilder.AddPrimaryKey(
                name: "PK_NhanVienDuToan",
                table: "NhanVienDuToan",
                columns: new[] { "DuToanID", "NhanVienID" });

            migrationBuilder.CreateIndex(
                name: "IX_NhanVienDuToan_NhanVienID",
                table: "NhanVienDuToan",
                column: "NhanVienID");
        }
    }
}

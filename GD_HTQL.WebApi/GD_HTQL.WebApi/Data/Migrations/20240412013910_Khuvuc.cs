﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GD_HTQL.WebApi.Data.Migrations
{
    /// <inheritdoc />
    public partial class Khuvuc : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DMKhuVucKhuVucID",
                table: "DMTinh",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "KhuVucID",
                table: "DMTinh",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "DMKhuVuc",
                columns: table => new
                {
                    KhuVucID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TenKhuVuc = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DMKhuVuc", x => x.KhuVucID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DMTinh_DMKhuVucKhuVucID",
                table: "DMTinh",
                column: "DMKhuVucKhuVucID");

            migrationBuilder.AddForeignKey(
                name: "FK_DMTinh_DMKhuVuc_DMKhuVucKhuVucID",
                table: "DMTinh",
                column: "DMKhuVucKhuVucID",
                principalTable: "DMKhuVuc",
                principalColumn: "KhuVucID");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DMTinh_DMKhuVuc_DMKhuVucKhuVucID",
                table: "DMTinh");

            migrationBuilder.DropTable(
                name: "DMKhuVuc");

            migrationBuilder.DropIndex(
                name: "IX_DMTinh_DMKhuVucKhuVucID",
                table: "DMTinh");

            migrationBuilder.DropColumn(
                name: "DMKhuVucKhuVucID",
                table: "DMTinh");

            migrationBuilder.DropColumn(
                name: "KhuVucID",
                table: "DMTinh");
        }
    }
}

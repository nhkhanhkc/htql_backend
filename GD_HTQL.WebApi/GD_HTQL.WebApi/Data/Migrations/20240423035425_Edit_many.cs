﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GD_HTQL.WebApi.Data.Migrations
{
    /// <inheritdoc />
    public partial class Edit_many : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SanPhamID1",
                table: "SanPhamThongTu",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SanPhamID1",
                table: "SanPhamMonHoc",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "KhoiLopID1",
                table: "SanPhamKhoiLop",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SanPhamID1",
                table: "SanPhamKhoiLop",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_SanPhamThongTu_SanPhamID1",
                table: "SanPhamThongTu",
                column: "SanPhamID1");

            migrationBuilder.CreateIndex(
                name: "IX_SanPhamMonHoc_SanPhamID1",
                table: "SanPhamMonHoc",
                column: "SanPhamID1");

            migrationBuilder.CreateIndex(
                name: "IX_SanPhamKhoiLop_KhoiLopID1",
                table: "SanPhamKhoiLop",
                column: "KhoiLopID1");

            migrationBuilder.CreateIndex(
                name: "IX_SanPhamKhoiLop_SanPhamID1",
                table: "SanPhamKhoiLop",
                column: "SanPhamID1");

            migrationBuilder.AddForeignKey(
                name: "FK_SanPhamKhoiLop_KhoiLop_KhoiLopID1",
                table: "SanPhamKhoiLop",
                column: "KhoiLopID1",
                principalTable: "KhoiLop",
                principalColumn: "KhoiLopID");

            migrationBuilder.AddForeignKey(
                name: "FK_SanPhamKhoiLop_SanPham_SanPhamID1",
                table: "SanPhamKhoiLop",
                column: "SanPhamID1",
                principalTable: "SanPham",
                principalColumn: "SanPhamID");

            migrationBuilder.AddForeignKey(
                name: "FK_SanPhamMonHoc_SanPham_SanPhamID1",
                table: "SanPhamMonHoc",
                column: "SanPhamID1",
                principalTable: "SanPham",
                principalColumn: "SanPhamID");

            migrationBuilder.AddForeignKey(
                name: "FK_SanPhamThongTu_SanPham_SanPhamID1",
                table: "SanPhamThongTu",
                column: "SanPhamID1",
                principalTable: "SanPham",
                principalColumn: "SanPhamID");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SanPhamKhoiLop_KhoiLop_KhoiLopID1",
                table: "SanPhamKhoiLop");

            migrationBuilder.DropForeignKey(
                name: "FK_SanPhamKhoiLop_SanPham_SanPhamID1",
                table: "SanPhamKhoiLop");

            migrationBuilder.DropForeignKey(
                name: "FK_SanPhamMonHoc_SanPham_SanPhamID1",
                table: "SanPhamMonHoc");

            migrationBuilder.DropForeignKey(
                name: "FK_SanPhamThongTu_SanPham_SanPhamID1",
                table: "SanPhamThongTu");

            migrationBuilder.DropIndex(
                name: "IX_SanPhamThongTu_SanPhamID1",
                table: "SanPhamThongTu");

            migrationBuilder.DropIndex(
                name: "IX_SanPhamMonHoc_SanPhamID1",
                table: "SanPhamMonHoc");

            migrationBuilder.DropIndex(
                name: "IX_SanPhamKhoiLop_KhoiLopID1",
                table: "SanPhamKhoiLop");

            migrationBuilder.DropIndex(
                name: "IX_SanPhamKhoiLop_SanPhamID1",
                table: "SanPhamKhoiLop");

            migrationBuilder.DropColumn(
                name: "SanPhamID1",
                table: "SanPhamThongTu");

            migrationBuilder.DropColumn(
                name: "SanPhamID1",
                table: "SanPhamMonHoc");

            migrationBuilder.DropColumn(
                name: "KhoiLopID1",
                table: "SanPhamKhoiLop");

            migrationBuilder.DropColumn(
                name: "SanPhamID1",
                table: "SanPhamKhoiLop");
        }
    }
}

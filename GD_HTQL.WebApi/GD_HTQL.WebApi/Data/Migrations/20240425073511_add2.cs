﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GD_HTQL.WebApi.Data.Migrations
{
    /// <inheritdoc />
    public partial class add2 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SanPhamDuToan",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DuToanID = table.Column<int>(type: "int", nullable: false),
                    MaSanPham = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TenSanPham = table.Column<string>(type: "nvarchar(225)", maxLength: 225, nullable: false),
                    TieuChuanKyThuat = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    HangSanXuat = table.Column<string>(type: "nvarchar(225)", maxLength: 225, nullable: true),
                    XuatXu = table.Column<string>(type: "nvarchar(225)", maxLength: 225, nullable: true),
                    BaoHanh = table.Column<int>(type: "int", nullable: true),
                    NamSanXuat = table.Column<DateTime>(type: "datetime2", nullable: true),
                    NhaXuatBan = table.Column<string>(type: "nvarchar(225)", maxLength: 225, nullable: true),
                    DonViTinh = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    GiaVon = table.Column<decimal>(type: "decimal(18,0)", nullable: true),
                    GiaBan = table.Column<decimal>(type: "decimal(18,0)", nullable: true),
                    Thue = table.Column<decimal>(type: "decimal(18,0)", nullable: true),
                    SoLuong = table.Column<int>(type: "int", nullable: true),
                    DoiTuongSuDung = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ThuongHieu = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    HinhAnh = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TenLoaiSanPham = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TenMonHoc = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TenKhoiLop = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TenThongTu = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SanPhamDuToan", x => x.ID);
                    table.ForeignKey(
                        name: "FK_SanPhamDuToan_DuToan_DuToanID",
                        column: x => x.DuToanID,
                        principalTable: "DuToan",
                        principalColumn: "DuToanID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SanPhamDuToan_DuToanID",
                table: "SanPhamDuToan",
                column: "DuToanID");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SanPhamDuToan");
        }
    }
}

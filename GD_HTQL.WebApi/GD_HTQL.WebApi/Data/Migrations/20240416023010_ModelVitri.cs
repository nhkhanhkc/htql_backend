﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GD_HTQL.WebApi.Data.Migrations
{
    /// <inheritdoc />
    public partial class ModelVitri : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_NhaCungCap_LoaiNhaCungCap_LoaiNhaCungCapLoaiNCCID",
                table: "NhaCungCap");

            migrationBuilder.DropForeignKey(
                name: "FK_NhaThau_LoaiNhaThau_LoaiNhaThauLoaiNTID",
                table: "NhaThau");

            migrationBuilder.DropIndex(
                name: "IX_NhaThau_LoaiNhaThauLoaiNTID",
                table: "NhaThau");

            migrationBuilder.DropIndex(
                name: "IX_NhaCungCap_LoaiNhaCungCapLoaiNCCID",
                table: "NhaCungCap");

            migrationBuilder.DropColumn(
                name: "LoaiNhaThauLoaiNTID",
                table: "NhaThau");

            migrationBuilder.DropColumn(
                name: "LoaiNhaCungCapLoaiNCCID",
                table: "NhaCungCap");

            migrationBuilder.AlterColumn<int>(
                name: "LoaiNTID",
                table: "NhaThau",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "LoaiNCCID",
                table: "NhaCungCap",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "ViTri",
                columns: table => new
                {
                    ViTriID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ViDo = table.Column<float>(type: "real", nullable: false),
                    KinhDo = table.Column<float>(type: "real", nullable: false),
                    DiaChi = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ThoiGian = table.Column<DateTime>(type: "datetime2", nullable: false),
                    NhanVienID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ViTri", x => x.ViTriID);
                    table.ForeignKey(
                        name: "FK_ViTri_NhanVien_NhanVienID",
                        column: x => x.NhanVienID,
                        principalTable: "NhanVien",
                        principalColumn: "NhanVienID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_NhaThau_LoaiNTID",
                table: "NhaThau",
                column: "LoaiNTID");

            migrationBuilder.CreateIndex(
                name: "IX_NhaCungCap_LoaiNCCID",
                table: "NhaCungCap",
                column: "LoaiNCCID");

            migrationBuilder.CreateIndex(
                name: "IX_ViTri_NhanVienID",
                table: "ViTri",
                column: "NhanVienID");

            migrationBuilder.AddForeignKey(
                name: "FK_NhaCungCap_LoaiNhaCungCap_LoaiNCCID",
                table: "NhaCungCap",
                column: "LoaiNCCID",
                principalTable: "LoaiNhaCungCap",
                principalColumn: "LoaiNCCID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_NhaThau_LoaiNhaThau_LoaiNTID",
                table: "NhaThau",
                column: "LoaiNTID",
                principalTable: "LoaiNhaThau",
                principalColumn: "LoaiNTID",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_NhaCungCap_LoaiNhaCungCap_LoaiNCCID",
                table: "NhaCungCap");

            migrationBuilder.DropForeignKey(
                name: "FK_NhaThau_LoaiNhaThau_LoaiNTID",
                table: "NhaThau");

            migrationBuilder.DropTable(
                name: "ViTri");

            migrationBuilder.DropIndex(
                name: "IX_NhaThau_LoaiNTID",
                table: "NhaThau");

            migrationBuilder.DropIndex(
                name: "IX_NhaCungCap_LoaiNCCID",
                table: "NhaCungCap");

            migrationBuilder.AlterColumn<int>(
                name: "LoaiNTID",
                table: "NhaThau",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<int>(
                name: "LoaiNhaThauLoaiNTID",
                table: "NhaThau",
                type: "int",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "LoaiNCCID",
                table: "NhaCungCap",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<int>(
                name: "LoaiNhaCungCapLoaiNCCID",
                table: "NhaCungCap",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_NhaThau_LoaiNhaThauLoaiNTID",
                table: "NhaThau",
                column: "LoaiNhaThauLoaiNTID");

            migrationBuilder.CreateIndex(
                name: "IX_NhaCungCap_LoaiNhaCungCapLoaiNCCID",
                table: "NhaCungCap",
                column: "LoaiNhaCungCapLoaiNCCID");

            migrationBuilder.AddForeignKey(
                name: "FK_NhaCungCap_LoaiNhaCungCap_LoaiNhaCungCapLoaiNCCID",
                table: "NhaCungCap",
                column: "LoaiNhaCungCapLoaiNCCID",
                principalTable: "LoaiNhaCungCap",
                principalColumn: "LoaiNCCID");

            migrationBuilder.AddForeignKey(
                name: "FK_NhaThau_LoaiNhaThau_LoaiNhaThauLoaiNTID",
                table: "NhaThau",
                column: "LoaiNhaThauLoaiNTID",
                principalTable: "LoaiNhaThau",
                principalColumn: "LoaiNTID");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GD_HTQL.WebApi.Data.Migrations
{
    /// <inheritdoc />
    public partial class Upppp : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LichSuDuToan_DuToan_DuToanID",
                table: "LichSuDuToan");

            migrationBuilder.DropIndex(
                name: "IX_LichSuDuToan_DuToanID",
                table: "LichSuDuToan");

            migrationBuilder.AddColumn<int>(
                name: "TrangThaiID",
                table: "DuToan",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TrangThaiID",
                table: "DuToan");

            migrationBuilder.CreateIndex(
                name: "IX_LichSuDuToan_DuToanID",
                table: "LichSuDuToan",
                column: "DuToanID");

            migrationBuilder.AddForeignKey(
                name: "FK_LichSuDuToan_DuToan_DuToanID",
                table: "LichSuDuToan",
                column: "DuToanID",
                principalTable: "DuToan",
                principalColumn: "DuToanID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

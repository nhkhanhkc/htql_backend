﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GD_HTQL.WebApi.Data.Migrations
{
    /// <inheritdoc />
    public partial class FixNhanVien_Congviec : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NgayGiaoViec",
                table: "CongViec");

            migrationBuilder.DropColumn(
                name: "NguoiGiaoViecID",
                table: "CongViec");

            migrationBuilder.DropColumn(
                name: "NguoiTaoID",
                table: "CongViec");

            migrationBuilder.DropColumn(
                name: "NguoiThucHienID",
                table: "CongViec");

            migrationBuilder.RenameColumn(
                name: "TrangThai",
                table: "CongViec",
                newName: "TrangThaiID");

            migrationBuilder.RenameColumn(
                name: "NgayTao",
                table: "CongViec",
                newName: "ThoiGianDoiTrangThai");

            migrationBuilder.AlterColumn<decimal>(
                name: "Thue",
                table: "SanPham",
                type: "decimal(18,4)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "DonGia",
                table: "SanPham",
                type: "decimal(18,4)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "Gia",
                table: "DuToan",
                type: "decimal(18,4)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)",
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "NhanVienCongViec",
                columns: table => new
                {
                    NhanVienID = table.Column<int>(type: "int", nullable: false),
                    CongViecID = table.Column<int>(type: "int", nullable: false),
                    Loai = table.Column<int>(type: "int", nullable: false),
                    ThoiGan = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NhanVienCongViec", x => new { x.CongViecID, x.NhanVienID });
                    table.ForeignKey(
                        name: "FK_NhanVienCongViec_CongViec_CongViecID",
                        column: x => x.CongViecID,
                        principalTable: "CongViec",
                        principalColumn: "CongViecID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_NhanVienCongViec_NhanVien_NhanVienID",
                        column: x => x.NhanVienID,
                        principalTable: "NhanVien",
                        principalColumn: "NhanVienID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TrangThaiCongViec",
                columns: table => new
                {
                    TrangThaiID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TenTrangThai = table.Column<string>(type: "nvarchar(225)", maxLength: 225, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TrangThaiCongViec", x => x.TrangThaiID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CongViec_TrangThaiID",
                table: "CongViec",
                column: "TrangThaiID");

            migrationBuilder.CreateIndex(
                name: "IX_NhanVienCongViec_NhanVienID",
                table: "NhanVienCongViec",
                column: "NhanVienID");

            migrationBuilder.AddForeignKey(
                name: "FK_CongViec_TrangThaiCongViec_TrangThaiID",
                table: "CongViec",
                column: "TrangThaiID",
                principalTable: "TrangThaiCongViec",
                principalColumn: "TrangThaiID",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CongViec_TrangThaiCongViec_TrangThaiID",
                table: "CongViec");

            migrationBuilder.DropTable(
                name: "NhanVienCongViec");

            migrationBuilder.DropTable(
                name: "TrangThaiCongViec");

            migrationBuilder.DropIndex(
                name: "IX_CongViec_TrangThaiID",
                table: "CongViec");

            migrationBuilder.RenameColumn(
                name: "TrangThaiID",
                table: "CongViec",
                newName: "TrangThai");

            migrationBuilder.RenameColumn(
                name: "ThoiGianDoiTrangThai",
                table: "CongViec",
                newName: "NgayTao");

            migrationBuilder.AlterColumn<decimal>(
                name: "Thue",
                table: "SanPham",
                type: "decimal(18,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,4)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "DonGia",
                table: "SanPham",
                type: "decimal(18,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,4)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "Gia",
                table: "DuToan",
                type: "decimal(18,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,4)",
                oldNullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "NgayGiaoViec",
                table: "CongViec",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<int>(
                name: "NguoiGiaoViecID",
                table: "CongViec",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "NguoiTaoID",
                table: "CongViec",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "NguoiThucHienID",
                table: "CongViec",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GD_HTQL.WebApi.Data.Migrations
{
    /// <inheritdoc />
    public partial class FixDT : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Loai",
                table: "DuToan");

            migrationBuilder.AddColumn<string>(
                name: "ChiTiet",
                table: "DuToan",
                type: "nvarchar(max)",
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ChiTiet",
                table: "DuToan");

            migrationBuilder.AddColumn<int>(
                name: "Loai",
                table: "DuToan",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}

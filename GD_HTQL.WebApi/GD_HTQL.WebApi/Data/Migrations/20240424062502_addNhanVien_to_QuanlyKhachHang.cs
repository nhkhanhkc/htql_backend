﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GD_HTQL.WebApi.Data.Migrations
{
    /// <inheritdoc />
    public partial class addNhanVien_to_QuanlyKhachHang : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "NhanVienID",
                table: "TacGia",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "NhanVienID",
                table: "NSCoQuan",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "NhanVienID",
                table: "NhaThau",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "NhanVienID",
                table: "NhaCungCap",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "NhanVienID",
                table: "DaiLy",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TacGia_NhanVienID",
                table: "TacGia",
                column: "NhanVienID");

            migrationBuilder.CreateIndex(
                name: "IX_NSCoQuan_NhanVienID",
                table: "NSCoQuan",
                column: "NhanVienID");

            migrationBuilder.CreateIndex(
                name: "IX_NhaThau_NhanVienID",
                table: "NhaThau",
                column: "NhanVienID");

            migrationBuilder.CreateIndex(
                name: "IX_NhaCungCap_NhanVienID",
                table: "NhaCungCap",
                column: "NhanVienID");

            migrationBuilder.CreateIndex(
                name: "IX_DaiLy_NhanVienID",
                table: "DaiLy",
                column: "NhanVienID");

            migrationBuilder.AddForeignKey(
                name: "FK_DaiLy_NhanVien_NhanVienID",
                table: "DaiLy",
                column: "NhanVienID",
                principalTable: "NhanVien",
                principalColumn: "NhanVienID");

            migrationBuilder.AddForeignKey(
                name: "FK_NhaCungCap_NhanVien_NhanVienID",
                table: "NhaCungCap",
                column: "NhanVienID",
                principalTable: "NhanVien",
                principalColumn: "NhanVienID");

            migrationBuilder.AddForeignKey(
                name: "FK_NhaThau_NhanVien_NhanVienID",
                table: "NhaThau",
                column: "NhanVienID",
                principalTable: "NhanVien",
                principalColumn: "NhanVienID");

            migrationBuilder.AddForeignKey(
                name: "FK_NSCoQuan_NhanVien_NhanVienID",
                table: "NSCoQuan",
                column: "NhanVienID",
                principalTable: "NhanVien",
                principalColumn: "NhanVienID");

            migrationBuilder.AddForeignKey(
                name: "FK_TacGia_NhanVien_NhanVienID",
                table: "TacGia",
                column: "NhanVienID",
                principalTable: "NhanVien",
                principalColumn: "NhanVienID");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DaiLy_NhanVien_NhanVienID",
                table: "DaiLy");

            migrationBuilder.DropForeignKey(
                name: "FK_NhaCungCap_NhanVien_NhanVienID",
                table: "NhaCungCap");

            migrationBuilder.DropForeignKey(
                name: "FK_NhaThau_NhanVien_NhanVienID",
                table: "NhaThau");

            migrationBuilder.DropForeignKey(
                name: "FK_NSCoQuan_NhanVien_NhanVienID",
                table: "NSCoQuan");

            migrationBuilder.DropForeignKey(
                name: "FK_TacGia_NhanVien_NhanVienID",
                table: "TacGia");

            migrationBuilder.DropIndex(
                name: "IX_TacGia_NhanVienID",
                table: "TacGia");

            migrationBuilder.DropIndex(
                name: "IX_NSCoQuan_NhanVienID",
                table: "NSCoQuan");

            migrationBuilder.DropIndex(
                name: "IX_NhaThau_NhanVienID",
                table: "NhaThau");

            migrationBuilder.DropIndex(
                name: "IX_NhaCungCap_NhanVienID",
                table: "NhaCungCap");

            migrationBuilder.DropIndex(
                name: "IX_DaiLy_NhanVienID",
                table: "DaiLy");

            migrationBuilder.DropColumn(
                name: "NhanVienID",
                table: "TacGia");

            migrationBuilder.DropColumn(
                name: "NhanVienID",
                table: "NSCoQuan");

            migrationBuilder.DropColumn(
                name: "NhanVienID",
                table: "NhaThau");

            migrationBuilder.DropColumn(
                name: "NhanVienID",
                table: "NhaCungCap");

            migrationBuilder.DropColumn(
                name: "NhanVienID",
                table: "DaiLy");
        }
    }
}

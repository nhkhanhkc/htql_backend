﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GD_HTQL.WebApi.Data.Migrations
{
    /// <inheritdoc />
    public partial class TT : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DuToanID",
                table: "LichSuDuToan",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "TrangThaiDuToanTrangThaiID",
                table: "DuToan",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_LichSuDuToan_DuToanID",
                table: "LichSuDuToan",
                column: "DuToanID");

            migrationBuilder.CreateIndex(
                name: "IX_DuToan_TrangThaiDuToanTrangThaiID",
                table: "DuToan",
                column: "TrangThaiDuToanTrangThaiID");

            migrationBuilder.AddForeignKey(
                name: "FK_DuToan_TrangThaiDuToan_TrangThaiDuToanTrangThaiID",
                table: "DuToan",
                column: "TrangThaiDuToanTrangThaiID",
                principalTable: "TrangThaiDuToan",
                principalColumn: "TrangThaiID");

            migrationBuilder.AddForeignKey(
                name: "FK_LichSuDuToan_DuToan_DuToanID",
                table: "LichSuDuToan",
                column: "DuToanID",
                principalTable: "DuToan",
                principalColumn: "DuToanID",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DuToan_TrangThaiDuToan_TrangThaiDuToanTrangThaiID",
                table: "DuToan");

            migrationBuilder.DropForeignKey(
                name: "FK_LichSuDuToan_DuToan_DuToanID",
                table: "LichSuDuToan");

            migrationBuilder.DropIndex(
                name: "IX_LichSuDuToan_DuToanID",
                table: "LichSuDuToan");

            migrationBuilder.DropIndex(
                name: "IX_DuToan_TrangThaiDuToanTrangThaiID",
                table: "DuToan");

            migrationBuilder.DropColumn(
                name: "DuToanID",
                table: "LichSuDuToan");

            migrationBuilder.DropColumn(
                name: "TrangThaiDuToanTrangThaiID",
                table: "DuToan");
        }
    }
}

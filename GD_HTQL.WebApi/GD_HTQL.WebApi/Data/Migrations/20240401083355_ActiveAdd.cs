﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GD_HTQL.WebApi.Data.Migrations
{
    /// <inheritdoc />
    public partial class ActiveAdd : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Active",
                table: "TacGia",
                type: "bit",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Active",
                table: "QuanLyTuongTac",
                type: "bit",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Active",
                table: "NSCoQuan",
                type: "bit",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Active",
                table: "NSCanBo",
                type: "bit",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Active",
                table: "NhaThau",
                type: "bit",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Active",
                table: "NhaCungCap",
                type: "bit",
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Active",
                table: "TacGia");

            migrationBuilder.DropColumn(
                name: "Active",
                table: "QuanLyTuongTac");

            migrationBuilder.DropColumn(
                name: "Active",
                table: "NSCoQuan");

            migrationBuilder.DropColumn(
                name: "Active",
                table: "NSCanBo");

            migrationBuilder.DropColumn(
                name: "Active",
                table: "NhaThau");

            migrationBuilder.DropColumn(
                name: "Active",
                table: "NhaCungCap");
        }
    }
}

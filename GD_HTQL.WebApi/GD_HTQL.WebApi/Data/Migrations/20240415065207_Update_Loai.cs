﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GD_HTQL.WebApi.Data.Migrations
{
    /// <inheritdoc />
    public partial class Update_Loai : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ThongTinThem",
                table: "NhaThau",
                newName: "GhiChu");

            migrationBuilder.RenameColumn(
                name: "ThongTinThem",
                table: "NhaCungCap",
                newName: "Ghichu");

            migrationBuilder.AlterColumn<string>(
                name: "MaSoThue",
                table: "NSCoQuan",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(50)",
                oldMaxLength: 50);

            migrationBuilder.AddColumn<int>(
                name: "XaID",
                table: "NSCoQuan",
                type: "int",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "MaSoThue",
                table: "NhaThau",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(50)",
                oldMaxLength: 50);

            migrationBuilder.AddColumn<string>(
                name: "ChucVu",
                table: "NhaThau",
                type: "nvarchar(150)",
                maxLength: 150,
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "LoaiNTID",
                table: "NhaThau",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "LoaiNhaThauLoaiNTID",
                table: "NhaThau",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NhanVienPhuTrach",
                table: "NhaThau",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SoDienThoai",
                table: "NhaThau",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "MaSoThue",
                table: "NhaCungCap",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(50)",
                oldMaxLength: 50);

            migrationBuilder.AddColumn<string>(
                name: "ChucVu",
                table: "NhaCungCap",
                type: "nvarchar(150)",
                maxLength: 150,
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "LoaiNCCID",
                table: "NhaCungCap",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "LoaiNhaCungCapLoaiNCCID",
                table: "NhaCungCap",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NhanVienPhuTrach",
                table: "NhaCungCap",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SoDienThoai",
                table: "NhaCungCap",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true);

            migrationBuilder.CreateTable(
                name: "LoaiNhaCungCap",
                columns: table => new
                {
                    LoaiNCCID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TenLoai = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LoaiNhaCungCap", x => x.LoaiNCCID);
                });

            migrationBuilder.CreateTable(
                name: "LoaiNhaThau",
                columns: table => new
                {
                    LoaiNTID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TenLoai = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LoaiNhaThau", x => x.LoaiNTID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_NhaThau_LoaiNhaThauLoaiNTID",
                table: "NhaThau",
                column: "LoaiNhaThauLoaiNTID");

            migrationBuilder.CreateIndex(
                name: "IX_NhaCungCap_LoaiNhaCungCapLoaiNCCID",
                table: "NhaCungCap",
                column: "LoaiNhaCungCapLoaiNCCID");

            migrationBuilder.AddForeignKey(
                name: "FK_NhaCungCap_LoaiNhaCungCap_LoaiNhaCungCapLoaiNCCID",
                table: "NhaCungCap",
                column: "LoaiNhaCungCapLoaiNCCID",
                principalTable: "LoaiNhaCungCap",
                principalColumn: "LoaiNCCID");

            migrationBuilder.AddForeignKey(
                name: "FK_NhaThau_LoaiNhaThau_LoaiNhaThauLoaiNTID",
                table: "NhaThau",
                column: "LoaiNhaThauLoaiNTID",
                principalTable: "LoaiNhaThau",
                principalColumn: "LoaiNTID");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_NhaCungCap_LoaiNhaCungCap_LoaiNhaCungCapLoaiNCCID",
                table: "NhaCungCap");

            migrationBuilder.DropForeignKey(
                name: "FK_NhaThau_LoaiNhaThau_LoaiNhaThauLoaiNTID",
                table: "NhaThau");

            migrationBuilder.DropTable(
                name: "LoaiNhaCungCap");

            migrationBuilder.DropTable(
                name: "LoaiNhaThau");

            migrationBuilder.DropIndex(
                name: "IX_NhaThau_LoaiNhaThauLoaiNTID",
                table: "NhaThau");

            migrationBuilder.DropIndex(
                name: "IX_NhaCungCap_LoaiNhaCungCapLoaiNCCID",
                table: "NhaCungCap");

            migrationBuilder.DropColumn(
                name: "XaID",
                table: "NSCoQuan");

            migrationBuilder.DropColumn(
                name: "ChucVu",
                table: "NhaThau");

            migrationBuilder.DropColumn(
                name: "LoaiNTID",
                table: "NhaThau");

            migrationBuilder.DropColumn(
                name: "LoaiNhaThauLoaiNTID",
                table: "NhaThau");

            migrationBuilder.DropColumn(
                name: "NhanVienPhuTrach",
                table: "NhaThau");

            migrationBuilder.DropColumn(
                name: "SoDienThoai",
                table: "NhaThau");

            migrationBuilder.DropColumn(
                name: "ChucVu",
                table: "NhaCungCap");

            migrationBuilder.DropColumn(
                name: "LoaiNCCID",
                table: "NhaCungCap");

            migrationBuilder.DropColumn(
                name: "LoaiNhaCungCapLoaiNCCID",
                table: "NhaCungCap");

            migrationBuilder.DropColumn(
                name: "NhanVienPhuTrach",
                table: "NhaCungCap");

            migrationBuilder.DropColumn(
                name: "SoDienThoai",
                table: "NhaCungCap");

            migrationBuilder.RenameColumn(
                name: "GhiChu",
                table: "NhaThau",
                newName: "ThongTinThem");

            migrationBuilder.RenameColumn(
                name: "Ghichu",
                table: "NhaCungCap",
                newName: "ThongTinThem");

            migrationBuilder.AlterColumn<string>(
                name: "MaSoThue",
                table: "NSCoQuan",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(50)",
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "MaSoThue",
                table: "NhaThau",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(50)",
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "MaSoThue",
                table: "NhaCungCap",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(50)",
                oldMaxLength: 50,
                oldNullable: true);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GD_HTQL.WebApi.Data.Migrations
{
    /// <inheritdoc />
    public partial class ChangeTacGia : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ChucVu",
                table: "TacGia",
                newName: "ChucVuTacGia");

            migrationBuilder.AddColumn<int>(
                name: "TinhID",
                table: "NSCoQuan",
                type: "int",
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TinhID",
                table: "NSCoQuan");

            migrationBuilder.RenameColumn(
                name: "ChucVuTacGia",
                table: "TacGia",
                newName: "ChucVu");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GD_HTQL.WebApi.Data.Migrations
{
    /// <inheritdoc />
    public partial class Upp111 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BuocThiTruong",
                table: "QuanLyTuongTac");

            migrationBuilder.AddColumn<int>(
                name: "BuocThiTruongID",
                table: "QuanLyTuongTac",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<decimal>(
                name: "DoanhThuDuKien",
                table: "QuanLyTuongTac",
                type: "decimal(18,0)",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "BuocThiTruong",
                columns: table => new
                {
                    BuocThiTruongID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BuocThiTruongTen = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BuocThiTruong", x => x.BuocThiTruongID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_QuanLyTuongTac_BuocThiTruongID",
                table: "QuanLyTuongTac",
                column: "BuocThiTruongID");

            migrationBuilder.AddForeignKey(
                name: "FK_QuanLyTuongTac_BuocThiTruong_BuocThiTruongID",
                table: "QuanLyTuongTac",
                column: "BuocThiTruongID",
                principalTable: "BuocThiTruong",
                principalColumn: "BuocThiTruongID",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_QuanLyTuongTac_BuocThiTruong_BuocThiTruongID",
                table: "QuanLyTuongTac");

            migrationBuilder.DropTable(
                name: "BuocThiTruong");

            migrationBuilder.DropIndex(
                name: "IX_QuanLyTuongTac_BuocThiTruongID",
                table: "QuanLyTuongTac");

            migrationBuilder.DropColumn(
                name: "BuocThiTruongID",
                table: "QuanLyTuongTac");

            migrationBuilder.DropColumn(
                name: "DoanhThuDuKien",
                table: "QuanLyTuongTac");

            migrationBuilder.AddColumn<string>(
                name: "BuocThiTruong",
                table: "QuanLyTuongTac",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}

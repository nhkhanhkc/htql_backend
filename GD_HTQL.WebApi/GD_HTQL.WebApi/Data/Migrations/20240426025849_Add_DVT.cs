﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GD_HTQL.WebApi.Data.Migrations
{
    /// <inheritdoc />
    public partial class Add_DVT : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "DonViTinh",
                table: "SanPham",
                newName: "Model");

            migrationBuilder.AddColumn<int>(
                name: "DVTID",
                table: "SanPham",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "DonViTinh",
                columns: table => new
                {
                    DVTID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TenDVT = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DonViTinh", x => x.DVTID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SanPham_DVTID",
                table: "SanPham",
                column: "DVTID");

            migrationBuilder.AddForeignKey(
                name: "FK_SanPham_DonViTinh_DVTID",
                table: "SanPham",
                column: "DVTID",
                principalTable: "DonViTinh",
                principalColumn: "DVTID");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SanPham_DonViTinh_DVTID",
                table: "SanPham");

            migrationBuilder.DropTable(
                name: "DonViTinh");

            migrationBuilder.DropIndex(
                name: "IX_SanPham_DVTID",
                table: "SanPham");

            migrationBuilder.DropColumn(
                name: "DVTID",
                table: "SanPham");

            migrationBuilder.RenameColumn(
                name: "Model",
                table: "SanPham",
                newName: "DonViTinh");
        }
    }
}

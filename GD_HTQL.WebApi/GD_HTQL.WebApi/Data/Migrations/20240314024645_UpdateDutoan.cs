﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GD_HTQL.WebApi.Data.Migrations
{
    /// <inheritdoc />
    public partial class UpdateDutoan : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DuToan_NhanVien_NhanVienID",
                table: "DuToan");

            migrationBuilder.RenameColumn(
                name: "TrangThai",
                table: "DuToan",
                newName: "TrangThaiID");

            migrationBuilder.RenameColumn(
                name: "NhanVienID",
                table: "DuToan",
                newName: "TrangThaiDuToanTrangThaiID");

            migrationBuilder.RenameIndex(
                name: "IX_DuToan_NhanVienID",
                table: "DuToan",
                newName: "IX_DuToan_TrangThaiDuToanTrangThaiID");

            migrationBuilder.AlterColumn<int>(
                name: "UuTien",
                table: "CongViec",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<DateTime>(
                name: "ThoiGianDoiTrangThai",
                table: "CongViec",
                type: "datetime2",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AlterColumn<DateTime>(
                name: "DeadLine",
                table: "CongViec",
                type: "datetime2",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.CreateTable(
                name: "NhanVienDuToan",
                columns: table => new
                {
                    NhanVienID = table.Column<int>(type: "int", nullable: false),
                    DuToanID = table.Column<int>(type: "int", nullable: false),
                    Loai = table.Column<int>(type: "int", nullable: false),
                    ThoiGan = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NhanVienDuToan", x => new { x.DuToanID, x.NhanVienID });
                    table.ForeignKey(
                        name: "FK_NhanVienDuToan_DuToan_DuToanID",
                        column: x => x.DuToanID,
                        principalTable: "DuToan",
                        principalColumn: "DuToanID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_NhanVienDuToan_NhanVien_NhanVienID",
                        column: x => x.NhanVienID,
                        principalTable: "NhanVien",
                        principalColumn: "NhanVienID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TrangThaiDuToan",
                columns: table => new
                {
                    TrangThaiID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TenTrangThai = table.Column<string>(type: "nvarchar(225)", maxLength: 225, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TrangThaiDuToan", x => x.TrangThaiID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_NhanVienDuToan_NhanVienID",
                table: "NhanVienDuToan",
                column: "NhanVienID");

            migrationBuilder.AddForeignKey(
                name: "FK_DuToan_TrangThaiDuToan_TrangThaiDuToanTrangThaiID",
                table: "DuToan",
                column: "TrangThaiDuToanTrangThaiID",
                principalTable: "TrangThaiDuToan",
                principalColumn: "TrangThaiID",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DuToan_TrangThaiDuToan_TrangThaiDuToanTrangThaiID",
                table: "DuToan");

            migrationBuilder.DropTable(
                name: "NhanVienDuToan");

            migrationBuilder.DropTable(
                name: "TrangThaiDuToan");

            migrationBuilder.RenameColumn(
                name: "TrangThaiID",
                table: "DuToan",
                newName: "TrangThai");

            migrationBuilder.RenameColumn(
                name: "TrangThaiDuToanTrangThaiID",
                table: "DuToan",
                newName: "NhanVienID");

            migrationBuilder.RenameIndex(
                name: "IX_DuToan_TrangThaiDuToanTrangThaiID",
                table: "DuToan",
                newName: "IX_DuToan_NhanVienID");

            migrationBuilder.AlterColumn<int>(
                name: "UuTien",
                table: "CongViec",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "ThoiGianDoiTrangThai",
                table: "CongViec",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "DeadLine",
                table: "CongViec",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_DuToan_NhanVien_NhanVienID",
                table: "DuToan",
                column: "NhanVienID",
                principalTable: "NhanVien",
                principalColumn: "NhanVienID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

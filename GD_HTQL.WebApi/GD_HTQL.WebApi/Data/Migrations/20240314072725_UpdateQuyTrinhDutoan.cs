﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GD_HTQL.WebApi.Data.Migrations
{
    /// <inheritdoc />
    public partial class UpdateQuyTrinhDutoan : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DuToan_TrangThaiDuToan_TrangThaiID",
                table: "DuToan");

            migrationBuilder.DropTable(
                name: "NhanVienDuToan");

            migrationBuilder.DropIndex(
                name: "IX_DuToan_TrangThaiID",
                table: "DuToan");

            migrationBuilder.AddColumn<int>(
                name: "TrangThaiDuToanTrangThaiID",
                table: "DuToan",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "LichSuDuToan",
                columns: table => new
                {
                    LichSuDuToanID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NhanVienID = table.Column<int>(type: "int", nullable: false),
                    DuToanID = table.Column<int>(type: "int", nullable: false),
                    ThoiGan = table.Column<DateTime>(type: "datetime2", nullable: false),
                    GhiChu = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TrangThaiID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LichSuDuToan", x => x.LichSuDuToanID);
                    table.ForeignKey(
                        name: "FK_LichSuDuToan_DuToan_DuToanID",
                        column: x => x.DuToanID,
                        principalTable: "DuToan",
                        principalColumn: "DuToanID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_LichSuDuToan_NhanVien_NhanVienID",
                        column: x => x.NhanVienID,
                        principalTable: "NhanVien",
                        principalColumn: "NhanVienID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_LichSuDuToan_TrangThaiDuToan_TrangThaiID",
                        column: x => x.TrangThaiID,
                        principalTable: "TrangThaiDuToan",
                        principalColumn: "TrangThaiID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DuToan_TrangThaiDuToanTrangThaiID",
                table: "DuToan",
                column: "TrangThaiDuToanTrangThaiID");

            migrationBuilder.CreateIndex(
                name: "IX_LichSuDuToan_DuToanID",
                table: "LichSuDuToan",
                column: "DuToanID");

            migrationBuilder.CreateIndex(
                name: "IX_LichSuDuToan_NhanVienID",
                table: "LichSuDuToan",
                column: "NhanVienID");

            migrationBuilder.CreateIndex(
                name: "IX_LichSuDuToan_TrangThaiID",
                table: "LichSuDuToan",
                column: "TrangThaiID");

            migrationBuilder.AddForeignKey(
                name: "FK_DuToan_TrangThaiDuToan_TrangThaiDuToanTrangThaiID",
                table: "DuToan",
                column: "TrangThaiDuToanTrangThaiID",
                principalTable: "TrangThaiDuToan",
                principalColumn: "TrangThaiID");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DuToan_TrangThaiDuToan_TrangThaiDuToanTrangThaiID",
                table: "DuToan");

            migrationBuilder.DropTable(
                name: "LichSuDuToan");

            migrationBuilder.DropIndex(
                name: "IX_DuToan_TrangThaiDuToanTrangThaiID",
                table: "DuToan");

            migrationBuilder.DropColumn(
                name: "TrangThaiDuToanTrangThaiID",
                table: "DuToan");

            migrationBuilder.CreateTable(
                name: "NhanVienDuToan",
                columns: table => new
                {
                    NhanVienDuToanID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DuToanID = table.Column<int>(type: "int", nullable: false),
                    NhanVienID = table.Column<int>(type: "int", nullable: false),
                    GhiChu = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Loai = table.Column<int>(type: "int", nullable: false),
                    ThoiGan = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NhanVienDuToan", x => x.NhanVienDuToanID);
                    table.ForeignKey(
                        name: "FK_NhanVienDuToan_DuToan_DuToanID",
                        column: x => x.DuToanID,
                        principalTable: "DuToan",
                        principalColumn: "DuToanID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_NhanVienDuToan_NhanVien_NhanVienID",
                        column: x => x.NhanVienID,
                        principalTable: "NhanVien",
                        principalColumn: "NhanVienID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DuToan_TrangThaiID",
                table: "DuToan",
                column: "TrangThaiID");

            migrationBuilder.CreateIndex(
                name: "IX_NhanVienDuToan_DuToanID",
                table: "NhanVienDuToan",
                column: "DuToanID");

            migrationBuilder.CreateIndex(
                name: "IX_NhanVienDuToan_NhanVienID",
                table: "NhanVienDuToan",
                column: "NhanVienID");

            migrationBuilder.AddForeignKey(
                name: "FK_DuToan_TrangThaiDuToan_TrangThaiID",
                table: "DuToan",
                column: "TrangThaiID",
                principalTable: "TrangThaiDuToan",
                principalColumn: "TrangThaiID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

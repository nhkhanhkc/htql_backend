﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GD_HTQL.WebApi.Data.Migrations
{
    /// <inheritdoc />
    public partial class BH_add : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "BaoHanh",
                table: "SanPham",
                type: "int",
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BaoHanh",
                table: "SanPham");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GD_HTQL.WebApi.Data.Migrations
{
    /// <inheritdoc />
    public partial class UpKhuVuc1 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DMTinh_DMKhuVuc_DMKhuVucKhuVucID",
                table: "DMTinh");

            migrationBuilder.DropIndex(
                name: "IX_DMTinh_DMKhuVucKhuVucID",
                table: "DMTinh");

            migrationBuilder.DropColumn(
                name: "DMKhuVucKhuVucID",
                table: "DMTinh");

            migrationBuilder.AlterColumn<int>(
                name: "KhuVucID",
                table: "DMTinh",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_DMTinh_KhuVucID",
                table: "DMTinh",
                column: "KhuVucID");

            migrationBuilder.AddForeignKey(
                name: "FK_DMTinh_DMKhuVuc_KhuVucID",
                table: "DMTinh",
                column: "KhuVucID",
                principalTable: "DMKhuVuc",
                principalColumn: "KhuVucID",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DMTinh_DMKhuVuc_KhuVucID",
                table: "DMTinh");

            migrationBuilder.DropIndex(
                name: "IX_DMTinh_KhuVucID",
                table: "DMTinh");

            migrationBuilder.AlterColumn<int>(
                name: "KhuVucID",
                table: "DMTinh",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<int>(
                name: "DMKhuVucKhuVucID",
                table: "DMTinh",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_DMTinh_DMKhuVucKhuVucID",
                table: "DMTinh",
                column: "DMKhuVucKhuVucID");

            migrationBuilder.AddForeignKey(
                name: "FK_DMTinh_DMKhuVuc_DMKhuVucKhuVucID",
                table: "DMTinh",
                column: "DMKhuVucKhuVucID",
                principalTable: "DMKhuVuc",
                principalColumn: "KhuVucID");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GD_HTQL.WebApi.Data.Migrations
{
    /// <inheritdoc />
    public partial class fixFile : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "UrlFile",
                table: "FileVanBan",
                newName: "FileUrl");

            migrationBuilder.RenameColumn(
                name: "TenFile",
                table: "FileVanBan",
                newName: "FileType");

            migrationBuilder.RenameColumn(
                name: "UrlFile",
                table: "FileDinhKem",
                newName: "FileUrl");

            migrationBuilder.RenameColumn(
                name: "TenFile",
                table: "FileDinhKem",
                newName: "FileType");

            migrationBuilder.AddColumn<byte[]>(
                name: "FileData",
                table: "FileVanBan",
                type: "varbinary(max)",
                nullable: false,
                defaultValue: new byte[0]);

            migrationBuilder.AddColumn<string>(
                name: "FileName",
                table: "FileVanBan",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<byte[]>(
                name: "FileData",
                table: "FileDinhKem",
                type: "varbinary(max)",
                nullable: false,
                defaultValue: new byte[0]);

            migrationBuilder.AddColumn<string>(
                name: "FileName",
                table: "FileDinhKem",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FileData",
                table: "FileVanBan");

            migrationBuilder.DropColumn(
                name: "FileName",
                table: "FileVanBan");

            migrationBuilder.DropColumn(
                name: "FileData",
                table: "FileDinhKem");

            migrationBuilder.DropColumn(
                name: "FileName",
                table: "FileDinhKem");

            migrationBuilder.RenameColumn(
                name: "FileUrl",
                table: "FileVanBan",
                newName: "UrlFile");

            migrationBuilder.RenameColumn(
                name: "FileType",
                table: "FileVanBan",
                newName: "TenFile");

            migrationBuilder.RenameColumn(
                name: "FileUrl",
                table: "FileDinhKem",
                newName: "UrlFile");

            migrationBuilder.RenameColumn(
                name: "FileType",
                table: "FileDinhKem",
                newName: "TenFile");
        }
    }
}

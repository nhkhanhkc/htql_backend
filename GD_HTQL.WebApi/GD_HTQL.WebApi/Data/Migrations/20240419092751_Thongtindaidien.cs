﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GD_HTQL.WebApi.Data.Migrations
{
    /// <inheritdoc />
    public partial class Thongtindaidien : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "NDDChucVu",
                table: "NhaThau",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NDDSoDienThoai",
                table: "NhaThau",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NDDChucVu",
                table: "NhaCungCap",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NDDSoDienThoai",
                table: "NhaCungCap",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NDDChucVu",
                table: "DaiLy",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NDDSoDienThoai",
                table: "DaiLy",
                type: "nvarchar(max)",
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NDDChucVu",
                table: "NhaThau");

            migrationBuilder.DropColumn(
                name: "NDDSoDienThoai",
                table: "NhaThau");

            migrationBuilder.DropColumn(
                name: "NDDChucVu",
                table: "NhaCungCap");

            migrationBuilder.DropColumn(
                name: "NDDSoDienThoai",
                table: "NhaCungCap");

            migrationBuilder.DropColumn(
                name: "NDDChucVu",
                table: "DaiLy");

            migrationBuilder.DropColumn(
                name: "NDDSoDienThoai",
                table: "DaiLy");
        }
    }
}

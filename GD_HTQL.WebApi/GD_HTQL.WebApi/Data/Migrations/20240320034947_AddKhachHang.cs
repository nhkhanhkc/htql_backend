﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GD_HTQL.WebApi.Data.Migrations
{
    /// <inheritdoc />
    public partial class AddKhachHang : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_NSCoQuan_DMTinh_TinhID",
                table: "NSCoQuan");

            migrationBuilder.DropForeignKey(
                name: "FK_QuanLyTuongTac_NSCanBo_CanBoID",
                table: "QuanLyTuongTac");

            migrationBuilder.DropIndex(
                name: "IX_NSCoQuan_TinhID",
                table: "NSCoQuan");

            migrationBuilder.DropColumn(
                name: "DanhGia",
                table: "QuanLyTuongTac");

            migrationBuilder.DropColumn(
                name: "TinhID",
                table: "NSCoQuan");

            migrationBuilder.RenameColumn(
                name: "CanBoID",
                table: "QuanLyTuongTac",
                newName: "CoQuanID");

            migrationBuilder.RenameIndex(
                name: "IX_QuanLyTuongTac_CanBoID",
                table: "QuanLyTuongTac",
                newName: "IX_QuanLyTuongTac_CoQuanID");

            migrationBuilder.AddColumn<string>(
                name: "CCCD",
                table: "TacGia",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "GioiTinh",
                table: "TacGia",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "NgaySinh",
                table: "TacGia",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "TieuChuanKyThuat",
                table: "SanPham",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(225)",
                oldMaxLength: 225);

            migrationBuilder.AlterColumn<decimal>(
                name: "Thue",
                table: "SanPham",
                type: "decimal(18,0)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,4)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "GiaKhachHang",
                table: "SanPham",
                type: "decimal(18,0)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,4)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "GiaDaiLy",
                table: "SanPham",
                type: "decimal(18,0)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,4)",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BuocThiTruong",
                table: "QuanLyTuongTac",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CanBoTiepXuc",
                table: "QuanLyTuongTac",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NhomHangQuanTam",
                table: "QuanLyTuongTac",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ThongTinLienHe",
                table: "QuanLyTuongTac",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ThongTinTiepXuc",
                table: "QuanLyTuongTac",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "HuyenID",
                table: "NSCoQuan",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TinhID",
                table: "NhaThau",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "TinhID",
                table: "NhaCungCap",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_NSCoQuan_HuyenID",
                table: "NSCoQuan",
                column: "HuyenID");

            migrationBuilder.CreateIndex(
                name: "IX_NhaThau_TinhID",
                table: "NhaThau",
                column: "TinhID");

            migrationBuilder.CreateIndex(
                name: "IX_NhaCungCap_TinhID",
                table: "NhaCungCap",
                column: "TinhID");

            migrationBuilder.AddForeignKey(
                name: "FK_NhaCungCap_DMTinh_TinhID",
                table: "NhaCungCap",
                column: "TinhID",
                principalTable: "DMTinh",
                principalColumn: "TinhID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_NhaThau_DMTinh_TinhID",
                table: "NhaThau",
                column: "TinhID",
                principalTable: "DMTinh",
                principalColumn: "TinhID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_NSCoQuan_DMHuyen_HuyenID",
                table: "NSCoQuan",
                column: "HuyenID",
                principalTable: "DMHuyen",
                principalColumn: "HuyenID");

            migrationBuilder.AddForeignKey(
                name: "FK_QuanLyTuongTac_NSCoQuan_CoQuanID",
                table: "QuanLyTuongTac",
                column: "CoQuanID",
                principalTable: "NSCoQuan",
                principalColumn: "CoQuanID",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_NhaCungCap_DMTinh_TinhID",
                table: "NhaCungCap");

            migrationBuilder.DropForeignKey(
                name: "FK_NhaThau_DMTinh_TinhID",
                table: "NhaThau");

            migrationBuilder.DropForeignKey(
                name: "FK_NSCoQuan_DMHuyen_HuyenID",
                table: "NSCoQuan");

            migrationBuilder.DropForeignKey(
                name: "FK_QuanLyTuongTac_NSCoQuan_CoQuanID",
                table: "QuanLyTuongTac");

            migrationBuilder.DropIndex(
                name: "IX_NSCoQuan_HuyenID",
                table: "NSCoQuan");

            migrationBuilder.DropIndex(
                name: "IX_NhaThau_TinhID",
                table: "NhaThau");

            migrationBuilder.DropIndex(
                name: "IX_NhaCungCap_TinhID",
                table: "NhaCungCap");

            migrationBuilder.DropColumn(
                name: "CCCD",
                table: "TacGia");

            migrationBuilder.DropColumn(
                name: "GioiTinh",
                table: "TacGia");

            migrationBuilder.DropColumn(
                name: "NgaySinh",
                table: "TacGia");

            migrationBuilder.DropColumn(
                name: "BuocThiTruong",
                table: "QuanLyTuongTac");

            migrationBuilder.DropColumn(
                name: "CanBoTiepXuc",
                table: "QuanLyTuongTac");

            migrationBuilder.DropColumn(
                name: "NhomHangQuanTam",
                table: "QuanLyTuongTac");

            migrationBuilder.DropColumn(
                name: "ThongTinLienHe",
                table: "QuanLyTuongTac");

            migrationBuilder.DropColumn(
                name: "ThongTinTiepXuc",
                table: "QuanLyTuongTac");

            migrationBuilder.DropColumn(
                name: "HuyenID",
                table: "NSCoQuan");

            migrationBuilder.DropColumn(
                name: "TinhID",
                table: "NhaThau");

            migrationBuilder.DropColumn(
                name: "TinhID",
                table: "NhaCungCap");

            migrationBuilder.RenameColumn(
                name: "CoQuanID",
                table: "QuanLyTuongTac",
                newName: "CanBoID");

            migrationBuilder.RenameIndex(
                name: "IX_QuanLyTuongTac_CoQuanID",
                table: "QuanLyTuongTac",
                newName: "IX_QuanLyTuongTac_CanBoID");

            migrationBuilder.AlterColumn<string>(
                name: "TieuChuanKyThuat",
                table: "SanPham",
                type: "nvarchar(225)",
                maxLength: 225,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AlterColumn<decimal>(
                name: "Thue",
                table: "SanPham",
                type: "decimal(18,4)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,0)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "GiaKhachHang",
                table: "SanPham",
                type: "decimal(18,4)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,0)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "GiaDaiLy",
                table: "SanPham",
                type: "decimal(18,4)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,0)",
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DanhGia",
                table: "QuanLyTuongTac",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TinhID",
                table: "NSCoQuan",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_NSCoQuan_TinhID",
                table: "NSCoQuan",
                column: "TinhID");

            migrationBuilder.AddForeignKey(
                name: "FK_NSCoQuan_DMTinh_TinhID",
                table: "NSCoQuan",
                column: "TinhID",
                principalTable: "DMTinh",
                principalColumn: "TinhID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_QuanLyTuongTac_NSCanBo_CanBoID",
                table: "QuanLyTuongTac",
                column: "CanBoID",
                principalTable: "NSCanBo",
                principalColumn: "CanBoID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GD_HTQL.WebApi.Data.Migrations
{
    /// <inheritdoc />
    public partial class Update_DuToan : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_NhanVienDuToan",
                table: "NhanVienDuToan");

            migrationBuilder.DropColumn(
                name: "Gia",
                table: "DuToan");

            migrationBuilder.AddColumn<int>(
                name: "NhanVienDuToanID",
                table: "NhanVienDuToan",
                type: "int",
                nullable: false,
                defaultValue: 0)
                .Annotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AddColumn<string>(
                name: "GhiChu",
                table: "NhanVienDuToan",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddPrimaryKey(
                name: "PK_NhanVienDuToan",
                table: "NhanVienDuToan",
                column: "NhanVienDuToanID");

            migrationBuilder.CreateIndex(
                name: "IX_NhanVienDuToan_NhanVienID",
                table: "NhanVienDuToan",
                column: "NhanVienID");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_NhanVienDuToan",
                table: "NhanVienDuToan");

            migrationBuilder.DropIndex(
                name: "IX_NhanVienDuToan_NhanVienID",
                table: "NhanVienDuToan");

            migrationBuilder.DropColumn(
                name: "NhanVienDuToanID",
                table: "NhanVienDuToan");

            migrationBuilder.DropColumn(
                name: "GhiChu",
                table: "NhanVienDuToan");

            migrationBuilder.AddColumn<decimal>(
                name: "Gia",
                table: "DuToan",
                type: "decimal(18,4)",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_NhanVienDuToan",
                table: "NhanVienDuToan",
                columns: new[] { "NhanVienID", "DuToanID" });
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GD_HTQL.WebApi.Data.Migrations
{
    /// <inheritdoc />
    public partial class Update_key : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LichSuDuToan_TrangThaiDuToan_TrangThaiID",
                table: "LichSuDuToan");

            migrationBuilder.DropIndex(
                name: "IX_LichSuDuToan_TrangThaiID",
                table: "LichSuDuToan");

            migrationBuilder.AddColumn<int>(
                name: "TrangThaiDuToanTrangThaiID",
                table: "LichSuDuToan",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TrangThaiDuToanTrangThaiID",
                table: "DuToan",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TrangThaiID",
                table: "DuToan",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_LichSuDuToan_TrangThaiDuToanTrangThaiID",
                table: "LichSuDuToan",
                column: "TrangThaiDuToanTrangThaiID");

            migrationBuilder.CreateIndex(
                name: "IX_DuToan_TrangThaiDuToanTrangThaiID",
                table: "DuToan",
                column: "TrangThaiDuToanTrangThaiID");

            migrationBuilder.AddForeignKey(
                name: "FK_DuToan_TrangThaiDuToan_TrangThaiDuToanTrangThaiID",
                table: "DuToan",
                column: "TrangThaiDuToanTrangThaiID",
                principalTable: "TrangThaiDuToan",
                principalColumn: "TrangThaiID");

            migrationBuilder.AddForeignKey(
                name: "FK_LichSuDuToan_TrangThaiDuToan_TrangThaiDuToanTrangThaiID",
                table: "LichSuDuToan",
                column: "TrangThaiDuToanTrangThaiID",
                principalTable: "TrangThaiDuToan",
                principalColumn: "TrangThaiID");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DuToan_TrangThaiDuToan_TrangThaiDuToanTrangThaiID",
                table: "DuToan");

            migrationBuilder.DropForeignKey(
                name: "FK_LichSuDuToan_TrangThaiDuToan_TrangThaiDuToanTrangThaiID",
                table: "LichSuDuToan");

            migrationBuilder.DropIndex(
                name: "IX_LichSuDuToan_TrangThaiDuToanTrangThaiID",
                table: "LichSuDuToan");

            migrationBuilder.DropIndex(
                name: "IX_DuToan_TrangThaiDuToanTrangThaiID",
                table: "DuToan");

            migrationBuilder.DropColumn(
                name: "TrangThaiDuToanTrangThaiID",
                table: "LichSuDuToan");

            migrationBuilder.DropColumn(
                name: "TrangThaiDuToanTrangThaiID",
                table: "DuToan");

            migrationBuilder.DropColumn(
                name: "TrangThaiID",
                table: "DuToan");

            migrationBuilder.CreateIndex(
                name: "IX_LichSuDuToan_TrangThaiID",
                table: "LichSuDuToan",
                column: "TrangThaiID");

            migrationBuilder.AddForeignKey(
                name: "FK_LichSuDuToan_TrangThaiDuToan_TrangThaiID",
                table: "LichSuDuToan",
                column: "TrangThaiID",
                principalTable: "TrangThaiDuToan",
                principalColumn: "TrangThaiID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

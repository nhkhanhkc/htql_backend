﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GD_HTQL.WebApi.Data.Migrations
{
    /// <inheritdoc />
    public partial class Update_dtt : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DuToan_TrangThaiDuToan_TrangThaiDuToanTrangThaiID",
                table: "DuToan");

            migrationBuilder.DropForeignKey(
                name: "FK_LichSuDuToan_DuToan_DuToanID",
                table: "LichSuDuToan");

            migrationBuilder.DropForeignKey(
                name: "FK_LichSuDuToan_TrangThaiDuToan_TrangThaiDuToanTrangThaiID",
                table: "LichSuDuToan");

            migrationBuilder.DropIndex(
                name: "IX_LichSuDuToan_DuToanID",
                table: "LichSuDuToan");

            migrationBuilder.DropIndex(
                name: "IX_LichSuDuToan_TrangThaiDuToanTrangThaiID",
                table: "LichSuDuToan");

            migrationBuilder.DropIndex(
                name: "IX_DuToan_TrangThaiDuToanTrangThaiID",
                table: "DuToan");

            migrationBuilder.DropColumn(
                name: "DuToanID",
                table: "LichSuDuToan");

            migrationBuilder.DropColumn(
                name: "TrangThaiDuToanTrangThaiID",
                table: "LichSuDuToan");

            migrationBuilder.DropColumn(
                name: "TrangThaiID",
                table: "LichSuDuToan");

            migrationBuilder.DropColumn(
                name: "TrangThaiDuToanTrangThaiID",
                table: "DuToan");

            migrationBuilder.DropColumn(
                name: "TrangThaiID",
                table: "DuToan");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DuToanID",
                table: "LichSuDuToan",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "TrangThaiDuToanTrangThaiID",
                table: "LichSuDuToan",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TrangThaiID",
                table: "LichSuDuToan",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "TrangThaiDuToanTrangThaiID",
                table: "DuToan",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TrangThaiID",
                table: "DuToan",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_LichSuDuToan_DuToanID",
                table: "LichSuDuToan",
                column: "DuToanID");

            migrationBuilder.CreateIndex(
                name: "IX_LichSuDuToan_TrangThaiDuToanTrangThaiID",
                table: "LichSuDuToan",
                column: "TrangThaiDuToanTrangThaiID");

            migrationBuilder.CreateIndex(
                name: "IX_DuToan_TrangThaiDuToanTrangThaiID",
                table: "DuToan",
                column: "TrangThaiDuToanTrangThaiID");

            migrationBuilder.AddForeignKey(
                name: "FK_DuToan_TrangThaiDuToan_TrangThaiDuToanTrangThaiID",
                table: "DuToan",
                column: "TrangThaiDuToanTrangThaiID",
                principalTable: "TrangThaiDuToan",
                principalColumn: "TrangThaiID");

            migrationBuilder.AddForeignKey(
                name: "FK_LichSuDuToan_DuToan_DuToanID",
                table: "LichSuDuToan",
                column: "DuToanID",
                principalTable: "DuToan",
                principalColumn: "DuToanID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_LichSuDuToan_TrangThaiDuToan_TrangThaiDuToanTrangThaiID",
                table: "LichSuDuToan",
                column: "TrangThaiDuToanTrangThaiID",
                principalTable: "TrangThaiDuToan",
                principalColumn: "TrangThaiID");
        }
    }
}

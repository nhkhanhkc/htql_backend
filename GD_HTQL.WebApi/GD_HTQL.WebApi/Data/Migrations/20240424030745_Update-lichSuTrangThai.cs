﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GD_HTQL.WebApi.Data.Migrations
{
    /// <inheritdoc />
    public partial class UpdatelichSuTrangThai : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TrangThaiDuToanID",
                table: "LichSuDuToan",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_LichSuDuToan_DuToanID",
                table: "LichSuDuToan",
                column: "DuToanID");

            migrationBuilder.CreateIndex(
                name: "IX_LichSuDuToan_TrangThaiDuToanID",
                table: "LichSuDuToan",
                column: "TrangThaiDuToanID");

            migrationBuilder.AddForeignKey(
                name: "FK_LichSuDuToan_DuToan_DuToanID",
                table: "LichSuDuToan",
                column: "DuToanID",
                principalTable: "DuToan",
                principalColumn: "DuToanID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_LichSuDuToan_TrangThaiDuToan_TrangThaiDuToanID",
                table: "LichSuDuToan",
                column: "TrangThaiDuToanID",
                principalTable: "TrangThaiDuToan",
                principalColumn: "TrangThaiID");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LichSuDuToan_DuToan_DuToanID",
                table: "LichSuDuToan");

            migrationBuilder.DropForeignKey(
                name: "FK_LichSuDuToan_TrangThaiDuToan_TrangThaiDuToanID",
                table: "LichSuDuToan");

            migrationBuilder.DropIndex(
                name: "IX_LichSuDuToan_DuToanID",
                table: "LichSuDuToan");

            migrationBuilder.DropIndex(
                name: "IX_LichSuDuToan_TrangThaiDuToanID",
                table: "LichSuDuToan");

            migrationBuilder.DropColumn(
                name: "TrangThaiDuToanID",
                table: "LichSuDuToan");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GD_HTQL.WebApi.Data.Migrations
{
    /// <inheritdoc />
    public partial class Add_Vitri : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "LoaiDaiLy",
                columns: table => new
                {
                    LoaiDLID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TenLoai = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LoaiDaiLy", x => x.LoaiDLID);
                });

            migrationBuilder.CreateTable(
                name: "DaiLy",
                columns: table => new
                {
                    DaiLyID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TenDaiLy = table.Column<string>(type: "nvarchar(225)", maxLength: 225, nullable: false),
                    MaSoThue = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    DiaChi = table.Column<string>(type: "nvarchar(225)", maxLength: 225, nullable: true),
                    NguoiDaiDien = table.Column<string>(type: "nvarchar(225)", maxLength: 225, nullable: true),
                    GhiChu = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Active = table.Column<bool>(type: "bit", nullable: true),
                    TinhID = table.Column<int>(type: "int", nullable: false),
                    NhanVienPhuTrach = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    ChucVu = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    SoDienThoai = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    LoaiDLID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DaiLy", x => x.DaiLyID);
                    table.ForeignKey(
                        name: "FK_DaiLy_DMTinh_TinhID",
                        column: x => x.TinhID,
                        principalTable: "DMTinh",
                        principalColumn: "TinhID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DaiLy_LoaiDaiLy_LoaiDLID",
                        column: x => x.LoaiDLID,
                        principalTable: "LoaiDaiLy",
                        principalColumn: "LoaiDLID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DaiLy_LoaiDLID",
                table: "DaiLy",
                column: "LoaiDLID");

            migrationBuilder.CreateIndex(
                name: "IX_DaiLy_TinhID",
                table: "DaiLy",
                column: "TinhID");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DaiLy");

            migrationBuilder.DropTable(
                name: "LoaiDaiLy");
        }
    }
}

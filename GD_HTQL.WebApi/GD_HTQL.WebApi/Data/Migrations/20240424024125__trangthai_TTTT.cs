﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GD_HTQL.WebApi.Data.Migrations
{
    /// <inheritdoc />
    public partial class _trangthai_TTTT : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DuToan_TrangThaiDuToan_TrangThaiDuToanTrangThaiID",
                table: "DuToan");

            migrationBuilder.DropIndex(
                name: "IX_DuToan_TrangThaiDuToanTrangThaiID",
                table: "DuToan");

            migrationBuilder.DropColumn(
                name: "TrangThaiDuToanTrangThaiID",
                table: "DuToan");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TrangThaiDuToanTrangThaiID",
                table: "DuToan",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_DuToan_TrangThaiDuToanTrangThaiID",
                table: "DuToan",
                column: "TrangThaiDuToanTrangThaiID");

            migrationBuilder.AddForeignKey(
                name: "FK_DuToan_TrangThaiDuToan_TrangThaiDuToanTrangThaiID",
                table: "DuToan",
                column: "TrangThaiDuToanTrangThaiID",
                principalTable: "TrangThaiDuToan",
                principalColumn: "TrangThaiID");
        }
    }
}

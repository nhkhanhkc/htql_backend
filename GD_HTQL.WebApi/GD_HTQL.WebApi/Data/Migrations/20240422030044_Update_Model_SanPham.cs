﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GD_HTQL.WebApi.Data.Migrations
{
    /// <inheritdoc />
    public partial class Update_Model_SanPham : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SanPham_KhoiLop_KhoiLopID",
                table: "SanPham");

            migrationBuilder.DropForeignKey(
                name: "FK_SanPham_MonHoc_MonHocID",
                table: "SanPham");

            migrationBuilder.DropIndex(
                name: "IX_SanPham_KhoiLopID",
                table: "SanPham");

            migrationBuilder.DropIndex(
                name: "IX_SanPham_MonHocID",
                table: "SanPham");

            migrationBuilder.DropColumn(
                name: "KhoiLopID",
                table: "SanPham");

            migrationBuilder.DropColumn(
                name: "MonHocID",
                table: "SanPham");

            migrationBuilder.RenameColumn(
                name: "GiaKhachHang",
                table: "SanPham",
                newName: "GiaVon");

            migrationBuilder.AddColumn<bool>(
                name: "Active",
                table: "SanPham",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<decimal>(
                name: "GiaCDT",
                table: "SanPham",
                type: "decimal(18,0)",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "GiaTH_TT",
                table: "SanPham",
                type: "decimal(18,0)",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "GiaTTR",
                table: "SanPham",
                type: "decimal(18,0)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ThuongHieu",
                table: "SanPham",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Active",
                table: "MonHoc",
                type: "bit",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Active",
                table: "KhoiLop",
                type: "bit",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "SanPhamKhoiLop",
                columns: table => new
                {
                    SanPhamID = table.Column<int>(type: "int", nullable: false),
                    KhoiLopID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SanPhamKhoiLop", x => new { x.KhoiLopID, x.SanPhamID });
                    table.ForeignKey(
                        name: "FK_SanPhamKhoiLop_KhoiLop_KhoiLopID",
                        column: x => x.KhoiLopID,
                        principalTable: "KhoiLop",
                        principalColumn: "KhoiLopID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SanPhamKhoiLop_SanPham_SanPhamID",
                        column: x => x.SanPhamID,
                        principalTable: "SanPham",
                        principalColumn: "SanPhamID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SanPhamMonHoc",
                columns: table => new
                {
                    SanPhamID = table.Column<int>(type: "int", nullable: false),
                    MonHocID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SanPhamMonHoc", x => new { x.MonHocID, x.SanPhamID });
                    table.ForeignKey(
                        name: "FK_SanPhamMonHoc_MonHoc_MonHocID",
                        column: x => x.MonHocID,
                        principalTable: "MonHoc",
                        principalColumn: "MonHocID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SanPhamMonHoc_SanPham_SanPhamID",
                        column: x => x.SanPhamID,
                        principalTable: "SanPham",
                        principalColumn: "SanPhamID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ThongTu",
                columns: table => new
                {
                    ThongTuID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TenThongTu = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: false),
                    Active = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ThongTu", x => x.ThongTuID);
                });

            migrationBuilder.CreateTable(
                name: "SanPhamThongTu",
                columns: table => new
                {
                    SanPhamID = table.Column<int>(type: "int", nullable: false),
                    ThongTuID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SanPhamThongTu", x => new { x.SanPhamID, x.ThongTuID });
                    table.ForeignKey(
                        name: "FK_SanPhamThongTu_SanPham_SanPhamID",
                        column: x => x.SanPhamID,
                        principalTable: "SanPham",
                        principalColumn: "SanPhamID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SanPhamThongTu_ThongTu_ThongTuID",
                        column: x => x.ThongTuID,
                        principalTable: "ThongTu",
                        principalColumn: "ThongTuID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SanPhamKhoiLop_SanPhamID",
                table: "SanPhamKhoiLop",
                column: "SanPhamID");

            migrationBuilder.CreateIndex(
                name: "IX_SanPhamMonHoc_SanPhamID",
                table: "SanPhamMonHoc",
                column: "SanPhamID");

            migrationBuilder.CreateIndex(
                name: "IX_SanPhamThongTu_ThongTuID",
                table: "SanPhamThongTu",
                column: "ThongTuID");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SanPhamKhoiLop");

            migrationBuilder.DropTable(
                name: "SanPhamMonHoc");

            migrationBuilder.DropTable(
                name: "SanPhamThongTu");

            migrationBuilder.DropTable(
                name: "ThongTu");

            migrationBuilder.DropColumn(
                name: "Active",
                table: "SanPham");

            migrationBuilder.DropColumn(
                name: "GiaCDT",
                table: "SanPham");

            migrationBuilder.DropColumn(
                name: "GiaTH_TT",
                table: "SanPham");

            migrationBuilder.DropColumn(
                name: "GiaTTR",
                table: "SanPham");

            migrationBuilder.DropColumn(
                name: "ThuongHieu",
                table: "SanPham");

            migrationBuilder.DropColumn(
                name: "Active",
                table: "MonHoc");

            migrationBuilder.DropColumn(
                name: "Active",
                table: "KhoiLop");

            migrationBuilder.RenameColumn(
                name: "GiaVon",
                table: "SanPham",
                newName: "GiaKhachHang");

            migrationBuilder.AddColumn<int>(
                name: "KhoiLopID",
                table: "SanPham",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MonHocID",
                table: "SanPham",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_SanPham_KhoiLopID",
                table: "SanPham",
                column: "KhoiLopID");

            migrationBuilder.CreateIndex(
                name: "IX_SanPham_MonHocID",
                table: "SanPham",
                column: "MonHocID");

            migrationBuilder.AddForeignKey(
                name: "FK_SanPham_KhoiLop_KhoiLopID",
                table: "SanPham",
                column: "KhoiLopID",
                principalTable: "KhoiLop",
                principalColumn: "KhoiLopID");

            migrationBuilder.AddForeignKey(
                name: "FK_SanPham_MonHoc_MonHocID",
                table: "SanPham",
                column: "MonHocID",
                principalTable: "MonHoc",
                principalColumn: "MonHocID");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GD_HTQL.WebApi.Data.Migrations
{
    /// <inheritdoc />
    public partial class table_NguonVon : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NguonVon",
                table: "NT_GiaTriCoHoi");

            migrationBuilder.AddColumn<int>(
                name: "NguonVonID",
                table: "NT_GiaTriCoHoi",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "NguonVon",
                columns: table => new
                {
                    NguonVonID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TenNguonVon = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NguonVon", x => x.NguonVonID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_NT_GiaTriCoHoi_NguonVonID",
                table: "NT_GiaTriCoHoi",
                column: "NguonVonID");

            migrationBuilder.AddForeignKey(
                name: "FK_NT_GiaTriCoHoi_NguonVon_NguonVonID",
                table: "NT_GiaTriCoHoi",
                column: "NguonVonID",
                principalTable: "NguonVon",
                principalColumn: "NguonVonID",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_NT_GiaTriCoHoi_NguonVon_NguonVonID",
                table: "NT_GiaTriCoHoi");

            migrationBuilder.DropTable(
                name: "NguonVon");

            migrationBuilder.DropIndex(
                name: "IX_NT_GiaTriCoHoi_NguonVonID",
                table: "NT_GiaTriCoHoi");

            migrationBuilder.DropColumn(
                name: "NguonVonID",
                table: "NT_GiaTriCoHoi");

            migrationBuilder.AddColumn<string>(
                name: "NguonVon",
                table: "NT_GiaTriCoHoi",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}

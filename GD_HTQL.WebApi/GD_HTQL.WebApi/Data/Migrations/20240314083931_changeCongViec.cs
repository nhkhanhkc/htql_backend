﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GD_HTQL.WebApi.Data.Migrations
{
    /// <inheritdoc />
    public partial class changeCongViec : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CongViec_TrangThaiCongViec_TrangThaiID",
                table: "CongViec");

            migrationBuilder.DropTable(
                name: "NhanVienCongViec");

            migrationBuilder.DropIndex(
                name: "IX_CongViec_TrangThaiID",
                table: "CongViec");

            migrationBuilder.DropColumn(
                name: "TrangThaiID",
                table: "CongViec");

            migrationBuilder.CreateTable(
                name: "LSGiaoViec",
                columns: table => new
                {
                    LSGiaoViecID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NhanVienID = table.Column<int>(type: "int", nullable: false),
                    CongViecID = table.Column<int>(type: "int", nullable: false),
                    ThoiGan = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LSGiaoViec", x => x.LSGiaoViecID);
                    table.ForeignKey(
                        name: "FK_LSGiaoViec_CongViec_CongViecID",
                        column: x => x.CongViecID,
                        principalTable: "CongViec",
                        principalColumn: "CongViecID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_LSGiaoViec_NhanVien_NhanVienID",
                        column: x => x.NhanVienID,
                        principalTable: "NhanVien",
                        principalColumn: "NhanVienID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "LSThucHienCongViec",
                columns: table => new
                {
                    LSThucHienID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NhanVienID = table.Column<int>(type: "int", nullable: false),
                    CongViecID = table.Column<int>(type: "int", nullable: false),
                    ThoiGan = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TrangThaiID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LSThucHienCongViec", x => x.LSThucHienID);
                    table.ForeignKey(
                        name: "FK_LSThucHienCongViec_CongViec_CongViecID",
                        column: x => x.CongViecID,
                        principalTable: "CongViec",
                        principalColumn: "CongViecID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_LSThucHienCongViec_NhanVien_NhanVienID",
                        column: x => x.NhanVienID,
                        principalTable: "NhanVien",
                        principalColumn: "NhanVienID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_LSThucHienCongViec_TrangThaiCongViec_TrangThaiID",
                        column: x => x.TrangThaiID,
                        principalTable: "TrangThaiCongViec",
                        principalColumn: "TrangThaiID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_LSGiaoViec_CongViecID",
                table: "LSGiaoViec",
                column: "CongViecID");

            migrationBuilder.CreateIndex(
                name: "IX_LSGiaoViec_NhanVienID",
                table: "LSGiaoViec",
                column: "NhanVienID");

            migrationBuilder.CreateIndex(
                name: "IX_LSThucHienCongViec_CongViecID",
                table: "LSThucHienCongViec",
                column: "CongViecID");

            migrationBuilder.CreateIndex(
                name: "IX_LSThucHienCongViec_NhanVienID",
                table: "LSThucHienCongViec",
                column: "NhanVienID");

            migrationBuilder.CreateIndex(
                name: "IX_LSThucHienCongViec_TrangThaiID",
                table: "LSThucHienCongViec",
                column: "TrangThaiID");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "LSGiaoViec");

            migrationBuilder.DropTable(
                name: "LSThucHienCongViec");

            migrationBuilder.AddColumn<int>(
                name: "TrangThaiID",
                table: "CongViec",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "NhanVienCongViec",
                columns: table => new
                {
                    CongViecID = table.Column<int>(type: "int", nullable: false),
                    NhanVienID = table.Column<int>(type: "int", nullable: false),
                    Loai = table.Column<int>(type: "int", nullable: false),
                    ThoiGan = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NhanVienCongViec", x => new { x.CongViecID, x.NhanVienID });
                    table.ForeignKey(
                        name: "FK_NhanVienCongViec_CongViec_CongViecID",
                        column: x => x.CongViecID,
                        principalTable: "CongViec",
                        principalColumn: "CongViecID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_NhanVienCongViec_NhanVien_NhanVienID",
                        column: x => x.NhanVienID,
                        principalTable: "NhanVien",
                        principalColumn: "NhanVienID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CongViec_TrangThaiID",
                table: "CongViec",
                column: "TrangThaiID");

            migrationBuilder.CreateIndex(
                name: "IX_NhanVienCongViec_NhanVienID",
                table: "NhanVienCongViec",
                column: "NhanVienID");

            migrationBuilder.AddForeignKey(
                name: "FK_CongViec_TrangThaiCongViec_TrangThaiID",
                table: "CongViec",
                column: "TrangThaiID",
                principalTable: "TrangThaiCongViec",
                principalColumn: "TrangThaiID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

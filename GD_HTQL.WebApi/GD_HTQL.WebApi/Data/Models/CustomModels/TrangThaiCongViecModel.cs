﻿namespace GD_HTQL.WebApi.Data.Models.CustomModels
{
    public class TrangThaiCongViecModel
    {
        public int CongViecID { get; set; }
        public int TrangThaiID { get; set; }
        public int NhanVienID { get; set; }
    }
}

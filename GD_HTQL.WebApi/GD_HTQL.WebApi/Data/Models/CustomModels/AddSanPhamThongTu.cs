﻿namespace GD_HTQL.WebApi.Data.Models.CustomModels
{
    public class AddSanPhamThongTu
    {
        public required List<int> lstSanPham {  get; set; }
        public int ThongTuID { get; set; }
    }
}

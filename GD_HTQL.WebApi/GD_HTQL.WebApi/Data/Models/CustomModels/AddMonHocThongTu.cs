﻿namespace GD_HTQL.WebApi.Data.Models.CustomModels
{
    public class AddMonHocThongTu
    {
        public required List<int> lstMonHoc { get; set; }
        public int ThongTuID { get; set; }
    }
}

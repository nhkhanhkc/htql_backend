﻿using GD_HTQL.WebApi.Data.Models.Dtos.SanPham;

namespace GD_HTQL.WebApi.Data.Models.CustomModels
{
    public class SanPhamCustomModel
    {
        public SanPhamDto sanPham { get; set; }
        public List<int>? MonHoc { get; set; }
        public List<int>? KhoiLop { get; set; }
        public List<int>? ThongTu { get; set; }
    }
}

﻿using GD_HTQL.WebApi.Data.Models.Dtos.CongViec;

namespace GD_HTQL.WebApi.Data.Models.CustomModels
{
    public class CongViecModel
    {
        public virtual required CongViecDto CongViecDto { get; set; }
        public virtual List<FileCongViecDto>? FileCongViecDto {  get; set; }
    }
}

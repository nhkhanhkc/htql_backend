﻿namespace GD_HTQL.WebApi.Data.Models.Abstract
{
    public interface ICommonAbstract
    {
        DateTime? CreateDate { set; get; }
        string CreateBy { set; get; }
        DateTime? UpdateDate { set; get; }
        string UpdateBy { set; get; }
        DateTime? ActiveDate { set; get; }
        string? ActiveBy { set; get; }
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace GD_HTQL.WebApi.Data.Models.Abstract
{
    public abstract class CommonAbstract : ICommonAbstract
    {
        public DateTime? CreateDate { set; get; }
        [StringLength(50)]
        public string? CreateBy { set; get; }
        public DateTime? UpdateDate { set; get; }
        [StringLength(50)]
        public string? UpdateBy { set; get; }
        public DateTime? ActiveDate { set; get; }
        [StringLength(50)]
        public string? ActiveBy { set; get; }
    }
}

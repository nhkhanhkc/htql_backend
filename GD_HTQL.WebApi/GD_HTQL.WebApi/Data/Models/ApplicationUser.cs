﻿using GD_HTQL.WebApi.Data.Models.EF.NhanVienModels;
using Microsoft.AspNetCore.Identity;
using System;

namespace GD_HTQL.WebApi.Data.Models
{
    public class ApplicationUser : IdentityUser
    {
        public string? HoVaTen { get; set; }     
        public NhanVien NhanVien { get; set; }
    }
}

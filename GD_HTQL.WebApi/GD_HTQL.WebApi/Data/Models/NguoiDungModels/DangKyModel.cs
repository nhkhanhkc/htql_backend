﻿namespace GD_HTQL.WebApi.Models.UserModels
{
    public class DangKyModel
    {
        public string TenDangNhap { get; set; }
        public string MatKhau { get; set; } 
        public string HoVaTen { get; set;}
        public List<string>? Quyen {  get; set;}
    }
}

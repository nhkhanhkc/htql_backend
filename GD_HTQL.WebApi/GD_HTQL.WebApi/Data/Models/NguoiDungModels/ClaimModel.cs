﻿namespace GD_HTQL.WebApi.Data.Models.NguoiDungModels
{
    public class ClaimModel
    {
        public string ClaimType { get; set; }
        public string ClaimValue { get; set;}
    }
}

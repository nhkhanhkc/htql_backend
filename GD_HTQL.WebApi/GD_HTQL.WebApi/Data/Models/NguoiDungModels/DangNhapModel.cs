﻿using System.ComponentModel.DataAnnotations;

namespace GD_HTQL.WebApi.Models.UserModels
{
    public class DangNhapModel
    {
        public string TenDangNhap { get; set; }
        public string MatKhau { get; set; }
    }
}

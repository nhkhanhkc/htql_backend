﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using GD_HTQL.WebApi.Data.Models.Abstract;

namespace GD_HTQL.WebApi.Data.Models.EF.NhanVienModels
{
    public class PhongBan : CommonAbstract
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int PhongBanID { get; set; }
        [StringLength(225)]
        public required string TenPhongBan { get; set; }
        public string? ChiTiet { get; set; }
        public int CongTyID { get; set; }
        public bool Active { get; set; }
        public required virtual CongTy CongTy { get; set; }
        public ICollection<ChucVu>? ChucVu { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace GD_HTQL.WebApi.Data.Models.EF.NhanVienModels
{
    public class ViTri
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int ViTriID { get; set; }
        public float ViDo { get; set; }
        public float KinhDo { get; set; }
        public string DiaChi { get; set; }
        public DateTime ThoiGian { get; set; }
        public int NhanVienID { get; set; }
        public virtual NhanVien NhanVien { get; set; }
    }
}

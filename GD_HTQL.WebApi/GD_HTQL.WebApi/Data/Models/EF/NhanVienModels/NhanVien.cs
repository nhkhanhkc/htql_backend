﻿using GD_HTQL.WebApi.Data.Models.Abstract;
using GD_HTQL.WebApi.Data.Models.EF.DuAnModels;
using GD_HTQL.WebApi.Data.Models.EF.SanPhamModels;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GD_HTQL.WebApi.Data.Models.EF.NhanVienModels
{
    public class NhanVien : CommonAbstract
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int NhanVienID { get; set; }
        [StringLength(225)]
        public required string TenNhanVien { get; set; }
        public int? GioiTinh { get; set; }
        public DateTime? NgaySinh { get; set; }
        [StringLength(50)]
        public string? SoDienThoai { get; set; }
        [StringLength(225)]
        public string? Email { get; set; }
        public string? DiaChi { get; set; }
        public bool Active { get; set; }
        public string UserID { get; set; }
        public DateTime? NgayKyHopDong { get; set; }
        public virtual ApplicationUser User { get; set; }
        public virtual ICollection<FileDinhKem>? FileDinhKem { get; set; }
        public virtual ICollection<QuanLyTuongTac>? TuongTac { get; set; }
        public virtual ICollection<LichSuDuToan>? LichSuDuToan { get; set; }
        public virtual ICollection<ChucVu>? ChucVu { get; set; }
    }
}

﻿using GD_HTQL.WebApi.Data.Models.Abstract;
using GD_HTQL.WebApi.Data.Models.EF.DanhMucModels;
using System.ComponentModel.DataAnnotations;
namespace GD_HTQL.WebApi.Data.Models.EF.NhanVienModels
{
    public class CongTy : CommonAbstract
    {
        public int CongTyID { get; set; }
        [StringLength(225)]
        public required string TenCongTy { get; set; }
        [StringLength(50)]
        public required string MaSoThue { get; set; }
        public int TinhID { get; set; }
        public virtual DMTinh? Tinh { get; set; }
        [StringLength(225)]
        public string? DiaChi { get; set; }
        public bool Active { get; set; }
        public ICollection<PhongBan>? PhongBan { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace GD_HTQL.WebApi.Data.Models.EF.NhanVienModels
{
    public class FileDinhKem
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int FileID { get; set; }
        [StringLength(225)]
        public required string FileName { get; set; }
        [StringLength(225)]
        public string? FileType { get; set; }
        public int NhanVienID { get; set; }
        public int LoaiID { get; set; }
        [ForeignKey("LoaiID")]
        public virtual FileLoai? FileLoai { get; set; }
        public string? FileUrl { get; set; }
        public virtual NhanVien? NhanVien { get; set; }
    }
}

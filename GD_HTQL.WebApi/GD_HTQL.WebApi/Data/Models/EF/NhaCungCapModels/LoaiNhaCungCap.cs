﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace GD_HTQL.WebApi.Data.Models.EF.NhaCungCapModels
{
    public class LoaiNhaCungCap
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int LoaiNCCID { get; set; }
        [StringLength(50)]
        public required string TenLoai { get; set; }
        public virtual ICollection<NhaCungCap>? NhaCungCap { get; set; }
    }
}

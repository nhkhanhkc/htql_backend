﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace GD_HTQL.WebApi.Data.Models.EF.DanhMucModels
{
    public class DMKhuVuc
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int KhuVucID { get; set; }
        [StringLength(50)]
        public string TenKhuVuc { get; set; }
        public virtual ICollection<DMTinh>? DMTinh { get; set; }

    }
}

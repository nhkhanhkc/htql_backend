﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using GD_HTQL.WebApi.Data.Models.Abstract;

namespace GD_HTQL.WebApi.Data.Models.EF.NghiPhepModels
{
    public class NghiPhep : CommonAbstract
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int NghiPhepID { get; set; }
        public int NguoiTaoID { get; set; }
        [StringLength(225)]
        public string? HoTen { get; set; }
        public int CongTyID { get; set; }
        [StringLength(225)]
        public string? TenCongTy { get; set; }
        public int ChucVuID { get; set; }
        [StringLength(225)]
        public string? TenChucVu { get; set; }
        public int PhongBanID { get; set; }
        [StringLength(225)]
        public string? TenPhongBan { get; set; }
        [StringLength(500)]
        public string? LyDo { get; set; }
        public float NgayNghi { get; set; }
        public string? Thu { get; set; }
        public DateTime TuNgay { get; set; }
        public DateTime DenNgay { get; set; }
        public int LoaiID { get; set; }
        [ForeignKey("LoaiID")]
        public virtual NghiPhepLoai? NghiPhepLoai { get; set; }
        public virtual ICollection<NghiPhep_LichSu>? NghiPhep_LichSu { get; set; }
    }
}

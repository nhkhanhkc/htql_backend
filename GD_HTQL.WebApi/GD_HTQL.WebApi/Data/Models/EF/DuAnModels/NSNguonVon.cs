﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace GD_HTQL.WebApi.Data.Models.EF.DuAnModels
{
    public class NSNguonVon
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int NguonVonID { get; set; }
        [StringLength(50)]
        public required string TenNguonVon { get; set; }
    }
}

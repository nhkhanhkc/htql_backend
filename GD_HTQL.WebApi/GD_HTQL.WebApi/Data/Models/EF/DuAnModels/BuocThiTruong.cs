﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace GD_HTQL.WebApi.Data.Models.EF.DuAnModels
{
    public class BuocThiTruong
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int BuocThiTruongID { get; set; }
        [StringLength(50)]
        public string BuocThiTruongTen { get; set; }
        public virtual ICollection<QuanLyTuongTac>? QuanLyTuongTac { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using GD_HTQL.WebApi.Data.Models.Abstract;

namespace GD_HTQL.WebApi.Data.Models.EF.VanBanModels
{
    public class VanBan : CommonAbstract
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int VanBanID { get; set; }
        [StringLength(225)]
        public required string TenVanBan { get; set; }
        [StringLength(50)]
        public string? SoVanBan { get; set; }
        public required string NoiDung { get; set; }
        public string? TrichYeu { get; set; }
        public DateTime? NgayBanHanh { get; set; }
        public DateTime? NgayHetHan { get; set; }
        [StringLength(50)]
        public string? NguoiKy { get; set; }
        public int LoaiVanBanID { get; set; }
        public required virtual LoaiVanBan LoaiVanBan { get; set; }
        public virtual ICollection<FileVanBan>? FileVanBan { get; set; }

    }
}

﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace GD_HTQL.WebApi.Data.Models.EF.VanBanModels
{
    public class LoaiVanBan
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int LoaiVanBanID { get; set; }
        [StringLength(50)]
        public required string TenLoaiVanBan { get; set; }
        public string? MoTa { get; set; }
        public virtual ICollection<VanBan>? VanBan { get; set; }
    }
}

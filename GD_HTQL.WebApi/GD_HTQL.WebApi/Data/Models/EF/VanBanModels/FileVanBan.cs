﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace GD_HTQL.WebApi.Data.Models.EF.VanBanModels
{
    public class FileVanBan
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int FileID { get; set; }
        [StringLength(225)]
        public required string FileName { get; set; }
        [StringLength(225)]
        public string? FileType { get; set; }
        public string? FileBase64 { get; set; }
        public int VanBanID { get; set; }
        public virtual VanBan? VanBan { get; set; }
    }
}

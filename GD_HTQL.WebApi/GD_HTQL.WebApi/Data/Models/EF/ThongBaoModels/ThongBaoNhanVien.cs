﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace GD_HTQL.WebApi.Data.Models.EF.ThongBaoModels
{
    public class ThongBaoNhanVien
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }
        public int ThongBaoID { get; set; }
        public int NhanVienID { get; set; }
        public string? TenNhanVien { get; set; }
    }
}

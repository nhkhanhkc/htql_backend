﻿using GD_HTQL.WebApi.Data.Models.Abstract;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GD_HTQL.WebApi.Data.Models.EF.CongTacModels
{
    public class KHCT_Ngay : CommonAbstract
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int NgayID { get; set; }
        public int NguoiTaoID { get; set; }
        [StringLength(225)]
        public string? HoTen { get; set; }
        public int ChucVuID { get; set; }
        [StringLength(225)]
        public string? TenChucVu { get; set; }
        [StringLength(225)]
        public int PhongBanID { get; set; }
        [StringLength(225)]
        public string? TenPhongBan { get; set; }
        public int CongTyID { get; set; }
        [StringLength(225)]
        public string? TenCongTy { get; set; }
        public DateTime TuNgay { get; set; }
        public DateTime DenNgay { get; set; }
        [DefaultValue(true)]
        public string? MucDich { get; set; }
        public bool? Active { get; set; }
        public int? KHCTCongTyID { get; set; }
        [StringLength(225)]
        public string? KHCTTenCongTy { get; set; }
        public int? TuanID { get; set; }
        public virtual KHCT_Tuan? KHCT_Tuan { get; set; }
        public virtual ICollection<KHCT_Ngay_LichSu>? KHCT_Ngay_LichSu { get; set; }
        public virtual ICollection<KHCT_NguoiDiCung>? KHCT_NguoiDiCung { get; set; }
        public virtual ICollection<KHCT_NoiDung>? KHCT_NoiDung { get; set; }
        public virtual ICollection<KHCT_Xe>? KHCT_Xe { get; set; }
        public virtual ICollection<KHCT_ChiPhi>? KHCT_ChiPhi { get; set; }
        public virtual ICollection<KHCT_ChiPhiKhac>? KHCT_ChiPhiKhac { get; set; }
    }
}

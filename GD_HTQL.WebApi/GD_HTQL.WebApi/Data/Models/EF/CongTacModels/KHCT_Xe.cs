﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace GD_HTQL.WebApi.Data.Models.EF.CongTacModels
{
    public class KHCT_Xe
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int ID { get; set; }
        [StringLength(225)]
        public string? NoiDi { get; set; }
        [StringLength(225)]
        public string? NoiDen { get; set; }
        public int KmTamTinh { get; set; }
        public DateTime? NgaySuDung { get; set; }
        public string? GhiChu { get; set; }
        public int NgayID { get; set; }
        [ForeignKey("NgayID")]
        public virtual KHCT_Ngay? KHCT_Ngay { get; set; }
    }
}

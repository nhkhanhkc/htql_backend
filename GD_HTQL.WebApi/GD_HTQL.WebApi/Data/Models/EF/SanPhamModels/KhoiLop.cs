﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GD_HTQL.WebApi.Data.Models.EF.SanPhamModels
{
    public class KhoiLop
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int KhoiLopID { get; set; }
        [StringLength(225)]
        public required string TenKhoiLop { get; set; }
        public virtual ICollection<SanPham>? SanPham { get; set; }
        [DefaultValue(true)]
        public bool? Active { get; set; }
    }
}

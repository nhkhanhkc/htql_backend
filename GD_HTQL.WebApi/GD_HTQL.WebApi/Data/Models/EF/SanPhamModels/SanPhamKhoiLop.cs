﻿namespace GD_HTQL.WebApi.Data.Models.EF.SanPhamModels
{
    public class SanPhamKhoiLop
    {
        public int SanPhamID { get; set; }
        public int KhoiLopID { get; set; }
    }
}

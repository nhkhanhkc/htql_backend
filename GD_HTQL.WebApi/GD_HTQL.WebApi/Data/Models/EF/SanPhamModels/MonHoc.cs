﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace GD_HTQL.WebApi.Data.Models.EF.SanPhamModels
{
    public class MonHoc
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int MonHocID { get; set; }
        [StringLength(225)]
        public required string TenMonHoc { get; set; }
        public virtual ICollection<SanPham>? SanPham { get; set; }
        [DefaultValue(true)]
        public bool? Active { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace GD_HTQL.WebApi.Data.Models.EF.SanPhamModels
{
    public class SanPhamDuToan
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int ID { get; set; }
        //public int DuToanID { get; set; }
        //public virtual DuToan? DuToan { get; set; }
        public string? MaSanPham { get; set; }
        [StringLength(225)]
        public required string TenSanPham { get; set; }

        [DisplayFormat(HtmlEncode = true)]
        public required string TieuChuanKyThuat { get; set; }
        [StringLength(225)]
        public string? HangSanXuat { get; set; }
        [StringLength(225)]
        public string? XuatXu { get; set; }
        public int? BaoHanh { get; set; }
        public DateTime? NamSanXuat { get; set; }
        [StringLength(225)]
        public string? NhaXuatBan { get; set; }
        public string? DonViTinh { get; set; }
        [Column(TypeName = "decimal(18,0)")]
        public decimal? GiaVon { get; set; }
        [Column(TypeName = "decimal(18,0)")]
        public decimal? GiaBan { get; set; }

        [Column(TypeName = "decimal(18,0)")]
        public decimal? Thue { get; set; }
        public int? SoLuong { get; set; }
        public string? DoiTuongSuDung { get; set; }
        public string? ThuongHieu { get; set; }
        public string? HinhAnh { get; set; }
        public string? TenLoaiSanPham { get; set; }
        public string? TenMonHoc { get; set; }
        public string? TenKhoiLop { get; set; }
        public string? TenThongTu { get; set; }
    }
}

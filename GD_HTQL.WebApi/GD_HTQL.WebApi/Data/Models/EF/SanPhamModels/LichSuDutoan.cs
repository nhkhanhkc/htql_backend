﻿using System.ComponentModel.DataAnnotations.Schema;
using GD_HTQL.WebApi.Data.Models.EF.NhanVienModels;

namespace GD_HTQL.WebApi.Data.Models.EF.SanPhamModels
{
    public class LichSuDuToan
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int LichSuDuToanID { get; set; }
        public int NhanVienID { get; set; }
        public virtual NhanVien? NhanVien { get; set; }
        public int DuToanID { get; set; }
        public virtual DuToan? DuToan { get; set; }
        public int TrangThaiDuToanID { get; set; }
        public virtual TrangThaiDuToan? TrangThaiDuToan { get; set; }
        public DateTime ThoiGan { get; set; }
        public string? GhiChu { get; set; }

    }
}

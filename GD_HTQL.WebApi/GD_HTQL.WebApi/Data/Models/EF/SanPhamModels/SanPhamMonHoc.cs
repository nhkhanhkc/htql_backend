﻿namespace GD_HTQL.WebApi.Data.Models.EF.SanPhamModels
{
    public class SanPhamMonHoc
    {
        public int SanPhamID { get; set; }
        public int MonHocID { get; set; }
    }
}

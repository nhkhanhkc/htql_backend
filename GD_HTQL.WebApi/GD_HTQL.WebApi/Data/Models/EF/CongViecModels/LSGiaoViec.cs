﻿using System.ComponentModel.DataAnnotations.Schema;
using GD_HTQL.WebApi.Data.Models.EF.NhanVienModels;

namespace GD_HTQL.WebApi.Data.Models.EF.CongViecModels
{
    public class LSGiaoViec
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int LSGiaoViecID { get; set; }
        public int NhanVienID { get; set; }
        public virtual NhanVien? NhanVien { get; set; }
        public int CongViecID { get; set; }
        public virtual CongViec? CongViec { get; set; }
        public DateTime ThoiGan { get; set; }

        // Loại = 0 Công việc mới tạo ; loại = 1 công việc đã giao
        public int Loai { get; set; }
    }
}

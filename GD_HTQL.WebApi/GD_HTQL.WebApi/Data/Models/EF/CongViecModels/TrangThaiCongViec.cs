﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace GD_HTQL.WebApi.Data.Models.EF.CongViecModels
{
    public class TrangThaiCongViec
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int TrangThaiID { get; set; }
        [StringLength(225)]
        public required string TenTrangThai { get; set; }
    }
}

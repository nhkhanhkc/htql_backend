﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using GD_HTQL.WebApi.Data.Models.EF.NhanVienModels;

namespace GD_HTQL.WebApi.Data.Models.EF.CongViecModels
{
    public class LichSuCongViec
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int LSID { get; set; }
        public int NhanVienID { get; set; }
        [ForeignKey("NhanVienID")]
        public virtual NhanVien? NhanVien { get; set; }
        public int CongViecID { get; set; }
        public virtual CongViec? CongViec { get; set; }
        public string? MoTa { get; set; }
        public DateTime ThoiGian { get; set; }
        public int TrangThaiID { get; set; }
        [ForeignKey("TrangThaiID")]
        public virtual TrangThaiCongViec? TrangThaiCongViec { get; set; }
        public int? TienDo { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace GD_HTQL.WebApi.Data.Models.EF.NhaThauModels
{
    public class LoaiNhaThau
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int LoaiNTID { get; set; }
        [StringLength(50)]
        public required string TenLoai { get; set; }
        public virtual ICollection<NhaThau>? NhaThau { get; set; }
    }
}

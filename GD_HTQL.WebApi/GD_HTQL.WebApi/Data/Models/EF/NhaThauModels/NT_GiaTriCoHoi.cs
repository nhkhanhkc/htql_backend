﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using GD_HTQL.WebApi.Data.Models.EF.NhanVienModels;
using GD_HTQL.WebApi.Data.Models.Abstract;
using GD_HTQL.WebApi.Data.Models.EF.SanPhamModels;

namespace GD_HTQL.WebApi.Data.Models.EF.NhaThauModels
{
    public class NT_GiaTriCoHoi : CommonAbstract
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int GTCHID { get; set; }
        public int NhaThauID { get; set; }
        public virtual NhaThau NhaThau { get; set; }
        public DateTime? ThoiGianTiepXuc { get; set; }
        [StringLength(500)]
        public string? CanBoTiepXuc { get; set; }
        [StringLength(500)]
        public string? SoDienThoai { get; set; }
        [StringLength(500)]
        public virtual ICollection<LoaiSanPham>? LoaiSanPham { get; set; }
        public int NguonVonID { get; set; }
        public virtual NguonVon? NguonVon { get; set; }
        public bool IsQuanTamHopTac { get; set; }
        public bool IsKyHopDongNguyenTac { get; set; }
        public string? LyDoKhongHopTac { get; set; }
        public string? GhiChu { get; set; }
        public int NhanVienID { get; set; }
        [ForeignKey("NhanVienID")]
        public virtual NhanVien? NhanVien { get; set; }
        public int? TheID { get; set; }
        [ForeignKey("TheID")]
        public virtual NT_The? NT_The { get; set; }
        [DefaultValue(true)]
        public bool? Active { get; set; }

    }
}

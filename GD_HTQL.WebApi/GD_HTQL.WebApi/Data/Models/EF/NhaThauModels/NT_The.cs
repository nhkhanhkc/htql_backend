﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace GD_HTQL.WebApi.Data.Models.EF.NhaThauModels
{
    public class NT_The
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int ID { get; set; }
        [StringLength(225)]
        public required string TenThe { get; set; }
    }
}

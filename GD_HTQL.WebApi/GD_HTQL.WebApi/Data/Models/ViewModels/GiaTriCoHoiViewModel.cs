﻿using GD_HTQL.WebApi.Data.Models.Dtos.NhanVien;
using GD_HTQL.WebApi.Data.Models.Dtos.NhaThau;
using GD_HTQL.WebApi.Data.Models.Dtos.SanPham;
using GD_HTQL.WebApi.Data.Models.EF.NhaThauModels;

namespace GD_HTQL.WebApi.Data.Models.ViewModels
{
    public class GiaTriCoHoiViewModel
    {
        public int GTCHID { get; set; }
        public int NhaThauID { get; set; }
        public virtual NhaThauDto? NhaThau { get; set; }
        public DateTime? ThoiGianTiepXuc { get; set; }
        public string? CanBoTiepXuc { get; set; }
        public string? SoDienThoai { get; set; }
        public int NguonVonID { get; set; }
        public virtual NguonVonDto? NguonVon { get; set; }
        public bool IsQuanTamHopTac { get; set; }
        public bool IsKyHopDongNguyenTac { get; set; }
        public string? LyDoKhongHopTac { get; set; }
        public string? GhiChu { get; set; }
        public int NhanVienID { get; set; }
        public int? TheID { get; set; }
        public virtual NT_The? NT_The { get; set; }
        public virtual NhanVienDto? NhanVien { get; set; }
        public virtual ICollection<ChucVuView>? lstChucVuView { get; set; } = new List<ChucVuView>();
        public virtual ICollection<LoaiSanPhamDto>? LoaiSanPham { get; set; }
    }
}

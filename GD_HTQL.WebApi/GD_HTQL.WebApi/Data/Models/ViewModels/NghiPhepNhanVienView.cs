﻿namespace GD_HTQL.WebApi.Data.Models.ViewModels
{
    public class NghiPhepNhanVienView
    {
        public int NhanVienID { get; set; }
        public float? PhepNam { get; set; }
        public float? PhepDuocSuDung { get; set; }
        public float? PhepDaSuDung { get; set; }
        public float? PhepCoTheSuDung { get; set; }
    }
}

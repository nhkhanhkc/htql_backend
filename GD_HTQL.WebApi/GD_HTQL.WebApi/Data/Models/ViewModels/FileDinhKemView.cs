﻿using GD_HTQL.WebApi.Data.Models.EF.NhanVienModels;

namespace GD_HTQL.WebApi.Data.Models.ViewModels
{
    public class FileDinhKemView
    {
        public int FileID { get; set; }
        public required string FileName { get; set; }
        public string? FileType { get; set; }
        public string? FileUrl { get; set; }
        public int LoaiID { get; set; }
        public virtual FileLoai? FileLoai { get; set; }
    }
}

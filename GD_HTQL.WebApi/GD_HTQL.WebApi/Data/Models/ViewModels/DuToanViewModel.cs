﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace GD_HTQL.WebApi.Data.Models.ViewModels
{
    public class DuToanViewModel
    {
            
        public int DuToanID { get; set; }
        [StringLength(225)]
        public required string TenDuToan { get; set; }
        public string? GhiChu { get; set; }
        public string? SanPhamDuToan { get; set; }
        public string? LoaiGia { get; set; }
        [Column(TypeName = "decimal(18,0)")]
        public decimal? TongGiaVon { get; set; }
        [Column(TypeName = "decimal(18,0)")]
        public decimal? TongGiaBan { get; set; }     
        public int? NguoiTaoID { get; set; }
        public string? TenNguoiTao { get; set; }
        public string? CreateBy  { get; set; }
        public int TrangThaiID { get; set; }
        public string? TenTrangThai { get; set; }
        public int? NguoiDuyetID { get; set; }
        public string? TenNguoiDuyet { get; set; }
    }
}

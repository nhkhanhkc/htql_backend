﻿using GD_HTQL.WebApi.Data.Models.Dtos.CongTac;

namespace GD_HTQL.WebApi.Data.Models.ViewModels
{
    public class KHCT_Ngay_ShortView
    {
        public int NgayID { get; set; }
        public int NguoiTaoID { get; set; }
        public string? HoTen { get; set; }
        public int ChucVuID { get; set; }
        public string? TenChucVu { get; set; }
        public int PhongBanID { get; set; }
        public string? TenPhongBan { get; set; }
        public int CongTyID { get; set; }
        public string? TenCongTy { get; set; }
        public DateTime TuNgay { get; set; }
        public DateTime DenNgay { get; set; }
        public string? MucDich { get; set; }
        public int? TuanID { get; set; }
        public int? KHCTCongTyID { get; set; }
        public string? KHCTTenCongTy { get; set; }
        public virtual KHCT_TuanDto? KHCT_Tuan { get; set; }
        public virtual List<KHCT_Ngay_LichSuDto>? KHCT_Ngay_LichSu { get; set; }
    }
}

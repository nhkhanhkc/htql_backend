﻿using GD_HTQL.WebApi.Data.Models.Dtos.DanhMuc;
using GD_HTQL.WebApi.Data.Models.Dtos.NhaThau;
using GD_HTQL.WebApi.Data.Models.EF.NhaThauModels;
namespace GD_HTQL.WebApi.Data.Models.ViewModels
{
    public class NT_DuToanView
    {      
        public int DuToanID { get; set; }
        public required string TenDuToan { get; set; }
        public string? DiaChi { get; set; }
        public string? FileNhaThau { get; set; }
        public string? TenFileNhaThau { get; set; }
        public string? FileCongTy { get; set; }
        public string? TenFileCongTy { get; set; }
        public decimal? DoanhThuDuKien { get; set; }
        public string? GhiChu { get; set; }
        public int? TinhID { get; set; }
        public virtual DMTinhDto? Tinh { get; set; }
        public int TrangThaiID { get; set; }
        public virtual NT_TrangThaiDuToan? NT_TrangThaiDuToan { get; set; }
        public int NhaThauID { get; set; }
        public virtual NhaThauDto? NhaThau { get; set; }
        public DateTime? CreateDate { set; get; }
        public string? CreateBy { set; get; }
        public DateTime? UpdateDate { set; get; }
        public string? UpdateBy { set; get; }
        public bool? KetQua { get; set; }
        public string? LyDoThatBai { get; set; }
        public int NhanVienID { get; set; }
        public NhanVienShortView? NhanVien { get; set; }
    }
}

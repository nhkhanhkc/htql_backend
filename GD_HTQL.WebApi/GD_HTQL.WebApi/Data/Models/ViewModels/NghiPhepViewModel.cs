﻿using GD_HTQL.WebApi.Data.Models.Dtos;
using GD_HTQL.WebApi.Data.Models.EF.NghiPhepModels;
using System.ComponentModel.DataAnnotations;

namespace GD_HTQL.WebApi.Data.Models.ViewModels
{
    public class NghiPhepViewModel
    {
        public int NghiPhepID { get; set; }
        public int NguoiTaoID { get; set; }
        [StringLength(225)]
        public string? HoTen { get; set; }
        public int CongTyID { get; set; }
        [StringLength(225)]
        public string? TenCongTy { get; set; }
        public int ChucVuID { get; set; }
        [StringLength(225)]
        public string? TenChucVu { get; set; }
        public int PhongBanID { get; set; }
        [StringLength(225)]
        public string? TenPhongBan { get; set; }
        [StringLength(500)]
        public string? LyDo { get; set; }
        public float NgayNghi { get; set; }
        public string? Thu { get; set; }
        public DateTime TuNgay { get; set; }
        public DateTime DenNgay { get; set; }
        public DateTime CreateDate { get; set; }
        public int LoaiID { get; set; }
        public virtual NghiPhepLoai? NghiPhepLoai { get; set; }
        public virtual ICollection<NghiPhep_LichSuView>? NghiPhep_LichSu { get; set; }
        public float? PhepNam { get; set; }
        public float? PhepDuocSuDung { get; set; }
        public float? PhepDaSuDung { get; set; }
        public float? PhepCoTheSuDung { get; set; }
    }
}

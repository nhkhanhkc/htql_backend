﻿using System.ComponentModel.DataAnnotations;

namespace GD_HTQL.WebApi.Data.Models.ViewModels
{
    public class NhanVienViewModel
    {
        public int NhanVienID { get; set; }      
        public required string TenNhanVien { get; set; }
        public string? GioiTinh { get; set; }
        public string? NgaySinh { get; set; }     
        public string? SoDienThoai { get; set; }
        public string? Email { get; set; }
        public string? DiaChi { get; set; }
        public string? Avartar { get; set; }
        public bool Active { get; set; }
        public string? NgayKyHopDong { get; set; }
        public virtual ICollection<ChucVuView>? lstChucVuView { get; set; }

    }
}

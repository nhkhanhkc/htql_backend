﻿using System.ComponentModel.DataAnnotations.Schema;

namespace GD_HTQL.WebApi.Data.Models.ViewModels
{
    public class TuongTacViewModel
    {
        public int TuongTacID { get; set; }     
        public string? TenCoQuan { get; set; }
        public string? TenTinh { get; set; }
        public DateTime? ThoiGian { get; set; }
        public string? ThongTinLienHe { get; set; }
        public string? BuocThiTruong { get; set; }
        public string? ThongTinTiepXuc { get; set; }
        public string? CanBoTiepXuc { get; set; }
        public string? NhomHangQuanTam { get; set; }
        public string? GhiChu { get; set; }
        public string? TenNhanVien {  get; set; }
        public int NhanVienID { get; set; }

        [Column(TypeName = "decimal(18,0)")]
        public decimal DoanhThuDuKien { get; set; }
        public int? NguonVonID { get; set; }
        public string? TenNguonVon { get; set; }
        public DateTime? ThoiGianKetThucDuKien { get; set; }
    }
}

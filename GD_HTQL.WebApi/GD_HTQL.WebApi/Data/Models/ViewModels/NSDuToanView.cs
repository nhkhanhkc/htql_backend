﻿using GD_HTQL.WebApi.Data.Models.Dtos.DuAn;
using System.ComponentModel.DataAnnotations;

namespace GD_HTQL.WebApi.Data.Models.ViewModels
{
    public class NSDuToanView
    {
        public int DuToanID { get; set; }     
        public required string TenDuToan { get; set; }    
        public string? FileCoQuan { get; set; }      
        public string? TenFileCoQuan { get; set; }    
        public string? FileCongTy { get; set; }      
        public string? TenFileCongTy { get; set; }      
        public decimal? DoanhThuDuKien { get; set; }
        public string? GhiChu { get; set; }
        public DateTime? ThoiGian { get; set; }
        public int BuocThiTruongID { get; set; }      
        public virtual BuocThiTruongDto? BuocThiTruong { get; set; }
        public int CoQuanID { get; set; }
        public virtual NSCoQuanDto? NSCoQuan { get; set; }
        public int NhanVienID { get; set; }      
        public virtual NhanVienShortView? NhanVien { get; set; }
        public bool? KetQua { get; set; }
        [StringLength(500)]
        public string? LyDoThatBai { get; set; }
        public decimal? DoanhThuThucTe { get; set; }
        public DateTime? ThoiGianKetThucDuKien { get; set; }
    }
}

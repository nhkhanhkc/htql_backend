﻿using GD_HTQL.WebApi.Data.Models.Dtos.DanhMuc;
using GD_HTQL.WebApi.Data.Models.Dtos.DuAn;

namespace GD_HTQL.WebApi.Data.Models.ViewModels
{
    public class NSCoQuanViewModel
    {
        public int CoQuanID { get; set; }
        public required string TenCoQuan { get; set; }
        public string? MaSoThue { get; set; }
        public int? TinhID { get; set; }     
        public int? HuyenID { get; set; }
        public int? XaID { get; set; }
        public virtual DMHuyenDto? Huyen { get; set; }
        public string? DiaChi { get; set; }
        public virtual ICollection<NSCanBoDto>? NSCanBo { get; set; }
        public virtual ICollection<QLTuongTacView>? TuongTac { get; set; }
        public int? NhanVienID { get; set; }
        public virtual NhanVienShortView? NhanVien { get; set; }
        public virtual ICollection<NSDuToanView>? NSDuToan { get; set; }
        public virtual DMTinhDto? Tinh { get; set; }
    }
}

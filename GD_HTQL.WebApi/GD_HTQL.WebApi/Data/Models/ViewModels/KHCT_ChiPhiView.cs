﻿using System.ComponentModel.DataAnnotations.Schema;
using GD_HTQL.WebApi.Data.Models.EF.CongTacModels;

namespace GD_HTQL.WebApi.Data.Models.ViewModels
{
    public class KHCT_ChiPhiView
    {
        public int ID { get; set; }
        public int SoLuong { get; set; }    
        public decimal? DonGia { get; set; }      
        public decimal? ThanhTien { get; set; }
        public string? GhiChu { get; set; }
        public int LoaiChiPhiID { get; set; }      
        public virtual KHCT_LoaiChiPhi? KHCT_LoaiChiPhi { get; set; }
        public int HangMucChiPhiID { get; set; }    
        public virtual KHCT_HangMucChiPhi? KHCT_HangMucChiPhi { get; set; }
    }
}

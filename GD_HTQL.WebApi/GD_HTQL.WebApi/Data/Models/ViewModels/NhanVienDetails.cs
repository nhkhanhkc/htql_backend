﻿using GD_HTQL.WebApi.Data.Models.Dtos;

namespace GD_HTQL.WebApi.Data.Models.ViewModels
{
    public class NhanVienDetails
    {     
        public int NhanVienID { get; set; } 
        public required string TenNhanVien { get; set; }
        public string? GioiTinh { get; set; }
        public DateTime? NgaySinh { get; set; }
        public DateTime? NgayKyHopDong { get; set; }
        public string? SoDienThoai { get; set; }
        public string? Email { get; set; }
        public string? DiaChi { get; set; }
        public bool Active { get; set; }
        public virtual ICollection<FileDinhKemView>? lstFile { get; set; }
        public virtual ICollection<ChucVuView> lstChucVuView { get; set; } = new List<ChucVuView>();
    }
}

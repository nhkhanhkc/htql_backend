﻿namespace GD_HTQL.WebApi.Data.Models.ImportModels
{
    public class ImportError
    {
        public string STT { get; set; }
        public string MaLoi { get; set; }
    }
}

﻿namespace GD_HTQL.WebApi.Data.Models.ImportModels
{
    public class ImportResult
    {
        public List<ImportError>? ImportError { get; set; }
        public int Error { get; set; }
        public int Success { get; set; }
    }
}

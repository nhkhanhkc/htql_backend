﻿using GD_HTQL.WebApi.Data.Models.Dtos;
using GD_HTQL.WebApi.Data.Models.ViewModels;

namespace GD_HTQL.WebApi.Data.Models.ImportModels
{
    public class ImportSanPhamDuToanResult
    {
        public List<ImportError>? ImportError { get; set; }
        public List<SanPhamViewDto>? lstSanPham { get; set; }
        public int Error { get; set; }
        public int Success { get; set; }
    }
}

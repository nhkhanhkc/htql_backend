﻿namespace GD_HTQL.WebApi.Data.Models.Dtos.NhanVien
{
    public class FileDinhKemDto
    {
        public int FileID { get; set; }
        public required string FileName { get; set; }
        public string? FileType { get; set; }
        public string? FileUrl { get; set; }
        public int LoaiID { get; set; }

    }
}

﻿namespace GD_HTQL.WebApi.Data.Models.Dtos.NhanVien
{
    public class NhanVienDto
    {
        public int NhanVienID { get; set; }
        public string TenNhanVien { get; set; }
        public int? GioiTinh { get; set; }
        public DateTime? NgaySinh { get; set; }
        public string? SoDienThoai { get; set; }
        public string? Email { get; set; }
        public string? DiaChi { get; set; }
        public DateTime? NgayKyHopDong { get; set; }
    }
}

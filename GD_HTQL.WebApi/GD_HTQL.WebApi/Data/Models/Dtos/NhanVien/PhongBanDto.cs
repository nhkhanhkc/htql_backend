﻿namespace GD_HTQL.WebApi.Data.Models.Dtos.NhanVien
{
    public class PhongBanDto
    {
        public int PhongBanID { get; set; }
        public string TenPhongBan { get; set; }
        public string? ChiTiet { get; set; }
        public int CongTyID { get; set; }
    }
}

﻿namespace GD_HTQL.WebApi.Data.Models.Dtos.NhanVien
{
    public class ViTriDto
    {
        public int ViTriID { get; set; }
        public float ViDo { get; set; }
        public float KinhDo { get; set; }
        public string DiaChi { get; set; }
        public DateTime ThoiGian { get; set; }
        public int NhanVienID { get; set; }
    }
}

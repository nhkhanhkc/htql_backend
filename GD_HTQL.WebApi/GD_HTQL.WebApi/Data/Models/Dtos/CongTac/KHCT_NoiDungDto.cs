﻿namespace GD_HTQL.WebApi.Data.Models.Dtos.CongTac
{
    public class KHCT_NoiDungDto
    {
        public int ID { get; set; }
        public string? NoiDen { get; set; }
        public string? TenBuocThiTruong { get; set; }
        public string? ChiTiet { get; set; }
        public DateTime? NgayThucHien { get; set; }
        public string? GhiChu { get; set; }
        public int CoQuanID { get; set; }
        public int NgayID { get; set; }
    }
}

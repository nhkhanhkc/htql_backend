﻿using System.ComponentModel.DataAnnotations.Schema;

namespace GD_HTQL.WebApi.Data.Models.Dtos.CongTac
{
    public class KHCT_ChiPhiKhacDto
    {
        public int ID { get; set; }
        public string? HangMuc { get; set; }
        public int SoLuong { get; set; }
        [Column(TypeName = "decimal(18,0)")]
        public decimal? DonGia { get; set; }
        [Column(TypeName = "decimal(18,0)")]
        public decimal? ThanhTien { get; set; }
        public string? GhiChu { get; set; }
        public int NgayID { get; set; }
    }
}

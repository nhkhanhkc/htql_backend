﻿using System.ComponentModel.DataAnnotations;

namespace GD_HTQL.WebApi.Data.Models.Dtos.CongTac
{
    public class KHCT_TuanDto
    {
        public int TuanID { get; set; }
        public string? TieuDe { get; set; }
        public string? NoiDung { get; set; }
        public int Tuan_Thang { get; set; }
        public int Nam { get; set; }
        public int ThangID { get; set; }
        public int? NguoiDuyetID { get; set; }
        public string? NguoiDuyet { get; set; }
        public DateTime TuNgay { get; set; }
        public DateTime DenNgay { get; set; }

    }
}

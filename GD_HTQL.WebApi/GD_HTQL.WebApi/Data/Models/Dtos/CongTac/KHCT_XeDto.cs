﻿using System.ComponentModel.DataAnnotations;

namespace GD_HTQL.WebApi.Data.Models.Dtos.CongTac
{
    public class KHCT_XeDto
    {
        public int ID { get; set; }
        public string? NoiDi { get; set; }
        public string? NoiDen { get; set; }
        public int KmTamTinh { get; set; }
        public DateTime? NgaySuDung { get; set; }
        public string? GhiChu { get; set; }
        public int NgayID { get; set; }
    }
}

﻿using GD_HTQL.WebApi.Data.Models.EF.CongTacModels;
using GD_HTQL.WebApi.Data.Models.ViewModels;

namespace GD_HTQL.WebApi.Data.Models.Dtos.CongTac
{
    public class KHCT_Ngay_LichSuDto
    {
        public int LSID { get; set; }
        public int NhanVienID { get; set; }
        public virtual NhanVienShortView? NhanVien { get; set; }
        public DateTime ThoiGan { get; set; }
        public int TrangThaiID { get; set; }
        public virtual KHCT_Ngay_TrangThai? KHCT_Ngay_TrangThai { get; set; }
        public int? NguoiDuyetID { get; set; }
        public string? TenNguoiDuyet { get; set; }
    }
}

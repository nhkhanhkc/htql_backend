﻿using System.ComponentModel.DataAnnotations;

namespace GD_HTQL.WebApi.Data.Models.Dtos.DaiLy
{
    public class LoaiDaiLyDto
    {
        public int LoaiDLID { get; set; }
        public required string TenLoai { get; set; }
    }
}

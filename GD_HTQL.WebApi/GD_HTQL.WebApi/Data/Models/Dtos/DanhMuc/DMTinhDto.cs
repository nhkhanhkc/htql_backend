﻿namespace GD_HTQL.WebApi.Data.Models.Dtos.DanhMuc
{
    public class DMTinhDto
    {
        public int TinhID { get; set; }
        public string TenTinh { get; set; }
        public int KhuVucID { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace GD_HTQL.WebApi.Data.Models.Dtos.DanhMuc
{
    public class KhuVucDto
    {
        public int KhuVucID { get; set; }
        public string TenKhuVuc { get; set; }
    }
}

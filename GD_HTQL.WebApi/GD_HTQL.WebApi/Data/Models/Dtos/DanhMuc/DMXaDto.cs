﻿namespace GD_HTQL.WebApi.Data.Models.Dtos.DanhMuc
{
    public class DMXaDto
    {
        public int XaID { get; set; }
        public string TenXa { get; set; }
        public int HuyenID { get; set; }
    }
}

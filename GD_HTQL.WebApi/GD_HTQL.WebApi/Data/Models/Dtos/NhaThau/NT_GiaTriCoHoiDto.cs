﻿using GD_HTQL.WebApi.Data.Models.Dtos.SanPham;

namespace GD_HTQL.WebApi.Data.Models.Dtos.NhaThau
{
    public class NT_GiaTriCoHoiDto
    {
        public int GTCHID { get; set; }
        public int NhaThauID { get; set; }
        public DateTime? ThoiGianTiepXuc { get; set; }
        public string? CanBoTiepXuc { get; set; }
        public string? SoDienThoai { get; set; }
        public string? NhomHangQuanTam { get; set; }
        public int NguonVonID { get; set; }
        public bool IsQuanTamHopTac { get; set; }
        public bool IsKyHopDongNguyenTac { get; set; }
        public string? LyDoKhongHopTac { get; set; }
        public string? GhiChu { get; set; }
        public int? TheID { get; set; }
        public virtual ICollection<LoaiSanPhamDto>? LoaiSanPhamDto { get; set; }
    }
}

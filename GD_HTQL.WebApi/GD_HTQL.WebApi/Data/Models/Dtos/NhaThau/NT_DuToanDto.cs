﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace GD_HTQL.WebApi.Data.Models.Dtos.NhaThau
{
    public class NT_DuToanDto
    {
        public int DuToanID { get; set; }
        public required string TenDuToan { get; set; }
        public string? DiaChi { get; set; }
        public string? TenFileNhaThau { get; set; }
        public string? FileNhaThau { get; set; }
        public string? TenFileCongTy { get; set; }
        public string? FileCongTy { get; set; }
        public decimal? DoanhThuDuKien { get; set; }
        public string? GhiChu { get; set; }
        public int? TinhID { get; set; }
        public int TrangThaiID { get; set; }
        public int NhaThauID { get; set; }
        public bool? KetQua { get; set; }
        public string? LyDoThatBai { get; set; }
    }
}

﻿namespace GD_HTQL.WebApi.Data.Models.Dtos.NhaThau
{
    public class NhaThauDto
    {
        public int NhaThauID { get; set; }
        public required string TenCongTy { get; set; }
        public string? MaSoThue { get; set; }
        public string? DiaChi { get; set; }
        public string? NguoiDaiDien { get; set; }
        public string? NDDChucVu { get; set; }
        public string? NDDSoDienThoai { get; set; }
        public string? GhiChu { get; set; }
        public int? TinhID { get; set; }
        public string? NhanVienPhuTrach { get; set; }
        public string? ChucVu { get; set; }
        public string? SoDienThoai { get; set; }
        public int LoaiNTID { get; set; }
        public string? Email { get; set; }
        public int? LoaiHopTacID { get; set; }
        public int? DiaBanID { get; set; }

    }
}

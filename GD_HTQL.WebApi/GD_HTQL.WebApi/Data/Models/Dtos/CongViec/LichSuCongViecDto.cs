﻿using System.ComponentModel.DataAnnotations.Schema;

namespace GD_HTQL.WebApi.Data.Models.Dtos.CongViec
{
    public class LichSuCongViecDto
    {
        public int LSID { get; set; }
        public int CongViecID { get; set; }
        public string? MoTa { get; set; }
        public int TrangThaiID { get; set; }
        public int? TienDo { get; set; }
    }
}

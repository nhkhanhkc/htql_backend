﻿using System.ComponentModel.DataAnnotations;

namespace GD_HTQL.WebApi.Data.Models.Dtos.DuAn
{
    public class BuocThiTruongDto
    {
        public int BuocThiTruongID { get; set; }
        public string BuocThiTruongTen { get; set; }
    }
}

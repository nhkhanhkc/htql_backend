﻿using System.ComponentModel.DataAnnotations.Schema;

namespace GD_HTQL.WebApi.Data.Models.Dtos.DuAn
{
    public class QuanLyTuongTacDto
    {
        public int TuongTacID { get; set; }
        public int CoQuanID { get; set; }
        public DateTime? ThoiGian { get; set; }
        public string? ThongTinLienHe { get; set; }
        public int BuocThiTruongID { get; set; }
        public string? ThongTinTiepXuc { get; set; }
        public string? CanBoTiepXuc { get; set; }
        public string? NhomHangQuanTam { get; set; }
        [Column(TypeName = "decimal(18,0)")]
        public decimal? DoanhThuDuKien { get; set; }
        public string? GhiChu { get; set; }
        public int? NguonVonID { get; set; }
        public DateTime? ThoiGianKetThucDuKien { get; set; }
    }
}

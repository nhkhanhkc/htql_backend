﻿using System.ComponentModel.DataAnnotations;
namespace GD_HTQL.WebApi.Data.Models.Dtos.SanPham
{
    public class SanPhamDto
    {
        public int SanPhamID { get; set; }
        [StringLength(225)]
        public required string MaSanPham { get; set; }
        [StringLength(225)]
        public required string TenSanPham { get; set; }
        [DisplayFormat(HtmlEncode = true)]
        public required string TieuChuanKyThuat { get; set; }
        [StringLength(225)]
        public string? HangSanXuat { get; set; }
        [StringLength(225)]
        public string? XuatXu { get; set; }
        public int? BaoHanh { get; set; }
        public int? NamSanXuat { get; set; }
        [StringLength(225)]
        public string? NhaXuatBan { get; set; }
        public int? DVTID { get; set; }
        public decimal? GiaVon { get; set; }
        public decimal? GiaDaiLy { get; set; }
        public decimal? GiaTTR { get; set; }
        public decimal? GiaTH_TT { get; set; }
        public decimal? GiaCDT { get; set; }
        public decimal? Thue { get; set; }
        public int? SoLuong { get; set; }
        public string? DoiTuongSuDung { get; set; }
        public string? ThuongHieu { get; set; }
        public string? HinhAnh { get; set; }
        public string? Model { get; set; }
        public int? LoaiSanPhamID { get; set; }
        public virtual ICollection<MonHocDto>? MonHoc { get; set; }
        public virtual ICollection<KhoiLopDto>? KhoiLop { get; set; }
        public virtual ICollection<ThongTuDto>? ThongTu { get; set; }
    }
}

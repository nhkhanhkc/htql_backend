﻿using System.ComponentModel.DataAnnotations;

namespace GD_HTQL.WebApi.Data.Models.Dtos.SanPham
{
    public class KhoiLopDto
    {
        public int KhoiLopID { get; set; }
        public required string TenKhoiLop { get; set; }
    }
}

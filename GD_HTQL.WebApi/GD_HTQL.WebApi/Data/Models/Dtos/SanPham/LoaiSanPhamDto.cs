﻿using System.ComponentModel.DataAnnotations;

namespace GD_HTQL.WebApi.Data.Models.Dtos.SanPham
{
    public class LoaiSanPhamDto
    {
        public int LoaiSanPhamID { get; set; }
        public required string TenLoaiSanPham { get; set; }
    }
}

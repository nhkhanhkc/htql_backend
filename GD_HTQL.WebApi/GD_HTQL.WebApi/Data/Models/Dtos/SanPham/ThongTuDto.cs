﻿using System.ComponentModel.DataAnnotations;

namespace GD_HTQL.WebApi.Data.Models.Dtos.SanPham
{
    public class ThongTuDto
    {
        public int ThongTuID { get; set; }
        public required string TenThongTu { get; set; }
        public string? MoTa { get; set; }
    }
}

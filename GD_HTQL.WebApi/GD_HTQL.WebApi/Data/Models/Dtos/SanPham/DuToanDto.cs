﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GD_HTQL.WebApi.Data.Models.Dtos.SanPham
{
    public class DuToanDto
    {
        public int DuToanID { get; set; }
        [StringLength(225)]
        public required string TenDuToan { get; set; }
        public string? GhiChu { get; set; }
        public string? SanPhamDuToan { get; set; }
        public string? LoaiGia { get; set; }
        [Column(TypeName = "decimal(18,0)")]
        public decimal? TongGiaVon { get; set; }
        [Column(TypeName = "decimal(18,0)")]
        public decimal? TongGiaBan { get; set; }
    }
}

﻿namespace GD_HTQL.WebApi.Data.Models.Dtos.SanPham
{
    public class MoHocViewDto
    {
        public int MonHocID { get; set; }
        public required string TenMonHoc { get; set; }
        public virtual ICollection<SanPhamDto>? SanPham { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace GD_HTQL.WebApi.Data.Models.Dtos.NghiPhep
{
    public class NghiPhepDto
    {
        public int NghiPhepID { get; set; }

        public string? LyDo { get; set; }
        public float NgayNghi { get; set; }
        public string? Thu { get; set; }
        public DateTime TuNgay { get; set; }
        public DateTime DenNgay { get; set; }
        public int? NguoiDuyetID { get; set; }
        public string? TenNguoiDuyet { get; set; }
        public int LoaiID { get; set; }
    }
}

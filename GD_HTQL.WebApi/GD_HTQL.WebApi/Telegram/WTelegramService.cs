﻿using TL;

namespace GD_HTQL.WebApi.Telegram
{
    public sealed class WTelegramService : BackgroundService
    {
        public readonly WTelegram.Client Client;
        public User User => Client.User;
        public string ConfigNeeded = "connecting";
        private readonly IConfiguration _config;
        public WTelegramService(IConfiguration config, ILogger<WTelegramService> logger)
        {
            _config = config;
            WTelegram.Helpers.Log = (lvl, msg) => logger.Log((LogLevel)lvl, msg);
            Client = new WTelegram.Client(what => Config(what));
        }
        public override void Dispose()
        {
            Client.Dispose();
            base.Dispose();
        }
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            ConfigNeeded = await DoLogin(Config("phone_number"));
        }

        public async Task<string> DoLogin(string loginInfo)
        {
            return ConfigNeeded = await Client.Login(loginInfo);
        }
        static string Config(string what)
        {
            if (what == "api_id") return "22171410";
            if (what == "api_hash") return "fccbf305b76b86f0ec66d2a7f201b4e0";
            if (what == "phone_number") return "+84939158155";
            //if (what == "verification_code") { Console.Write("Code: "); return Console.ReadLine(); }
            //if (what == "first_name") return "John";   // if sign-up is required
            //if (what == "last_name") return "Doe";     // if sign-up is required
            //if (what == "password") return "secret!";  // if user has enabled 2FA
            return null;
        }
    }
}

﻿using System;
using System.Globalization;
using System.Security.Claims;

namespace GD_HTQL.WebApi.Common
{
    public class ClassCommon
    {
        private static Random random = new Random();
       
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }
        public static string RemoveUnicode(string text)
        {
            string[] arr1 = new string[] { "á", "à", "ả", "ã", "ạ", "â", "ấ", "ầ", "ẩ", "ẫ", "ậ", "ă", "ắ", "ằ", "ẳ", "ẵ", "ặ",
            "đ",
            "é","è","ẻ","ẽ","ẹ","ê","ế","ề","ể","ễ","ệ",
            "í","ì","ỉ","ĩ","ị",
            "ó","ò","ỏ","õ","ọ","ô","ố","ồ","ổ","ỗ","ộ","ơ","ớ","ờ","ở","ỡ","ợ",
            "ú","ù","ủ","ũ","ụ","ư","ứ","ừ","ử","ữ","ự",
            "ý","ỳ","ỷ","ỹ","ỵ", " "};
            string[] arr2 = new string[] { "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a",
            "d",
            "e","e","e","e","e","e","e","e","e","e","e",
            "i","i","i","i","i",
            "o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o",
            "u","u","u","u","u","u","u","u","u","u","u",
            "y","y","y","y","y", ""};
            for (int i = 0; i < arr1.Length; i++)
            {
                text = text.Replace(arr1[i], arr2[i]);
                text = text.Replace(arr1[i].ToUpper(), arr2[i].ToUpper());
            }
            return text;
        }       
        public static int GetUserID(ClaimsPrincipal User)
        {
            int _nhanVienID = 0;
            if (User.HasClaim(c => c.Type == "NhanVienID"))
            {
                _nhanVienID = int.Parse(User.Claims.FirstOrDefault(c => c.Type == "NhanVienID").Value);
            }
            return _nhanVienID;          
        }

        public static string GetTenNhanVien(ClaimsPrincipal User)
        {
            string _tenNhanVien= "";
            if (User.HasClaim(c => c.Type == "TenNhanVien"))
            {
                _tenNhanVien = User.Claims.FirstOrDefault(c => c.Type == "TenNhanVien").Value;
            }
            return _tenNhanVien;
        }

        public static List<string> GetRoles(ClaimsPrincipal User)
        {        
            var objRoleList =  User.Claims.Where(c => c.Type == ClaimTypes.Role).Select(x=>x.Value).ToList();          
            return objRoleList;
        }
        public static DateTime GetDateStartWeek(int week, int year)
        {
            var start = GetNextMonday(new DateTime(year, 1, 1).AddDays((week - 1) * 7));        
            return start;
        }
        public static DateTime GetDateEndWeek(int week, int year)
        {
            var start = GetNextMonday(new DateTime(year, 1, 1).AddDays((week - 1) * 7));
            var end = start.AddDays(6);
            return end;
        }
        public static DateTime GetNextMonday(DateTime datetime)
        {
            return datetime.AddDays((7 - (int)datetime.DayOfWeek + (int)DayOfWeek.Monday) % 7);
        }

        public static DateTime GetDateStartMonth(int month, int year)
        {
            var first = new DateTime(year, month, 1);
            var last = first.AddMonths(1).AddDays(-1);
            return first;
        }
        public static DateTime GetDateEndMonth(int month, int year)
        {
            var first = new DateTime(year, month, 1);
            var last = first.AddMonths(1).AddDays(-1);
            return last;
        }
        public static int GetNumberWeek(DateTime date)
        {
     
            DateTimeFormatInfo dfi = DateTimeFormatInfo.CurrentInfo;          
            Calendar cal = dfi.Calendar;
            int week = cal.GetWeekOfYear(date, CalendarWeekRule.FirstDay, DayOfWeek.Monday);
            return week;
        }
        public static int GetWeekOfMonth(DateTime date)
        {
            DateTime beginningOfMonth = new DateTime(date.Year, date.Month, 1);

            while (date.Date.AddDays(1).DayOfWeek != CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek)
                date = date.AddDays(1);

            return (int)Math.Truncate((double)date.Subtract(beginningOfMonth).TotalDays / 7f) + 1;
        }      
    }
}

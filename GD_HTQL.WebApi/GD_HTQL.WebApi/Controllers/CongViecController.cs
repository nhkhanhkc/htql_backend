﻿using AutoMapper;
using GD_HTQL.WebApi.Common;
using GD_HTQL.WebApi.Data.Models.CustomModels;
using GD_HTQL.WebApi.Data.Models.Dtos.CongViec;
using GD_HTQL.WebApi.Data.Models.EF.CongViecModels;
using GD_HTQL.WebApi.Infrastructure;
using GD_HTQL.WebApi.Services.IRepository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
namespace GD_HTQL.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CongViecController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ISendTelegramRepository _thongbao;
        public CongViecController(IUnitOfWork unitOfWork, IMapper mapper, ISendTelegramRepository thongbao)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _thongbao = thongbao;
        }
        [HttpGet]
        [Route("GetAll")]
        public async Task<IActionResult> Get()
        {          
            return Ok(await _unitOfWork.CongViecRepos.GetAll());
        }
        [HttpGet]
        [Route("GetDanhSach")]
        public async Task<IActionResult> GetDanhSach()
        {
            return Ok(await _unitOfWork.CongViecRepos.GetDanhSach(0,0,false,false,0));
        }

        [HttpGet]
        [Route("GetTrangThai")]
        public async Task<IActionResult> GetTrangThai()
        {
            return Ok(await _unitOfWork.CongViecRepos.GetTrangThai());
        }
        [HttpGet]
        [Route("GetDoUuTien")]
        public async Task<IActionResult> GetDoUuTien()
        {
            return Ok(await _unitOfWork.CongViecRepos.GetDoUuTien());
        }

        [HttpGet]
        [Route("GetDsByNhom/{ID}")]
        public async Task<IActionResult> GetDsByNhom(int ID)
        {
            return Ok(await _unitOfWork.CongViecRepos.GetDanhSach(0, 0, false, false, ID));
        }
        [HttpGet]
        [Route("GetDSNhanViec")]
        public async Task<IActionResult> GetDSNhanViec()
        {
            var currentUser = HttpContext.User;      
            int  _nhanVienID = ClassCommon.GetUserID(currentUser);
            return Ok(await _unitOfWork.CongViecRepos.GetDanhSach(_nhanVienID,0,false,false,0));
        }
        [HttpGet]
        [Route("GetDSNhanViecByWeek")]
        public async Task<IActionResult> GetDSNhanViecByWeek()
        {
            var currentUser = HttpContext.User;
            int _nhanVienID = ClassCommon.GetUserID(currentUser);
            return Ok(await _unitOfWork.CongViecRepos.GetDanhSach(_nhanVienID, 0, true, false,0));
        }
        [HttpGet]
        [Route("GetDSNhanViecByMonth")]
        public async Task<IActionResult> GetDSNhanViecByMonth()
        {
            var currentUser = HttpContext.User;
            int _nhanVienID = ClassCommon.GetUserID(currentUser);
            return Ok(await _unitOfWork.CongViecRepos.GetDanhSach(_nhanVienID, 0, false, true,0));
        }
        [HttpGet]
        [Route("GetDsGiaoViec")]
        public async Task<IActionResult> GetDsGiaoViec()
        {
            var currentUser = HttpContext.User;
            int _nhanVienID = ClassCommon.GetUserID(currentUser);
            return Ok(await _unitOfWork.CongViecRepos.GetDanhSach(0, _nhanVienID, false, false,0));
        }
        [HttpGet]
        [Route("GetDsGiaoViecByWeek")]
        public async Task<IActionResult> GetDsGiaoViecByWeek()
        {
            var currentUser = HttpContext.User;
            int _nhanVienID = ClassCommon.GetUserID(currentUser);
            return Ok(await _unitOfWork.CongViecRepos.GetDanhSach(0, _nhanVienID, true, false,0));
        }

        [HttpGet]
        [Route("GetDsGiaoViecByMonth")]
        public async Task<IActionResult> GetDsGiaoViecByMonth()
        {
            var currentUser = HttpContext.User;
            int _nhanVienID = ClassCommon.GetUserID(currentUser);
            return Ok(await _unitOfWork.CongViecRepos.GetDanhSach(0,_nhanVienID,false,true,0));
        }

        [HttpGet]
        [Route("GetById/{Id}")]
        public IActionResult GetCongViecById(int Id)
        {
            return Ok( _unitOfWork.CongViecRepos.GetById(Id));
        }
        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> Post( CongViecModel congViecMod)
        {

            // Kiểm tra nhân viên báo cáo tiếp xúc
            int _nguoiThucHien = congViecMod.CongViecDto.NguoiThucHienID;
            int _nhanVienID = 0;
            var currentUser = HttpContext.User;        
            _nhanVienID = ClassCommon.GetUserID(currentUser);
            if (_nhanVienID == 0)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Nhân viên không được tìm thấy");
            }
            else
            {
                var objCongViec = _mapper.Map<CongViec>(congViecMod.CongViecDto);
                objCongViec.CreateDate = DateTime.Now;
                objCongViec.CreateBy = ClassCommon.GetTenNhanVien(currentUser);
                objCongViec.NguoiTaoID = _nhanVienID;
                objCongViec.Active = true;
                var result = await _unitOfWork.CongViecRepos.Add(objCongViec);
                _unitOfWork.Save();
                if (result.CongViecID == 0)
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
                }
                else
                {
                    //LichSuCongViec lichsu = new LichSuCongViec();
                    //lichsu.CongViecID = objCongViec.CongViecID;
                    //lichsu.NhanVienID = _nguoiThucHien;
                    //lichsu.ThoiGian = DateTime.Now;
                    //lichsu.TrangThaiID = 1;
                    //lichsu.MoTa = "";
                    //lichsu.TienDo = 0;
                    //await _unitOfWork.LichSuCongViecRepos.Add(lichsu);
                    //_unitOfWork.Save();

                    // Add file công việc
                    if (congViecMod.FileCongViecDto != null )
                    {
                        await _unitOfWork.FileRepos.AddFileCongViec(congViecMod.FileCongViecDto, result.CongViecID);
                    }
                    // Gửi tin nhắn telegram
                    try
                    {
                        string tieude = objCongViec.CreateBy.ToUpper() + " ĐÃ VỪA TẠO MỚI MỘT CÔNG VIỆC CHO " + objCongViec.NguoiThucHienTen.ToUpper();
                        await _thongbao.SendTele_GiaoViec(objCongViec.CongViecID, tieude, 0);
                    }
                    catch
                    {
                        return Ok(await _unitOfWork.CongViecRepos.GetDetail(result.CongViecID));
                    }                  
                    return Ok(await _unitOfWork.CongViecRepos.GetDetail(result.CongViecID));
                }
            }         
        }
        [HttpPut]
        [Route("Update")]
        public async Task<IActionResult> Put(CongViecModel congViecMod)
        {
            var objCongViec = _mapper.Map<CongViec>(congViecMod.CongViecDto);
            var obj = _unitOfWork.CongViecRepos.GetById(objCongViec.CongViecID);
            objCongViec.Active = true;
            objCongViec.CreateDate = obj.CreateDate;
            objCongViec.CreateBy = obj.CreateBy;
            objCongViec.UpdateDate = DateTime.Now;
            var currentUser = HttpContext.User;
            objCongViec.UpdateBy = ClassCommon.GetTenNhanVien(currentUser);
            objCongViec.NguoiTaoID = ClassCommon.GetUserID(currentUser);
            _unitOfWork.CongViecRepos.Update(objCongViec);
            _unitOfWork.Save();

            if (congViecMod.FileCongViecDto != null)
            {
                await _unitOfWork.FileRepos.AddFileCongViec(congViecMod.FileCongViecDto, objCongViec.CongViecID);
            }
            try
            {
                string tieude = objCongViec.CreateBy.ToUpper() + " ĐÃ VỪA CẬP NHẬT MỘT CÔNG VIỆC CHO " + objCongViec.NguoiThucHienTen.ToUpper();
                await _thongbao.SendTele_GiaoViec(objCongViec.CongViecID, tieude, 0);
            }
            catch
            {
                return Ok("Cập nhật thành công");
            }
            return Ok("Cập nhật thành công");
        }

        [HttpPut]
        [Route("UpdateProgress")]
        public async Task<IActionResult> UpdateProgress(LichSuCongViecDto lsDto)
        {
            var currentUser = HttpContext.User;
            var objLichSu = _mapper.Map<LichSuCongViec>(lsDto);
            if (objLichSu.LSID ==0)
            {
                objLichSu.ThoiGian = DateTime.Now;               
                objLichSu.NhanVienID = ClassCommon.GetUserID(currentUser);
               var result = await _unitOfWork.LichSuCongViecRepos.Add(objLichSu);
                _unitOfWork.Save();
                string tieude = ClassCommon.GetTenNhanVien(currentUser).ToUpper() + " BÁO CÁO TIẾN ĐỘ CÔNG VIỆC";
                await _thongbao.SendTele_GiaoViec(result.CongViecID, tieude, result.LSID);
            }
            else
            {
                objLichSu.ThoiGian = DateTime.Now;             
                objLichSu.NhanVienID = ClassCommon.GetUserID(currentUser);
                _unitOfWork.LichSuCongViecRepos.Update(objLichSu);
                _unitOfWork.Save();
                try
                {
                    string tieude = ClassCommon.GetTenNhanVien(currentUser).ToUpper() + " CẬP NHẬT TIẾN ĐỘ CÔNG VIỆC";
                    await _thongbao.SendTele_GiaoViec(objLichSu.CongViecID, tieude, objLichSu.LSID);
                }
                catch
                {
                    return Ok("Cập nhật thành công");
                }              
            }
            return Ok("Cập nhật thành công");
        }
     
        [HttpDelete]
        [Route("Delete")]
        public async Task<JsonResult> Delete(int id)
        {
            var currentUser = HttpContext.User;
            var objCongViec = _unitOfWork.CongViecRepos.GetById(id);
            // Xóa lịch sử công việc
            var objLichSu = await _unitOfWork.LichSuCongViecRepos.Find(x => x.CongViecID == id);
            if (objLichSu.Count() > 0)
            {
                _unitOfWork.LichSuCongViecRepos.RemoveRange(objLichSu);
                _unitOfWork.Save();
            }
            try
            {
                string tieude = ClassCommon.GetTenNhanVien(currentUser).ToUpper() + " ĐÃ VỪA CẬP NHẬT MỘT CÔNG VIỆC CHO " + objCongViec.NguoiThucHienTen.ToUpper();
                await _thongbao.SendTele_GiaoViec(objCongViec.CongViecID, tieude, 0);
            }
            catch
            {
                _unitOfWork.CongViecRepos.Remove(objCongViec);
                _unitOfWork.Save();
                return new JsonResult("Xóa thành công");
            }        
            _unitOfWork.CongViecRepos.Remove(objCongViec);
            _unitOfWork.Save();
            return new JsonResult("Xóa thành công");
        }

        [HttpDelete]
        [Route("DeleteLichSu")]
        public JsonResult DeleteLichSu(int id)
        {
            var objLichSu = _unitOfWork.LichSuCongViecRepos.GetById(id);
            _unitOfWork.LichSuCongViecRepos.Remove(objLichSu);
            _unitOfWork.Save();
            return new JsonResult("Xóa thành công");
        }

        [HttpPut]
        [Route("changeActive")]
        public IActionResult changeActive(int ID)
        {
            var objCongViec = _unitOfWork.CongViecRepos.GetById(ID);
            if (objCongViec.Active == true)
            {
                objCongViec.Active = false;
            }
            else
            {
                objCongViec.Active = true;
            }
            var currentUser = HttpContext.User;
            objCongViec.ActiveBy = ClassCommon.GetTenNhanVien(currentUser);
            objCongViec.ActiveDate = DateTime.Now;
            _unitOfWork.CongViecRepos.Update(objCongViec);
            _unitOfWork.Save();
            return Ok("Cập nhật thành công");
        }
    }
}

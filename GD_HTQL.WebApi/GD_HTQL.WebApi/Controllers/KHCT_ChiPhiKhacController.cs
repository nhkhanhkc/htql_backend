﻿using AutoMapper;
using GD_HTQL.WebApi.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using GD_HTQL.WebApi.Data.Models.EF.CongTacModels;
using GD_HTQL.WebApi.Data.Models.Dtos.CongTac;

namespace GD_HTQL.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class KHCT_ChiPhiKhacController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public KHCT_ChiPhiKhacController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetDs/{KHCT_Id}")]
        public async Task<IActionResult> GetDs(int KHCT_Id)
        {
            return Ok(await _unitOfWork.KHCT_ChiPhiKhacRepos.Find(x => x.NgayID == KHCT_Id));
        }


        [HttpGet]
        [Route("GetById/{Id}")]
        public IActionResult GetById(int Id)
        {
            return Ok(_unitOfWork.KHCT_ChiPhiKhacRepos.GetById(Id));
        }

        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> Post(KHCT_ChiPhiKhacDto KHCT_ChiPhiKhacDto)
        {
            var objKHCT_ChiPhiKhac = _mapper.Map<KHCT_ChiPhiKhac>(KHCT_ChiPhiKhacDto);
            var result = await _unitOfWork.KHCT_ChiPhiKhacRepos.Add(objKHCT_ChiPhiKhac);
            _unitOfWork.Save();
            if (result.ID == 0)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
            }
            else
            {
                return Ok("Thêm mới thành công");
            }
        }
        [HttpPut]
        [Route("Update")]
        public IActionResult Put(KHCT_ChiPhiKhacDto KHCT_ChiPhiKhacDto)
        {
            var objKHCT_ChiPhiKhac = _mapper.Map<KHCT_ChiPhiKhac>(KHCT_ChiPhiKhacDto);
            _unitOfWork.KHCT_ChiPhiKhacRepos.Update(objKHCT_ChiPhiKhac);
            _unitOfWork.Save();
            return Ok("Cập nhật thành công");
        }
        [HttpDelete]
        [Route("Delete")]
        public JsonResult Delete(int id)
        {
            var objKHCT_ChiPhiKhac = _unitOfWork.KHCT_ChiPhiKhacRepos.GetById(id);
            _unitOfWork.KHCT_ChiPhiKhacRepos.Remove(objKHCT_ChiPhiKhac);
            _unitOfWork.Save();
            return new JsonResult("Xóa thành công");
        }
    }
}

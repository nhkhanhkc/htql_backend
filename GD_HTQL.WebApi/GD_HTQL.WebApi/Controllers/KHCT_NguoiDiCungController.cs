﻿using AutoMapper;
using GD_HTQL.WebApi.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using GD_HTQL.WebApi.Data.Models.EF.CongTacModels;
using GD_HTQL.WebApi.Data.Models.Dtos.CongTac;

namespace GD_HTQL.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class KHCT_NguoiDiCungController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public KHCT_NguoiDiCungController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetDs/{KHCT_Id}")]
        public async Task<IActionResult> GetDs(int KHCT_Id)
        {
            return Ok(await _unitOfWork.KHCT_NguoiDiCungRepos.Find(x=>x.NgayID == KHCT_Id));
        }
        [HttpGet]
        [Route("GetById/{Id}")]
        public IActionResult GetById(int Id)
        {
            return Ok(_unitOfWork.KHCT_NguoiDiCungRepos.GetById(Id));
        }
       
        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> Post(KHCT_NguoiDiCungDto KHCT_NguoiDiCungDto)
        {
            var objKHCT_NguoiDiCung = _mapper.Map<KHCT_NguoiDiCung>(KHCT_NguoiDiCungDto);
            var result = await _unitOfWork.KHCT_NguoiDiCungRepos.Add(objKHCT_NguoiDiCung);
            _unitOfWork.Save();
            if (result.ID == 0)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
            }
            else
            {
                return Ok("Thêm mới thành công");
            }
        }
        [HttpPut]
        [Route("Update")]
        public IActionResult Put(KHCT_NguoiDiCungDto KHCT_NguoiDiCungDto)
        {
            var objKHCT_NguoiDiCung = _mapper.Map<KHCT_NguoiDiCung>(KHCT_NguoiDiCungDto);
            _unitOfWork.KHCT_NguoiDiCungRepos.Update(objKHCT_NguoiDiCung);
            _unitOfWork.Save();
            return Ok("Cập nhật thành công");
        }
        [HttpDelete]
        [Route("Delete")]
        public JsonResult Delete(int id)
        {
            var objKHCT_NguoiDiCung = _unitOfWork.KHCT_NguoiDiCungRepos.GetById(id);
            _unitOfWork.KHCT_NguoiDiCungRepos.Remove(objKHCT_NguoiDiCung);
            _unitOfWork.Save();
            return new JsonResult("Xóa thành công");
        }
    }
}

﻿using AutoMapper;
using GD_HTQL.WebApi.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using GD_HTQL.WebApi.Data.Models.EF.NhaThauModels;
using GD_HTQL.WebApi.Data.Models.Dtos.NhaThau;

namespace GD_HTQL.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class LoaiNhaThauController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public LoaiNhaThauController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;

        }
        [HttpGet]
        [Route("GetAll")]
        public async Task<IActionResult> Get()
        {
            return Ok(await _unitOfWork.LoaiNhaThauRepos.GetAll());
        }
        [HttpGet]
        [Route("GetById/{Id}")]
        public IActionResult GetLoaiNhaThauById(int Id)
        {
            return Ok(_unitOfWork.LoaiNhaThauRepos.GetById(Id));
        }
        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> Post(LoaiNhaThauDto LoaiNhaThauDto)
        {
            var objLoaiNhaThau = _mapper.Map<LoaiNhaThau>(LoaiNhaThauDto);
            var result = await _unitOfWork.LoaiNhaThauRepos.Add(objLoaiNhaThau);
            _unitOfWork.Save();
            if (result.LoaiNTID == 0)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
            }
            else
            {
                return Ok("Thêm mới thành công");
            }
        }
        [HttpPut]
        [Route("Update")]
        public IActionResult Put(LoaiNhaThauDto LoaiNhaThauDto)
        {
            var objLoaiNhaThau = _mapper.Map<LoaiNhaThau>(LoaiNhaThauDto);
            _unitOfWork.LoaiNhaThauRepos.Update(objLoaiNhaThau);
            _unitOfWork.Save();
            return Ok("Cập nhật thành công");
        }
        [HttpDelete]
        [Route("Delete")]
        public JsonResult Delete(int id)
        {
            var objLoaiNhaThau = _unitOfWork.LoaiNhaThauRepos.GetById(id);
            _unitOfWork.LoaiNhaThauRepos.Remove(objLoaiNhaThau);
            _unitOfWork.Save();
            return new JsonResult("Xóa thành công");
        }
    }
}

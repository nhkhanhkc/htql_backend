﻿using AutoMapper;
using GD_HTQL.WebApi.Common;
using GD_HTQL.WebApi.Data.Models.CustomModels;
using GD_HTQL.WebApi.Data.Models.EF.NhanVienModels;
using GD_HTQL.WebApi.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace GD_HTQL.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class NhanVienController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly UserManager<IdentityUser> _userManager;
        public NhanVienController(IUnitOfWork unitOfWork, IMapper mapper, UserManager<IdentityUser> userManager)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _userManager = userManager;
        }
        [HttpGet]
        [Route("GetNhanVien")]
        public async Task<IActionResult> Get()
        {
            return Ok(await _unitOfWork.NhanVienRepos.GetNhanVien(true));
        }

        [HttpGet]
        [Route("GetShort")]
        public async Task<IActionResult> GetShort()
        {
            return Ok(await _unitOfWork.NhanVienRepos.GetNhanVien(true));
        }

        [HttpGet]
        [Route("GetNhanVienKhoa")]
        public async Task<IActionResult> GetNhanVienKhoa()
        {
            return Ok(await _unitOfWork.NhanVienRepos.GetNhanVien(false));
        }
        [HttpGet]
        [Route("GetNhanVienByTuKhoa")]
        public async Task<IActionResult> GetNhanVienByTuKhoa(string Key)
        {
            return Ok(await _unitOfWork.NhanVienRepos.GetNhanVienByTuKhoa(Key));
        }
        [HttpGet]
        [Route("GetNhanVienById/{Id}")]
        public IActionResult GetNhanVienById(int Id)
        {
            return Ok(_unitOfWork.NhanVienRepos.GetById(Id));
        }

        [HttpGet]
        [Route("GetDetail/{Id}")]
        public async Task<IActionResult> GetDetail(int Id)
        {
            return Ok(await _unitOfWork.NhanVienRepos.GetDetails(Id));
        }

        [HttpGet]
        [Route("GetUserLogin")]
        public async Task<IActionResult> GetUserLogin()
        {
            var currentUser = HttpContext.User;
            int _nhanvienID = ClassCommon.GetUserID(currentUser);
            return Ok(await _unitOfWork.NhanVienRepos.GetDetails(_nhanvienID));
        }

        [HttpPost]
        [Route("AddNhanVien")]
        public async Task<IActionResult> Post(NhanVien objNhanVien)
        {
            objNhanVien.Active = true;
            var result = await _unitOfWork.NhanVienRepos.Add(objNhanVien);
            _unitOfWork.Save();
            if (result.NhanVienID == 0)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
            }
            return Ok("Thêm mới thành công");
        }
        [HttpPut]
        [Route("UpdateNhanVien")]
        public async Task<IActionResult> Put(NhanVienModel objNhanVienModel)
        {            
            var objNhanVien = _mapper.Map<NhanVien>(objNhanVienModel.NhanVienDto);
          
            objNhanVien.Active = true;          
            var obj = _unitOfWork.NhanVienRepos.GetById(objNhanVien.NhanVienID);
            objNhanVien.CreateDate = obj.CreateDate;
            objNhanVien.CreateBy = obj.CreateBy;
            objNhanVien.UpdateDate = DateTime.Now;
            objNhanVien.UserID = obj.UserID;
            var currentUser = HttpContext.User;

            objNhanVien.UpdateBy = ClassCommon.GetTenNhanVien(currentUser);
            _unitOfWork.NhanVienRepos.Update(objNhanVien);
            _unitOfWork.Save();

            // Xóa file đính kèm củ
            //_unitOfWork.FileRepos.RemoveFileDinhKem(objNhanVien.NhanVienID);

            if (objNhanVienModel.FileDinhKemDto != null)
            {
                await _unitOfWork.FileRepos.AddFileDinhKem(objNhanVienModel.FileDinhKemDto, objNhanVienModel.NhanVienDto.NhanVienID);
            }           
            return Ok("Cập nhật thành công");
        }

        [HttpDelete]       
        [Route("DeleteNhanVien")]
        public async Task<JsonResult> Delete(int id)
        {
            var objNhanVien = _unitOfWork.NhanVienRepos.GetById(id);
            var objNhanVienChucVu = await _unitOfWork.ChucVuRepos.GetByNhanVien(id);
            var objNhanVienCoQuan = await _unitOfWork.NSCoQuanRepos.Find(x => x.NhanVienID == id);
            var objNhanVienThau = await _unitOfWork.NhaThauRepos.Find(x => x.NhanVienID == id);
            var objNhaCungCap = await _unitOfWork.NhaCungCapRepos.Find(x => x.NhanVienID == id);
            var objCongTac = await _unitOfWork.KHCT_Ngay_LichSuRepos.Find(x => x.NhanVienID == id);
            var objTacGia = await _unitOfWork.TacGiaRepos.Find(x => x.NhanVienID == id);
            if (objNhanVienChucVu.Count() == 0 && objNhanVienCoQuan.Count() == 0 && objNhanVienThau.Count() == 0 && objNhaCungCap.Count() == 0 && objCongTac.Count() == 0 && objTacGia.Count() == 0)
            {
                _unitOfWork.NhanVienRepos.Remove(objNhanVien);
                _unitOfWork.Save();
                return new JsonResult("Xóa thành công");
            }
            else
            {
                return new JsonResult("Nhân viên đã phát sinh dữ liệu không thể xóa");
            }
        }

        // Vị trí nhân viên
        [HttpGet]
        [Route("GetByChucVu/{ID}")]
        public async Task<IActionResult> GetByChucVu(int ID)
        {
            var result = await _unitOfWork.NhanVienRepos.GetByChucVu(ID);
            return Ok(result);
        }

        // Vị trí nhân viên
        [HttpPost]
       [Route("ThemChucVu")]
        public async Task<IActionResult> ThemChucVu(ChucVuNhanVien chucVuNhanVien)
        {
            if (_unitOfWork.NhanVienRepos.CheckChucVuExits(chucVuNhanVien) == true)
            {
                var result = await _unitOfWork.NhanVienRepos.ThemChucVu(chucVuNhanVien);
                if (result.ChucVuID == 0)
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
                }
                else
                {
                    return Ok("Thêm mới thành công");
                }
            }
            else
            {
                return Ok("Nhân viên đã thuộc chức vụ bạn chọn");
            }            
        }

        [HttpDelete]
        [Route("XoaChucVu")]
        public JsonResult XoaChucVu(ChucVuNhanVien objChucVuNhanVien)
        {
            _unitOfWork.NhanVienRepos.XoaChucVu(objChucVuNhanVien);
            return new JsonResult("Xóa thành công");
        }

        [HttpPut]
        [Route("DoiTrangThai")]
        public async Task<IActionResult> DoiTrangThai(int id)
        {
            var currentUser = HttpContext.User;
            var result = await _unitOfWork.NhanVienRepos.DoiTrangThai(id,currentUser);
            if (result == null)
            {
                return new JsonResult("Người dùng không tồn tại");
            }
            else 
            {
                var user = await _userManager.FindByIdAsync(result.UserID);
                if (result.Active ==true)
                {
                    await _userManager.SetLockoutEnabledAsync(user, true);
                    await _userManager.SetLockoutEndDateAsync(user, DateTime.Now.AddYears(100));
                }
                else
                {
                    await _userManager.SetLockoutEnabledAsync(user, false);
                }
                return new JsonResult("Đổi trạng thành công");
            }
        }

        [HttpGet]
        [Route("GetLoaiFile")]
        public async Task<IActionResult> GetLoaiFile()
        {
            var result = await _unitOfWork.NhanVienRepos.GetLoaiFile();
            return Ok(result);
        }
    }
}

﻿using AutoMapper;
using GD_HTQL.WebApi.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using GD_HTQL.WebApi.Data.Models.EF.CongTacModels;
using GD_HTQL.WebApi.Data.Models.Dtos.CongTac;

namespace GD_HTQL.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class KHCT_ChiPhiController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public KHCT_ChiPhiController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        [HttpGet]
        [Route("GetDsChiPhi/{KHCT_Id}")]
        public async Task<IActionResult> GetDsChiPhi(int KHCT_Id)
        {
            return Ok(await _unitOfWork.KHCT_ChiPhiRepos.GetDsChiPhi(KHCT_Id));
        }

        [HttpGet]
        [Route("GetById/{Id}")]
        public IActionResult GetById(int Id)
        {
            return Ok(_unitOfWork.KHCT_ChiPhiRepos.GetById(Id));
        }

        [HttpGet]
        [Route("GetDsHangMuc")]
        public async Task<IActionResult> GetDsHangMuc()
        {
            return Ok(await _unitOfWork.KHCT_ChiPhiRepos.GetDsHangMuc());
        }
        [HttpGet]
        [Route("GetDsLoai")]
        public async Task<IActionResult> GetDsLoai()
        {
            return Ok(await _unitOfWork.KHCT_ChiPhiRepos.GetDsLoai());
        }

        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> Post(KHCT_ChiPhiDto KHCT_ChiPhiDto)
        {
            var objKHCT_ChiPhi = _mapper.Map<KHCT_ChiPhi>(KHCT_ChiPhiDto);
            var result = await _unitOfWork.KHCT_ChiPhiRepos.Add(objKHCT_ChiPhi);
            _unitOfWork.Save();
            if (result.ID == 0)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
            }
            else
            {
                return Ok("Thêm mới thành công");
            }
        }
        [HttpPut]
        [Route("Update")]
        public IActionResult Put(KHCT_ChiPhiDto KHCT_ChiPhiDto)
        {
            var objKHCT_ChiPhi = _mapper.Map<KHCT_ChiPhi>(KHCT_ChiPhiDto);
            _unitOfWork.KHCT_ChiPhiRepos.Update(objKHCT_ChiPhi);
            _unitOfWork.Save();
            return Ok("Cập nhật thành công");
        }
        [HttpDelete]
        [Route("Delete")]
        public JsonResult Delete(int id)
        {
            var objKHCT_ChiPhi = _unitOfWork.KHCT_ChiPhiRepos.GetById(id);
            _unitOfWork.KHCT_ChiPhiRepos.Remove(objKHCT_ChiPhi);
            _unitOfWork.Save();
            return new JsonResult("Xóa thành công");
        }
    }
}

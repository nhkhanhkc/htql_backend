﻿using GD_HTQL.WebApi.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using GD_HTQL.WebApi.Data.Models.EF.SanPhamModels;

namespace GD_HTQL.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CtyDuToanController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
      
        public CtyDuToanController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
          

        }
        [HttpGet]
        [Route("GetALl")]
        public async Task<IActionResult> GetALl()
        {
            var result = await _unitOfWork.CtyDuToanRepos.GetAll();
            return Ok(result);
        }

        [HttpGet]
        [Route("GetByID/{Id}")]
        public IActionResult Get(int Id)
        {
            var result =  _unitOfWork.CtyDuToanRepos.GetById(Id);
            return Ok(result);
        }
       

        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> Post(CtyDuToan ctyDutoan)
        {
           
            var result = await _unitOfWork.CtyDuToanRepos.Add(ctyDutoan);
            _unitOfWork.Save();
            if (result.ID == 0)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
            }
            else
            {
                return Ok("Thêm mới thành công");
            }
        }
        [HttpPut]
        [Route("Update")]
        public IActionResult Put(CtyDuToan ctyDutoan)
        {         
            _unitOfWork.CtyDuToanRepos.Update(ctyDutoan);
            _unitOfWork.Save();
            return Ok("Cập nhật thành công");
        }
        [HttpDelete]
        [Route("Delete")]
        public JsonResult Delete(int id)
        {
            var objXa = _unitOfWork.CtyDuToanRepos.GetById(id);
            _unitOfWork.CtyDuToanRepos.Remove(objXa);
            _unitOfWork.Save();
            return new JsonResult("Xóa thành công");
        }
    }
}

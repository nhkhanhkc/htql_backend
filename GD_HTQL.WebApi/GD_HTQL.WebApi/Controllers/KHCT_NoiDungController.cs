﻿using AutoMapper;
using GD_HTQL.WebApi.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using GD_HTQL.WebApi.Common;
using GD_HTQL.WebApi.Data.Models.EF.CongTacModels;
using GD_HTQL.WebApi.Data.Models.Dtos.CongTac;

namespace GD_HTQL.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class KHCT_NoiDungController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public KHCT_NoiDungController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;

        }
        [HttpGet]
        [Route("GetDs/{KHCT_Id}")]
        public async Task<IActionResult> GetDs(int KHCT_Id)
        {
            return Ok(await _unitOfWork.KHCT_NoiDungRepos.Find(x => x.NgayID == KHCT_Id));
        }
        [HttpGet]
        [Route("GetCoQuan")]
        public async Task<IActionResult> GetCoQuan()
        {
            var currentUser = HttpContext.User;
            int _nhanVienID = ClassCommon.GetUserID(currentUser);
            return Ok(await _unitOfWork.NSCoQuanRepos.GetByNhanVien(_nhanVienID));
        }

        [HttpGet]
        [Route("GetById/{Id}")]
        public IActionResult GetById(int Id)
        {
            return Ok(_unitOfWork.KHCT_NoiDungRepos.GetById(Id));
        }

        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> Post(KHCT_NoiDungDto KHCT_NoiDungDto)
        {
            var objKHCT_NoiDung = _mapper.Map<KHCT_NoiDung>(KHCT_NoiDungDto);
            var result = await _unitOfWork.KHCT_NoiDungRepos.Add(objKHCT_NoiDung);
            _unitOfWork.Save();
            if (result.ID == 0)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
            }
            else
            {
                return Ok("Thêm mới thành công");
            }
        }
        [HttpPut]
        [Route("Update")]
        public IActionResult Put(KHCT_NoiDungDto KHCT_NoiDungDto)
        {
            var objKHCT_NoiDung = _mapper.Map<KHCT_NoiDung>(KHCT_NoiDungDto);
            _unitOfWork.KHCT_NoiDungRepos.Update(objKHCT_NoiDung);
            _unitOfWork.Save();
            return Ok("Cập nhật thành công");
        }
        [HttpDelete]
        [Route("Delete")]
        public JsonResult Delete(int id)
        {
            var objKHCT_NoiDung = _unitOfWork.KHCT_NoiDungRepos.GetById(id);
            _unitOfWork.KHCT_NoiDungRepos.Remove(objKHCT_NoiDung);
            _unitOfWork.Save();
            return new JsonResult("Xóa thành công");
        }
    }
}

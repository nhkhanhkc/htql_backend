﻿using AutoMapper;
using GD_HTQL.WebApi.Common;
using GD_HTQL.WebApi.Data.Models.Dtos.DuAn;
using GD_HTQL.WebApi.Data.Models.EF.DuAnModels;
using GD_HTQL.WebApi.Infrastructure;
using GD_HTQL.WebApi.Services.IRepository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GD_HTQL.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class QuanLyTuongTacController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ISendTelegramRepository _thongbao;
        public QuanLyTuongTacController(IUnitOfWork unitOfWork,IMapper mapper, ISendTelegramRepository thongbao)
        {
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            _mapper = mapper;
            _thongbao = thongbao;
        }
        [HttpGet]
        [Route("GetQuanLyTuongTac")]
        public async Task<IActionResult> Get()
        {
            return Ok(await _unitOfWork.QuanLyTuongTacRepos.GetQuanLyTuongTac());
        }

        [HttpGet]
        [Route("GetDetails")]
        public async Task<IActionResult> GetDetails()
        {
            return Ok(await _unitOfWork.QuanLyTuongTacRepos.GetDetails());
        }

        [HttpGet]
        [Route("GetbyNhanVien")]
        public async Task<IActionResult> GetbyNhanVien(int nhanvienID)
        {
            return Ok(await _unitOfWork.QuanLyTuongTacRepos.Find(x=>x.NhanVienID == nhanvienID));
        }
        [HttpGet]
        [Route("GetBuocThiTruong")]
        public async Task<IActionResult> GetBuocThiTruong()
        {
            return Ok(await _unitOfWork.QuanLyTuongTacRepos.GetBuocThiTruong());
        }
        [HttpGet]
        [Route("GetNguonVon")]
        public async Task<IActionResult> GetNguonVon()
        {
            return Ok(await _unitOfWork.QuanLyTuongTacRepos.GetNguonVon());
        }
        [HttpGet]
        [Route("GetByTuKhoa")]
        public async Task<IActionResult> GetQuanLyTuongTacByTuKhoa(string? Key,int tinhID )
        {
            return Ok(await _unitOfWork.QuanLyTuongTacRepos.GetQuanLyTuongTacByTuKhoa(Key, tinhID));
        }

        [HttpGet]
        [Route("GetByCoQuan")]
        public async Task<IActionResult> GetByCoQuan(int coQuanID)
        {
            var objTuongTac = await _unitOfWork.QuanLyTuongTacRepos.Find(x => x.CoQuanID == coQuanID);
            return Ok(objTuongTac);
        }
        [HttpGet]
        [Route("GetByBuocThiTruong")]
        public async Task<IActionResult> GetByBuocThiTruong(int BuocThiTruongID)
        {
            var objTuongTac = await _unitOfWork.QuanLyTuongTacRepos.Find(x => x.BuocThiTruongID == BuocThiTruongID);
            return Ok(objTuongTac);
        }

        [HttpGet]
        [Route("GetQuanLyTuongTacById/{Id}")]
        public IActionResult GetQuanLyTuongTacById(int Id)
        {

            return Ok( _unitOfWork.QuanLyTuongTacRepos.GetById(Id));
        }
        [HttpPost]
        [Route("AddQuanLyTuongTac")]
        public async Task<IActionResult> Post(QuanLyTuongTacDto quanLyTuongTacDto)
        {
            // Kiểm tra nhân viên báo cáo tiếp xúc                
            var currentUser = HttpContext.User;
            int _nhanVienID = ClassCommon.GetUserID(currentUser);
            if (_nhanVienID == 0)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Nhân viên không được tìm thấy");
            }
            else
            {
                var objQuanLyTuongTac = _mapper.Map<QuanLyTuongTac>(quanLyTuongTacDto);
                objQuanLyTuongTac.NhanVienID = _nhanVienID;
                objQuanLyTuongTac.Active = true;
                objQuanLyTuongTac.CreateDate = DateTime.Now;
                objQuanLyTuongTac.CreateBy = ClassCommon.GetTenNhanVien(currentUser);
                var result = await _unitOfWork.QuanLyTuongTacRepos.Add(objQuanLyTuongTac);
                _unitOfWork.Save();
                if (result.TuongTacID == 0)
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
                }
                else
                {
                    try
                    {
                        string tieude = objQuanLyTuongTac.CreateBy.ToUpper() + " ĐÃ VỪA THÊM MỚI BÁO CÁO TIẾP XÚC";
                        await _thongbao.SendTeleDuAn_CoHoi(result.TuongTacID, tieude);
                    }
                    catch
                    {
                        return Ok(result);
                    }               
                    return Ok(result);
                }
            }            
        }
        [HttpPut]
        [Route("UpdateQuanLyTuongTac")]
        public async Task<IActionResult> Put(QuanLyTuongTacDto quanLyTuongTacDto)
        {
            var currentUser = HttpContext.User;
            int _nhanVienID = ClassCommon.GetUserID(currentUser);
            var objQuanLyTuongTac = _mapper.Map<QuanLyTuongTac>(quanLyTuongTacDto);
            objQuanLyTuongTac.NhanVienID = _nhanVienID;
            objQuanLyTuongTac.Active = true;
            var obj = _unitOfWork.QuanLyTuongTacRepos.GetById(objQuanLyTuongTac.TuongTacID);

            objQuanLyTuongTac.CreateDate = obj.CreateDate;
            objQuanLyTuongTac.CreateBy = obj.CreateBy;
            objQuanLyTuongTac.UpdateDate = DateTime.Now;
            objQuanLyTuongTac.UpdateBy = ClassCommon.GetTenNhanVien(currentUser);
            _unitOfWork.QuanLyTuongTacRepos.Update(objQuanLyTuongTac);
            _unitOfWork.Save();
            try
            {
                string tieude = objQuanLyTuongTac.UpdateBy.ToUpper() + " ĐÃ VỪA CẬP NHẬT BÁO CÁO TIẾP XÚC";
                await _thongbao.SendTeleDuAn_CoHoi(objQuanLyTuongTac.TuongTacID, tieude);
            }
            catch
            {
                return Ok("Cập nhật thành công");
            }        
            return Ok("Cập nhật thành công");
        }
        [HttpDelete]
        [Route("DeleteQuanLyTuongTac")]
        public async Task<JsonResult> Delete(int id)
        {
            var currentUser = HttpContext.User;
            var objTuongTac = _unitOfWork.QuanLyTuongTacRepos.GetById(id);
            try
            {
                string tieude = ClassCommon.GetTenNhanVien(currentUser).ToUpper() + " ĐÃ VỪA XÓA BÁO CÁO TIẾP XÚC";
                await _thongbao.SendTeleDuAn_CoHoi(id, tieude);
            }
            catch
            {
                _unitOfWork.QuanLyTuongTacRepos.Remove(objTuongTac);
                _unitOfWork.Save();
                return new JsonResult("Xóa thành công");
            }         
            _unitOfWork.QuanLyTuongTacRepos.Remove(objTuongTac);
            _unitOfWork.Save();       
            return new JsonResult("Xóa thành công");
        }
    }
}

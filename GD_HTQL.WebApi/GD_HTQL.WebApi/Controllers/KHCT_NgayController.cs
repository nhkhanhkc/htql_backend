﻿using AutoMapper;
using GD_HTQL.WebApi.Common;
using GD_HTQL.WebApi.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using GD_HTQL.WebApi.Data.Models.CustomModels;
using GD_HTQL.WebApi.Data.Models.EF.CongTacModels;
using GD_HTQL.WebApi.Data.Models.Dtos.CongTac;
using GD_HTQL.WebApi.Services.IRepository;

namespace GD_HTQL.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class KHCT_NgayController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ISendTelegramRepository _thongbao;
        public KHCT_NgayController(IUnitOfWork unitOfWork, IMapper mapper, ISendTelegramRepository thongbao)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _thongbao = thongbao;
        }

        [HttpGet]
        [Route("GetAll")]
        public async Task<IActionResult> GetAll()
        {
            var currentUser = HttpContext.User;
            if (CheckRoles.CheckRoleGroupCongViec(currentUser) == true)
            {
                //var lstCongViec = await _unitOfWork.KHCT_NgayRepos.GetAll();
                var lstCongViec = await _unitOfWork.KHCT_NgayRepos.LstKHCT_NgayView(0, false, false);
                return Ok(lstCongViec);
            }
            else
            {
                int _nhanVienID = ClassCommon.GetUserID(currentUser);
                if (_nhanVienID == 0)
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
                }
                else
                {
                    var lstCongViec = await _unitOfWork.KHCT_NgayRepos.LstKHCT_NgayView(0, false, false);
                    return Ok(lstCongViec);
                }
            }
        }

        [HttpGet]
        [Route("GetbyWeek")]
        public async Task<IActionResult> GetbyWeek()
        {
            var currentUser = HttpContext.User;
            if (CheckRoles.CheckRoleGroupCongViec(currentUser) == true)
            {
                var lstCongViec = await _unitOfWork.KHCT_NgayRepos.LstKHCT_NgayView(0,true,false);
                return Ok(lstCongViec);
            }
            else
            {
                int _nhanVienID = ClassCommon.GetUserID(currentUser);
                if (_nhanVienID == 0)
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
                }
                else
                {
                    var lstCongViec = await _unitOfWork.KHCT_NgayRepos.LstKHCT_NgayView(_nhanVienID, true, false);
                    return Ok(lstCongViec);
                }
            }
        }

        [HttpGet]
        [Route("GetbyMonth")]
        public async Task<IActionResult> GetbyMonth()
        {
            var currentUser = HttpContext.User;          
          
            if (CheckRoles.CheckRoleGroupCongViec(currentUser) == true)
            {
                var lstCongViec = await _unitOfWork.KHCT_NgayRepos.LstKHCT_NgayView(0, false, true);

                return Ok(lstCongViec);
            }
            else
            {
                int _nhanVienID = ClassCommon.GetUserID(currentUser);
                if (_nhanVienID == 0)
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
                }
                else
                {
                    var lstCongViec = await _unitOfWork.KHCT_NgayRepos.LstKHCT_NgayView(_nhanVienID, false, true);
                    return Ok(lstCongViec);
                }
            }
        }

        [HttpGet]
        [Route("GetById/{Id}")]
        public KHCT_Ngay GetById(int Id)
        {
            return _unitOfWork.KHCT_NgayRepos.GetById(Id);
        }
        [HttpGet]
        [Route("GetDetailsById/{Id}")]
        public async Task<IActionResult> GetDetailsById(int Id)
        {
            return Ok(await _unitOfWork.KHCT_NgayRepos.GetDetailsByID(Id));
        }

        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> Add(KHCT_NgayDto KHCT_NgayDto)
        {
           
            var currentUser = HttpContext.User;
            int _nhanvienID = ClassCommon.GetUserID(currentUser);
            string _tennhanvien = ClassCommon.GetTenNhanVien(currentUser);
            var objKHCT_Ngay = _mapper.Map<KHCT_Ngay>(KHCT_NgayDto);
            objKHCT_Ngay.Active = true;
            objKHCT_Ngay.CreateDate = DateTime.Now;

            objKHCT_Ngay.CreateBy = _tennhanvien;
            objKHCT_Ngay.NguoiTaoID = _nhanvienID;

            // Lấy thông tin nhân viên 
            var objNhanVien = await _unitOfWork.NhanVienRepos.GetDetails(_nhanvienID);
            if(objNhanVien != null)
            {
                objKHCT_Ngay.HoTen = objNhanVien.TenNhanVien;
                if (objNhanVien.lstChucVuView.Count() > 0)
                {
                    var objChuVuView = objNhanVien.lstChucVuView.FirstOrDefault();
                    if (objChuVuView != null)
                    {
                        objKHCT_Ngay.ChucVuID = objChuVuView.lstChucVu ==null ? 0: objChuVuView.lstChucVu.ChucVuID;
                        objKHCT_Ngay.TenChucVu = objChuVuView.lstChucVu == null ? "" : objChuVuView.lstChucVu.TenChucVu;

                        objKHCT_Ngay.PhongBanID = objChuVuView.lstPhongBan == null ? 0: objChuVuView.lstPhongBan.PhongBanID;
                        objKHCT_Ngay.TenPhongBan = objChuVuView.lstPhongBan ==null ? "" : objChuVuView.lstPhongBan.TenPhongBan;
                      
                        objKHCT_Ngay.CongTyID = objChuVuView.lstCongTy == null ? 0 : objChuVuView.lstCongTy.CongTyID;
                        objKHCT_Ngay.TenCongTy = objChuVuView.lstCongTy == null ? "" : objChuVuView.lstCongTy.TenCongTy;
                    }                  
                }              
            }
            var result = await _unitOfWork.KHCT_NgayRepos.Add(objKHCT_Ngay);
            _unitOfWork.Save();
            if (result.NgayID == 0)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
            }
            else
            {
                // Add lịch sử kế hoạch công tác ngày      
                KHCT_Ngay_LichSu LichSu = new KHCT_Ngay_LichSu();
                LichSu.NgayID = result.NgayID;
                LichSu.NhanVienID = result.NguoiTaoID;
                LichSu.ThoiGan = DateTime.Now;
                LichSu.TrangThaiID = 1;
                LichSu.NguoiDuyetID = KHCT_NgayDto.NguoiDuyetID;
                LichSu.TenNguoiDuyet = KHCT_NgayDto.TenNguoiDuyet;
                var resultLichSu =  await _unitOfWork.KHCT_Ngay_LichSuRepos.Add(LichSu);
                _unitOfWork.Save();
                try
                {                
                    string tieude = objKHCT_Ngay.CreateBy.ToUpper() + " VỪA TẠO MỚI KẾ HOẠCH CÔNG TÁC";
                    await _thongbao.SendTeleKHCT_Ngay(result.NgayID, tieude);
                }
                catch
                {
                    return Ok(result);
                }
                return Ok(result);
            }          
        }

        [HttpPut]
        [Route("Update")]
        public async Task<IActionResult> Update(KHCT_NgayDto KHCT_NgayDto)
        {
            var objKHCT_Ngay = _mapper.Map<KHCT_Ngay>(KHCT_NgayDto);
            var obj = _unitOfWork.KHCT_NgayRepos.GetById(objKHCT_Ngay.NgayID);
            objKHCT_Ngay.Active = true;
            objKHCT_Ngay.CreateDate = obj.CreateDate;
            objKHCT_Ngay.CreateBy = obj.CreateBy;

            objKHCT_Ngay.ChucVuID = obj.ChucVuID;
            objKHCT_Ngay.TenChucVu = obj.TenChucVu;
            objKHCT_Ngay.PhongBanID = obj.PhongBanID;
            objKHCT_Ngay.TenPhongBan = obj.TenPhongBan;
            objKHCT_Ngay.CongTyID = obj.CongTyID;
            objKHCT_Ngay.TenCongTy = obj.TenCongTy;

            objKHCT_Ngay.UpdateDate = DateTime.Now;
            var currentUser = HttpContext.User;
            objKHCT_Ngay.UpdateBy = ClassCommon.GetTenNhanVien(currentUser);
            objKHCT_Ngay.NguoiTaoID = ClassCommon.GetUserID(currentUser);                             
            _unitOfWork.KHCT_NgayRepos.Update(objKHCT_Ngay);
            _unitOfWork.Save();
            try
            {
                string tieude = objKHCT_Ngay.UpdateBy.ToUpper() + " VỪA CẬP NHẬT KẾ HOẠCH CÔNG TÁC";
                await _thongbao.SendTeleKHCT_Ngay(objKHCT_Ngay.NgayID, tieude);
            }
            catch
            {
                return Ok("Cập nhật thành công");
            }

            return Ok("Cập nhật thành công");
        }
        [HttpDelete]
        [Route("Delete")]
        public async Task<JsonResult> Delete(int id)
        {
            var currentUser = HttpContext.User;
            int _nhanVienID = ClassCommon.GetUserID(currentUser);

            var objDelete = _unitOfWork.KHCT_NgayRepos.GetById(id);
            if (objDelete != null && objDelete.NguoiTaoID == _nhanVienID)
            {
                // Xóa lịch sử duyệt
                var objLichSu = await _unitOfWork.KHCT_Ngay_LichSuRepos.Find(x => x.NgayID == id);
                if (objLichSu.Count() > 0)
                {
                    _unitOfWork.KHCT_Ngay_LichSuRepos.RemoveRange(objLichSu);
                    _unitOfWork.Save();
                }
                // Xóa người đi cùng
                var objDiCung = await _unitOfWork.KHCT_NguoiDiCungRepos.Find(x => x.NgayID == id);
                if (objDiCung.Count() > 0)
                {
                    _unitOfWork.KHCT_NguoiDiCungRepos.RemoveRange(objDiCung);
                    _unitOfWork.Save();
                }
                // Xóa Nội dung
                var objNoiDung = await _unitOfWork.KHCT_NoiDungRepos.Find(x => x.NgayID == id);
                if (objNoiDung.Count() > 0)
                {
                    _unitOfWork.KHCT_NoiDungRepos.RemoveRange(objNoiDung);
                    _unitOfWork.Save();
                }
                // Xóa Nội dung Xe
                var objxe = await _unitOfWork.KHCT_XeRepos.Find(x => x.NgayID == id);
                if (objxe.Count() > 0)
                {
                    _unitOfWork.KHCT_XeRepos.RemoveRange(objxe);
                    _unitOfWork.Save();
                }
                // Xóa Chi phí
                var objChiPhi = await _unitOfWork.KHCT_ChiPhiRepos.Find(x => x.NgayID == id);
                if (objChiPhi.Count() > 0)
                {
                    _unitOfWork.KHCT_ChiPhiRepos.RemoveRange(objChiPhi);
                    _unitOfWork.Save();
                }

                // Xóa Chi phí khác
                var objChiPhiKhac = await _unitOfWork.KHCT_ChiPhiKhacRepos.Find(x => x.NgayID == id);
                if (objChiPhiKhac.Count() > 0)
                {
                    _unitOfWork.KHCT_ChiPhiKhacRepos.RemoveRange(objChiPhiKhac);
                    _unitOfWork.Save();
                }

                try
                {
                    string tieude = ClassCommon.GetTenNhanVien(currentUser).ToUpper() + " VỪA XÓA KẾ HOẠCH CÔNG TÁC";
                    await _thongbao.SendTeleKHCT_Ngay(objDelete.NgayID, tieude);
                }
                catch
                {
                    _unitOfWork.KHCT_NgayRepos.Remove(objDelete);
                    _unitOfWork.Save();
                }

                _unitOfWork.KHCT_NgayRepos.Remove(objDelete);
                _unitOfWork.Save();
                return new JsonResult("Xóa thành công");
            }
            else
            {
                return new JsonResult("Chỉ xóa được kế hoạch công tác do mình tạo");
            }          
        }
        [HttpPut]
        [Route("changeActive")]
        public IActionResult changeActive(int ID)
        {
            var objKHCT_Ngay = _unitOfWork.KHCT_NgayRepos.GetById(ID);
            if (objKHCT_Ngay.Active == true)
            {
                objKHCT_Ngay.Active = false;
            }
            else
            {
                objKHCT_Ngay.Active = true;
            }
            var currentUser = HttpContext.User;
            objKHCT_Ngay.ActiveBy = ClassCommon.GetTenNhanVien(currentUser);
            objKHCT_Ngay.ActiveDate = DateTime.Now;
            _unitOfWork.KHCT_NgayRepos.Update(objKHCT_Ngay);
            _unitOfWork.Save();
            return Ok("Cập nhật thành công");
        }

        [HttpPut]
        [Route("TrangThai")]
        public async Task<IActionResult> CapNhatTrangThai(KTCT_Ngay_DoiTrangThai _doiTrangThai)
        {
            var currentUser = HttpContext.User;
            int _nhanVienID = ClassCommon.GetUserID(currentUser);
            // Add lịch sử kế hoạch công tác ngày      
            KHCT_Ngay_LichSu LichSu = new KHCT_Ngay_LichSu();
            LichSu.NgayID = _doiTrangThai.NgayID;
            LichSu.NhanVienID = _nhanVienID;
            LichSu.ThoiGan = DateTime.Now;
            LichSu.TrangThaiID = _doiTrangThai.TrangThaiID;
            LichSu.NguoiDuyetID = _doiTrangThai.NguoiDuyetID;
            LichSu.TenNguoiDuyet = _doiTrangThai.TenNguoiDuyet;
            var resultLichSu = await _unitOfWork.KHCT_Ngay_LichSuRepos.Add(LichSu);
            _unitOfWork.Save();
            try
            {
                string _trangthai = "DUYỆT";
                if (_doiTrangThai.TrangThaiID == 6)
                {
                    _trangthai = "TỪ CHỐI";
                }
                var objNgay = _unitOfWork.KHCT_NgayRepos.GetById(_doiTrangThai.NgayID);
                string tieude = ClassCommon.GetTenNhanVien(currentUser).ToUpper() + " VỪA " + _trangthai + " KẾ HOẠCH CÔNG TÁC CỦA " + objNgay.HoTen.ToUpper();
                await _thongbao.SendTeleKHCT_Ngay(_doiTrangThai.NgayID, tieude);
            }
            catch
            {
                return Ok("Cập nhật thành công");
            }
            return Ok("Cập nhật thành công");
        }

        [HttpGet]
        [Route("GetTrangThai")]
        public async Task<IActionResult> GetTrangThai()
        {
             var result = await _unitOfWork.KHCT_NgayRepos.GetTrangThai();
            return Ok(result);
        }
    }
}

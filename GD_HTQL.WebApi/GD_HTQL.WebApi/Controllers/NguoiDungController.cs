﻿using GD_HTQL.WebApi.Common;
using GD_HTQL.WebApi.Data.Models;
using GD_HTQL.WebApi.Data.Models.EF.NhanVienModels;
using GD_HTQL.WebApi.Infrastructure;
using GD_HTQL.WebApi.Models.NguoiDungModels;
using GD_HTQL.WebApi.Models.UserModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace GD_HTQL.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NguoiDungController : ControllerBase
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly SignInManager<IdentityUser> _sign;

        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IConfiguration _configuration;
        private readonly IUnitOfWork _unitOfWork;

        public NguoiDungController(
            UserManager<IdentityUser> userManager,
            RoleManager<IdentityRole> roleManager,
            IConfiguration configuration,
             IUnitOfWork unitOfWork,
            SignInManager<IdentityUser> sign)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _configuration = configuration;
            _unitOfWork = unitOfWork;
            _sign = sign;
        }

        [HttpPost]
        [Route("DangNhap")]
        public async Task<IActionResult> Login([FromBody] DangNhapModel model)
        {
            var user = await _userManager.FindByNameAsync(model.TenDangNhap);
            if (user != null)
            {
                var result = await _sign.CheckPasswordSignInAsync(user, model.MatKhau, false);

                if (result.Succeeded && !result.IsLockedOut)
                {                   
                    var userRoles = await _userManager.GetRolesAsync(user);
                    var userClaimList = await _userManager.GetClaimsAsync(user);
                    var objNhanVien = _unitOfWork.NhanVienRepos.GetNhanVienByUserID(user.Id).Result;
                    var authClaims = new List<Claim>
                    {
                          new Claim(ClaimTypes.Name, user.UserName),
                          new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                          new Claim("NhanVienID", objNhanVien.NhanVienID.ToString()),
                          new Claim("TenNhanVien", objNhanVien.TenNhanVien.ToString())

                    };
                    foreach (var userRole in userRoles)
                    {
                        authClaims.Add(new Claim(ClaimTypes.Role, userRole));                      
                    }
                    authClaims.AddRange(userClaimList);
                    var token = GetToken(authClaims);
                   
                    return Ok(new
                    {
                        token = new JwtSecurityTokenHandler().WriteToken(token),
                        expiration = token.ValidTo,
                        userID = objNhanVien.NhanVienID,
                        HoVaTen = objNhanVien.TenNhanVien
                    });
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }                            
        }

        [Authorize]
        [HttpPost]
        [Route("DangKy")]
        public async Task<IActionResult> Register([FromBody] DangKyModel model)
        {
            var userExists = await _userManager.FindByNameAsync(model.TenDangNhap);
            if (userExists != null)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new Response { Status = "Error", Message = "Tên đăng nhập đã tồn tại" });
            }
            else
            {
               
                ApplicationUser user = new()
                {
                    SecurityStamp = Guid.NewGuid().ToString(),
                    UserName = model.TenDangNhap,
                    HoVaTen = model.HoVaTen,
                    LockoutEnabled = false                                                         
                };
                var result = await _userManager.CreateAsync(user, model.MatKhau);
                if (!result.Succeeded)
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, new Response { Status = "Error", Message = "Đăng ký thất bại vui lòng kiểm tra lại" });
                }
                else
                {
                    if (model.Quyen != null && model.Quyen.Count > 0)
                    {
                        foreach (var item in model.Quyen)
                        {
                            if (_roleManager.RoleExistsAsync(item).GetAwaiter().GetResult())
                            {
                                _userManager.AddToRoleAsync(user, item).GetAwaiter().GetResult();
                            }
                            else
                            {
                                _roleManager.CreateAsync(new IdentityRole(item)).GetAwaiter().GetResult();
                                _userManager.AddToRoleAsync(user, item).GetAwaiter().GetResult();
                            }
                        }
                    }
                    var currentUser = HttpContext.User;
                    string _createBy = ClassCommon.GetTenNhanVien(currentUser);
                    // Thêm user vào bảng nhân viên
                    NhanVien nhanVien = new NhanVien() {
                     TenNhanVien = model.HoVaTen,
                     UserID = user.Id,
                     Active = true,
                     User = user,
                     CreateDate = DateTime.Now,
                     CreateBy = _createBy
                     };
                    
                    await _unitOfWork.NhanVienRepos.Add(nhanVien);
                    _unitOfWork.Save();
                    return Ok(new Response { Status = "Success", Message = "Đăng ký người dùng thành công!" });
                }                
            }                                 
        }

        [Authorize]
        [HttpPost]
        [Route("DoiMatKhau")]
        public async Task<IActionResult> DoiMatKhau(DoiMatKhau model)
        {
            var objNhanVien = _unitOfWork.NhanVienRepos.GetById(model.nhanVienID);
           
            if (objNhanVien == null)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Người dùng không tồn tại");
            }
            else
            {  
                var user = await _userManager.FindByIdAsync(objNhanVien.UserID);
                if (user == null)
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, "Người dùng không tồn tại");
                }
                else
                {
                    IdentityResult result = await _userManager.ChangePasswordAsync(user, model.MatKhauCu, model.MatKhauMoi);
                    if (!result.Succeeded)
                    {
                        return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
                    }
                    else
                    {
                        return Ok("Đổi mật khẩu thành công");
                    }
                }              
            }          
        }

        [Authorize]
        [HttpPost]
        [Route("DatLaiMatKhau")]
        public async Task<IActionResult> DatLaiMatKhau(DatLaiMatKhau model)
        {
            var objNhanVien = _unitOfWork.NhanVienRepos.GetById(model.nhanVienID);
            if (objNhanVien == null)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Người dùng không tồn tại");
            } 
            else
            {
                var user = await _userManager.FindByIdAsync(objNhanVien.UserID);
                if (user == null)
                {
                    return Ok("Người dùng không tồn tại");
                }
                else
                {
                    string token = await _userManager.GeneratePasswordResetTokenAsync(user);
                    IdentityResult result = await _userManager.ResetPasswordAsync(user, token, model.MatKhauMoi);
                    if (!result.Succeeded)
                    {
                        return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
                    }
                    else
                    {
                        return Ok("Đổi mật khẩu thành công");
                    }
                }
            }
            
        }

        [Authorize]
        [HttpPost]
        [Route("EditTenDangNhap")]
        public async Task<IActionResult> EditTenDangNhap(CapNhatModel model)
        {
            var objNhanVien = _unitOfWork.NhanVienRepos.GetById(model.nhanVienID);
            if (objNhanVien == null)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Người dùng không tồn tại");
            }
            else
            {
                var user = await _userManager.FindByIdAsync(objNhanVien.UserID);
                if (user == null)
                {
                    return Ok("Người dùng không tồn tại");
                }
                else
                {
                    user.UserName = model.TenDangNhap;
                    var result = await _userManager.UpdateAsync(user);

                    if (!result.Succeeded)
                    {
                        return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
                    }
                    else
                    {
                        return Ok("Đổi tên đăng nhập thành công");
                    }
                }
            }               
        }

        [Authorize]
        [HttpDelete]
        [Route("Delete")]
        public async Task<ActionResult> DeleteNguoiDung(int nhanVienID)
        {
            var objNhanVien = _unitOfWork.NhanVienRepos.GetById(nhanVienID);
            if (objNhanVien == null)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Người dùng không tồn tại");
            }
            else
            {
                var user = await _userManager.FindByIdAsync(objNhanVien.UserID);
                if (user == null)
                {
                    return Ok("Người dùng không tồn tại");
                }
                else
                {

                    var objNhanVienChucVu = await _unitOfWork.ChucVuRepos.GetByNhanVien(objNhanVien.NhanVienID);
                    if (objNhanVienChucVu.Count() == 0)
                    {
                        _unitOfWork.NhanVienRepos.Remove(objNhanVien);
                        _unitOfWork.Save();
                        var result = await _userManager.DeleteAsync(user);
                        if (!result.Succeeded)
                        {
                            return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
                        }
                        else
                        {
                            return Ok("Xóa người dùng thành công");
                        }
                    }
                    else
                    {
                        return Ok("Nhân viên đã phát sinh dữ liệu không thể xóa");
                    }
                   
                }
            }
               
        }

        [Authorize]
        [HttpPost]
        [Route("KhoaNguoiDung")]
        public async Task<ActionResult> KhoaNguoiDung(int nhanVienID)
        {
            var objNhanVien = _unitOfWork.NhanVienRepos.GetById(nhanVienID);
            if (objNhanVien == null)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Người dùng không tồn tại");
            }
            else
            {
               
                var user = await _userManager.FindByIdAsync(objNhanVien.UserID);

                if (user == null)
                {
                    return Ok("Người dùng không tồn tại");
                }
                else
                {
                    var currentUser = HttpContext.User;
                    var resultNguoiDung = await _unitOfWork.NhanVienRepos.DoiTrangThai(nhanVienID, currentUser);
                    var result = await _userManager.SetLockoutEnabledAsync(user, true);
                    var ThoiGianKhoa = await _userManager.SetLockoutEndDateAsync(user, DateTime.Now.AddYears(100));
                    if (!result.Succeeded)
                    {
                        return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
                    }
                    else
                    {
                        return Ok("Khóa người dùng thành công");
                    }
                }
            }             
        }

        [Authorize]
        [HttpPost]
        [Route("MoKhoaNguoiDung")]
        public async Task<ActionResult> MoKhoaNguoiDung(int nhanVienID)
        {
            var objNhanVien = _unitOfWork.NhanVienRepos.GetById(nhanVienID);
            if (objNhanVien == null)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Người dùng không tồn tại");
            }
            else
            {             
                var user = await _userManager.FindByIdAsync(objNhanVien.UserID);
                if (user == null)
                {
                    return Ok("Người dùng không tồn tại");
                }
                else
                {
                    var currentUser = HttpContext.User;
                    var resultNguoiDung = await _unitOfWork.NhanVienRepos.DoiTrangThai(nhanVienID, currentUser);
                    var result = await _userManager.SetLockoutEnabledAsync(user, false);
                    if (!result.Succeeded)
                    {
                        return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
                    }
                    else
                    {
                        return Ok("Mở khóa người dùng thành công");
                    }
                }
            }              
        }
        private JwtSecurityToken GetToken(List<Claim> authClaims)
        {
            var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Secret"]));

            var token = new JwtSecurityToken(
                issuer: _configuration["JWT:ValidIssuer"],
                audience: _configuration["JWT:ValidAudience"],
                expires: DateTime.Now.AddHours(24),
                claims: authClaims,
                signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)
                );

            return token;
        }
    }
}

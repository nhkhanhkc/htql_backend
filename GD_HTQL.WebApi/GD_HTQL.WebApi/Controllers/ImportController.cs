﻿using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using GD_HTQL.WebApi.Services.IRepository;
using GD_HTQL.WebApi.Data.Models.ImportModels;
using System.Globalization;
using GD_HTQL.WebApi.Infrastructure;
using GD_HTQL.WebApi.Common;
using AutoMapper;
using GD_HTQL.WebApi.Data.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using GD_HTQL.WebApi.Data.Models.EF.DuAnModels;
using GD_HTQL.WebApi.Data.Models.EF.NhaThauModels;
using GD_HTQL.WebApi.Data.Models.EF.SanPhamModels;
using GD_HTQL.WebApi.Data.Models.EF.NhaCungCapModels;
using GD_HTQL.WebApi.Data.Models.EF.TacGiaModels;
using GD_HTQL.WebApi.Data.Models.EF.DaiLyModels;

namespace GD_HTQL.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ImportController : ControllerBase
    {
        private readonly IImportRepository _import;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public ImportController(IImportRepository import , IUnitOfWork unitOfWork, IMapper mapper)
        {
            _import = import;          
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        [HttpPost("importNhaCungCap")]
        public async Task<IActionResult> ImportNhaCungCap(IFormFile formFile, CancellationToken cancellationToken)
        {
            if (formFile == null || formFile.Length <= 0)
            {
                return null;
            }

            if (!Path.GetExtension(formFile.FileName).Equals(".xlsx", StringComparison.OrdinalIgnoreCase))
            {
                return Ok("Loại file không được hổ trợ");
            }

            var list = new List<NhaCungCap>();
            var listError = new List<ImportError>();
            using (var stream = new MemoryStream())
            {
                await formFile.CopyToAsync(stream, cancellationToken);
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                using (var package = new ExcelPackage(stream))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[0];
                    var rowCount = worksheet.Dimension.Rows;
                    var currentUser = HttpContext.User;
                    int _nhanVienID = ClassCommon.GetUserID(currentUser);
                    string _tenNhanVien = ClassCommon.GetTenNhanVien(currentUser);
                    for (int row = 6; row <= rowCount; row++)
                    {
                        try
                        {
                            int _TinhID = await _import.GetTinhID(worksheet.Cells[row, 4].Text);
                            int _loaiNCCID = await _import.GetLoaiNhaCungCapID(worksheet.Cells[row, 12].Text);
                            string _masothue = string.IsNullOrWhiteSpace(worksheet.Cells[row, 3].Text) ? "" : worksheet.Cells[row, 3].Text.Trim();
                            if (_TinhID == 0)
                            {
                                listError.Add(new ImportError
                                {
                                    STT = worksheet.Cells[row, 1].Text,
                                    MaLoi = "Tên tỉnh không được tìm thấy"
                                });
                                continue;
                            }
                            else if (_loaiNCCID == 0)
                            {
                                listError.Add(new ImportError
                                {
                                    STT = worksheet.Cells[row, 1].Text,
                                    MaLoi = "Loại nhà cung cấp không được tìm thấy"
                                });
                                continue;
                            }
                            else if (_masothue != "" && await _import.CheckMST_NhaCungCap(_masothue) == false)
                            {
                                listError.Add(new ImportError
                                {
                                    STT = worksheet.Cells[row, 1].Text,
                                    MaLoi = "Mã số thuế đã tồn tại"
                                });
                                continue;
                            }
                            else
                            {
                                list.Add(new NhaCungCap
                                {
                                    TenCongTy = worksheet.Cells[row, 2].Text.Trim(),
                                    MaSoThue = _masothue,
                                    TinhID = _TinhID,
                                    DiaChi = string.IsNullOrWhiteSpace(worksheet.Cells[row, 5].Text) ? "" : worksheet.Cells[row, 5].Text.Trim(),
                                    NguoiDaiDien = string.IsNullOrWhiteSpace(worksheet.Cells[row, 6].Text) ? "" : worksheet.Cells[row, 6].Text.Trim(),
                                    NDDChucVu = string.IsNullOrWhiteSpace(worksheet.Cells[row, 7].Text) ? "" : worksheet.Cells[row, 7].Text.Trim(),
                                    NDDSoDienThoai = string.IsNullOrWhiteSpace(worksheet.Cells[row, 8].Text) ? "" : worksheet.Cells[row, 8].Text.Trim(),
                                    NhanVienPhuTrach = string.IsNullOrWhiteSpace(worksheet.Cells[row, 9].Text) ? "" : worksheet.Cells[row, 9].Text.Trim(),
                                    ChucVu = string.IsNullOrWhiteSpace(worksheet.Cells[row, 10].Text) ? "" : worksheet.Cells[row, 10].Text.Trim(),
                                    SoDienThoai = string.IsNullOrWhiteSpace(worksheet.Cells[row, 11].Text) ? "" : worksheet.Cells[row, 11].Text.Trim(),
                                    LoaiNCCID = _loaiNCCID,
                                    Ghichu = string.IsNullOrWhiteSpace(worksheet.Cells[row, 13].Text) ? "" : worksheet.Cells[row, 13].Text.Trim(),
                                    Active = true,
                                    NhanVienID = _nhanVienID,
                                    CreateDate = DateTime.Now,
                                    CreateBy = _tenNhanVien
                                });
                            }                          
                        }
                        catch (Exception ex)
                        {
                            listError.Add(new ImportError
                            {
                                STT = worksheet.Cells[row, 1].Text,                              
                                MaLoi = ex.ToString()
                            });
                            continue;
                        }                                                      
                    }
                    if (list.Count > 0)
                    {
                         _unitOfWork.NhaCungCapRepos.AddRange(list);
                        _unitOfWork.Save();
                    }
                    var objImportResult = new ImportResult
                    {                       
                        ImportError = listError,
                        Success = list.Count(),
                        Error = listError.Count()
                    };
                    return Ok(objImportResult);
                }
            }         
        }

        [HttpPost("importNhaThau")]
        public async Task<IActionResult> ImportNhaThau(IFormFile formFile, CancellationToken cancellationToken)
        {
            if (formFile == null || formFile.Length <= 0)
            {
                return null;
            }

            if (!Path.GetExtension(formFile.FileName).Equals(".xlsx", StringComparison.OrdinalIgnoreCase))
            {
                return Ok("Loại file không được hổ trợ");
            }

            var list = new List<NhaThau>();
            var listError = new List<ImportError>();
            using (var stream = new MemoryStream())
            {
                await formFile.CopyToAsync(stream, cancellationToken);
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                using (var package = new ExcelPackage(stream))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[0];
                    var rowCount = worksheet.Dimension.Rows;
                    var currentUser = HttpContext.User;
                    int _nhanVienID = ClassCommon.GetUserID(currentUser);
                    string _tenNhanVien = ClassCommon.GetTenNhanVien(currentUser);

                    for (int row = 6; row <= rowCount; row++)
                    {
                        try
                        {
                            int _TinhID = await _import.GetTinhID(worksheet.Cells[row, 4].Text);
                            int _loaiNTID = await _import.GetLoaiNhaThauID(worksheet.Cells[row, 12].Text);
                            int _loaiHinhHopTac = await _import.GetLoaiHinhHopTacID(worksheet.Cells[row, 13].Text);
                            int _diaBanHoatDong = await _import.GetDiaBanHoatDongID(worksheet.Cells[row, 14].Text);
                            string _masothue = string.IsNullOrWhiteSpace(worksheet.Cells[row, 3].Text) ? "" : worksheet.Cells[row, 3].Text.Trim();
                            if (_TinhID == 0)
                            {
                                listError.Add(new ImportError
                                {
                                    STT = worksheet.Cells[row, 1].Text.Trim(),
                                    MaLoi = "Tên tỉnh không được tìm thấy"
                                });
                                continue;
                            }
                            else if (_loaiNTID == 0)
                            {
                                listError.Add(new ImportError
                                {
                                    STT = worksheet.Cells[row, 1].Text,
                                    MaLoi = "Loại nhà thầu không được tìm thấy"
                                });
                                continue;
                            }
                            else if (_masothue !="" && await _import.CheckMST_NhaThau(_masothue) == false)
                            {
                                listError.Add(new ImportError
                                {
                                    STT = worksheet.Cells[row, 1].Text,
                                    MaLoi = "Mã số thuế đã tồn tại"
                                });
                                continue;
                            }
                            else
                            {
                                list.Add(new NhaThau
                                {
                                    TenCongTy = worksheet.Cells[row, 2].Text.Trim(),
                                    MaSoThue = _masothue,
                                    TinhID = _TinhID,
                                    DiaChi = string.IsNullOrWhiteSpace(worksheet.Cells[row, 5].Text) ? "" : worksheet.Cells[row, 5].Text.Trim(),
                                    Email  = string.IsNullOrWhiteSpace(worksheet.Cells[row, 6].Text) ? "" : worksheet.Cells[row, 6].Text.Trim(),
                                    NguoiDaiDien = string.IsNullOrWhiteSpace(worksheet.Cells[row, 7].Text) ? "" : worksheet.Cells[row, 7].Text.Trim(),
                                    NDDChucVu = string.IsNullOrWhiteSpace(worksheet.Cells[row, 8].Text) ? "" : worksheet.Cells[row, 8].Text.Trim(),
                                    NDDSoDienThoai = string.IsNullOrWhiteSpace(worksheet.Cells[row, 9].Text) ? "" : worksheet.Cells[row, 9].Text.Trim(),
                                    NhanVienPhuTrach = string.IsNullOrWhiteSpace(worksheet.Cells[row, 10].Text) ? "" : worksheet.Cells[row, 10].Text.Trim(),
                                    ChucVu = string.IsNullOrWhiteSpace(worksheet.Cells[row, 11].Text) ? "" : worksheet.Cells[row, 11].Text.Trim(),
                                    SoDienThoai = string.IsNullOrWhiteSpace(worksheet.Cells[row, 12].Text) ? "" : worksheet.Cells[row, 12].Text.Trim(),
                                    LoaiNTID = _loaiNTID,
                                    LoaiHopTacID = _loaiHinhHopTac == 0 ? null : _loaiHinhHopTac,
                                    DiaBanID = _diaBanHoatDong == 0 ? null : _diaBanHoatDong,
                                    GhiChu = string.IsNullOrWhiteSpace(worksheet.Cells[row, 16].Text) ? "" : worksheet.Cells[row, 16].Text.Trim(),
                                    Active = true,
                                    NhanVienID = _nhanVienID,
                                    CreateDate = DateTime.Now,
                                    CreateBy = _tenNhanVien
                                });
                            }                        
                        }
                        catch (Exception ex)
                        {
                            listError.Add(new ImportError
                            {
                                STT = worksheet.Cells[row, 1].Text,
                                MaLoi = ex.ToString()
                            });
                            continue;
                        }
                    }

                    if (list.Count > 0)
                    {
                         _unitOfWork.NhaThauRepos.AddRange(list);
                        _unitOfWork.Save();
                    }
                    var objImportResult = new ImportResult
                    {                      
                        ImportError = listError,
                        Success = list.Count(),
                        Error = listError.Count()
                    };
                    return Ok(objImportResult);
                }
            }        
        }

        [HttpPost("importDaiLy")]
        public async Task<IActionResult> importDaiLy(IFormFile formFile, CancellationToken cancellationToken)
        {
            if (formFile == null || formFile.Length <= 0)
            {
                return null;
            }

            if (!Path.GetExtension(formFile.FileName).Equals(".xlsx", StringComparison.OrdinalIgnoreCase))
            {
                return Ok("Loại file không được hổ trợ");
            }

            var list = new List<DaiLy>();
            var listError = new List<ImportError>();
            using (var stream = new MemoryStream())
            {
                await formFile.CopyToAsync(stream, cancellationToken);
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                using (var package = new ExcelPackage(stream))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[0];
                    var rowCount = worksheet.Dimension.Rows;
                    var currentUser = HttpContext.User;
                    int _nhanVienID = ClassCommon.GetUserID(currentUser);
                    string _tenNhanVien = ClassCommon.GetTenNhanVien(currentUser);
                    for (int row = 6; row <= rowCount; row++)
                    {
                        try
                        {
                            int _TinhID = await _import.GetTinhID(worksheet.Cells[row, 4].Text);
                            int _loaiDLID = await _import.GetLoaiDaiLyID(worksheet.Cells[row, 12].Text);
                            string _masothue = string.IsNullOrWhiteSpace(worksheet.Cells[row, 3].Text) ? "" : worksheet.Cells[row, 3].Text.Trim();
                            if (_TinhID == 0)
                            {
                                listError.Add(new ImportError
                                {
                                    STT = worksheet.Cells[row, 1].Text,
                                    MaLoi = "Tên tỉnh không được tìm thấy"
                                });
                                continue;
                            }
                            else if (_loaiDLID == 0)
                            {
                                listError.Add(new ImportError
                                {
                                    STT = worksheet.Cells[row, 1].Text,
                                    MaLoi = "Loại nhà thầu không được tìm thấy"
                                });
                                continue;
                            }
                            else if (_masothue != "" && await _import.CheckMST_DaiLy(_masothue) == false)
                            {
                                listError.Add(new ImportError
                                {
                                    STT = worksheet.Cells[row, 1].Text,
                                    MaLoi = "Mã số thuế đã tồn tại"
                                });
                                continue;
                            }
                            else
                            {
                                list.Add(new DaiLy
                                {
                                    TenDaiLy = worksheet.Cells[row, 2].Text.Trim(),
                                    MaSoThue = _masothue,
                                    TinhID = _TinhID,
                                    DiaChi = string.IsNullOrWhiteSpace(worksheet.Cells[row, 5].Text) ? "" : worksheet.Cells[row, 5].Text.Trim(),
                                    NguoiDaiDien = string.IsNullOrWhiteSpace(worksheet.Cells[row, 6].Text) ? "" : worksheet.Cells[row, 6].Text.Trim(),
                                    NDDChucVu = string.IsNullOrWhiteSpace(worksheet.Cells[row, 7].Text) ? "" : worksheet.Cells[row, 7].Text.Trim(),
                                    NDDSoDienThoai = string.IsNullOrWhiteSpace(worksheet.Cells[row, 8].Text) ? "" : worksheet.Cells[row, 8].Text.Trim(),
                                    NhanVienPhuTrach = string.IsNullOrWhiteSpace(worksheet.Cells[row, 9].Text) ? "" : worksheet.Cells[row, 9].Text.Trim(),
                                    ChucVu = string.IsNullOrWhiteSpace(worksheet.Cells[row, 10].Text) ? "" : worksheet.Cells[row, 10].Text.Trim(),
                                    SoDienThoai = string.IsNullOrWhiteSpace(worksheet.Cells[row, 11].Text) ? "" : worksheet.Cells[row, 11].Text.Trim(),
                                    LoaiDLID = _loaiDLID,
                                    GhiChu = string.IsNullOrWhiteSpace(worksheet.Cells[row, 13].Text) ? "" : worksheet.Cells[row, 13].Text.Trim(),
                                    Active = true,
                                    NhanVienID = _nhanVienID,
                                    CreateDate = DateTime.Now,
                                    CreateBy = _tenNhanVien
                                });
                            }
                        }
                        catch (Exception ex)
                        {
                            listError.Add(new ImportError
                            {
                                STT = worksheet.Cells[row, 1].Text,
                                MaLoi = ex.ToString()
                            });
                            continue;
                        }
                    }

                    if (list.Count > 0)
                    {
                        _unitOfWork.DaiLyRepos.AddRange(list);
                        _unitOfWork.Save();
                    }
                    var objImportResult = new ImportResult
                    {
                        ImportError = listError,
                        Success = list.Count(),
                        Error = listError.Count()
                    };
                    return Ok(objImportResult);
                }
            }
        }


        [HttpPost("importTacGia")]
        public async Task<IActionResult> ImportTacGia(IFormFile formFile, CancellationToken cancellationToken)
        {
            if (formFile == null || formFile.Length <= 0)
            {
                return null;
            }

            if (!Path.GetExtension(formFile.FileName).Equals(".xlsx", StringComparison.OrdinalIgnoreCase))
            {
                return Ok("Loại file không được hổ trợ");
            }

            var list = new List<TacGia>();
            var listError = new List<ImportError>();
            using (var stream = new MemoryStream())
            {
                await formFile.CopyToAsync(stream, cancellationToken);
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                using (var package = new ExcelPackage(stream))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[0];
                    var rowCount = worksheet.Dimension.Rows;
                    var currentUser = HttpContext.User;
                    int _nhanVienID = ClassCommon.GetUserID(currentUser);
                    string _tenNhanVien = ClassCommon.GetTenNhanVien(currentUser);
                    for (int row = 6; row <= rowCount; row++)
                    {
                        try
                        {
                            string _ngaySinh = worksheet.Cells[row, 3].Text + " 00:00:00";
                             if (!string.IsNullOrEmpty(worksheet.Cells[row, 3].Text) && !_import.CheckDateTime(_ngaySinh))
                            {
                                listError.Add(new ImportError
                                {
                                    STT = worksheet.Cells[row, 1].Text,
                                    MaLoi = "Ngày sinh không đúng định dạng"
                                });
                                continue;
                            }
                            else
                            {
                                int _gioiTinh = -1;
                                if (!string.IsNullOrWhiteSpace(worksheet.Cells[row, 7].Text) && worksheet.Cells[row, 7].Text.ToLower() == "nam")
                                {
                                    _gioiTinh = 1;
                                }
                                if (!string.IsNullOrWhiteSpace(worksheet.Cells[row, 7].Text) && worksheet.Cells[row, 7].Text.ToLower() == "nữ")
                                {
                                    _gioiTinh = 0;
                                }

                                list.Add(new TacGia
                                {
                                    TenTacGia = worksheet.Cells[row, 2].Text.Trim(),
                                    NgaySinh = string.IsNullOrWhiteSpace(worksheet.Cells[row, 3].Text) ? null : DateTime.ParseExact(_ngaySinh, Common.ClassParameter.formatDate, CultureInfo.InvariantCulture),
                                    GioiTinh = _gioiTinh,
                                    CCCD = string.IsNullOrWhiteSpace(worksheet.Cells[row, 5].Text) ? "" : worksheet.Cells[row, 5].Text.Trim(),
                                    SoDienThoai = string.IsNullOrWhiteSpace(worksheet.Cells[row, 6].Text) ? "" : worksheet.Cells[row, 6].Text.Trim(),
                                    Email = string.IsNullOrWhiteSpace(worksheet.Cells[row, 7].Text) ? "" : worksheet.Cells[row, 7].Text.Trim(),
                                    DonViCongTac = string.IsNullOrWhiteSpace(worksheet.Cells[row, 8].Text) ? "" : worksheet.Cells[row, 8].Text.Trim(),
                                    ChucVuTacGia = string.IsNullOrWhiteSpace(worksheet.Cells[row, 9].Text) ? "" : worksheet.Cells[row, 9].Text.Trim(),
                                    MonChuyenNghanh = string.IsNullOrWhiteSpace(worksheet.Cells[row, 10].Text) ? "" : worksheet.Cells[row, 10].Text.Trim(),
                                    Active = true,
                                    NhanVienID = _nhanVienID,
                                    CreateDate = DateTime.Now,
                                    CreateBy = _tenNhanVien
                                });
                            }                         
                        }
                        catch (Exception ex)
                        {
                            listError.Add(new ImportError
                            {
                                STT = worksheet.Cells[row, 1].Text,
                                MaLoi = ex.ToString()
                            });
                            continue;
                        }
                    }
                    if (list.Count > 0)
                    {
                        _unitOfWork.TacGiaRepos.AddRange(list);
                        _unitOfWork.Save();
                    }
                    var objImportResult = new ImportResult
                    {                    
                        ImportError = listError,
                        Success = list.Count(),
                        Error = listError.Count()
                    };
                    return Ok(objImportResult);
                }
            }          
        }

        [HttpPost("importKhachHang")]
        public async Task<IActionResult> ImportKhachHang(IFormFile formFile, CancellationToken cancellationToken)
        {
            if (formFile == null || formFile.Length <= 0)
            {
                return null;
            }

            if (!Path.GetExtension(formFile.FileName).Equals(".xlsx", StringComparison.OrdinalIgnoreCase))
            {
                return Ok("Loại file không được hổ trợ");
            }          
            var listError = new List<ImportError>();          
            using (var stream = new MemoryStream())
            {
                await formFile.CopyToAsync(stream, cancellationToken);
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                using (var package = new ExcelPackage(stream))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[0];
                    //ExcelWorksheet worksheet = new ExcelPackage().Workbook.Worksheets.Add("Sheet1");
                    var rowCount = worksheet.Dimension.Rows;
                    int _CoQuanId = 0;
                    int _countSuccess = 0;
                    var currentUser = HttpContext.User;
                    int _nhanVienID = ClassCommon.GetUserID(currentUser);
                    string _tenNhanVien = ClassCommon.GetTenNhanVien(currentUser);
                    for (int row = 6; row <= rowCount; row++)
                    {
                        try
                        {
                            if (! string.IsNullOrEmpty(worksheet.Cells[row, 2].Text)  && worksheet.Cells[row, 2].Text != worksheet.Cells[row -1, 2].Text)
                            {
                                int _TinhID = await _import.GetTinhID(worksheet.Cells[row, 4].Text);
                                int _HuyenID = await _import.GetHuyenID(worksheet.Cells[row, 5].Text, _TinhID);
                                int _xaID = await _import.GetXaID(worksheet.Cells[row, 6].Text, _HuyenID);
                                string _masothue = string.IsNullOrWhiteSpace(worksheet.Cells[row, 3].Text) ? "" : worksheet.Cells[row, 3].Text.Trim();
                                if (_TinhID == 0)
                                {
                                    _CoQuanId = 0;
                                    listError.Add(new ImportError
                                    {
                                        STT = worksheet.Cells[row, 1].Text,
                                        MaLoi = "Tên tỉnh không được tìm thấy"
                                    });
                                    continue;
                                }
                                else if (await _import.GetHuyenID(worksheet.Cells[row, 5].Text, _TinhID) == 0)
                                {
                                    _CoQuanId = 0;
                                    listError.Add(new ImportError
                                    {
                                        STT = worksheet.Cells[row, 1].Text,
                                        MaLoi = "Tên huyện không được tìm thấy"
                                    });
                                    continue;
                                }
                                else if (_masothue != "" && await _import.CheckMST_CoQuan(_masothue) == false)
                                {
                                    listError.Add(new ImportError
                                    {
                                        STT = worksheet.Cells[row, 1].Text,
                                        MaLoi = "Mã số thuế đã tồn tại"
                                    });
                                    continue;
                                }
                                else 
                                {
                                    var objCoQuan = (new NSCoQuan
                                    {
                                        TenCoQuan = worksheet.Cells[row, 2].Text.Trim(),
                                        MaSoThue = _masothue,
                                        TinhID = _TinhID,
                                        HuyenID = _HuyenID,
                                        XaID = _xaID,
                                        Active = true,
                                        NhanVienID = _nhanVienID,
                                        CreateDate = DateTime.Now,
                                        CreateBy = _tenNhanVien
                                    });
                                    var CoQuanReturn = await _unitOfWork.NSCoQuanRepos.Add(objCoQuan);
                                    _unitOfWork.Save();
                                    _CoQuanId = CoQuanReturn.CoQuanID;
                                    _countSuccess++;
                                }
                            }
                            if (!string.IsNullOrEmpty(worksheet.Cells[row, 7].Text) && _CoQuanId > 0)
                            {
                                try
                                {
                                    var listCanBo = new List<NSCanBo>();
                                    int GioiTinh = -1;
                                    if (!string.IsNullOrWhiteSpace(worksheet.Cells[row, 8].Text) && worksheet.Cells[row, 8].Text.ToLower() == "nam")
                                    {
                                        GioiTinh = 1;
                                    }
                                    if (!string.IsNullOrWhiteSpace(worksheet.Cells[row, 8].Text) && worksheet.Cells[row, 8].Text.ToLower() == "nữ")
                                    {
                                        GioiTinh = 0;
                                    }
                                    var objCanBo = (new NSCanBo
                                    {
                                        HoVaTen = worksheet.Cells[row, 7].Text.Trim(),
                                        GioiTinh = GioiTinh,
                                        SoDienThoai = string.IsNullOrWhiteSpace(worksheet.Cells[row, 9].Text) ? "" : worksheet.Cells[row, 9].Text,
                                        Email = string.IsNullOrWhiteSpace(worksheet.Cells[row, 10].Text) ? "" : worksheet.Cells[row, 10].Text,
                                        ChucVu = string.IsNullOrWhiteSpace(worksheet.Cells[row, 11].Text) ? "" : worksheet.Cells[row, 11].Text,
                                        CoQuanID = _CoQuanId,
                                        Active = true,
                                        CreateDate = DateTime.Now,
                                        CreateBy = _tenNhanVien
                                    });

                                     await  _unitOfWork.NSCanBoRepos.Add(objCanBo);
                                    _unitOfWork.Save();
                                    _countSuccess++;
                                }
                                catch (Exception ex) 
                                {
                                    listError.Add(new ImportError
                                    {
                                        STT = worksheet.Cells[row, 1].Text,
                                        MaLoi = ex.ToString()
                                    });
                                    continue;
                                }                             
                            }                          
                        }
                        catch (Exception ex)
                        {
                            _CoQuanId = 0;
                            listError.Add(new ImportError
                            {
                                STT = worksheet.Cells[row, 1].Text,
                                MaLoi = ex.ToString()
                            });
                            continue;
                        }
                    }
                    var objImportResult = new ImportResult
                    {
                        ImportError = listError,
                        Success = _countSuccess,
                        Error = listError.Count()
                    };
                    return Ok(objImportResult);
                }
            }          
        }

        [HttpPost("importBaoCaoTiepXuc")]
        public async Task<IActionResult> ImportBaoCaoTiepXuc(IFormFile formFile, CancellationToken cancellationToken)
        {
            if (formFile == null || formFile.Length <= 0)
            {
                return null;
            }

            if (!Path.GetExtension(formFile.FileName).Equals(".xlsx", StringComparison.OrdinalIgnoreCase))
            {
                return Ok("Loại file không được hổ trợ");
            }

            var list = new List<QuanLyTuongTac>();
            var listError = new List<ImportError>();
            using (var stream = new MemoryStream())
            {
                await formFile.CopyToAsync(stream, cancellationToken);
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                using (var package = new ExcelPackage(stream))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[0];
                    var rowCount = worksheet.Dimension.Rows;
                    // Kiểm tra nhân viên báo cáo tiếp xúc                
                    var currentUser = HttpContext.User;
                    int _nhanVienID = ClassCommon.GetUserID(currentUser);
                    string _tenNhanVien = ClassCommon.GetTenNhanVien(currentUser);
                    if (_nhanVienID == 0)
                    {
                        listError.Add(new ImportError
                        {
                            STT = "#",
                            MaLoi = "Nhân viên không được tìm thấy"
                        });
                    }
                    else
                    {
                        for (int row = 7; row <= rowCount; row++)
                        {
                            try
                            {
                                int _coquanID = await _import.GetCoQuanIDByName(worksheet.Cells[row, 2].Text);
                                int _buocThiTruongID = await _import.GetBuocThiTruongID(worksheet.Cells[row, 7].Text);
                                string _thoigiantiepxuc = worksheet.Cells[row, 11].Text + " 00:00:00";
                                if (_coquanID == 0)
                                {
                                    listError.Add(new ImportError
                                    {
                                        STT = worksheet.Cells[row, 1].Text,
                                        MaLoi = "Tên cơ quan không được tìm thấy"
                                    });
                                    continue;
                                }
                                else if (_buocThiTruongID == 0)
                                {
                                    listError.Add(new ImportError
                                    {
                                        STT = worksheet.Cells[row, 1].Text,
                                        MaLoi = "Bước thị trường không đúng"
                                    });
                                }
                                else if (!string.IsNullOrEmpty(worksheet.Cells[row, 10].Text) && !_import.CheckDecimal(worksheet.Cells[row, 10].Text))
                                {
                                    listError.Add(new ImportError
                                    {
                                        STT = worksheet.Cells[row, 1].Text,
                                        MaLoi = "Doanh thu dự kiến không đúng định dạng"
                                    });
                                    continue;
                                }
                                else if (!string.IsNullOrEmpty(worksheet.Cells[row, 11].Text) && !_import.CheckDateTime(_thoigiantiepxuc))
                                {
                                    listError.Add(new ImportError
                                    {
                                        STT = worksheet.Cells[row, 1].Text,
                                        MaLoi = "Thời gian tiếp xúc không đúng định dạng"
                                    });
                                    continue;
                                }
                               
                                else
                                {
                                    list.Add(new QuanLyTuongTac
                                    {
                                        CoQuanID = _coquanID,
                                        ThongTinLienHe = string.IsNullOrWhiteSpace(worksheet.Cells[row, 5].Text) ? "" : worksheet.Cells[row, 3].Text.Trim(),
                                        CanBoTiepXuc = string.IsNullOrWhiteSpace(worksheet.Cells[row, 6].Text) ? "" : worksheet.Cells[row, 4].Text,
                                        BuocThiTruongID =  _buocThiTruongID,
                                        ThongTinTiepXuc = string.IsNullOrWhiteSpace(worksheet.Cells[row, 8].Text) ? "" : worksheet.Cells[row, 6].Text.Trim(),
                                        NhomHangQuanTam = string.IsNullOrWhiteSpace(worksheet.Cells[row, 9].Text) ? "" : worksheet.Cells[row, 7].Text.Trim(),
                                        DoanhThuDuKien = string.IsNullOrEmpty(worksheet.Cells[row, 10].Text) ? 0 : Convert.ToDecimal(worksheet.Cells[row, 10].Text.Replace(",", string.Empty).Replace(".", string.Empty)),
                                        ThoiGian = string.IsNullOrEmpty(worksheet.Cells[row, 11].Text) ? null : Convert.ToDateTime(_thoigiantiepxuc),                                      
                                        GhiChu = string.IsNullOrWhiteSpace(worksheet.Cells[row, 12].Text) ? "" : worksheet.Cells[row, 12].Text.Trim(),
                                        NhanVienID = _nhanVienID,
                                        Active = true,
                                        CreateDate = DateTime.Now,
                                        CreateBy = _tenNhanVien
                                    }) ;
                                }
                            }
                            catch (Exception ex)
                            {
                                listError.Add(new ImportError
                                {
                                    STT = worksheet.Cells[row, 1].Text,
                                    MaLoi = ex.ToString()
                                });
                                continue;
                            }
                        }

                        if (list.Count > 0)
                        {
                            _unitOfWork.QuanLyTuongTacRepos.AddRange(list);
                            _unitOfWork.Save();
                        }
                    }
                 
                    var objImportResult = new ImportResult
                    {                    
                        ImportError = listError,
                        Success = list.Count(),
                        Error = listError.Count()
                    };
                    return Ok(objImportResult);
                }
            }          
        }

        [HttpPost("importSanPham")]
        public async Task<IActionResult> ImportSanPham(IFormFile formFile, CancellationToken cancellationToken)
        {        
            if (!Path.GetExtension(formFile.FileName).Equals(".xlsx", StringComparison.OrdinalIgnoreCase))
            {
                return Ok("Loại file không được hổ trợ");
            }

            var list = new List<SanPham>();
            var listError = new List<ImportError>();
            using (var stream = new MemoryStream())
            {
                await formFile.CopyToAsync(stream, cancellationToken);
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                using (var package = new ExcelPackage(stream))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[0];
                    var rowCount = worksheet.Dimension.Rows;

                    for (int row = 3; row <= rowCount; row++)
                    {
                        try
                        {
                            // Check mã sản phẩm đã tồn tại chưa
                            if (await _import.CheckMaSanPham(worksheet.Cells[row, 2].Text.Trim()))
                            {
                                int _loaiSanPhamID = await _import.GetLoaiSanPhamIDByName(worksheet.Cells[row, 10].Text); 
                                int _DVTID = await _import.GetDonViTinh(worksheet.Cells[row, 16].Text);

                                if (!string.IsNullOrEmpty(worksheet.Cells[row, 10].Text) && _loaiSanPhamID == 0)
                                {
                                    listError.Add(new ImportError
                                    {
                                        STT = worksheet.Cells[row, 1].Text,
                                        MaLoi = "Loại sản phẩm không được tìm thấy trong hệ thống"
                                    });
                                    continue;
                                }
                                if (!string.IsNullOrEmpty(worksheet.Cells[row, 16].Text) && _DVTID == 0)
                                {
                                    listError.Add(new ImportError
                                    {
                                        STT = worksheet.Cells[row, 1].Text,
                                        MaLoi = "Đơn vị tính không được tìm thấy trong hệ thống"
                                    });
                                    continue;
                                }
                                else if (! string.IsNullOrEmpty(worksheet.Cells[row, 17].Text) && !_import.CheckDecimal(worksheet.Cells[row, 17].Text))
                                {
                                    listError.Add(new ImportError
                                    {
                                        STT = worksheet.Cells[row, 1].Text,
                                        MaLoi = "Giá vốn không đúng đinh dạng"
                                    });
                                    continue;
                                }
                                else if (!string.IsNullOrEmpty(worksheet.Cells[row, 18].Text) && !_import.CheckDecimal(worksheet.Cells[row, 18].Text))
                                {
                                    listError.Add(new ImportError
                                    {
                                        STT = worksheet.Cells[row, 1].Text,
                                        MaLoi = "Giá đại lý không đúng định dạng"
                                    });
                                    continue;
                                }
                                else if (!string.IsNullOrEmpty(worksheet.Cells[row, 19].Text) && !_import.CheckDecimal(worksheet.Cells[row, 19].Text))
                                {
                                    listError.Add(new ImportError
                                    {
                                        STT = worksheet.Cells[row, 1].Text,
                                        MaLoi = "Giá trường không đúng định dạng"
                                    });
                                    continue;
                                }
                                else if (!string.IsNullOrEmpty(worksheet.Cells[row, 20].Text) && !_import.CheckDecimal(worksheet.Cells[row, 20].Text))
                                {
                                    listError.Add(new ImportError
                                    {
                                        STT = worksheet.Cells[row, 1].Text,
                                        MaLoi = "Giá huyện - tỉnh không đúng định dạng"
                                    });
                                    continue;
                                }
                                else if (!string.IsNullOrEmpty(worksheet.Cells[row, 21].Text) && !_import.CheckDecimal(worksheet.Cells[row, 21].Text))
                                {
                                    listError.Add(new ImportError
                                    {
                                        STT = worksheet.Cells[row, 1].Text,
                                        MaLoi = "Giá CDT không đúng định dạng"
                                    });
                                    continue;
                                }
                                else if (!string.IsNullOrEmpty(worksheet.Cells[row, 22].Text) && !_import.CheckDecimal(worksheet.Cells[row, 22].Text))
                                {
                                    listError.Add(new ImportError
                                    {
                                        STT = worksheet.Cells[row, 1].Text,
                                        MaLoi = "Thuế không đúng định dạng"
                                    });
                                    continue;
                                }
                                else
                                {
                                    var sanPham = new SanPham
                                    {
                                        MaSanPham = worksheet.Cells[row, 2].Text.Trim(),
                                        TenSanPham = worksheet.Cells[row, 3].Text.Trim(),
                                        TieuChuanKyThuat = worksheet.Cells[row, 4].Text.Trim(),
                                        NhaXuatBan = worksheet.Cells[row, 5].Text.Trim(),
                                        XuatXu = worksheet.Cells[row, 6].Text.Trim(),
                                        BaoHanh = Convert.ToInt32(worksheet.Cells[row, 7].Text),
                                        NamSanXuat = Convert.ToInt32(worksheet.Cells[row, 8].Text),
                                        Model = worksheet.Cells[row, 9].Text.Trim(),
                                        LoaiSanPhamID = _loaiSanPhamID,
                                        // 10 LoaiSanPham
                                        // 11 danh mục
                                        // 12 khối lớp
                                        // 13 môn học
                                        ThuongHieu = worksheet.Cells[row, 14].Text,
                                        HangSanXuat = worksheet.Cells[row, 15].Text.Trim(),
                                        DVTID = _DVTID,
                                        GiaVon = string.IsNullOrEmpty(worksheet.Cells[row, 17].Text) ? null : Convert.ToDecimal(worksheet.Cells[row, 17].Text.Trim().Replace(",", string.Empty).Replace(".", string.Empty)),
                                        GiaDaiLy = string.IsNullOrEmpty(worksheet.Cells[row, 18].Text) ? null : Convert.ToDecimal(worksheet.Cells[row, 18].Text.Trim().Replace(",", string.Empty).Replace(".", string.Empty)),
                                        GiaTTR = string.IsNullOrEmpty(worksheet.Cells[row, 19].Text) ? null : Convert.ToDecimal(worksheet.Cells[row, 19].Text.Trim().Replace(",", string.Empty).Replace(".", string.Empty)),
                                        GiaTH_TT = string.IsNullOrEmpty(worksheet.Cells[row, 20].Text) ? null : Convert.ToDecimal(worksheet.Cells[row, 20].Text.Trim().Replace(",", string.Empty).Replace(".", string.Empty)),
                                        GiaCDT = string.IsNullOrEmpty(worksheet.Cells[row, 21].Text) ? null : Convert.ToDecimal(worksheet.Cells[row, 21].Text.Trim().Replace(",", string.Empty).Replace(".", string.Empty)),
                                        Thue = string.IsNullOrEmpty(worksheet.Cells[row, 22].Text) ? null : Convert.ToDecimal(worksheet.Cells[row, 22].Text.Trim().Replace(",", string.Empty).Replace(".", string.Empty)),
                                        DoiTuongSuDung = worksheet.Cells[row, 23].Text,
                                        SoLuong = Convert.ToInt32(worksheet.Cells[row, 24].Text)
                                    };

                                   var result =   await _unitOfWork.SanPhamRepos.Add(sanPham);
                                    _unitOfWork.Save();

                                    // Add danh muc
                                    if(!string.IsNullOrEmpty(worksheet.Cells[row, 11].Text))
                                    {
                                        await _import.InsertSanPham_ThongTu(worksheet.Cells[row, 11].Text , result.SanPhamID);
                                    }

                                    // Add khối lớp 
                                    if (!string.IsNullOrEmpty(worksheet.Cells[row, 12].Text))
                                    {
                                        await _import.InsertSanPham_Lop(worksheet.Cells[row, 12].Text, result.SanPhamID);
                                    }
                                    // Add môn học 
                                    if (!string.IsNullOrEmpty(worksheet.Cells[row, 13].Text))
                                    {
                                        await _import.InsertSanPham_Monhoc(worksheet.Cells[row, 13].Text, result.SanPhamID);
                                    }

                                    list.Add(sanPham);                                
                                }                             
                            }
                            else
                            {
                                listError.Add(new ImportError
                                {
                                    STT = worksheet.Cells[row, 1].Text,                                  
                                    MaLoi = "Mã sản phẩm đã tồn tại"
                                });
                                continue;
                            }
                        }
                        catch (Exception ex)
                        {
                            listError.Add(new ImportError
                            {
                                STT = worksheet.Cells[row, 1].Text,                             
                                MaLoi = ex.ToString()
                            });
                            continue;
                        }                                                                         
                    }

                    //if (list.Count > 0)
                    //{
                    //    _unitOfWork.SanPhamRepos.AddRange(list);
                    //    _unitOfWork.Save();
                    //}
                    var objImportResult = new ImportResult
                    {                      
                        ImportError = listError,
                        Success = list.Count(),
                        Error = listError.Count()
                    };
                    return Ok(objImportResult);
                }
            }           
        }


        [HttpPost("importDuToan/{column}/{firstRow}")]
        public async Task<IActionResult> importDuToan(IFormFile formFile, int column, int firstRow, CancellationToken cancellationToken)
        {
            if (formFile == null || formFile.Length <= 0)
            {
                return null;
            }

            if (!Path.GetExtension(formFile.FileName).Equals(".xlsx", StringComparison.OrdinalIgnoreCase))
            {
                return Ok("Loại file không được hổ trợ");
            }

            var lstSanPham = new List<SanPhamViewDto>();
            var listError = new List<ImportError>();         
            using (var stream = new MemoryStream())
            {
                await formFile.CopyToAsync(stream, cancellationToken);
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                using (var package = new ExcelPackage(stream))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[0];
                    var rowCount = worksheet.Dimension.Rows;
                    var currentUser = HttpContext.User;
                    int _nhanVienID = ClassCommon.GetUserID(currentUser);

                    for (int row = firstRow; row <= rowCount; row++)
                    {
                        try
                        {
                            var objSanPham = _import.GetLikeTenSanPham(worksheet.Cells[row, column].Text);                        
                            if (objSanPham == null)
                            {
                                listError.Add(new ImportError
                                {
                                    STT = worksheet.Cells[row, 1].Text,
                                    MaLoi = "Tên sản phẩm không được tìm thấy"
                                });
                                continue;
                            }                           
                            else
                            {
                                SanPhamViewDto objDto = new SanPhamViewDto();
                                objDto = _mapper.Map<SanPhamViewDto>(objSanPham);

                                lstSanPham.Add(objDto);
                            }
                        }
                        catch (Exception ex)
                        {
                            listError.Add(new ImportError
                            {
                                STT = worksheet.Cells[row, 1].Text,
                                MaLoi = ex.ToString()
                            });
                            continue;
                        }
                    }

                    var objImportResult = new ImportSanPhamDuToanResult
                    {
                        ImportError = listError,
                        lstSanPham = lstSanPham,
                        Success = lstSanPham.Count(),
                        Error = listError.Count()
                    };
                    return Ok(objImportResult);
                }
            }
        }

    }
}

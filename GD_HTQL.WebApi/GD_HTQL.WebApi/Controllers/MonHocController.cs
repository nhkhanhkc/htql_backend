﻿using AutoMapper;
using GD_HTQL.WebApi.Data.Models.Dtos.SanPham;
using GD_HTQL.WebApi.Data.Models.EF.SanPhamModels;
using GD_HTQL.WebApi.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GD_HTQL.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class MonHocController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public MonHocController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;

        }
        [HttpGet]
        [Route("GetMonHoc")]
        public async Task<IActionResult> Get()
        {
            return Ok(await _unitOfWork.MonHocRepos.GetAll());
        }
        [HttpGet]
        [Route("GetMonHocById/{Id}")]
        public IActionResult GetMonHocById(int Id)
        {
            return Ok( _unitOfWork.MonHocRepos.GetById(Id));
        }
        [HttpPost]
        [Route("AddMonHoc")]
        public async Task<IActionResult> Post(MonHocDto monHocDto)
        {
            var objMonHoc = _mapper.Map<MonHoc>(monHocDto);
            objMonHoc.Active = true;
            var result = await _unitOfWork.MonHocRepos.Add(objMonHoc);
            _unitOfWork.Save();
            if (result.MonHocID == 0)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
            }
            else
            {
                return Ok("Thêm mới thành công");
            }
        }
        [HttpPut]
        [Route("UpdateMonHoc")]
        public IActionResult Put(MonHocDto monHocDto)
        {
            var objMonHoc = _mapper.Map<MonHoc>(monHocDto);
            objMonHoc.Active = true;
            _unitOfWork.MonHocRepos.Update(objMonHoc);
            _unitOfWork.Save();
            return Ok("Cập nhật thành công");
        }
        [HttpDelete]
        [Route("DeleteMonHoc")]
        public JsonResult Delete(int id)
        {
            var objMonHoc = _unitOfWork.MonHocRepos.GetById(id);
            _unitOfWork.MonHocRepos.Remove(objMonHoc);
            _unitOfWork.Save();
            return new JsonResult("Xóa thành công");
        }
        [HttpPut]
        [Route("changeActive")]
        public IActionResult changeActive(int ID)
        {
            var objMonHoc = _unitOfWork.MonHocRepos.GetById(ID);
            if (objMonHoc.Active == true)
            {
                objMonHoc.Active = false;
            }
            else
            {
                objMonHoc.Active = true;
            }
            _unitOfWork.MonHocRepos.Update(objMonHoc);
            _unitOfWork.Save();
            return Ok("Cập nhật thành công");
        }
    }
}

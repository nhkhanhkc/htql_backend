﻿using AutoMapper;
using GD_HTQL.WebApi.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using GD_HTQL.WebApi.Data.Models.EF.SanPhamModels;
using GD_HTQL.WebApi.Data.Models.Dtos.SanPham;

namespace GD_HTQL.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class DonViTinhController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public DonViTinhController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;

        }
        [HttpGet]
        [Route("GetAll")]
        public async Task<IActionResult> Get()
        {
            return Ok(await _unitOfWork.DonViTinhRepos.GetAll());
        }
        [HttpGet]
        [Route("GetById/{Id}")]
        public IActionResult GetDonViTinhById(int Id)
        {
            return Ok(_unitOfWork.DonViTinhRepos.GetById(Id));
        }
        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> Post(DonViTinhDto DonViTinhDto)
        {
            var objDonViTinh = _mapper.Map<DonViTinh>(DonViTinhDto);
            var result = await _unitOfWork.DonViTinhRepos.Add(objDonViTinh);
            _unitOfWork.Save();
            if (result.DVTID == 0)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
            }
            else
            {
                return Ok("Thêm mới thành công");
            }
        }
        [HttpPut]
        [Route("Update")]
        public IActionResult Put(DonViTinhDto DonViTinhDto)
        {
            var objDonViTinh = _mapper.Map<DonViTinh>(DonViTinhDto);
            _unitOfWork.DonViTinhRepos.Update(objDonViTinh);
            _unitOfWork.Save();
            return Ok("Cập nhật thành công");
        }
        [HttpDelete]
        [Route("Delete")]
        public JsonResult Delete(int id)
        {
            var objDonViTinh = _unitOfWork.DonViTinhRepos.GetById(id);
            _unitOfWork.DonViTinhRepos.Remove(objDonViTinh);
            _unitOfWork.Save();
            return new JsonResult("Xóa thành công");
        }
    }
}

﻿using AutoMapper;
using GD_HTQL.WebApi.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using GD_HTQL.WebApi.Data.Models.EF.NhaCungCapModels;
using GD_HTQL.WebApi.Data.Models.Dtos.NhaCungCap;

namespace GD_HTQL.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class LoaiNhaCungCapController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public LoaiNhaCungCapController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;

        }
        [HttpGet]
        [Route("GetAll")]
        public async Task<IActionResult> Get()
        {
            return Ok(await _unitOfWork.LoaiNhaCungCapRepos.GetAll());
        }
        [HttpGet]
        [Route("GetById/{Id}")]
        public IActionResult GetLoaiNhaCungCapById(int Id)
        {
            return Ok(_unitOfWork.LoaiNhaCungCapRepos.GetById(Id));
        }
        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> Post(LoaiNhaCungCapDto LoaiNhaCungCapDto)
        {
            var objLoaiNhaCungCap = _mapper.Map<LoaiNhaCungCap>(LoaiNhaCungCapDto);
            var result = await _unitOfWork.LoaiNhaCungCapRepos.Add(objLoaiNhaCungCap);
            _unitOfWork.Save();
            if (result.LoaiNCCID == 0)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
            }
            else
            {
                return Ok("Thêm mới thành công");
            }
        }
        [HttpPut]
        [Route("Update")]
        public IActionResult Put(LoaiNhaCungCapDto LoaiNhaCungCapDto)
        {
            var objLoaiNhaCungCap = _mapper.Map<LoaiNhaCungCap>(LoaiNhaCungCapDto);
            _unitOfWork.LoaiNhaCungCapRepos.Update(objLoaiNhaCungCap);
            _unitOfWork.Save();
            return Ok("Cập nhật thành công");
        }
        [HttpDelete]
        [Route("Delete")]
        public JsonResult Delete(int id)
        {
            var objLoaiNhaCungCap = _unitOfWork.LoaiNhaCungCapRepos.GetById(id);
            _unitOfWork.LoaiNhaCungCapRepos.Remove(objLoaiNhaCungCap);
            _unitOfWork.Save();
            return new JsonResult("Xóa thành công");
        }
    }
}

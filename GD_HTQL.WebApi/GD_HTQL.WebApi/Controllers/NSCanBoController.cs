﻿using AutoMapper;
using GD_HTQL.WebApi.Common;
using GD_HTQL.WebApi.Data.Models.Dtos.DuAn;
using GD_HTQL.WebApi.Data.Models.EF.DuAnModels;
using GD_HTQL.WebApi.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GD_HTQL.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class NSCanBoController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public NSCanBoController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork ??
                throw new ArgumentNullException(nameof(unitOfWork));
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetCanBo")]
        public async Task<IActionResult> GetCanBo()
        {
            var result = await _unitOfWork.NSCanBoRepos.Find(x => x.Active == true);
            return Ok(result);
        }
        [HttpGet]
        [Route("GetNSCanBoByTuKhoa")]
        public async Task<IActionResult> GetNSCanBoByTuKhoa(string? Key, int CanBoID)
        {
            return Ok(await _unitOfWork.NSCanBoRepos.GetNSCanBoByTuKhoa(Key,CanBoID));
        }
        [HttpGet]
        [Route("GetNSCanBoByCoQuan/{id}")]
        public async Task<IActionResult> GetNSCanBoByCoQuan(int id)
        {
            return Ok(await _unitOfWork.NSCanBoRepos.Find(x=>x.CoQuanID ==id && x.Active ==true));
        }

        [HttpGet]
        [Route("GetCanBoById/{Id}")]
        public IActionResult GetCanBoById(int Id)
        {
            return Ok(_unitOfWork.NSCanBoRepos.GetById(Id));
        }
        [HttpPost]
        [Route("AddCanBo")]
        public async Task<IActionResult> AddCanBo(NSCanBoDto objNSCanBo)
        {
            var objCanBo = _mapper.Map<NSCanBo>(objNSCanBo);
            objCanBo.Active = true;
            objCanBo.CreateDate = DateTime.Now;
            var currentUser = HttpContext.User;
            objCanBo.CreateBy = ClassCommon.GetTenNhanVien(currentUser);
            var result = await _unitOfWork.NSCanBoRepos.Add(objCanBo);
            _unitOfWork.Save();
            if (result.CoQuanID == 0)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
            }
            else
            {
                return Ok(result);
            }          
        }
        [HttpPut]
        [Route("UpdateCanBo")]
        public IActionResult UpdateCanBo(NSCanBoDto objNSCanBo)
        {
            var objCanBo = _mapper.Map<NSCanBo>(objNSCanBo);
            objCanBo.Active = true;
            var obj = _unitOfWork.NSCanBoRepos.GetById(objCanBo.CanBoID);

            objCanBo.CreateDate = obj.CreateDate;
            objCanBo.CreateBy = obj.CreateBy;
            objCanBo.UpdateDate = DateTime.Now;
            var currentUser = HttpContext.User;
            objCanBo.UpdateBy = ClassCommon.GetTenNhanVien(currentUser);
            _unitOfWork.NSCanBoRepos.Update(objCanBo);
            _unitOfWork.Save();
            return Ok("Cập nhật thành công");
        }

        [HttpDelete]   
        [Route("DeleteNSCanBo")]
        public JsonResult DeleteCanBo(int id)
        {
            var objCanBo = _unitOfWork.NSCanBoRepos.GetById(id);
            _unitOfWork.NSCanBoRepos.Remove(objCanBo);
            _unitOfWork.Save();
            return new JsonResult("Xóa thành công");
        }

        [HttpPut]
        [Route("changeActive")]
        public IActionResult changeActive(int ID)
        {
            var objCanBo = _unitOfWork.NSCanBoRepos.GetById(ID);
            if (objCanBo.Active == true)
            {
                objCanBo.Active = false;
            }
            else
            {
                objCanBo.Active = true;
            }
            var currentUser = HttpContext.User;
            objCanBo.ActiveBy = ClassCommon.GetTenNhanVien(currentUser);
            objCanBo.ActiveDate = DateTime.Now;
            _unitOfWork.NSCanBoRepos.Update(objCanBo);
            _unitOfWork.Save();
            return Ok("Cập nhật thành công");
        }
    }
}

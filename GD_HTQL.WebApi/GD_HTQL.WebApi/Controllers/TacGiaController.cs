﻿using AutoMapper;
using GD_HTQL.WebApi.Common;
using GD_HTQL.WebApi.Data.Models.Dtos.TacGia;
using GD_HTQL.WebApi.Data.Models.EF.TacGiaModels;
using GD_HTQL.WebApi.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GD_HTQL.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class TacGiaController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public TacGiaController(IUnitOfWork unitOfWork,IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetTacGia")]
        public async Task<IEnumerable<TacGia>> GetTacGia()
        {
            var result = await _unitOfWork.TacGiaRepos.Find(x => x.Active == true);
            return result;
        }
        [HttpGet]
        [Route("GetTacGiaByTuKhoa")]
        public async Task<IActionResult> GetTacGiaByTuKhoa(string? Key)
        {
            var result = await _unitOfWork.TacGiaRepos.GetTacGiaByTuKhoa(Key);
            return Ok(result);
        }
        [HttpGet]
        [Route("GetTacGiaById/{Id}")]
        public TacGia GetTacGiaById(int Id)
        {
            return _unitOfWork.TacGiaRepos.GetById(Id);
        }
        [HttpPost]
        [Route("AddTacGia")]
        public async Task<IActionResult> AddTacGia(TacGiaDto tacGiaDto)
        {
            var objTacGia = _mapper.Map<TacGia>(tacGiaDto);
            objTacGia.Active = true;
            objTacGia.CreateDate = DateTime.Now;   
            var currentUser = HttpContext.User;
            objTacGia.CreateBy = ClassCommon.GetTenNhanVien(currentUser);
            objTacGia.NhanVienID = ClassCommon.GetUserID(currentUser);
            var result = await _unitOfWork.TacGiaRepos.Add(objTacGia);
            _unitOfWork.Save();
            if (result.TacGiaID == 0)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
            }
            return Ok(result);
        }
        [HttpPut]
        [Route("UpdateTacGia")]
        public IActionResult UpdateTacGia(TacGiaDto tacGiaDto)
        {
           
            var objTacGia = _mapper.Map<TacGia>(tacGiaDto);

            var obj = _unitOfWork.TacGiaRepos.GetById(objTacGia.TacGiaID);
            objTacGia.Active = true;
            objTacGia.CreateDate = obj.CreateDate;
            objTacGia.CreateBy = obj.CreateBy;
            objTacGia.UpdateDate = DateTime.Now;

            var currentUser = HttpContext.User;
            objTacGia.UpdateBy = ClassCommon.GetTenNhanVien(currentUser);
            objTacGia.NhanVienID = ClassCommon.GetUserID(currentUser);
            _unitOfWork.TacGiaRepos.Update(objTacGia);
            _unitOfWork.Save();
            return Ok("Cập nhật thành công");
        }
        [HttpDelete]
        [Route("DeleteTacGia")]
        public JsonResult DeleteTacGia(int id)
        {
            var objDelete = _unitOfWork.TacGiaRepos.GetById(id);
            _unitOfWork.TacGiaRepos.Remove(objDelete);
            _unitOfWork.Save();
            return new JsonResult("Xóa thành công");
        }
        [HttpPut]
        [Route("changeActive")]
        public IActionResult changeActive(int ID)
        {
            var objTacGia = _unitOfWork.TacGiaRepos.GetById(ID);
            if (objTacGia.Active == true)
            {
                objTacGia.Active = false;
            }
            else
            {
                objTacGia.Active = true;
            }

            var currentUser = HttpContext.User;
            objTacGia.ActiveBy = ClassCommon.GetTenNhanVien(currentUser);
            objTacGia.ActiveDate = DateTime.Now;
            _unitOfWork.TacGiaRepos.Update(objTacGia);
            _unitOfWork.Save();
            return Ok("Cập nhật thành công");
        }
    }
}

﻿using AutoMapper;
using GD_HTQL.WebApi.Common;
using GD_HTQL.WebApi.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using GD_HTQL.WebApi.Services.IRepository;
using GD_HTQL.WebApi.Data.Models.EF.DuAnModels;
using GD_HTQL.WebApi.Data.Models.Dtos.DuAn;

namespace GD_HTQL.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class NSDuToanController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ISendTelegramRepository _thongbao;
        public NSDuToanController(IUnitOfWork unitOfWork, IMapper mapper, ISendTelegramRepository thongbao)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _thongbao = thongbao;
        }

        [HttpGet]
        [Route("GetAll")]
        public async Task<IActionResult> Get()
        {
            return Ok(await _unitOfWork.NSDuToanRepos.GetDanhSach(0));
        }      

        [HttpGet]
        [Route("GetbyCoQuanID")]
        public async Task<IActionResult> GetbyCoQuanID(int coQuanID)
        {
            return Ok(await _unitOfWork.NSDuToanRepos.GetDanhSach(coQuanID));
        }

        [HttpGet]
        [Route("GetById/{Id}")]
        public IActionResult GetbyId(int Id)
        {
            return Ok(_unitOfWork.NSDuToanRepos.GetById(Id));
        }
        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> Post(NSDuToanDto NSDuToanDto)
        {
            var objNSDuToan = _mapper.Map<NSDuToan>(NSDuToanDto);
            objNSDuToan.CreateDate = DateTime.Now;
            var currentUser = HttpContext.User;
            objNSDuToan.CreateBy = ClassCommon.GetTenNhanVien(currentUser);
            objNSDuToan.NhanVienID = ClassCommon.GetUserID(currentUser);

            if (!string.IsNullOrEmpty(NSDuToanDto.FileCongTy) && !string.IsNullOrEmpty(NSDuToanDto.TenFileCongTy))
            {
                objNSDuToan.FileCongTy = await _unitOfWork.NSDuToanRepos.SaveFile(NSDuToanDto.FileCongTy, NSDuToanDto.TenFileCongTy);
            }

            if (!string.IsNullOrEmpty(NSDuToanDto.FileCoQuan) && !string.IsNullOrEmpty(NSDuToanDto.TenFileCoQuan))
            {
                objNSDuToan.FileCoQuan = await _unitOfWork.NSDuToanRepos.SaveFile(NSDuToanDto.FileCoQuan, NSDuToanDto.TenFileCoQuan);
            }

            var result = await _unitOfWork.NSDuToanRepos.Add(objNSDuToan);

            _unitOfWork.Save();
            if (result.DuToanID == 0)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
            }
            else
            {
                try
                {
                    string tieude = objNSDuToan.CreateBy.ToUpper() + " ĐÃ VỪA THÊM MỚI GIÁ TRỊ CƠ HỘI";
                    await _thongbao.SendTeleDuAn_CoHoi(result.DuToanID, tieude);
                }
                catch
                {
                    return Ok(result);
                }             
                return Ok(result);
            }
        }
        [HttpPut]
        [Route("Update")]
        public async Task<IActionResult> Put(NSDuToanDto NSDuToanDto)
        {
            var objNSDuToan = _mapper.Map<NSDuToan>(NSDuToanDto);
            var obj = _unitOfWork.NSDuToanRepos.GetById(objNSDuToan.DuToanID);

            objNSDuToan.CreateDate = obj.CreateDate;
            objNSDuToan.CreateBy = obj.CreateBy;
            objNSDuToan.NhanVienID = obj.NhanVienID;
            objNSDuToan.UpdateDate = DateTime.Now;
            var currentUser = HttpContext.User;
            objNSDuToan.UpdateBy = ClassCommon.GetTenNhanVien(currentUser);

            // Kiểm tra xem file có giống file trước đó không 

            if (NSDuToanDto.TenFileCongTy != obj.TenFileCongTy)
            {
                if (!string.IsNullOrEmpty(NSDuToanDto.FileCongTy) && !string.IsNullOrEmpty(NSDuToanDto.TenFileCongTy))
                {
                    objNSDuToan.FileCongTy = await _unitOfWork.NSDuToanRepos.SaveFile(NSDuToanDto.FileCongTy, NSDuToanDto.TenFileCongTy);
                    if (!string.IsNullOrEmpty(obj.FileCongTy))
                    {
                        _unitOfWork.NSDuToanRepos.DeleteFile(obj.FileCongTy);
                    }
                }
                else
                {
                    objNSDuToan.FileCongTy = "";
                    objNSDuToan.TenFileCongTy = "";
                }
            }

            if (NSDuToanDto.TenFileCoQuan != obj.TenFileCoQuan)
            {
                if (!string.IsNullOrEmpty(NSDuToanDto.FileCoQuan) && !string.IsNullOrEmpty(NSDuToanDto.TenFileCoQuan))
                {
                    objNSDuToan.FileCoQuan = await _unitOfWork.NSDuToanRepos.SaveFile(NSDuToanDto.FileCoQuan, NSDuToanDto.TenFileCoQuan);
                    if (!string.IsNullOrEmpty(obj.FileCoQuan))
                    {
                        _unitOfWork.NSDuToanRepos.DeleteFile(obj.FileCoQuan);
                    }
                }
                else
                {
                    objNSDuToan.FileCoQuan = "";
                    objNSDuToan.TenFileCoQuan = "";
                }
            }
            _unitOfWork.NSDuToanRepos.Update(objNSDuToan);
            _unitOfWork.Save();
            try
            {
                string tieude = objNSDuToan.UpdateBy.ToUpper() + " ĐÃ VỪA CẬP NHẬT GIÁ TRỊ CƠ HỘI";
                await _thongbao.SendTeleDuAn_CoHoi(objNSDuToan.DuToanID, tieude);
            }
            catch
            {
                return Ok("Cập nhật thành công");
            }         
            return Ok("Cập nhật thành công");
        }

        [HttpDelete]
        [Route("Delete")]
        public async Task<JsonResult> Delete(int id)
        {
            var objNSDuToan = _unitOfWork.NSDuToanRepos.GetById(id);
            // Xóa file
            if (!string.IsNullOrEmpty(objNSDuToan.FileCoQuan))
            {
                _unitOfWork.NSDuToanRepos.DeleteFile(objNSDuToan.FileCoQuan);
            }
            if (!string.IsNullOrEmpty(objNSDuToan.FileCoQuan))
            {
                _unitOfWork.NSDuToanRepos.DeleteFile(objNSDuToan.FileCoQuan);
            }
            var currentUser = HttpContext.User;
            try
            {
                string tieude = ClassCommon.GetTenNhanVien(currentUser).ToUpper() + " ĐÃ VỪA XÓA GIÁ TRỊ CƠ HỘI";
                await _thongbao.SendTeleDuAn_CoHoi(id, tieude);
            }
            catch
            {
                _unitOfWork.NSDuToanRepos.Remove(objNSDuToan);
                _unitOfWork.Save();
                return new JsonResult("Xóa thành công");
            }        
            _unitOfWork.NSDuToanRepos.Remove(objNSDuToan);
            _unitOfWork.Save();
            return new JsonResult("Xóa thành công");
        }
        [HttpGet]
        [Route("GetBuocThiTruong")]
        public async Task<IActionResult> GetBuocThiTruong()
        {
            return Ok(await _unitOfWork.NSDuToanRepos.GetBuocThiTruong());
        }
    }
}

﻿using AutoMapper;
using GD_HTQL.WebApi.Common;
using GD_HTQL.WebApi.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using GD_HTQL.WebApi.Data.Models.EF.CongTacModels;
using GD_HTQL.WebApi.Data.Models.Dtos.CongTac;
using GD_HTQL.WebApi.Services.IRepository;

namespace GD_HTQL.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class KHCT_ThangController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ISendTelegramRepository _thongbao;
        public KHCT_ThangController(IUnitOfWork unitOfWork, IMapper mapper, ISendTelegramRepository thongbao)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _thongbao = thongbao;
        }
        [HttpGet]
        [Route("GetAll")]
        public async Task<IEnumerable<KHCT_Thang>> GetAll()
        {
            var result = await _unitOfWork.KHCT_ThangRepos.Find(x => x.Active == true);
            return result;
        }
        [HttpGet]
        [Route("GetById/{Id}")]
        public KHCT_Thang GetById(int Id)
        {
            return _unitOfWork.KHCT_ThangRepos.GetById(Id);
        }
        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> Add(KHCT_ThangDto KHCT_ThangDto)
        {
            var objKHCT_Thang = _mapper.Map<KHCT_Thang>(KHCT_ThangDto);
            objKHCT_Thang.Active = true;
            objKHCT_Thang.CreateDate = DateTime.Now;
            objKHCT_Thang.IsApprove = null;
            var currentUser = HttpContext.User;
            objKHCT_Thang.CreateBy = ClassCommon.GetTenNhanVien(currentUser);
            objKHCT_Thang.NguoiTaoID = ClassCommon.GetUserID(currentUser);
            var result = await _unitOfWork.KHCT_ThangRepos.Add(objKHCT_Thang);
            _unitOfWork.Save();
            if (result.ThangID == 0)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
            }
            else
            {
                try
                {
                    string tieude = objKHCT_Thang.CreateBy.ToUpper() + " VỪA TẠO MỚI KẾ HOẠCH CÔNG TÁC THÁNG " + result.Thang;
                    await _thongbao.SendTeleKHCT_Thang(result.ThangID, tieude);
                }
                catch
                {
                    return Ok(result);
                }
                return Ok(result);
            }         
        }
        [HttpPut]
        [Route("Update")]
        public async Task<IActionResult> Update(KHCT_ThangDto KHCT_ThangDto)
        {
            var objKHCT_Thang = _mapper.Map<KHCT_Thang>(KHCT_ThangDto);
            var obj = _unitOfWork.KHCT_ThangRepos.GetById(objKHCT_Thang.ThangID);
            objKHCT_Thang.Active = true;
            objKHCT_Thang.CreateDate = obj.CreateDate;
            objKHCT_Thang.CreateBy = obj.CreateBy;
            objKHCT_Thang.NguoiTaoID = obj.NguoiTaoID;
            objKHCT_Thang.IsApprove = obj.IsApprove;
            objKHCT_Thang.UpdateDate = DateTime.Now;
            var currentUser = HttpContext.User;
            objKHCT_Thang.UpdateBy = ClassCommon.GetTenNhanVien(currentUser);
            //objKHCT_Thang.NhanVienID = ClassCommon.GetUserID(currentUser);
            _unitOfWork.KHCT_ThangRepos.Update(objKHCT_Thang);
            _unitOfWork.Save();
            try
            {
                string tieude = objKHCT_Thang.UpdateBy.ToUpper() + " VỪA CẬP NHẬT KẾ HOẠCH CÔNG TÁC THÁNG " + objKHCT_Thang.Thang;
                await _thongbao.SendTeleKHCT_Thang(objKHCT_Thang.ThangID, tieude);
            }
            catch
            {
                return Ok("Cập nhật thành công");
            }

            return Ok("Cập nhật thành công");
        }

        [HttpPost]
        [Route("Duyet/{ID}")]
        public async Task<IActionResult> Duyet(int ID, bool trangthai)
        {
            var objKHCT_Thang = _unitOfWork.KHCT_ThangRepos.GetById(ID);          
            objKHCT_Thang.IsApprove = trangthai;         
            var currentUser = HttpContext.User;
            objKHCT_Thang.NguoiDuyet = ClassCommon.GetTenNhanVien(currentUser);
            objKHCT_Thang.NguoiDuyetID = ClassCommon.GetUserID(currentUser);
            _unitOfWork.KHCT_ThangRepos.Update(objKHCT_Thang);
            _unitOfWork.Save();
            try
            {
                string _trangthai = "";
                if(trangthai == true)
                {
                    _trangthai = "DUYỆT";
                }
                else
                {
                    _trangthai = "TỪ CHỐI";
                }
                string tieude = objKHCT_Thang.NguoiDuyet.ToUpper() + " VỪA "+ _trangthai + " KẾ HOẠCH CÔNG TÁC THÁNG " + objKHCT_Thang.Thang + " CỦA " + objKHCT_Thang.CreateBy.ToUpper();
                await _thongbao.SendTeleKHCT_Thang(objKHCT_Thang.ThangID, tieude);
            }
            catch
            {
                return Ok("Cập nhật thành công");
            }
            return Ok("Cập nhật thành công");
        }

        [HttpDelete]
        [Route("Delete")]
        public  async Task<JsonResult> Delete(int id)
        {
            var currentUser = HttpContext.User;         
            var objDelete = _unitOfWork.KHCT_ThangRepos.GetById(id);
            var objKHT = await _unitOfWork.KHCT_TuanRepos.Find(x => x.ThangID == id);
            if (objKHT.Count() == 0)
            {
                try
                {
                    string tieude = ClassCommon.GetTenNhanVien(currentUser).ToUpper() + " VỪA XÓA KẾ HOẠCH CÔNG TÁC THÁNG " + objDelete.Thang;
                    await _thongbao.SendTeleKHCT_Thang(objDelete.ThangID, tieude);
                }
                catch
                {
                    _unitOfWork.KHCT_ThangRepos.Remove(objDelete);
                    _unitOfWork.Save();
                }
                _unitOfWork.KHCT_ThangRepos.Remove(objDelete);
                _unitOfWork.Save();
                return new JsonResult("Xóa thành công");
            }
            else
            {
                return new JsonResult("Đã có kế hoạch công tác tuần thuộc tháng này. vui lòng xóa kế hoạch tuần trước");   
            }                      
        }

        [HttpPut]
        [Route("changeActive")]
        public IActionResult changeActive(int ID)
        {
            var objKHCT_Thang = _unitOfWork.KHCT_ThangRepos.GetById(ID);
            if (objKHCT_Thang.Active == true)
            {
                objKHCT_Thang.Active = false;
            }
            else
            {
                objKHCT_Thang.Active = true;
            }
            var currentUser = HttpContext.User;
            objKHCT_Thang.ActiveBy = ClassCommon.GetTenNhanVien(currentUser);
            objKHCT_Thang.ActiveDate = DateTime.Now;
            _unitOfWork.KHCT_ThangRepos.Update(objKHCT_Thang);
            _unitOfWork.Save();
            return Ok("Cập nhật thành công");
        }
    }
}

﻿using AutoMapper;
using GD_HTQL.WebApi.Data.Models.Dtos.DanhMuc;
using GD_HTQL.WebApi.Data.Models.EF.DanhMucModels;
using GD_HTQL.WebApi.Infrastructure;
using Microsoft.AspNetCore.Mvc;

namespace GD_HTQL.WebApi.Controllers
{
    //[Authorize(Policy = "quantriKhachhang")]
    //[Authorize(Roles = "Admin")]

    [Route("api/[controller]")]
    [ApiController]
    public class TinhController : ControllerBase
    {      
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public TinhController( IUnitOfWork unitOfWork, IMapper mapper)
        {        
            _unitOfWork = unitOfWork;
            _mapper = mapper;

        }
        [HttpGet]
        [Route("GetTinh")]
        public async Task<IActionResult> Get()
        {
            var result = await _unitOfWork.DMTinhRepos.GetAll();
            return Ok(result);
        }
        [HttpGet]
        [Route("GetTinhById/{Id}")]
        public IActionResult GetTinhById(int Id)
        {
            return Ok( _unitOfWork.DMTinhRepos.GetById(Id));
        }
        [HttpPost]
        [Route("AddTinh")]
        public async Task<IActionResult> Post(DMTinhDto objTinh)
        {
            var newTinh = _mapper.Map<DMTinh>(objTinh);
            var result = await _unitOfWork.DMTinhRepos.Add(newTinh);
             _unitOfWork.Save();
            if (result.TinhID == 0)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
            }
            else
            {
                return Ok("Thêm mới thành công");
            }         
        }
        [HttpPut]
        [Route("UpdateTinh")]
        public IActionResult Put(DMTinhDto objTinh)
        {
            var UpdateTinh = _mapper.Map<DMTinh>(objTinh);
             _unitOfWork.DMTinhRepos.Update(UpdateTinh);
            _unitOfWork.Save();
            return Ok("Cập nhật thành công");
        }

        [HttpDelete]       
        [Route("DeleteTinh")]
        public JsonResult Delete(int id)
        {     
            var objTinh =  _unitOfWork.DMTinhRepos.GetById(id);
            _unitOfWork.DMTinhRepos.Remove(objTinh);
            _unitOfWork.Save();
            return new JsonResult("Xóa thành công");
        }     
    }
}

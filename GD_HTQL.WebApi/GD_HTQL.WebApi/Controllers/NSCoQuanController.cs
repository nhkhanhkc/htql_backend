﻿using AutoMapper;
using GD_HTQL.WebApi.Common;
using GD_HTQL.WebApi.Data.Models.Dtos.DuAn;
using GD_HTQL.WebApi.Data.Models.EF.DuAnModels;
using GD_HTQL.WebApi.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GD_HTQL.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class NSCoQuanController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public NSCoQuanController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork ??
                throw new ArgumentNullException(nameof(unitOfWork));
            _mapper = mapper;
        }

        [HttpGet]
        [Route("GetCoQuan")]
        public async Task<IActionResult> GetCoQuan()
        {            
            var currentUser = HttpContext.User;
            if (CheckRoles.CheckRoleGroupKhachHang(currentUser) == true)
            {
                var lstCoQuan = await _unitOfWork.NSCoQuanRepos.GetAll();
                return Ok(lstCoQuan);
            }
            else
            {
                int _nhanVienID = ClassCommon.GetUserID(currentUser);
                if (_nhanVienID == 0)
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
                }
                else
                {
                    var lstCoQuan = await _unitOfWork.NSCoQuanRepos.GetByNhanVien(_nhanVienID);
                    return Ok(lstCoQuan);
                }
            }                              
        }

        [HttpGet]
        [Route("GetDanhSach")]
        public async Task<IActionResult> GetDanhSach()
        {
            var currentUser = HttpContext.User;
            if (CheckRoles.CheckRoleGroupKhachHang(currentUser) == true)
            {
                var lstCoQuan = await _unitOfWork.NSCoQuanRepos.GetDanhSach(0);
                return Ok(lstCoQuan);
            }
            else
            {
                int _nhanVienID = ClassCommon.GetUserID(currentUser);
                if (_nhanVienID == 0)
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
                }
                else
                {
                    var lstCoQuan = await _unitOfWork.NSCoQuanRepos.GetDanhSach(_nhanVienID);
                    return Ok(lstCoQuan);
                }
            }          
        }


        [HttpGet]
        [Route("GetBaoCao")]
        public async Task<IActionResult> GetBaoCao()
        {
            var currentUser = HttpContext.User;
            if (CheckRoles.CheckRoleGroupKhachHang(currentUser) == true)
            {
                var lstCoQuan = await _unitOfWork.NSCoQuanRepos.GetBaoCao(0);
                return Ok(lstCoQuan);
            }
            else
            {
                int _nhanVienID = ClassCommon.GetUserID(currentUser);
                if (_nhanVienID == 0)
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
                }
                else
                {
                    var lstCoQuan = await _unitOfWork.NSCoQuanRepos.GetBaoCao(_nhanVienID);
                    //var lstCoQuan = await _unitOfWork.NSCoQuanRepos.GetBaoCao(0);
                    return Ok(lstCoQuan);
                }
            }
        }

        [HttpGet]
        [Route("GetCoQuanByTuKhoa")]
        public async Task<IActionResult> GetNSCoQuanByTuKhoa(string? Key,int TinhID)
        {
            return Ok(await _unitOfWork.NSCoQuanRepos.GetNSCoQuanByTuKhoa(Key, TinhID));

        }
        [HttpGet]
        [Route("GetCoQuanById/{Id}")]
        public IActionResult GetCoQuanById(int Id)
        {
            return Ok(_unitOfWork.NSCoQuanRepos.GetById(Id));
        }
        [HttpPost]
        [Route("AddCoQuan")]
        public async Task<IActionResult> AddCoQuan(NSCoQuanDto objNSCoQuan)
        {
            if (!String.IsNullOrWhiteSpace(objNSCoQuan.MaSoThue) && _unitOfWork.NhaThauRepos.CheckMaSoThue(objNSCoQuan.MaSoThue, 0) == false)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Mã số thuế đã tồn tại đã tồn tại");
            }
            else
            {
                var objCoQuan = _mapper.Map<NSCoQuan>(objNSCoQuan);
                objCoQuan.Active = true;
                objCoQuan.CreateDate = DateTime.Now;
                var currentUser = HttpContext.User;
                objCoQuan.CreateBy = ClassCommon.GetTenNhanVien(currentUser);
                int _nhanVienID = ClassCommon.GetUserID(currentUser);
                objCoQuan.NhanVienID = _nhanVienID;
                var result = await _unitOfWork.NSCoQuanRepos.Add(objCoQuan);
                _unitOfWork.Save();
                if (result.CoQuanID == 0)
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
                }
                else
                {
                    return Ok(result);
                }             
            }           
        }
        [HttpPut]
        [Route("UpdateCoQuan")]
        public IActionResult UpdateCoQuan(NSCoQuanDto objNSCoQuan)
        {
            if (!String.IsNullOrWhiteSpace(objNSCoQuan.MaSoThue) && _unitOfWork.NhaThauRepos.CheckMaSoThue(objNSCoQuan.MaSoThue, objNSCoQuan.CoQuanID) == false)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Mã số thuế đã tồn tại đã tồn tại");
            }
            else
            {
                var objCoQuan = _mapper.Map<NSCoQuan>(objNSCoQuan);
                objCoQuan.Active = true;
                var obj = _unitOfWork.NSCoQuanRepos.GetById(objCoQuan.CoQuanID);

                objCoQuan.CreateDate = obj.CreateDate;
                objCoQuan.CreateBy = obj.CreateBy;
                objCoQuan.UpdateDate = DateTime.Now;
                var currentUser = HttpContext.User;
                int _nhanVienID = ClassCommon.GetUserID(currentUser);
                objCoQuan.UpdateBy = ClassCommon.GetTenNhanVien(currentUser);
                objCoQuan.NhanVienID = _nhanVienID;
                _unitOfWork.NSCoQuanRepos.Update(objCoQuan);
                _unitOfWork.Save();
                return Ok("Cập nhật thành công");
            }          
        }
        [HttpDelete]      
        [Route("DeleteCoQuan")]
        public  async Task<JsonResult> DeleteCoQuan(int id)
        {
            var objCoQuan  = _unitOfWork.NSCoQuanRepos.GetById(id);

            // Xóa cán bộ 
            var objCanbo =  await _unitOfWork.NSCanBoRepos.Find(x=>x.CoQuanID ==id);
            if (objCanbo.Count() > 0)
            {
                _unitOfWork.NSCanBoRepos.RemoveRange(objCanbo);
                _unitOfWork.Save();
            }
            // Xóa báo cáo tiếp xúc

            var objQLTT = await _unitOfWork.QuanLyTuongTacRepos.Find(x => x.CoQuanID == id);
            if (objQLTT.Count() > 0)
            {
                _unitOfWork.QuanLyTuongTacRepos.RemoveRange(objQLTT);
                _unitOfWork.Save();
            }

            _unitOfWork.NSCoQuanRepos.Remove(objCoQuan);
            _unitOfWork.Save();          
            return new JsonResult("Xóa thành công");
        }

        [HttpPut]
        [Route("changeActive")]
        public IActionResult changeActive(int ID)
        {
            var objCoQuan = _unitOfWork.NSCoQuanRepos.GetById(ID);
            if (objCoQuan.Active == true)
            {
                objCoQuan.Active = false;
            }
            else
            {
                objCoQuan.Active = true;
            }
            var currentUser = HttpContext.User;
            objCoQuan.ActiveBy = ClassCommon.GetTenNhanVien(currentUser);
            objCoQuan.ActiveDate = DateTime.Now;
            _unitOfWork.NSCoQuanRepos.Update(objCoQuan);
            _unitOfWork.Save();
            return Ok("Cập nhật thành công");
        }
        [HttpGet]
        [Route("CheckMaSoThue")]
        public IActionResult CheckMaSoThue(string maSoThue, int coQuanID)
        {
            if (string.IsNullOrWhiteSpace(maSoThue))
            {
                bool Check = _unitOfWork.NSCoQuanRepos.CheckMaSoThue(maSoThue, coQuanID);
                if (Check)
                {
                    return Ok(new { message = true });
                }
                else
                {
                    return Ok(new { message = false });
                }
            }
            else
            {
                return Ok(new { message = true });
            }
        }
    }
}

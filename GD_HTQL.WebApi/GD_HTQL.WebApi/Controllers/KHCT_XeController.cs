﻿using AutoMapper;
using GD_HTQL.WebApi.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using GD_HTQL.WebApi.Data.Models.EF.CongTacModels;
using GD_HTQL.WebApi.Data.Models.Dtos.CongTac;

namespace GD_HTQL.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class KHCT_XeController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public KHCT_XeController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetDs/{KHCT_Id}")]
        public async Task<IActionResult> GetDs(int KHCT_Id)
        {
            return Ok(await _unitOfWork.KHCT_XeRepos.Find(x => x.NgayID == KHCT_Id));
        }


        [HttpGet]
        [Route("GetById/{Id}")]
        public IActionResult GetById(int Id)
        {
            return Ok(_unitOfWork.KHCT_XeRepos.GetById(Id));
        }

        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> Post(KHCT_XeDto KHCT_XeDto)
        {
            var objKHCT_Xe = _mapper.Map<KHCT_Xe>(KHCT_XeDto);
            var result = await _unitOfWork.KHCT_XeRepos.Add(objKHCT_Xe);
            _unitOfWork.Save();
            if (result.ID == 0)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
            }
            else
            {
                return Ok("Thêm mới thành công");
            }
        }
        [HttpPut]
        [Route("Update")]
        public IActionResult Put(KHCT_XeDto KHCT_XeDto)
        {
            var objKHCT_Xe = _mapper.Map<KHCT_Xe>(KHCT_XeDto);
            _unitOfWork.KHCT_XeRepos.Update(objKHCT_Xe);
            _unitOfWork.Save();
            return Ok("Cập nhật thành công");
        }
        [HttpDelete]
        [Route("Delete")]
        public JsonResult Delete(int id)
        {
            var objKHCT_Xe = _unitOfWork.KHCT_XeRepos.GetById(id);
            _unitOfWork.KHCT_XeRepos.Remove(objKHCT_Xe);
            _unitOfWork.Save();
            return new JsonResult("Xóa thành công");
        }
    }
}

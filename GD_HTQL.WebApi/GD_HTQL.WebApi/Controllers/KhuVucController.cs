﻿using AutoMapper;
using GD_HTQL.WebApi.Data.Models.Dtos.DanhMuc;
using GD_HTQL.WebApi.Data.Models.EF.DanhMucModels;
using GD_HTQL.WebApi.Infrastructure;
using Microsoft.AspNetCore.Mvc;

namespace GD_HTQL.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class KhuVucController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public KhuVucController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;

        }
        [HttpGet]
        [Route("GetAll")]
        public async Task<IActionResult> Get()
        {
            var result = await _unitOfWork.KhuVucRepos.GetAll();
            return Ok(result);
        }
        [HttpGet]
        [Route("GetById/{Id}")]
        public IActionResult GetTinhById(int Id)
        {
            return Ok(_unitOfWork.KhuVucRepos.GetById(Id));
        }
        [HttpGet]
        [Route("GetByKhuVuc")]
        public async Task<IActionResult> GetByKhuVucId(int Id)
        {
            return Ok(await _unitOfWork.KhuVucRepos.Find(x=>x.KhuVucID ==Id));
        }

        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> Post(KhuVucDto khuVucDto)
        {
            var objKhuVuc = _mapper.Map<DMKhuVuc>(khuVucDto);
            var result = await _unitOfWork.KhuVucRepos.Add(objKhuVuc);
            _unitOfWork.Save();
            if (result.KhuVucID == 0)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
            }
            else
            {
                return Ok("Thêm mới thành công");
            }
        }
        [HttpPut]
        [Route("Update")]
        public IActionResult Put(KhuVucDto khuVucDto)
        {
            var objKhuVuc = _mapper.Map<DMKhuVuc>(khuVucDto);
            _unitOfWork.KhuVucRepos.Update(objKhuVuc);
            _unitOfWork.Save();
            return Ok("Cập nhật thành công");
        }

        [HttpDelete]
        [Route("Delete")]
        public JsonResult Delete(int id)
        {
            var objKhuVuc = _unitOfWork.KhuVucRepos.GetById(id);
            _unitOfWork.KhuVucRepos.Remove(objKhuVuc);
            _unitOfWork.Save();
            return new JsonResult("Xóa thành công");
        }
    }
}

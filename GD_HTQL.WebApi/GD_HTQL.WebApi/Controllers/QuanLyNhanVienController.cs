﻿using GD_HTQL.WebApi.Data.Models.CustomModels;
using GD_HTQL.WebApi.Data.Models.EF.NhanVienModels;
using GD_HTQL.WebApi.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GD_HTQL.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class QuanLyNhanVienController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        public QuanLyNhanVienController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        [HttpGet]
        [Route("GetAll")]
        public async Task<IEnumerable<QuanLyNhanVien>> GetALl()
        {
            var result = await _unitOfWork.QuanLyNhanVienRepos.GetAll();
            return result;
        }

        [HttpGet]
        [Route("GetNhanVien")]
        public async Task<IActionResult> GetNhanVien(int quanLyID)
        {           
            var result = await _unitOfWork.QuanLyNhanVienRepos.Find(x=>x.QuanLyID == quanLyID);
            if (result.Count() > 0)
            {
                var lstNhanVienID = result.Select(x => x.NhanVienID).ToList();
                var qLNhanVienViewModels = await _unitOfWork.QuanLyNhanVienRepos.GetQLNhanVien(lstNhanVienID);
                return Ok(qLNhanVienViewModels);
            }
            else
            {
                return Ok("Bạn chưa có nhân viên để quản lý");
            }         
        }

        [HttpGet]
        [Route("GetById/{Id}")]
        public QuanLyNhanVien GetQuanLyNhanVienById(int Id)
        {
            return _unitOfWork.QuanLyNhanVienRepos.GetById(Id);
        }
        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> Add(QuanLyNhanVien objQuanLyNhanVien)
        {
            var obj = await _unitOfWork.QuanLyNhanVienRepos.Find(x => x.QuanLyID == objQuanLyNhanVien.QuanLyID && x.NhanVienID == objQuanLyNhanVien.NhanVienID);
            if (obj.Count() == 0)
            {
                var result = await _unitOfWork.QuanLyNhanVienRepos.Add(objQuanLyNhanVien);
                _unitOfWork.Save();
                if (result.ID == 0)
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
                }
                return Ok(result);
            }
            else
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Quản lý nhân viên đã tồn tại");
            }           
        }

        [HttpPost]
        [Route("AddMulti")]
        public IActionResult AddMulti(QuanLyNhanVienCustom objQuanLyNhanVien)
        {
            try
            {
                _unitOfWork.QuanLyNhanVienRepos.AddMulti(objQuanLyNhanVien);
                return Ok();
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
            }
           
        }

        [HttpPut]
        [Route("Update")]
        public IActionResult Update(QuanLyNhanVien objQuanLyNhanVien)
        {                     
            _unitOfWork.QuanLyNhanVienRepos.Update(objQuanLyNhanVien);
            _unitOfWork.Save();
            return Ok("Cập nhật thành công");
        }

        [HttpDelete]
        [Route("Delete")]
        public JsonResult Delete(int id)
        {
            var objDelete = _unitOfWork.QuanLyNhanVienRepos.GetById(id);
            _unitOfWork.QuanLyNhanVienRepos.Remove(objDelete);
            _unitOfWork.Save();
            return new JsonResult("Xóa thành công");
        }
        [HttpDelete]
        [Route("Delete/{quanLyID}/{nhanVienID}")]
        public async Task<JsonResult> Delete(int quanLyID, int nhanVienID)
        {
            var objDelete = await _unitOfWork.QuanLyNhanVienRepos.Find(x => x.QuanLyID == quanLyID && x.NhanVienID == nhanVienID);
            if (objDelete == null)
            {
                return new JsonResult("Không tìm thấy dữ liệu cần xóa");
            }
            else
            {
                _unitOfWork.QuanLyNhanVienRepos.RemoveRange(objDelete);
                _unitOfWork.Save();
                return new JsonResult("Xóa thành công");
            }
        }
    }
}

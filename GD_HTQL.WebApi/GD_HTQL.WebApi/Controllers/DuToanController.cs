﻿using AutoMapper;
using GD_HTQL.WebApi.Common;
using GD_HTQL.WebApi.Data.Models.Dtos.SanPham;
using GD_HTQL.WebApi.Data.Models.EF.SanPhamModels;
using GD_HTQL.WebApi.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GD_HTQL.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class DuToanController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public DuToanController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetAll")]
        public async Task<IActionResult> Get()
        {
            return Ok(await _unitOfWork.DuToanRepos.GetAll());
        }

        [HttpGet]
        [Route("GetDanhSach")]
        public async Task<IActionResult> GetDanhSach()
        {
            return Ok(await _unitOfWork.DuToanRepos.GetDanhSach());
        }

        [HttpGet]
        [Route("GetDsTrangThai")]
        public async Task<IActionResult> GetDsTrangThai()
        {
            return Ok(await _unitOfWork.DuToanRepos.GetDsTrangThai());
        }

        [HttpGet]
        [Route("GetByTuKhoa")]
        public async Task<IActionResult> GetDuToanByTuKhoa(string? Key)
        {
            return Ok(await _unitOfWork.DuToanRepos.GetDuToanByTuKhoa(Key));
        }
        [HttpGet]
        [Route("GetById/{Id}")]
        public IActionResult GetDuToanById(int Id)
        {
            return Ok( _unitOfWork.DuToanRepos.GetById(Id));
        }
        [HttpPost]
        public async Task<IActionResult> Post(DuToanDto duToanDto)
        {
            var objDuToan = _mapper.Map<DuToan>(duToanDto);
            var currentUser = HttpContext.User;
            int _nhanVienID = ClassCommon.GetUserID(currentUser);
            objDuToan.Active = true;
            objDuToan.NhanVienID = _nhanVienID;
            objDuToan.CreateDate = DateTime.Now;
            objDuToan.CreateBy = ClassCommon.GetTenNhanVien(currentUser);

            var result = await _unitOfWork.DuToanRepos.Add(objDuToan);
            _unitOfWork.Save();
            if (result.DuToanID == 0)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
            }
            else
            {                        
                // Thêm lịch sử dự toán          
                LichSuDuToan LichSu = new LichSuDuToan();
                LichSu.DuToanID = result.DuToanID;
                LichSu.NhanVienID = _nhanVienID;
                LichSu.ThoiGan = DateTime.Now;
                LichSu.TrangThaiDuToanID = 1;
                await _unitOfWork.LichSuDuToanRepos.Add(LichSu);
                return Ok("Thêm mới thành công");
            }
        }
        [HttpPut]
        [Route("Update")]
        public IActionResult Put(DuToanDto duToanDto)
        {
            var objDuToan = _mapper.Map<DuToan>(duToanDto);
            var obj = _unitOfWork.DuToanRepos.GetById(objDuToan.DuToanID);
            objDuToan.Active = true;
            objDuToan.CreateDate = obj.CreateDate;
            objDuToan.CreateBy = obj.CreateBy;
            objDuToan.UpdateDate = DateTime.Now;

            var currentUser = HttpContext.User;
            objDuToan.UpdateBy = ClassCommon.GetTenNhanVien(currentUser);
            objDuToan.NhanVienID = ClassCommon.GetUserID(currentUser);

            _unitOfWork.DuToanRepos.Update(objDuToan);
            _unitOfWork.Save();
            return Ok("Cập nhật thành công");
        }

        [HttpDelete]
        [Route("Delete")]
        public async Task<JsonResult> Delete(int id)
        {
            var objDuToan =  _unitOfWork.DuToanRepos.GetById(id);

            // Xóa lịch sử dự toán  

            var objLichSu = await _unitOfWork.LichSuDuToanRepos.Find(x => x.DuToanID == id);
            if(objLichSu.Count() > 0)
            {
                _unitOfWork.LichSuDuToanRepos.RemoveRange(objLichSu);
                _unitOfWork.Save();
            }
          _unitOfWork.DuToanRepos.Remove(objDuToan);                  
          _unitOfWork.Save();
            return new JsonResult("Xóa thành công");
        }

        [HttpPut]
        [Route("TrangThai/{DuToanID}/{TrangThaiID}")]
        public async Task<IActionResult> CapNhatTrangThai(int DuToanID ,int TrangThaiID)
        {
            var currentUser = HttpContext.User;
            int _nhanVienID = ClassCommon.GetUserID(currentUser);
            await _unitOfWork.DuToanRepos.CapNhatTrangThai(_nhanVienID, TrangThaiID,DuToanID);
            return Ok("Cập nhật thành công");
        }

        [HttpPut]
        [Route("changeActive")]
        public IActionResult changeActive(int ID)
        {
            var objDuToan = _unitOfWork.DuToanRepos.GetById(ID);
            if (objDuToan.Active == true)
            {
                objDuToan.Active = false;
            }
            else
            {
                objDuToan.Active = true;
            }
            var currentUser = HttpContext.User;
            objDuToan.ActiveBy = ClassCommon.GetTenNhanVien(currentUser);
            objDuToan.ActiveDate = DateTime.Now;
            _unitOfWork.DuToanRepos.Update(objDuToan);
            _unitOfWork.Save();
            return Ok("Cập nhật thành công");
        }
    }
}

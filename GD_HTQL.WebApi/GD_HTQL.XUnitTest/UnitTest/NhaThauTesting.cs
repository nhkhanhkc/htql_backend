﻿using AutoMapper;
using GD_HTQL.WebApi.Controllers;
using GD_HTQL.WebApi.Data.Models;
using GD_HTQL.WebApi.Data.Models.Dtos;
using GD_HTQL.WebApi.Services.IRepository;
using Microsoft.AspNetCore.Mvc;
using Moq;

namespace GD_HTQL.XUnitTest.UnitTest
{
    public class NhaThauTesting
    {
        private readonly Mock<INhaThauRepository> _nhathau;
      
        public NhaThauTesting()
        {
            _nhathau = new Mock<INhaThauRepository>();
        }
        [Fact]
        public void GetListNhaThau()
        {
            //arrange
            //var NhaThauList = GetNhaThauData();
            //_nhathau.Setup(x => x.GetNhaThau()).ReturnsAsync(NhaThauList);
            //var NhaThauController = new NhaThauController(_nhathau.Object);
            ////act
            //var GetResult = NhaThauController.GetNhaThau();
            //var NhaThauResult = (GetResult.Result as OkObjectResult).Value as List<NhaThau>;
            ////assert
            //Assert.NotNull(NhaThauResult);
            //Assert.Equal(GetNhaThauData().Count(), NhaThauResult.Count());
            //Assert.True(NhaThauList.Equals(NhaThauResult));
        }

        [Fact]
        public void GetNhaThauByID()
        {
            //arrange
            //var NhaThauList = GetNhaThauData().Skip(1).First();
            //_nhathau.Setup(x => x.GetNhaThauByID(2).Result).Returns(NhaThauList);
            //var NhaThauController = new NhaThauController(_nhathau.Object);
           
            ////act
            //var GetResult = NhaThauController.GetNhaThauById(2);
            //var NhaThauResult = (GetResult.Result as OkObjectResult).Value as NhaThau;
            ////assert
            //Assert.NotNull(NhaThauResult);
            //Assert.Equal(NhaThauList.NhaThauID, NhaThauResult.NhaThauID);
            //Assert.True(NhaThauList.NhaThauID == NhaThauResult.NhaThauID);
        }

        //[Theory]
        //[InlineData("Công Ty TNHH Số 1")]
        //public void GetNhaThauByTuKhoa(string _TenCongTy)
        //{
        //    //arrange
        //    var NhaThauList = GetNhaThauData();
        //    _nhathau.Setup(x => x.GetNhaThauByTuKhoa(_TenCongTy)).ReturnsAsync(NhaThauList);
        //    var NhaThauController = new NhaThauController(_nhathau.Object);
        //    var SearchResult = NhaThauController.GetNhaThauByTuKhoa(_TenCongTy);
        //    var NhaThauResult = (SearchResult.Result as OkObjectResult).Value as List<NhaThau>;
        //    //assert
        //    Assert.Equal(_TenCongTy, NhaThauResult.First().TenCongTy);
        //}

        [Fact]
        public void AddNhaThau()
        {
            //arrange
            //var NhaThauList = new NhaThauDto
            //{
            //    NhaThauID = 3,
            //    TenCongTy = "Công Ty TNHH Số 3",
            //    MaSoThue = "MST000003",
            //    DiaChi = "Vĩnh Long",
            //    NguoiDaiDien = "Nguyễn Văn C",
            //    ThongTinThem = "trường thpt Ninh Kiều",
            //    TinhID = 1
            //};
            //var config = new MapperConfiguration(cfg => {
            //    cfg.CreateMap<NhaThau, NhaThauDto>();
            //    cfg.CreateMap<NhaThauDto, NhaThau>();
            //});
            //var _mapper = config.CreateMapper();
            //_nhathau.Setup(x => x.InsertNhaThau(NhaThauList).Result).Returns(_mapper.Map<NhaThau>(NhaThauList));
            //var NhaThauController = new NhaThauController(_nhathau.Object);

            ////act
            //var addResult = NhaThauController.AddNhaThau(NhaThauList);
            //var NhaThauResult = (addResult.Result as OkObjectResult).Value as WebApi.Data.Models.NhaThau;
            ////assert
            //Assert.NotNull(NhaThauResult);
            //Assert.Equal(NhaThauList.NhaThauID, NhaThauResult.NhaThauID);
            //Assert.True(NhaThauList.NhaThauID == NhaThauResult.NhaThauID);
        }

        [Fact]
        public void UpdateNhaThau()
        {
            //arrange
            //var NhaThauList = new NhaThauDto
            //{
            //    NhaThauID = 3,
            //    TenCongTy = "Công Ty TNHH Số 3",
            //    MaSoThue = "MST000003",
            //    DiaChi = "Vĩnh Long",
            //    NguoiDaiDien = "Nguyễn Văn C",
            //    ThongTinThem = "trường thpt Ninh Kiều",
            //    TinhID = 1
            //};
            //var config = new MapperConfiguration(cfg => {
            //    cfg.CreateMap<NhaThau, NhaThauDto>();
            //    cfg.CreateMap<NhaThauDto, NhaThau>();
            //});
            //var _mapper = config.CreateMapper();
            //string _TenCongTy = "Công Ty TNHH Số 4";
            //NhaThauList.TenCongTy = _TenCongTy;
            //_nhathau.Setup(x => x.UpdateNhaThau(NhaThauList).Result).Returns(_mapper.Map<NhaThau>(NhaThauList));

            //var NhaThauController = new NhaThauController(_nhathau.Object);

            ////act
            //var addResult = NhaThauController.UpdateNhaThau(NhaThauList);
            //var NhaThauResult = (addResult.Result as OkObjectResult).Value as WebApi.Data.Models.NhaThau;
            ////assert
            //Assert.NotNull(NhaThauResult);
            //Assert.Equal(NhaThauList.NhaThauID, NhaThauResult.NhaThauID);
            //Assert.True(_TenCongTy == NhaThauResult.TenCongTy);
        }
        [Fact]
        public void DeleteNhaThau()
        {
            //arrange
            //var NhaThauList = GetNhaThauData().Skip(1).First();
            //_nhathau.Setup(x => x.GetNhaThauByID(2).Result).Returns(NhaThauList);
            //var NhaThauController = new NhaThauController(_nhathau.Object);

            ////act
            //var NhaThauResult = NhaThauController.DeleteNhaThau(2);

            ////assert
            //Assert.True("Xóa thành công" == NhaThauResult.Value);

        }


        //private List<NhaThau> GetNhaThauData()
        //{
        //    List<NhaThau> NhaThauData = new List<NhaThau>
        //{
        //    new NhaThau
        //    {
        //        NhaThauID = 1,
        //        TenCongTy = "Công Ty TNHH Số 1",
        //        MaSoThue = "MST000001",
        //        DiaChi = "Cần thơ",
        //        NguoiDaiDien = "Trưởng bộ môn",
        //        ThongTinThem="Nguyễn Văn A",
        //        TinhID = 1
        //    },
        //     new NhaThau
        //    {
        //        NhaThauID = 2,
        //        TenCongTy = "Công Ty TNHH Số 2",
        //        MaSoThue = "MST000002",
        //        DiaChi = "Hậu Giang",
        //        NguoiDaiDien = "Trưởng bộ môn",
        //        ThongTinThem="Nguyễn Văn B",
        //        TinhID = 1
        //    },
        //    new NhaThau
        //    {
        //        NhaThauID = 3,
        //         TenCongTy = "Công Ty TNHH Số 3",
        //        MaSoThue = "MST000003",
        //        DiaChi = "Vĩnh Long",
        //        NguoiDaiDien = "Nguyễn Văn C",
        //        ThongTinThem="trường thpt Ninh Kiều",
        //        TinhID = 1
        //    },
        //};
        //    return NhaThauData;
        //}
    }
}

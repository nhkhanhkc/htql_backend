﻿using AutoMapper;
using GD_HTQL.WebApi.Controllers;
using GD_HTQL.WebApi.Data.Models.Dtos;
using GD_HTQL.WebApi.Data.Models;
using GD_HTQL.WebApi.Services.IRepository;
using Microsoft.AspNetCore.Mvc;
using Moq;

namespace GD_HTQL.XUnitTest.UnitTest
{
    public class CanBoTesting
    {
        private readonly Mock<INSCanBoRepository> _CanBo;
        public CanBoTesting()
        {
            _CanBo = new Mock<INSCanBoRepository>();
        }
        [Fact]
        public void GetListCanBo()
        {
            ////arrange
            //var CanBoList = GetCanBoData();
            //_CanBo.Setup(x => x.GetNSCanBo()).ReturnsAsync(CanBoList);
            //var CanBoController = new NSCanBoController(_CanBo.Object);
            ////act
            //var GetResult = CanBoController.GetCanBo();
            //var CanBoResult = (GetResult.Result as OkObjectResult).Value as List<NSCanBo>;
            ////assert
            //Assert.NotNull(CanBoResult);
            //Assert.Equal(GetCanBoData().Count(), CanBoResult.Count());
            //Assert.True(CanBoList.Equals(CanBoResult));
        }

        [Fact]
        public void GetCanBoByID()
        {
            //arrange
            //var CanBoList = GetCanBoData().Skip(1).First();
            //_CanBo.Setup(x => x.GetNSCanBoByID(2).Result).Returns(CanBoList);
            //var CanBoController = new NSCanBoController(_CanBo.Object);

            ////act
            //var GetResult = CanBoController.GetCanBoById(2);
            //var CanBoResult = (GetResult.Result as OkObjectResult).Value as NSCanBo;
            ////assert
            //Assert.NotNull(CanBoResult);
            //Assert.Equal(CanBoList.CanBoID, CanBoResult.CanBoID);
            //Assert.True(CanBoList.CanBoID == CanBoResult.CanBoID);
        }

        [Theory]
        [InlineData("Lý Văn B")]
        public void GetCanBoByTuKhoa(string _TenCanBo)
        {
            //arrange
            //var CanBoList = GetCanBoData();
            //_CanBo.Setup(x => x.GetNSCanBoByTuKhoa(_TenCanBo, 0)).ReturnsAsync(CanBoList);
            //var CanBoController = new NSCanBoController(_CanBo.Object);
            //var SearchResult = CanBoController.GetNSCanBoByTuKhoa(_TenCanBo, 0);
            //var CanBoResult = (SearchResult.Result as OkObjectResult).Value as List<NSCanBo>;
            ////assert
            //Assert.Equal(GetCanBoData().Count(), CanBoResult.Count());
            //Assert.True(CanBoList.Equals(CanBoResult));
        }

        [Fact]
        public void AddCanBo()
        {
            //arrange
            //var CanBoList = new NSCanBoDto
            //{
            //    CanBoID = 4,
            //    HoVaTen = "Trần Văn E",
            //    GioiTinh = 1,
            //    SoDienThoai = "0909000888",
            //    Email = "tve@gmail.com",
            //    ChucVu = "Giám đốc trung tâm"
            //};
            //var config = new MapperConfiguration(cfg => {
            //    cfg.CreateMap<NSCanBo, NSCanBoDto>();
            //    cfg.CreateMap<NSCanBoDto, NSCanBo>();
            //});
            //var _mapper = config.CreateMapper();
            //_CanBo.Setup(x => x.InsertNSCanBo(CanBoList).Result).Returns(_mapper.Map<NSCanBo>(CanBoList));
            //var CanBoController = new NSCanBoController(_CanBo.Object);

            ////act
            //var addResult = CanBoController.AddCanBo(CanBoList);
            //var CanBoResult = (addResult.Result as OkObjectResult).Value as NSCanBo;
            ////assert
            //Assert.NotNull(CanBoResult);
            //Assert.Equal(CanBoList.CanBoID, CanBoResult.CanBoID);
            //Assert.True(CanBoList.CanBoID == CanBoResult.CanBoID);
        }

        [Fact]
        public void UpdateCanBo()
        {
            //arrange
            //var CanBoList = new NSCanBoDto
            //{
            //    CanBoID = 2,
            //    HoVaTen = "Lý Văn B",
            //    GioiTinh = 1,
            //    SoDienThoai = "0909000889",
            //    Email = "lvb@gmail.com",
            //    ChucVu = "Phó Giám đốc trung tâm"
            //};
            //var config = new MapperConfiguration(cfg => {
            //    cfg.CreateMap<NSCanBo, NSCanBoDto>();
            //    cfg.CreateMap<NSCanBoDto, NSCanBo>();
            //});
            //var _mapper = config.CreateMapper();
            //string _HoVaTen = "Lý Văn Bê";
            //CanBoList.HoVaTen = _HoVaTen;
            //_CanBo.Setup(x => x.UpdateNSCanBo(CanBoList).Result).Returns(_mapper.Map<NSCanBo>(CanBoList));

            //var CanBoController = new NSCanBoController(_CanBo.Object);

            ////act
            //var addResult = CanBoController.UpdateCanBo(CanBoList);
            //var CanBoResult = (addResult.Result as OkObjectResult).Value as NSCanBo;
            ////assert
            //Assert.NotNull(CanBoResult);
            //Assert.Equal(CanBoList.CanBoID, CanBoResult.CanBoID);
            //Assert.True(_HoVaTen == CanBoResult.HoVaTen);
        }
        [Fact]
        public void DeleteCanBo()
        {
            ////arrange
            //var CanBoList = GetCanBoData().Skip(1).First();
            //_CanBo.Setup(x => x.GetNSCanBoByID(2).Result).Returns(CanBoList);
            //var CanBoController = new NSCanBoController(_CanBo.Object);
            ////act
            //var CanBoResult = CanBoController.DeleteCanBo(2);
            ////assert
            //Assert.True("Xóa thành công" == CanBoResult.Value);
        }
        private List<NSCanBo> GetCanBoData()
        {
            List<NSCanBo> CanBoData = new List<NSCanBo>
        {
            new NSCanBo
            {
                CanBoID = 1,
                HoVaTen = "Lâm Văn A",
                GioiTinh = 1,
                SoDienThoai = "0909000887",
                Email = "lvb@gmail.com",
                ChucVu = "Giám đốc trung tâm"
            },
             new NSCanBo
            {
                CanBoID = 2,
                HoVaTen = "Lý Văn B",
                GioiTinh = 1,
                SoDienThoai = "0909000888",
                Email = "lvb@gmail.com",
                ChucVu = "Phó Giám đốc trung tâm"
            },
            new NSCanBo
            {
                CanBoID = 3,
                HoVaTen = "Nguyễn Thị C",
                GioiTinh = 0,
                SoDienThoai = "0909000889",
                Email = "lvb@gmail.com",
                ChucVu = "Trưởng phòng"
            },
        };
            return CanBoData;
        }
    }
}

﻿using GD_HTQL.WebApi.Controllers;
using GD_HTQL.WebApi.Data.Models;
using GD_HTQL.WebApi.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using Moq;


namespace GD_HTQL.XUnitTest.UnitTest
{
    public class TacGiaTesting
    {
        //private readonly Mock<ITacGiaRepository> _tacgia;
        private readonly Mock<IUnitOfWork> _unitOfWork;
        public TacGiaTesting()
        {
            //_tacgia = new Mock<ITacGiaRepository>();
            _unitOfWork = new Mock<IUnitOfWork>();
        }
        [Fact]
        public void GetListTacGia()
        {
            ////arrange
            //var tacgiaList = GetTacGiaData();
            //_unitOfWork.Setup(x => x.TacGiaRepos.GetAll()).ReturnsAsync(tacgiaList);
            //var tacGiaController = new TacGiaController(_unitOfWork.Object);
            ////act
            //var tacGiaResult = tacGiaController.GetTacGia();
            ////assert
            //Assert.NotNull(tacGiaResult);
            //Assert.Equal(GetTacGiaData().Count(), tacGiaResult.Result.Count());
            //Assert.True(tacgiaList.Equals(tacGiaResult.Result));
        }

        [Fact]
        public void GetTacGiaByID()
        {
            //arrange
            //var tacGiaList = GetTacGiaData().Skip(1).First();
            //_unitOfWork.Setup(x => x.TacGiaRepos.GetById(2)).Returns(tacGiaList);
            //var tacGiaController = new TacGiaController(_unitOfWork.Object);

            ////act
            //var tacGiaResult = tacGiaController.GetTacGiaById(2);

            ////assert
            //Assert.NotNull(tacGiaResult);
            //Assert.Equal(tacGiaList.TacGiaID, tacGiaResult.TacGiaID);
            //Assert.True(tacGiaList.TacGiaID == tacGiaResult.TacGiaID);
        }

        [Theory]
        [InlineData("Nguyễn Văn A")]
        public void GetTacGiaByTuKhoa(string _TenTacGia)
        {
            //arrange
            //var tacgiaList = GetTacGiaData();
            //_unitOfWork.Setup(x => x.TacGiaRepos.GetTacGiaByTuKhoa(_TenTacGia)).ReturnsAsync(tacgiaList);
            //var tacGiaController = new TacGiaController(_unitOfWork.Object);
            //var SearchResult = tacGiaController.GetTacGiaByTuKhoa(_TenTacGia);
            //var tacgiaResult = (SearchResult.Result as OkObjectResult).Value as List<TacGia>;
            ////assert
            //Assert.Equal(_TenTacGia, tacgiaResult.First().TenTacGia);
        }

        [Fact]
        public void AddTacGia()
        {
            //arrange
            //var tacgiaList = GetTacGiaData();
            //_unitOfWork.Setup(x => x.TacGiaRepos.Add(tacgiaList[2]).Result)
            //    .Returns(tacgiaList[2]);
            //var tacgiaController = new TacGiaController(_unitOfWork.Object);

            ////act
            //var addResult = tacgiaController.AddTacGia(tacgiaList[2]);
            //var tacgiaResult = (addResult.Result as OkObjectResult).Value as TacGia;
            ////assert
            //Assert.NotNull(tacgiaResult);
            //Assert.Equal(tacgiaList[2], tacgiaResult);
            //Assert.True(tacgiaList[2].TacGiaID == tacgiaResult.TacGiaID);
        }

        [Fact]
        public void UpdateTacGia()
        {
            //arrange
           // var tacgiaList = GetTacGiaData().Skip(1).First();
           // string _TenTacGia = "Nguyễn Văn D";
           // tacgiaList.TenTacGia = _TenTacGia;
           //var _unitReturn = _unitOfWork.Setup(x => x.TacGiaRepos.Update(tacgiaList));
               
           // var tacgiaController = new TacGiaController(_unitOfWork.Object);

           // //act
           // var addResult = tacgiaController.UpdateTacGia(tacgiaList);         
           // //assert                   
           // Assert.True(_unitReturn == addResult);
        }
        [Fact]
        public void DeleteTacGia()
        {
            //arrange
           // var tacGiaList = GetTacGiaData().Skip(1).First();
           //var UnitReturn = _unitOfWork.Setup(x => x.TacGiaRepos.GetById(2));
           // var tacGiaController = new TacGiaController(_unitOfWork.Object);

           // //act
           // var tacGiaResult = tacGiaController.DeleteTacGia(2);
           // //assert
           // Assert.True(UnitReturn == tacGiaResult.Value);

        }


        private List<TacGia> GetTacGiaData()
        {
            List<TacGia> TacGiaData = new List<TacGia>
        {
            new TacGia
            {
                TacGiaID = 1,
                TenTacGia = "Nguyễn Văn A",
                SoDienThoai = "0909000111",
                Email = "nva@gmail.com",
                ChucVuTacGia = "Trưởng bộ môn",
                DonViCongTac="trường thpt Ninh Kiều",
                MonChuyenNghanh ="Toán"
            },
             new TacGia
            {
                TacGiaID = 2,
                TenTacGia = "Nguyễn Văn B",
                SoDienThoai = "0909000222",
                Email = "nvb@gmail.com",
                ChucVuTacGia = "Phó bộ môn",
                DonViCongTac="trường thpt Cái Răng",
                MonChuyenNghanh ="Tiếng anh"
            },
             new TacGia
            {
                TacGiaID = 3,
                TenTacGia = "Nguyễn Văn C",
                SoDienThoai = "0909000333",
                Email = "nvc@gmail.com",
                ChucVuTacGia = "Trưởng bộ môn",
                DonViCongTac="trường thpt Bình Thủy",
                MonChuyenNghanh ="Văn"
            },
        };
            return TacGiaData;
        }
    }
}

﻿using AutoMapper;
using GD_HTQL.WebApi.Data.Models.Dtos;
using GD_HTQL.WebApi.Data.Models;
using GD_HTQL.WebApi.Services.IRepository;
using Microsoft.AspNetCore.Mvc;
using Moq;
using GD_HTQL.WebApi.Controllers;

namespace GD_HTQL.XUnitTest.UnitTest
{
    public class CoQuanTesting
    {
        private readonly Mock<INSCoQuanRepository> _coquan;
        public CoQuanTesting()
        {
            _coquan = new Mock<INSCoQuanRepository>();
        }
        [Fact]
        public void GetListCoQuan()
        {
            //arrange
            //var CoQuanList = GetCoQuanData();
            //_coquan.Setup(x => x.GetNSCoQuan()).ReturnsAsync(CoQuanList);
            //var CoQuanController = new NSCoQuanController(_coquan.Object);
            ////act
            //var GetResult = CoQuanController.GetCoQuan();
            //var CoQuanResult = (GetResult.Result as OkObjectResult).Value as List<NSCoQuan>;
            ////assert
            //Assert.NotNull(CoQuanResult);
            //Assert.Equal(GetCoQuanData().Count(), CoQuanResult.Count());
            //Assert.True(CoQuanList.Equals(CoQuanResult));
        }

        [Fact]
        public void GetCoQuanByID()
        {
            //arrange
            //var CoQuanList = GetCoQuanData().Skip(1).First();
            //_coquan.Setup(x => x.GetNSCoQuanByID(2).Result).Returns(CoQuanList);
            //var CoQuanController = new NSCoQuanController(_coquan.Object);

            ////act
            //var GetResult = CoQuanController.GetCoQuanById(2);
            //var CoQuanResult = (GetResult.Result as OkObjectResult).Value as NSCoQuan;
            ////assert
            //Assert.NotNull(CoQuanResult);
            //Assert.Equal(CoQuanList.CoQuanID, CoQuanResult.CoQuanID);
            //Assert.True(CoQuanList.CoQuanID == CoQuanResult.CoQuanID);
        }

        [Theory]
        [InlineData("Phòng giáo dục quận cái răng")]
        public void GetCoQuanByTuKhoa(string _TenCoQuan)
        {
            //arrange
            //var CoQuanList = GetCoQuanData();
            //_coquan.Setup(x => x.GetNSCoQuanByTuKhoa(_TenCoQuan, 0)).ReturnsAsync(CoQuanList);
            //var CoQuanController = new NSCoQuanController(_coquan.Object);
            //var SearchResult = CoQuanController.GetNSCoQuanByTuKhoa(_TenCoQuan, 0);
            //var CoQuanResult = (SearchResult.Result as OkObjectResult).Value as List<NSCoQuan>;
            ////assert
            //Assert.Equal(GetCoQuanData().Count(), CoQuanResult.Count());
            //Assert.True(CoQuanList.Equals(CoQuanResult));
        }

        [Fact]
        public void AddCoQuan()
        {
            //arrange
            //var CoQuanList = new NSCoQuanDto
            //{
            //    CoQuanID = 4,
            //    TenCoQuan = "Phòng giáo dục quận ô môn",
            //    MaSoThue = "MST000001",
            //    DiaChi = " ô môn cần thơ, Cần thơ",
            //    HuyenID = 665,
            //    TinhID = 59
            //};
            //var config = new MapperConfiguration(cfg => {
            //    cfg.CreateMap<NSCoQuan, NSCoQuanDto>();
            //    cfg.CreateMap<NSCoQuanDto, NSCoQuan>();
            //});
            //var _mapper = config.CreateMapper();
            //_coquan.Setup(x => x.InsertNSCoQuan(CoQuanList).Result).Returns(_mapper.Map<NSCoQuan>(CoQuanList));
            //var CoQuanController = new NSCoQuanController(_coquan.Object);

            ////act
            //var addResult = CoQuanController.AddCoQuan(CoQuanList);
            //var CoQuanResult = (addResult.Result as OkObjectResult).Value as NSCoQuan;
            ////assert
            //Assert.NotNull(CoQuanResult);
            //Assert.Equal(CoQuanList.CoQuanID, CoQuanResult.CoQuanID);
            //Assert.True(CoQuanList.CoQuanID == CoQuanResult.CoQuanID);
        }

        [Fact]
        public void UpdateCoQuan()
        {
            //arrange
            //var CoQuanList = new NSCoQuanDto
            //{
            //    CoQuanID = 2,
            //    TenCoQuan = "Phòng giáo dục quận cái răng",
            //    MaSoThue = "MST000001",
            //    DiaChi = " cái răng, Cần thơ",
            //    HuyenID = 665,
            //    TinhID = 59
            //};
            //var config = new MapperConfiguration(cfg => {
            //    cfg.CreateMap<NSCoQuan, NSCoQuanDto>();
            //    cfg.CreateMap<NSCoQuanDto, NSCoQuan>();
            //});
            //var _mapper = config.CreateMapper();
            //string _TenCongTy = "Phòng giáo dục quận phong điền";
            //CoQuanList.TenCoQuan = _TenCongTy;
            //_coquan.Setup(x => x.UpdateNSCoQuan(CoQuanList).Result).Returns(_mapper.Map<NSCoQuan>(CoQuanList));

            //var CoQuanController = new NSCoQuanController(_coquan.Object);

            ////act
            //var addResult = CoQuanController.UpdateCoQuan(CoQuanList);
            //var CoQuanResult = (addResult.Result as OkObjectResult).Value as NSCoQuan;
            ////assert
            //Assert.NotNull(CoQuanResult);
            //Assert.Equal(CoQuanList.CoQuanID, CoQuanResult.CoQuanID);
            //Assert.True(_TenCongTy == CoQuanResult.TenCoQuan);
        }
        [Fact]
        public void DeleteCoQuan()
        {
            //arrange
            //var CoQuanList = GetCoQuanData().Skip(1).First();
            //_coquan.Setup(x => x.GetNSCoQuanByID(2).Result).Returns(CoQuanList);
            //var CoQuanController = new NSCoQuanController(_coquan.Object);
            ////act
            //var CoQuanResult = CoQuanController.DeleteCoQuan(2);
            ////assert
            //Assert.True("Xóa thành công" == CoQuanResult.Value);
        }
        //private List<NSCoQuan> GetCoQuanData()
        //{
        //    List<NSCoQuan> CoQuanData = new List<NSCoQuan>
        //{
        //    new NSCoQuan
        //    {
        //        CoQuanID = 1,
        //        TenCoQuan = "Phòng giáo dục quận ninh kiều",
        //        MaSoThue = "MST000001",
        //        DiaChi = " ninh kiều, Cần thơ",
        //        HuyenID = 662,               
        //        TinhID = 59
        //    },
        //     new NSCoQuan
        //    {
        //        CoQuanID = 2,
        //        TenCoQuan = "Phòng giáo dục quận cái răng",
        //        MaSoThue = "MST000001",
        //        DiaChi = " cái răng, Cần thơ",
        //        HuyenID = 665,
        //        TinhID = 59
        //    },
        //    new NSCoQuan
        //    {
        //       CoQuanID = 3,
        //       TenCoQuan = "Phòng giáo dục quận bình thủy",
        //       MaSoThue = "MST000001",
        //       DiaChi = " bình thủy, Cần thơ",
        //       HuyenID = 664,
        //       TinhID = 59
        //    },
        //};
        //    return CoQuanData;
        //}
    }
}

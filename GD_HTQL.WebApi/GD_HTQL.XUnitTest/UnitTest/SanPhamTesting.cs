﻿using AutoMapper;
using GD_HTQL.WebApi.Controllers;
using GD_HTQL.WebApi.Data.Models.Dtos;
using GD_HTQL.WebApi.Data.Models;
using GD_HTQL.WebApi.Services.IRepository;
using Microsoft.AspNetCore.Mvc;
using Moq;
namespace GD_HTQL.XUnitTest.UnitTest
{
    public class SanPhamTesting
    {
        private readonly Mock<ISanPhamRepository> _sanpham;
        public SanPhamTesting()
        {
            _sanpham = new Mock<ISanPhamRepository>();
        }
        [Fact]
        public void GetListSanPham()
        {
            //arrange
            //var SanPhamList = GetSanPhamData();
            //_sanpham.Setup(x => x.GetSanPham()).ReturnsAsync(SanPhamList);
            //var SanPhamController = new SanPhamController(_sanpham.Object);
            ////act
            //var GetResult = SanPhamController.GetSanPham();
            //var SanPhamResult = (GetResult.Result as OkObjectResult).Value as List<SanPham>;
            ////assert
            //Assert.NotNull(SanPhamResult);
            //Assert.Equal(GetSanPhamData().Count(), SanPhamResult.Count());
            //Assert.True(SanPhamList.Equals(SanPhamResult));
        }

        [Fact]
        public void GetSanPhamByID()
        {
            //arrange
            //var SanPhamList = GetSanPhamData().Skip(1).First();
            //_sanpham.Setup(x => x.GetSanPhamByID(2).Result).Returns(SanPhamList);
            //var SanPhamController = new SanPhamController(_sanpham.Object);

            ////act
            //var GetResult = SanPhamController.GetSanPhamById(2);
            //var SanPhamResult = (GetResult.Result as OkObjectResult).Value as SanPham;
            ////assert
            //Assert.NotNull(SanPhamResult);
            //Assert.Equal(SanPhamList.SanPhamID, SanPhamResult.SanPhamID);
            //Assert.True(SanPhamList.SanPhamID == SanPhamResult.SanPhamID);
        }

        [Theory]
        [InlineData("Bộ học liệu điện tử - Tin học lớp 10")]
        public void GetSanPhamByTuKhoa(string _TenSanPham)
        {
            //arrange
            //var SanPhamList = GetSanPhamData();
            //_sanpham.Setup(x => x.GetSanPhamByTuKhoa(_TenSanPham)).ReturnsAsync(SanPhamList);
            //var SanPhamController = new SanPhamController(_sanpham.Object);
            //var SearchResult = SanPhamController.GetSanPhamByTuKhoa(_TenSanPham);
            //var SanPhamResult = (SearchResult.Result as OkObjectResult).Value as List<SanPham>;
            ////assert
            //Assert.Equal(GetSanPhamData().Count(), SanPhamResult.Count());
            //Assert.True(SanPhamList.Equals(SanPhamResult));
        }

        [Fact]
        public void AddSanPham()
        {
            //arrange
            //var SanPhamList = new SanPhamDto
            //{
            //    SanPhamID = 10,
            //    TenSanPham = "Bộ học liệu điện tử - Ngữ Văn lớp 10",
            //    MaSanPham = "GD39-000933VN",
            //    TieuChuanKyThuat = "Được đóng hộp có kích thước 8 x 8 x 2 cm, chất liệu I300",
            //    HangSanXuat = "GD Việt Nam",
            //    XuatXu = "VN",
            //    NhaXuatBan = "Song Phương",
            //    DonViTinh = "Bộ",
            //    GiaKhachHang = 100000,
            //    GiaDaiLy = 990000,
            //    Thue = 1000,
            //    SoLuong = 1,
            //    DoiTuongSuDung = "GV và HS",
            //    LoaiSanPhamID = 1,
            //    MonHocID = 1,
            //    KhoiLopID = 1,
            //};
            //var config = new MapperConfiguration(cfg => {
            //    cfg.CreateMap<SanPham, SanPhamDto>();
            //    cfg.CreateMap<SanPhamDto, SanPham>();
            //});
            //var _mapper = config.CreateMapper();
            //_sanpham.Setup(x => x.InsertSanPham(SanPhamList).Result).Returns(_mapper.Map<SanPham>(SanPhamList));

            //var SanPhamController = new SanPhamController(_sanpham.Object);
            ////act
            //var addResult = SanPhamController.AddSanPham(SanPhamList);
            //var SanPhamResult = (addResult.Result as OkObjectResult).Value as SanPham;
            ////assert
            //Assert.NotNull(SanPhamResult);
            //Assert.Equal(SanPhamList.SanPhamID, SanPhamResult.SanPhamID);
            //Assert.True(SanPhamList.SanPhamID == SanPhamResult.SanPhamID);
        }

        [Fact]
        public void UpdateSanPham()
        {
            //arrange
            //var SanPhamList = new SanPhamDto
            //{
            //    SanPhamID = 2,
            //    TenSanPham = "Bộ học liệu điện tử - Anh Văn lớp 10",
            //    MaSanPham = "GD39-00011VN",
            //    TieuChuanKyThuat = "Được đóng hộp có kích thước 8 x 8 x 2 cm, chất liệu I300",
            //    HangSanXuat = "GD Việt Nam",
            //    XuatXu = "VN",
            //    NhaXuatBan = "Song Phương",
            //    DonViTinh = "Bộ",
            //    GiaKhachHang = 100000,
            //    GiaDaiLy = 990000,
            //    Thue = 1000,
            //    SoLuong = 1,
            //    DoiTuongSuDung = "GV và HS",
            //    LoaiSanPhamID = 1,
            //    MonHocID = 1,
            //    KhoiLopID = 1,
            //};
            //var config = new MapperConfiguration(cfg => {
            //    cfg.CreateMap<SanPham, SanPhamDto>();
            //    cfg.CreateMap<SanPhamDto, SanPham>();
            //});
            //var _mapper = config.CreateMapper();
            //string _TenSanPham = "Bộ học liệu điện tử - Tin học lớp 10";
            //SanPhamList.TenSanPham = _TenSanPham;
            //_sanpham.Setup(x => x.UpdateSanPham(SanPhamList).Result).Returns(_mapper.Map<SanPham>(SanPhamList));

            //var SanPhamController = new SanPhamController(_sanpham.Object);

            ////act
            //var addResult = SanPhamController.UpdateSanPham(SanPhamList);
            //var SanPhamResult = (addResult.Result as OkObjectResult).Value as SanPham;
            ////assert
            //Assert.NotNull(SanPhamResult);
            //Assert.Equal(SanPhamList.SanPhamID, SanPhamResult.SanPhamID);
            //Assert.True(_TenSanPham == SanPhamResult.TenSanPham);
        }
        [Fact]
        public void DeleteSanPham()
        {
            //arrange
            //var SanPhamList = GetSanPhamData().Skip(1).First();
            //_sanpham.Setup(x => x.GetSanPhamByID(2).Result).Returns(SanPhamList);
            //var SanPhamController = new SanPhamController(_sanpham.Object);
            ////act
            //var SanPhamResult = SanPhamController.DeleteSanPham(2);
            ////assert
            //Assert.True("Xóa thành công" == SanPhamResult.Value);
        }
        //private List<SanPham> GetSanPhamData()
        //{
        //    List<SanPham> SanPhamData = new List<SanPham>
        //{
        //    new SanPham
        //    {
        //        SanPhamID = 1,
        //        TenSanPham = "Bộ học liệu điện tử - Ngữ Văn lớp 10",
        //        MaSanPham = "GD39-0001VN",
        //        TieuChuanKyThuat = "Được đóng hộp có kích thước 8 x 8 x 2 cm, chất liệu I300",
        //        HangSanXuat = "GD Việt Nam",
        //        XuatXu = "VN",
        //        NhaXuatBan = "Song Phương",
        //        DonViTinh = "Bộ",
        //        GiaKhachHang =100000,
        //        GiaDaiLy = 990000,
        //        Thue = 1000,
        //        SoLuong = 1,
        //        DoiTuongSuDung = "GV và HS",
        //        LoaiSanPhamID = 1,
        //        MonHocID = 1,
        //        KhoiLopID =1,
        //    },
        //     new SanPham
        //    {
        //        SanPhamID = 2,
        //        TenSanPham = "Bộ học liệu điện tử - Anh Văn lớp 10",
        //        MaSanPham = "GD39-0002VN",
        //        TieuChuanKyThuat = "Được đóng hộp có kích thước 8 x 8 x 2 cm, chất liệu I300",
        //        HangSanXuat = "GD Việt Nam",
        //        XuatXu = "VN",
        //        NhaXuatBan = "Song Phương",
        //        DonViTinh = "Bộ",
        //        GiaKhachHang =199000,
        //        GiaDaiLy = 990000,
        //        Thue = 1000,
        //        SoLuong = 1,
        //        DoiTuongSuDung = "GV và HS",
        //        LoaiSanPhamID = 1,
        //        MonHocID = 1,
        //        KhoiLopID =1,
        //    },
        //    new SanPham
        //    {
        //        SanPhamID = 3,
        //        TenSanPham = "Bộ học liệu điện tử - Toán lớp 10",
        //        MaSanPham = "GD39-0003VN",
        //        TieuChuanKyThuat = "Được đóng hộp có kích thước 8 x 8 x 2 cm, chất liệu I300",
        //        HangSanXuat = "GD Việt Nam",
        //        XuatXu = "VN",
        //        NhaXuatBan = "Song Phương",
        //        DonViTinh = "Bộ",
        //        GiaKhachHang =100000,
        //        GiaDaiLy = 990000,
        //        Thue = 1000,
        //        SoLuong = 1,
        //        DoiTuongSuDung = "GV và HS",
        //        LoaiSanPhamID = 1,
        //        MonHocID = 1,
        //        KhoiLopID =1,
        //    },
        //};
        //    return SanPhamData;
        //}
    }
}

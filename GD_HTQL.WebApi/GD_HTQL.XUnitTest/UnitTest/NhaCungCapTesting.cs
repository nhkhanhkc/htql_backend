﻿using AutoMapper;
using GD_HTQL.WebApi.Controllers;
using GD_HTQL.WebApi.Data.Models;
using GD_HTQL.WebApi.Data.Models.Dtos;
using GD_HTQL.WebApi.Services.IRepository;
using Microsoft.AspNetCore.Mvc;
using Moq;

namespace GD_HTQL.XUnitTest.UnitTest
{
    public class NhaCungCapTesting
    {
        private readonly Mock<INhaCungCapRepository> _nhaCungCap;      
        public NhaCungCapTesting()
        {
            _nhaCungCap = new Mock<INhaCungCapRepository>();            
        }
        [Fact]
        public void GetListNhaCungCap()
        {
            //arrange
            //var NhaCungCapList = GetNhaCungCapData();
            //_nhaCungCap.Setup(x => x.GetNhaCungCap()).ReturnsAsync(NhaCungCapList);
            //var _nhaCungCapController = new NhaCungCapController(_nhaCungCap.Object);
            ////act
            //var GetResult = _nhaCungCapController.GetNhaCungCap();
            //var NhaCungCapResult = (GetResult.Result as OkObjectResult).Value as List<NhaCungCap>;
            ////assert
            //Assert.NotNull(NhaCungCapResult);
            //Assert.Equal(GetNhaCungCapData().Count(), NhaCungCapResult.Count());
            //Assert.True(NhaCungCapList.Equals(NhaCungCapResult));
        }

        [Fact]
        public void GetNhaCungCapByID()
        {
            //arrange
            //var NhaCungCapList = GetNhaCungCapData().Skip(1).First();
            //_nhaCungCap.Setup(x => x.GetNhaCungCapByID(2).Result).Returns(NhaCungCapList);
            //var _nhaCungCapController = new NhaCungCapController(_nhaCungCap.Object);

            ////act
            //var GetResult = _nhaCungCapController.GetNhaCungCapById(2);
            //var NhaCungCapResult = (GetResult.Result as OkObjectResult).Value as NhaCungCap;
            ////assert
            //Assert.NotNull(NhaCungCapResult);
            //Assert.Equal(NhaCungCapList.NhaCungCapID, NhaCungCapResult.NhaCungCapID);
            //Assert.True(NhaCungCapList.NhaCungCapID == NhaCungCapResult.NhaCungCapID);
        }

        //[Theory]
        //[InlineData("Công Ty TNHH Số 1")]
        //public void GetNhaCungCapByTuKhoa(string _TenCongTy)
        //{
        //    //arrange
        //    var NhaCungCapList = GetNhaCungCapData();
        //    _nhaCungCap.Setup(x => x.GetNhaCungCapByTuKhoa(_TenCongTy)).ReturnsAsync(NhaCungCapList);
        //    var _nhaCungCapController = new NhaCungCapController(_nhaCungCap.Object);
        //    var SearchResult = _nhaCungCapController.GetNhaCungCapByTuKhoa(_TenCongTy);
        //    var NhaCungCapResult = (SearchResult.Result as OkObjectResult).Value as List<NhaCungCap>;
        //    //assert
        //    Assert.Equal(_TenCongTy, NhaCungCapResult.First().TenCongTy);
        //}

        [Fact]
        public void AddNhaCungCap()
        {
            //var NhaCungCapList = new NhaCungCapDto
            //{
            //    NhaCungCapID = 1,
            //    TenCongTy = "Công Ty TNHH Số 1",
            //    MaSoThue = "MST000001",
            //    DiaChi = "Cần thơ",
            //    NguoiDaiDien = "Trưởng bộ môn",
            //    ThongTinThem = "Nguyễn Văn A",
            //    TinhID = 1,

            //};
            //var config = new MapperConfiguration(cfg => {
            //    cfg.CreateMap<NhaCungCap, NhaCungCapDto>();
            //    cfg.CreateMap<NhaCungCapDto, NhaCungCap>();
            //});

            //var mapper = config.CreateMapper();
            ////arrange
            ////var NhaCungCapList = GetNhaCungCapData();
            //_nhaCungCap.Setup(x => x.InsertNhaCungCap(NhaCungCapList).Result).Returns(mapper.Map<NhaCungCap>(NhaCungCapList));
            //var _nhaCungCapController = new NhaCungCapController(_nhaCungCap.Object);

            ////act
            //var addResult = _nhaCungCapController.AddNhaCungCap(NhaCungCapList);
            //var NhaCungCapResult = (addResult.Result as OkObjectResult).Value as WebApi.Data.Models.NhaCungCap;
            ////assert
            //Assert.NotNull(NhaCungCapResult);
            //Assert.Equal(NhaCungCapList.NhaCungCapID, NhaCungCapResult.NhaCungCapID);
            //Assert.True(NhaCungCapList.NhaCungCapID == NhaCungCapResult.NhaCungCapID);
        }

        [Fact]
        public void UpdateNhaCungCap()
        {
            //arrange
          
            //var NhaCungCapList = new NhaCungCapDto
            //{
            //    NhaCungCapID = 1,
            //    TenCongTy = "Công Ty TNHH Số 1",
            //    MaSoThue = "MST000001",
            //    DiaChi = "Cần thơ",
            //    NguoiDaiDien = "Trưởng bộ môn",
            //    ThongTinThem = "Nguyễn Văn A",
            //    TinhID = 1,

            //};
            //var config = new MapperConfiguration(cfg => {
            //    cfg.CreateMap<NhaCungCap, NhaCungCapDto>();
            //    cfg.CreateMap<NhaCungCapDto, NhaCungCap>();
            //});

            //var _mapper = config.CreateMapper();
            //string _TenCongTy = "Công Ty TNHH Số 4";
            //NhaCungCapList.TenCongTy = _TenCongTy;
            //_nhaCungCap.Setup(x => x.UpdateNhaCungCap(NhaCungCapList).Result).Returns(_mapper.Map<NhaCungCap>(NhaCungCapList));
            //var _nhaCungCapController = new NhaCungCapController(_nhaCungCap.Object);

            ////act
            //var addResult = _nhaCungCapController.UpdateNhaCungCap(NhaCungCapList);
            //var NhaCungCapResult = (addResult.Result as OkObjectResult).Value as WebApi.Data.Models.NhaCungCap;
            ////assert
            //Assert.NotNull(NhaCungCapResult);
            //Assert.Equal(NhaCungCapList.NhaCungCapID, NhaCungCapResult.NhaCungCapID);
            //Assert.True(_TenCongTy == NhaCungCapResult.TenCongTy);
        }
        [Fact]
        public void DeleteNhaCungCap()
        {
            //arrange
            //var NhaCungCapList = GetNhaCungCapData().Skip(1).First();
            //_nhaCungCap.Setup(x => x.GetNhaCungCapByID(2).Result).Returns(NhaCungCapList);
            //var _nhaCungCapController = new NhaCungCapController(_nhaCungCap.Object);

            ////act
            //var NhaCungCapResult = _nhaCungCapController.DeleteNhaCungCap(2);

            ////assert
            //Assert.True("Xóa thành công" == NhaCungCapResult.Value);

        }


        //private List<NhaCungCap> GetNhaCungCapData()
        //{
        //    List<NhaCungCap> NhaCungCapData = new List<NhaCungCap>
        //{
        //    new NhaCungCap
        //    {
        //        NhaCungCapID = 1,
        //        TenCongTy = "Công Ty TNHH Số 1",
        //        MaSoThue = "MST000001",
        //        DiaChi = "Cần thơ",
        //        NguoiDaiDien = "Trưởng bộ môn",
        //        ThongTinThem="Nguyễn Văn A",
        //        TinhID =1,

        //    },
        //     new NhaCungCap
        //    {
        //        NhaCungCapID = 2,
        //        TenCongTy = "Công Ty TNHH Số 2",
        //        MaSoThue = "MST000002",
        //        DiaChi = "Hậu Giang",
        //        NguoiDaiDien = "Trưởng bộ môn",
        //        ThongTinThem="Nguyễn Văn B",
        //        TinhID =1
        //    },
        //     new NhaCungCap
        //    {
        //        NhaCungCapID = 3,
        //        TenCongTy = "Công Ty TNHH Số 3",
        //        MaSoThue = "MST000003",
        //        DiaChi = "Vĩnh Long",
        //        NguoiDaiDien = "Nguyễn Văn C",
        //        ThongTinThem="trường thpt Ninh Kiều",
        //        TinhID =1
        //    },
        //};
        //    return NhaCungCapData;
        //}
    }
}
